//! Helper library to wrap logging calls and serve as a shared dependency between crates

pub use log::debug as log_debug;
pub use log::error as log_error;
pub use log::info as log_info;
pub use log::trace as log_trace;
pub use log::warn as log_warn;
use log::Level;

fn level_to_str(l: Level) -> &'static str {
    match l {
        Level::Info => "I",
        Level::Debug => "D",
        Level::Error => "E",
        Level::Warn => "W",
        Level::Trace => "T",
    }
}

/// Initialize default logging system
pub fn init_log_system() -> Result<(), String> {
    if let Err(err) = fern::Dispatch::new()
        .format(|out, message, record| {
            out.finish(format_args!(
                "[{}][{}] {}",
                level_to_str(record.level()),
                record.target(),
                message
            ))
        })
        .level(log::LevelFilter::Debug)
        .chain(std::io::stdout())
        .apply()
    {
        return Err(format!("Failed to init log system: {}", err));
    }
    log::info!(
        "Log started on {}",
        chrono::Local::now().format("%Y/%m/%d %H:%M:%S")
    );
    Ok(())
}

pub mod prelude {
    pub use super::log_debug;
    pub use super::log_error;
    pub use super::log_info;
    pub use super::log_trace;
    pub use super::log_warn;
}
