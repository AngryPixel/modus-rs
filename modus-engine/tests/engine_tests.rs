use modus_engine::{Engine, EngineQuitState, Module};
use modus_scheduler::extractors::{Reader, Writer};
use modus_scheduler::resource::ResourceContainer;
use modus_scheduler::task::FunctionTask;
use modus_scheduler::task_container::{TaskContainer, TaskId, TaskScheduler};

#[test]
fn check_basic_engine_startup_and_shutdown() {
    const TASK_ID: TaskId = TaskId::new("Test");
    #[derive(Debug)]
    struct LoopState {
        counter: u32,
    }

    fn test_task(quit_state: Reader<EngineQuitState>, mut loop_state: Writer<LoopState>) {
        loop_state.counter += 1;
        if loop_state.counter > 5 {
            quit_state.request_quit()
        }
    }

    #[derive(Debug)]
    struct TestModule {}

    impl Module for TestModule {
        fn name(&self) -> &str {
            "test module"
        }

        fn register_tasks_and_resources(&mut self, engine: &mut Engine) -> Result<(), String> {
            engine
                .try_add_task(FunctionTask::new(TASK_ID, test_task), &[])
                .expect("Failed to add task");
            engine
                .try_add_resource(LoopState { counter: 0 })
                .expect("Failed to add resource");
            Ok(())
        }

        fn initialize(&mut self, _: &mut ResourceContainer) -> Result<(), String> {
            Ok(())
        }

        fn shutdown(&mut self, resources: &mut ResourceContainer, tasks: &mut TaskContainer) {
            tasks.remove_task(TASK_ID);
            resources.erase::<LoopState>()
        }

        fn update(&mut self, _: &mut TaskScheduler, _: &mut TaskContainer) {}
    }

    let run_result = Engine::new().add_module(TestModule {}).run();
    assert!(run_result);
}
