use crate::Engine;
use modus_scheduler::resource::ResourceContainer;
use modus_scheduler::task_container::{TaskContainer, TaskScheduler};
use std::fmt::Debug;

pub trait Module: Debug {
    fn name(&self) -> &str;

    fn register_tasks_and_resources(&mut self, engine: &mut Engine) -> Result<(), String>;

    fn initialize(&mut self, resources: &mut ResourceContainer) -> Result<(), String>;

    fn shutdown(&mut self, resources: &mut ResourceContainer, tasks: &mut TaskContainer);

    fn update(&mut self, scheduler: &mut TaskScheduler, tasks: &mut TaskContainer);
}
