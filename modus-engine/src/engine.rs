use crate::{EngineQuitState, Module};
use modus_log::prelude::*;
use modus_scheduler::executor::ParallelExecutor;
use modus_scheduler::resource::{ResourceContainer, ResourceContainerError, SendTraitCheck};
use modus_scheduler::task::Task;
use modus_scheduler::task_container::{TaskContainer, TaskId, TaskScheduler, TaskSchedulerError};
use std::any::Any;

#[derive(Debug)]
pub struct Engine {
    modules: Vec<Box<dyn Module>>,
    resources: ResourceContainer,
    tasks: TaskContainer,
    scheduler: TaskScheduler,
    executor: ParallelExecutor,
}

impl Engine {
    pub fn new() -> Self {
        let mut engine = Self {
            modules: Vec::with_capacity(8),
            resources: ResourceContainer::new(),
            tasks: TaskContainer::new(),
            scheduler: TaskScheduler::new(),
            executor: ParallelExecutor::new().expect("Failed to create executor"),
        };
        engine.add_engine_resources();
        engine
    }

    pub fn add_resource<T: Any + SendTraitCheck>(mut self, resource: T) -> Self {
        self.resources
            .add(resource)
            .expect("Failed to add resource");
        self
    }

    pub fn try_add_resource<T: Any + SendTraitCheck>(
        &mut self,
        resource: T,
    ) -> Result<(), ResourceContainerError> {
        self.resources.add(resource)?;
        Ok(())
    }

    pub fn add_task<T: Task>(mut self, task: T, dependencies: &[TaskId]) -> Self {
        self.try_add_task(task, dependencies)
            .expect("Failed to add task");
        self
    }

    pub fn try_add_task<T: Task>(
        &mut self,
        task: T,
        dependencies: &[TaskId],
    ) -> Result<(), TaskSchedulerError> {
        self.tasks.add_task(task, dependencies)?;
        Ok(())
    }

    pub fn add_module<T: Module + 'static>(mut self, mut module: T) -> Self {
        module
            .register_tasks_and_resources(&mut self)
            .expect("Failed to initialize module");
        self.modules.push(Box::new(module));
        self
    }

    pub fn run(mut self) -> bool {
        for module in &mut self.modules {
            if let Err(e) = module.initialize(&mut self.resources) {
                log_error!("Failed to initialize module {}: {}", module.name(), e);
                return false;
            }
        }

        let exit_success = loop {
            let (should_quit, exit_successful) = self
                .resources
                .get::<EngineQuitState>()
                .expect("Failed to retrieve quit state")
                .acquire::<EngineQuitState>()
                .status();
            if should_quit {
                break exit_successful;
            }
            for module in &mut self.modules {
                module.update(&mut self.scheduler, &mut self.tasks);
            }
            self.scheduler.reset();
            self.scheduler.add_enabled_tasks(&self.tasks);
            if let Err(e) = self.scheduler.validate(&self.tasks) {
                log_error!("Failed to schedule tasks: {:?}", e);
                break false;
            }
            self.executor
                .execute(&self.scheduler, &self.tasks, &self.resources);
        };
        exit_success
    }

    fn add_engine_resources(&mut self) {
        self.resources
            .add(EngineQuitState::new())
            .expect("Failed to add quit state");
    }
}

impl Drop for Engine {
    fn drop(&mut self) {
        self.executor.shutdown();
        for module in &mut self.modules {
            module.shutdown(&mut self.resources, &mut self.tasks);
        }
    }
}
