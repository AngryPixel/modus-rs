#[derive(Debug)]
pub struct EngineQuitState {
    quit_flag: std::sync::atomic::AtomicBool,
    quit_success: std::sync::atomic::AtomicBool,
}

impl EngineQuitState {
    pub fn new() -> Self {
        Self {
            quit_flag: std::sync::atomic::AtomicBool::new(false),
            quit_success: std::sync::atomic::AtomicBool::new(true),
        }
    }

    pub fn request_quit(&self) {
        self.quit_flag
            .store(true, std::sync::atomic::Ordering::SeqCst);
    }

    pub fn request_quit_with_error_status(&self) {
        self.quit_flag
            .store(true, std::sync::atomic::Ordering::SeqCst);
        self.quit_success
            .store(false, std::sync::atomic::Ordering::SeqCst);
    }

    pub fn status(&self) -> (bool, bool) {
        (
            self.quit_flag.load(std::sync::atomic::Ordering::SeqCst),
            self.quit_success.load(std::sync::atomic::Ordering::SeqCst),
        )
    }
}
