use crate::task::Task;
use crate::task_container::TaskSchedulerError::{
    Cycle, DependencyNotFound, DuplicateId, EmptyGraph,
};
use const_fnv1a_hash::fnv1a_hash_str_64;
use modus_core::sorting::{TopologicalSortError, TopologicalSorter};
use modus_core::HashMap;
use std::collections::hash_map::Entry;
use std::hash::Hash;

#[derive(Debug, Copy, Clone, Eq, PartialEq, Hash)]
pub struct TaskId(u64);

impl TaskId {
    pub const fn new(value: &'static str) -> Self {
        TaskId(fnv1a_hash_str_64(value))
    }
}

#[derive(Debug)]
pub struct TaskData {
    pub id: TaskId,
    pub dependencies: Vec<TaskId>,
    pub task: Box<dyn Task>,
    pub main_thread_only: bool,
    pub enabled: bool,
}

#[derive(Debug)]
pub struct TaskContainer {
    tasks: HashMap<TaskId, TaskData>,
}

#[derive(Debug, Copy, Clone, Eq, PartialEq)]
pub enum TaskSchedulerError {
    DuplicateId,
    Cycle(TaskId, TaskId),
    DependencyNotFound(TaskId),
    TaskNotFound(TaskId),
    EmptyGraph,
}

impl From<TopologicalSortError<TaskId>> for TaskSchedulerError {
    fn from(err: TopologicalSortError<TaskId>) -> Self {
        match err {
            TopologicalSortError::DuplicateNode(_) => DuplicateId,
            TopologicalSortError::DependencyCycle(k1, k2) => Cycle(k1, k2),
            TopologicalSortError::DependencyNotFound(k) => DependencyNotFound(k),
            TopologicalSortError::EmtpyGraph => EmptyGraph,
        }
    }
}

impl TaskContainer {
    pub fn new() -> Self {
        Self {
            tasks: HashMap::with_capacity(64),
        }
    }

    fn add_task_impl(
        &mut self,
        task: impl Task,
        main_thread: bool,
        dependencies: &[TaskId],
    ) -> Result<(), TaskSchedulerError> {
        let id = task.meta_data().id;
        match self.tasks.entry(id) {
            Entry::Occupied(_) => Err(TaskSchedulerError::DuplicateId),
            Entry::Vacant(v) => {
                v.insert(TaskData {
                    id,
                    task: Box::new(task),
                    dependencies: Vec::from(dependencies),
                    main_thread_only: main_thread,
                    enabled: true,
                });
                Ok(())
            }
        }
    }

    /// Add a task that can be executed on any thread
    pub fn add_task(
        &mut self,
        task: impl Task,
        dependencies: &[TaskId],
    ) -> Result<(), TaskSchedulerError> {
        self.add_task_impl(task, false, dependencies)
    }

    /// Add a task that can only be executed on the main thread
    pub fn add_main_thread_task(
        &mut self,
        task: impl Task,
        dependencies: &[TaskId],
    ) -> Result<(), TaskSchedulerError> {
        self.add_task_impl(task, true, dependencies)
    }

    pub fn remove_task(&mut self, task_id: TaskId) {
        self.tasks.remove(&task_id);
    }

    pub fn get_task(&self, task_id: TaskId) -> Option<&TaskData> {
        self.tasks.get(&task_id)
    }

    pub fn len(&self) -> usize {
        self.tasks.len()
    }

    pub fn set_enabled(&mut self, task_id: TaskId, enabled: bool) {
        if let Some(task) = self.tasks.get_mut(&task_id) {
            task.enabled = enabled
        }
    }
}

#[derive(Debug)]
pub struct TaskScheduler {
    tasks: Vec<TaskId>,
    task_order: Vec<TaskId>,
    sorter: TopologicalSorter<TaskId>,
}

impl TaskScheduler {
    pub fn new() -> Self {
        Self {
            tasks: Vec::with_capacity(16),
            task_order: Vec::new(),
            sorter: TopologicalSorter::with_capacity(16),
        }
    }

    pub fn add_task(&mut self, task_id: TaskId) {
        self.tasks.push(task_id);
    }

    pub fn add_enabled_tasks(&mut self, task_container: &TaskContainer) {
        self.tasks.reserve(task_container.len());
        for (_, task) in &task_container.tasks {
            if task.enabled {
                self.tasks.push(task.id);
            }
        }
    }

    pub fn validate(&mut self, task_container: &TaskContainer) -> Result<(), TaskSchedulerError> {
        self.sorter.clear();
        for task_id in &self.tasks {
            if let Some(task) = task_container.get_task(*task_id) {
                self.sorter.add(task.id, &task.dependencies)?;
            } else {
                return Err(TaskSchedulerError::TaskNotFound(*task_id));
            }
        }
        self.sorter.build_into(&mut self.task_order)?;
        Ok(())
    }

    pub fn task_order(&self) -> &[TaskId] {
        &self.task_order
    }

    pub fn reset(&mut self) {
        self.sorter.clear();
        self.tasks.clear();
        self.task_order.clear();
    }
}

impl Default for TaskScheduler {
    fn default() -> Self {
        Self::new()
    }
}
