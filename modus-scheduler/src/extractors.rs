use crate::resource::{
    ResourceAccessor, ResourceAccessorMut, ResourceContainer, ResourceContainerError, ResourceTrait,
};
use std::any::TypeId;
use std::ops::{Deref, DerefMut};

pub trait FromResourceContainer: Sized {
    fn extract_resource(container: &ResourceContainer) -> Result<Self, ResourceContainerError>;
}

pub trait MutabilityMarkerExtractor: FromResourceContainer {
    fn extract_mutability(read: &mut Vec<TypeId>, write: &mut Vec<TypeId>);
}

/// Provides a read only resource extractor
#[derive(Debug)]
pub struct Reader<T: ResourceTrait> {
    accessor: ResourceAccessor<T>,
}

impl<T: ResourceTrait> Deref for Reader<T> {
    type Target = T;
    fn deref(&self) -> &Self::Target {
        self.accessor.deref()
    }
}

impl<T: ResourceTrait> FromResourceContainer for Reader<T> {
    fn extract_resource(container: &ResourceContainer) -> Result<Self, ResourceContainerError> {
        if let Some(resource) = container.get::<T>() {
            Ok(Self {
                accessor: resource.acquire(),
            })
        } else {
            Err(ResourceContainerError::DoesNotExist)
        }
    }
}

impl<T: ResourceTrait> MutabilityMarkerExtractor for Reader<T> {
    fn extract_mutability(read: &mut Vec<TypeId>, _: &mut Vec<TypeId>) {
        read.push(TypeId::of::<T>())
    }
}

/// Provides a read & writable resource extractor
#[derive(Debug)]
pub struct Writer<T: ResourceTrait> {
    accessor: ResourceAccessorMut<T>,
}

impl<T: ResourceTrait> Deref for Writer<T> {
    type Target = T;
    fn deref(&self) -> &Self::Target {
        self.accessor.deref()
    }
}

impl<T: ResourceTrait> DerefMut for Writer<T> {
    fn deref_mut(&mut self) -> &mut Self::Target {
        self.accessor.deref_mut()
    }
}

impl<T: ResourceTrait> FromResourceContainer for Writer<T> {
    fn extract_resource(container: &ResourceContainer) -> Result<Self, ResourceContainerError> {
        if let Some(resource) = container.get::<T>() {
            Ok(Self {
                accessor: resource.acquire_mut(),
            })
        } else {
            Err(ResourceContainerError::DoesNotExist)
        }
    }
}

impl<T: ResourceTrait> MutabilityMarkerExtractor for Writer<T> {
    fn extract_mutability(_: &mut Vec<TypeId>, write: &mut Vec<TypeId>) {
        write.push(TypeId::of::<T>())
    }
}

macro_rules! from_resource_container_tuple({$($param:ident)*} => {
   impl<$($param:FromResourceContainer + 'static,)*> FromResourceContainer for ($($param,)*)  {
       #[allow(unused)]
       fn extract_resource(container:&ResourceContainer) -> Result<Self, ResourceContainerError>{
           Ok(($($param::extract_resource(container)?,)*))
       }
   }
   impl<$($param:MutabilityMarkerExtractor + 'static,)*> MutabilityMarkerExtractor for ($($param,)*)  {
       #[allow(unused)]
       fn extract_mutability(read:&mut Vec<TypeId>, write:&mut Vec<TypeId>) {
           $($param::extract_mutability(read, write);)*
       }
   }
});

from_resource_container_tuple! {}
from_resource_container_tuple! {A}
from_resource_container_tuple! {A B}
from_resource_container_tuple! {A B C}
from_resource_container_tuple! {A B C D}
from_resource_container_tuple! {A B C D E}
from_resource_container_tuple! {A B C D E F}
from_resource_container_tuple! {A B C D E F G}
from_resource_container_tuple! {A B C D E F G H}
from_resource_container_tuple! {A B C D E F G H I}
from_resource_container_tuple! {A B C D E F G H I J}
from_resource_container_tuple! {A B C D E F G H I J K}

impl<T: FromResourceContainer> FromResourceContainer for Option<T> {
    fn extract_resource(container: &ResourceContainer) -> Result<Self, ResourceContainerError> {
        match T::extract_resource(container) {
            Ok(r) => Ok(Some(r)),
            Err(e) => {
                if e == ResourceContainerError::DoesNotExist {
                    return Ok(None);
                }
                Err(e)
            }
        }
    }
}

#[cfg(test)]
mod tests {
    use crate::extractors::{FromResourceContainer, Reader, Writer};
    use crate::resource::ResourceContainer;

    struct CustomType1 {}
    struct CustomType2 {}

    #[test]
    fn extract_reader() {
        let mut container = ResourceContainer::new();
        container.add(CustomType1 {}).expect("Failed to add type");
        let result = Reader::<CustomType1>::extract_resource(&container);
        assert!(result.is_ok());
    }

    #[test]
    fn extract_writer() {
        let mut container = ResourceContainer::new();
        container.add(CustomType1 {}).expect("Failed to add type");
        let result = Writer::<CustomType1>::extract_resource(&container);
        assert!(result.is_ok());
    }

    #[test]
    fn extract_tuple() {
        let mut container = ResourceContainer::new();
        container.add(CustomType1 {}).expect("Failed to add type");
        container.add(CustomType2 {}).expect("Failed to add type");

        // empty
        {
            let result = <()>::extract_resource(&container);
            assert!(result.is_ok());
        }

        // one value
        {
            #[allow(unused_parens)]
            let result = <(Reader<CustomType1>)>::extract_resource(&container);
            assert!(result.is_ok());
        }

        // two values
        {
            let result = <(Reader<CustomType1>, Writer<CustomType2>)>::extract_resource(&container);
            assert!(result.is_ok());
        }
    }

    #[test]
    fn extract_optional() {
        let mut container = ResourceContainer::new();
        container.add(CustomType1 {}).expect("Failed to add type");
        {
            let result = Option::<Writer<CustomType1>>::extract_resource(&container)
                .expect("Failed get resource");
            assert!(result.is_some());
        }
        {
            let result = Option::<Writer<CustomType2>>::extract_resource(&container)
                .expect("Failed get resource");
            assert!(result.is_none());
        }
    }
}
