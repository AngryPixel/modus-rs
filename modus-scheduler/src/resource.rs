use modus_core::{FixedBitSet, HashMap};
use modus_log::log_warn;
use std::any::{Any, TypeId};
use std::cell::UnsafeCell;
use std::collections::hash_map::Entry;
use std::marker::PhantomData;
use std::ops::{Deref, DerefMut};
use std::ptr::NonNull;
use std::sync::atomic::AtomicI32;

#[derive(Debug, Clone)]
pub struct ResourceAccessSet {
    pub read_set: FixedBitSet,
    pub write_set: FixedBitSet,
}

impl ResourceAccessSet {
    pub fn new() -> Self {
        Self {
            read_set: FixedBitSet::new(),
            write_set: FixedBitSet::new(),
        }
    }

    pub fn with_container(container: &ResourceContainer) -> Self {
        Self {
            read_set: FixedBitSet::with_capacity(container.len()),
            write_set: FixedBitSet::with_capacity(container.len()),
        }
    }
    pub fn is_compatible(&self, other: &Self) -> bool {
        self.write_set.is_disjoint(&other.write_set)
            && self.write_set.is_disjoint(&other.read_set)
            && self.read_set.is_disjoint(&other.write_set)
    }

    fn clear(&mut self) {
        self.write_set.clear();
        self.read_set.clear();
    }
}

/// Use for merging and unmerging ResourceAccessSets since we need to handle the case of
/// multiple readers.
#[derive(Debug)]
pub struct MergedResourceAccessSet {
    access_set: ResourceAccessSet,
    reader_count: Vec<u8>,
}

impl MergedResourceAccessSet {
    pub fn new() -> Self {
        Self {
            access_set: ResourceAccessSet::new(),
            reader_count: Vec::new(),
        }
    }

    #[inline(always)]
    pub fn is_compatible(&self, set: &ResourceAccessSet) -> bool {
        self.access_set.is_compatible(set)
    }

    pub fn merge(&mut self, set: &ResourceAccessSet) {
        self.reader_count.resize(set.read_set.len(), 0);
        self.access_set.write_set.union_with(&set.write_set);
        self.access_set.read_set.union_with(&set.read_set);
        for index in set.read_set.ones() {
            self.reader_count[index] += 1;
        }
    }

    pub fn unmerge(&mut self, set: &ResourceAccessSet) {
        debug_assert!(set.read_set.len() == self.reader_count.len());
        self.access_set.write_set.difference_with(&set.write_set);

        for index in set.read_set.ones() {
            let reader_count = &mut self.reader_count[index];
            debug_assert!(*reader_count > 0);
            *reader_count -= 1;
            if *reader_count == 0 {
                self.access_set.read_set.set(index, false);
            }
        }
    }

    pub fn clear(&mut self) {
        self.reader_count.fill(0);
        self.access_set.clear();
    }
}

pub trait SendTraitCheck {
    const IS_SEND: bool;
}

impl<T: Send> SendTraitCheck for T {
    const IS_SEND: bool = true;
}

impl<T> SendTraitCheck for T {
    default const IS_SEND: bool = false;
}

pub trait ResourceTrait: Send + Sync + 'static {}
impl<T: Send + Sync + 'static> ResourceTrait for T {}

#[derive(Debug)]
pub struct Resource {
    access_counter: AtomicI32,
    is_send: bool,
    resource: UnsafeCell<Box<dyn Any>>,
    type_id: TypeId,
}

unsafe impl Send for Resource {}
unsafe impl Sync for Resource {}

#[derive(Debug)]
pub struct ResourceAccessor<T: Any> {
    resource: NonNull<Resource>,
    phantom: PhantomData<T>,
}

#[derive(Debug)]
pub struct ResourceAccessorMut<T: Any> {
    resource: NonNull<Resource>,
    phantom: PhantomData<T>,
}

#[derive(Debug, Copy, Clone, Eq, PartialEq)]
pub enum ResourceAccessError {
    InvalidType,
    MutabilityRule,
}

const ACCESS_COUNTER_UNUSED_VALUE: i32 = 0;
const ACCESS_COUNTER_WRITE_VALUE: i32 = i32::MIN;

impl Resource {
    fn new<T: Any + SendTraitCheck>(value: T) -> Self {
        Self {
            access_counter: AtomicI32::new(ACCESS_COUNTER_UNUSED_VALUE),
            is_send: T::IS_SEND,
            resource: UnsafeCell::new(Box::new(value)),
            type_id: TypeId::of::<T>(),
        }
    }

    pub fn try_acquire<T: Any>(&self) -> Result<ResourceAccessor<T>, ResourceAccessError> {
        if std::any::TypeId::of::<T>() != self.type_id {
            return Err(ResourceAccessError::InvalidType);
        }
        use std::sync::atomic::Ordering;
        let write_counter = self.access_counter.load(Ordering::SeqCst);
        if write_counter < 0 {
            return Err(ResourceAccessError::MutabilityRule);
        }
        self.access_counter.fetch_add(1, Ordering::SeqCst);
        Ok(ResourceAccessor::new(&self))
    }

    pub fn acquire<T: Any>(&self) -> ResourceAccessor<T> {
        self.try_acquire().expect("Failed to acquire read access")
    }

    pub fn try_acquire_mut<T: Any>(&self) -> Result<ResourceAccessorMut<T>, ResourceAccessError> {
        if std::any::TypeId::of::<T>() != self.type_id {
            return Err(ResourceAccessError::InvalidType);
        }
        use std::sync::atomic::Ordering;
        let result = self.access_counter.compare_exchange(
            ACCESS_COUNTER_UNUSED_VALUE,
            ACCESS_COUNTER_WRITE_VALUE,
            Ordering::SeqCst,
            Ordering::Relaxed,
        );
        if result.is_err() {
            return Err(ResourceAccessError::MutabilityRule);
        }
        Ok(ResourceAccessorMut::new(&self))
    }

    pub fn acquire_mut<T: Any>(&self) -> ResourceAccessorMut<T> {
        self.try_acquire_mut()
            .expect("Failed to acquire write access")
    }

    fn release(&self) {
        use std::sync::atomic::Ordering;
        if self.access_counter.fetch_sub(1, Ordering::SeqCst) <= 0 {
            panic!("Invalid read reference count")
        }
    }

    fn release_mut(&self) {
        use std::sync::atomic::Ordering;
        let result = self.access_counter.compare_exchange(
            ACCESS_COUNTER_WRITE_VALUE,
            ACCESS_COUNTER_UNUSED_VALUE,
            Ordering::SeqCst,
            Ordering::Relaxed,
        );
        if result.is_err() {
            panic!("Attempting to reset resource access info from invalid state")
        }
    }
}

impl<T: Any> ResourceAccessor<T> {
    fn new(resource: &Resource) -> Self {
        Self {
            resource: NonNull::from(resource),
            phantom: PhantomData,
        }
    }
}

impl<T: Any> Drop for ResourceAccessor<T> {
    fn drop(&mut self) {
        // Safety: We have guaranteed that accessing this resource is safe at this point
        unsafe {
            self.resource.as_ref().release();
        }
    }
}

impl<'r, T: Any> Deref for ResourceAccessor<T> {
    type Target = T;

    fn deref(&self) -> &Self::Target {
        // Safety: We have guaranteed that accessing this resource is safe at this point.
        // and that the resource is of the correct type
        unsafe { (*self.resource.as_ref().resource.get()).downcast_ref_unchecked::<T>() }
    }
}

impl<T: Any> ResourceAccessorMut<T> {
    fn new(resource: &Resource) -> Self {
        Self {
            resource: NonNull::from(resource),
            phantom: PhantomData,
        }
    }
}

impl<T: Any> Deref for ResourceAccessorMut<T> {
    type Target = T;

    fn deref(&self) -> &Self::Target {
        // Safety: We have guaranteed that accessing this resource is safe at this point.
        // and that the resource is of the correct type
        unsafe { (*self.resource.as_ref().resource.get()).downcast_ref_unchecked::<T>() }
    }
}

impl<T: Any> DerefMut for ResourceAccessorMut<T> {
    fn deref_mut(&mut self) -> &mut Self::Target {
        // Safety: We have guaranteed that accessing this resource is safe at this point.
        // and that the resource is of the correct type
        unsafe { (*self.resource.as_mut().resource.get()).downcast_mut_unchecked::<T>() }
    }
}

impl<T: Any> Drop for ResourceAccessorMut<T> {
    fn drop(&mut self) {
        // Safety: We have guaranteed that accessing this resource is safe at this point
        unsafe {
            self.resource.as_mut().release_mut();
        }
    }
}

#[derive(Debug)]
pub struct ResourceContainer {
    resources: Vec<Box<Resource>>,
    resource_type_map: HashMap<TypeId, usize>,
}

const INITIAL_RESOURCE_INDEX_MAP_CAPACITY: usize = 64;

#[derive(Debug, Copy, Clone, Eq, PartialEq)]
pub enum ResourceContainerError {
    Duplicate,
    DoesNotExist,
}

impl ResourceContainer {
    pub fn new() -> Self {
        Self {
            resources: Vec::with_capacity(INITIAL_RESOURCE_INDEX_MAP_CAPACITY),
            resource_type_map: HashMap::with_capacity(INITIAL_RESOURCE_INDEX_MAP_CAPACITY),
        }
    }

    pub fn add<T: Any + SendTraitCheck>(
        &mut self,
        value: T,
    ) -> Result<usize, ResourceContainerError> {
        let type_id = value.type_id();
        match self.resource_type_map.entry(type_id) {
            Entry::Occupied(_) => return Err(ResourceContainerError::Duplicate),
            Entry::Vacant(v) => {
                let index = self.resources.len();
                self.resources.push(Box::new(Resource::new(value)));
                v.insert(index);
                Ok(index)
            }
        }
    }

    pub fn get<T: Any>(&self) -> Option<&Resource> {
        let type_id = std::any::TypeId::of::<T>();
        let index = self.resource_type_map.get(&type_id)?;
        Some(&self.resources[*index])
    }

    pub fn get_index<T: Any>(&self) -> Option<usize> {
        let type_id = std::any::TypeId::of::<T>();
        let index = self.resource_type_map.get(&type_id)?;
        Some(*index)
    }

    pub fn get_index_by_type_id(&self, type_id: TypeId) -> Option<usize> {
        let index = self.resource_type_map.get(&type_id)?;
        Some(*index)
    }

    pub fn erase<T: Any>(&mut self) {
        let type_id = std::any::TypeId::of::<T>();
        if let Some(index) = self.resource_type_map.remove(&type_id) {
            // TODO: find a way to clean the memory state for this resource
            // after it is no longer required
            log_warn!(
                "Memory for resource {} index={} is not being erased",
                std::any::type_name::<T>(),
                index,
            )
        }
    }

    #[inline(always)]
    pub fn len(&self) -> usize {
        self.resources.len()
    }
}

#[cfg(test)]
mod tests {
    use crate::resource::{
        MergedResourceAccessSet, Resource, ResourceAccessSet, ResourceContainer,
    };
    use modus_core::FixedBitSet;

    struct NonSendType {
        _reference: *mut u32,
    }

    struct SendType {
        _v: u32,
    }
    unsafe impl Send for SendType {}

    #[test]
    fn resource_send_detection() {
        let non_send_resource = Resource::new(NonSendType {
            _reference: std::ptr::null_mut(),
        });
        let send_resource = Resource::new(SendType { _v: 20 });

        assert!(!non_send_resource.is_send);
        assert!(send_resource.is_send);
    }

    #[test]
    fn resource_access_rules() {
        let resource = Resource::new(SendType { _v: 20 });

        // read -> read
        {
            let read_access_1 = resource.try_acquire::<SendType>();
            assert!(read_access_1.is_ok());
            let read_access_2 = resource.try_acquire::<SendType>();
            assert!(read_access_2.is_ok());
        }
        // read -> write
        {
            let read_access = resource.try_acquire::<SendType>();
            assert!(read_access.is_ok());
            let write_access = resource.try_acquire_mut::<SendType>();
            assert!(write_access.is_err());
        }
        // write -> read
        {
            let write_access = resource.try_acquire_mut::<SendType>();
            assert!(write_access.is_ok());
            let read_access = resource.try_acquire::<SendType>();
            assert!(read_access.is_err());
        }
        // write -> write
        {
            let write_access_1 = resource.try_acquire_mut::<SendType>();
            assert!(write_access_1.is_ok());
            let write_access_2 = resource.try_acquire_mut::<SendType>();
            assert!(write_access_2.is_err());
        }
        // write -> read (fail) -> release write -> read
        {
            {
                let write_access = resource.try_acquire_mut::<SendType>();
                assert!(write_access.is_ok());
                let read_access = resource.try_acquire::<SendType>();
                assert!(read_access.is_err());
            }
            let read_access = resource.try_acquire::<SendType>();
            assert!(read_access.is_ok());
        }
        // write -> write(fail) -> release write -> write
        {
            {
                let write_access_1 = resource.try_acquire_mut::<SendType>();
                assert!(write_access_1.is_ok());
                let write_access_2 = resource.try_acquire_mut::<SendType>();
                assert!(write_access_2.is_err());
            }
            let write_access = resource.try_acquire_mut::<SendType>();
            assert!(write_access.is_ok());
        }
    }

    #[test]
    fn resource_container_duplicate_resource_type_addition() {
        let mut container = ResourceContainer::new();
        assert!(container.add(0u32).is_ok());
        assert!(container.add(12u32).is_err());
        assert!(container.add(SendType { _v: 12 }).is_ok())
    }

    const MAX_RESOURCE_TYPES: usize = 5;

    fn new_test_access_set() -> ResourceAccessSet {
        ResourceAccessSet {
            read_set: FixedBitSet::with_capacity(MAX_RESOURCE_TYPES),
            write_set: FixedBitSet::with_capacity(MAX_RESOURCE_TYPES),
        }
    }

    fn new_test_merged_access_set() -> MergedResourceAccessSet {
        MergedResourceAccessSet {
            access_set: new_test_access_set(),
            reader_count: Vec::with_capacity(MAX_RESOURCE_TYPES),
        }
    }

    #[test]
    fn access_set_merge() {
        {
            let mut access_set_1 = new_test_access_set();
            let mut access_set_2 = new_test_access_set();
            access_set_1.read_set.set(0, true);
            access_set_1.write_set.set(1, true);
            access_set_2.read_set.set(2, true);
            access_set_2.write_set.set(3, true);

            let mut merged_set = new_test_merged_access_set();
            merged_set.merge(&access_set_1);
            merged_set.merge(&access_set_2);

            assert!(merged_set.access_set.read_set[0]);
            assert!(merged_set.access_set.read_set[2]);
            assert!(merged_set.access_set.write_set[3]);
            assert!(merged_set.access_set.write_set[1]);

            merged_set.unmerge(&access_set_2);
            assert!(merged_set.access_set.read_set[0]);
            assert!(!merged_set.access_set.read_set[2]);
            assert!(merged_set.access_set.write_set[1]);
            assert!(!merged_set.access_set.write_set[3]);
        }
        {
            let mut access_set_1 = new_test_access_set();
            let mut access_set_2 = new_test_access_set();
            access_set_1.read_set.set(0, true);
            access_set_1.write_set.set(1, true);
            access_set_2.read_set.set(0, true);

            let mut merged_set = new_test_merged_access_set();
            merged_set.merge(&access_set_1);
            merged_set.merge(&access_set_2);

            assert!(merged_set.access_set.read_set[0]);
            assert!(merged_set.access_set.write_set[1]);

            merged_set.unmerge(&access_set_2);
            assert!(merged_set.access_set.read_set[0]);
            assert!(merged_set.access_set.write_set[1]);

            merged_set.unmerge(&access_set_1);
            assert!(!merged_set.access_set.read_set[0]);
            assert!(!merged_set.access_set.write_set[1]);
        }
    }

    #[test]
    fn access_set_mutability_rules() {
        {
            let mut access_set_1 = new_test_access_set();
            let mut access_set_2 = new_test_access_set();
            // Not compatible as both try to modify resource 1
            // R:0,W:1
            access_set_1.read_set.set(0, true);
            access_set_1.write_set.set(1, true);
            // R:1,W:1
            access_set_2.read_set.set(2, true);
            access_set_2.write_set.set(1, true);
            assert!(!access_set_1.is_compatible(&access_set_2));
        }
        {
            let mut access_set_1 = new_test_access_set();
            let mut access_set_2 = new_test_access_set();
            // Compatible
            // R:0,W:1
            access_set_1.read_set.set(0, true);
            access_set_1.write_set.set(1, true);
            // R:3,W:2
            access_set_2.read_set.set(3, true);
            access_set_2.write_set.set(2, true);
            assert!(access_set_1.is_compatible(&access_set_2));
        }
        {
            let mut access_set_1 = new_test_access_set();
            let mut access_set_2 = new_test_access_set();
            // Compatible
            // R:0,W:1
            access_set_1.read_set.set(0, true);
            access_set_1.write_set.set(1, true);
            // R:0,
            access_set_2.read_set.set(0, true);
            assert!(access_set_1.is_compatible(&access_set_2));
        }
        {
            let mut access_set_1 = new_test_access_set();
            let mut access_set_2 = new_test_access_set();
            // Not compatible
            // R:0,W:1
            access_set_1.read_set.set(0, true);
            access_set_1.write_set.set(1, true);
            // R:0,W1
            access_set_2.read_set.set(0, true);
            access_set_2.write_set.set(1, true);
            assert!(!access_set_1.is_compatible(&access_set_2));
        }
    }
}
