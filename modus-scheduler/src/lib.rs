//! Modus scheduling system - inspired by how Bevy and Yak handle concurrent execution in
//! projects.
//!
//! All resources need to be registered with the scheduler and then can be accessed during the
//! execution of tasks.
//!
//! Once all task have been collected an their dependencies evaluated, they are scheduled
//! and executed on the thread pool.
//!
//! The Task scheduler will ensure that mutability rules are enforced at run-time

#![allow(incomplete_features)]
#![feature(specialization)]
#![feature(downcast_unchecked)]

extern crate core;

pub mod executor;
pub mod extractors;
pub mod resource;
pub mod task;
pub mod task_container;
pub mod thread_pool;
