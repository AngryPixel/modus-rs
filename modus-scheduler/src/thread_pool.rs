use crate::resource::ResourceContainer;
use crate::task::Task;
use crate::task_container::TaskId;
use crossbeam_deque::{Steal, Worker};
use rand::Rng;
use std::borrow::BorrowMut;
use std::ptr::NonNull;
use std::sync::atomic::{AtomicBool, Ordering};
use std::sync::Arc;

pub(crate) struct Job {
    task: NonNull<dyn Task>,
    container: NonNull<ResourceContainer>,
    finished: std::sync::atomic::AtomicBool,
}

impl Job {
    pub(crate) fn is_finished(&self) -> bool {
        self.finished.load(Ordering::Acquire)
    }

    pub(crate) fn task_id(&self) -> TaskId {
        unsafe { self.task.as_ref().meta_data().id }
    }
}

unsafe impl Send for Job {}
type Queue = Worker<Arc<Job>>;

#[derive(Debug)]
struct ThreadData {
    queue: Queue,
    quit_flag: std::sync::atomic::AtomicBool,
}

impl ThreadData {
    fn new() -> Self {
        Self {
            queue: Queue::new_fifo(),
            quit_flag: AtomicBool::new(false),
        }
    }
}

unsafe impl Send for ThreadData {}
unsafe impl Sync for ThreadData {}

fn get_job_from_queue(thread_data: &Vec<ThreadData>, this_thread_index: usize) -> Option<Arc<Job>> {
    let this_thread_data = &thread_data[this_thread_index];
    let mut opt_job = this_thread_data.queue.pop();
    if opt_job.is_none() {
        let random_index = {
            const MAX_NUM_RANDOM_CALLS: i32 = 5;
            let mut random_index = 0;
            for i in 0..MAX_NUM_RANDOM_CALLS {
                random_index = rand::thread_rng().borrow_mut().gen::<usize>() % thread_data.len();
                if random_index == this_thread_index {
                    if i == MAX_NUM_RANDOM_CALLS - 1 {
                        std::thread::yield_now();
                        return None;
                    }
                    continue;
                } else {
                    break;
                }
            }
            random_index
        };

        loop {
            match thread_data[random_index].queue.stealer().steal() {
                Steal::Empty => {
                    std::thread::yield_now();
                    return None;
                }
                Steal::Success(job) => {
                    opt_job = Some(job);
                    break;
                }
                Steal::Retry => {
                    continue;
                }
            }
        }
    }
    opt_job
}

fn run_job(job: &Job) {
    unsafe {
        let task = job.task.as_ref();
        let container = job.container.as_ref();
        task.execute(container);
    }
    job.finished.store(true, Ordering::Release);
}

#[derive(Debug)]
struct WorkerThread {
    thread_index: usize,
    thread_handle: Option<std::thread::JoinHandle<()>>,
}

impl WorkerThread {
    fn new(index: usize) -> Self {
        debug_assert!(index > 0);
        Self {
            thread_index: index,
            thread_handle: None,
        }
    }

    fn start(
        &mut self,
        thread_name: &str,
        thread_data: Arc<Vec<ThreadData>>,
    ) -> Result<(), std::io::Error> {
        let thread_index = self.thread_index;
        let thread_handle = {
            std::thread::Builder::new()
                .name(format!("{} {:02}", thread_name, self.thread_index))
                .spawn(move || {
                    modus_profiler::register_thread!();
                    let this_thread_data = &thread_data[thread_index];
                    while !this_thread_data.quit_flag.load(Ordering::Acquire) {
                        if let Some(job) = get_job_from_queue(&thread_data, thread_index) {
                            run_job(&job);
                        } else {
                            std::thread::yield_now();
                        }
                    }
                })
        };

        self.thread_handle = Some(thread_handle?);
        Ok(())
    }
}

#[derive(Debug)]
pub(crate) struct ThreadPool {
    thread_data: Arc<Vec<ThreadData>>,
    threads: Vec<WorkerThread>,
}

impl ThreadPool {
    const MAIN_THREAD_INDEX: usize = 0;

    pub fn new(thread_count: usize, thread_name: Option<&str>) -> Result<Self, std::io::Error> {
        let thread_count = thread_count.max(1) + 1;
        let mut thread_data_vec = Vec::<ThreadData>::with_capacity(thread_count);
        thread_data_vec.resize_with(thread_count, ThreadData::new);
        let thread_data = Arc::new(thread_data_vec);
        let mut threads = Vec::with_capacity(thread_count);
        let thread_name = thread_name.unwrap_or("JobThread");
        for index in 1..thread_count {
            let mut thread = WorkerThread::new(index);
            thread.start(thread_name, thread_data.clone())?;
            threads.push(thread);
        }
        Ok(Self {
            thread_data,
            threads,
        })
    }

    pub fn shutdown(&mut self) {
        // set shutdown flag to true
        for thread in &self.threads {
            self.thread_data[thread.thread_index]
                .quit_flag
                .store(true, Ordering::Release);
        }

        // wait for join
        for thread in &mut self.threads {
            if let Some(handle) = thread.thread_handle.take() {
                if let Err(_) = handle.join() {
                    panic!("Failed to join job thread");
                }
            }
        }
    }

    pub(crate) fn spawn(&self, task: &dyn Task, container: &ResourceContainer) -> Arc<Job> {
        let job = Arc::new(Job {
            finished: std::sync::atomic::AtomicBool::new(false),
            task: NonNull::from(task),
            container: NonNull::from(container),
        });
        self.thread_data[0].queue.push(job.clone());
        job
    }

    pub(crate) fn run_in_place(&self, task: &dyn Task, container: &ResourceContainer) -> Arc<Job> {
        let job = Arc::new(Job {
            finished: std::sync::atomic::AtomicBool::new(true),
            task: NonNull::from(task),
            container: NonNull::from(container),
        });
        task.execute(container);
        job
    }

    pub(crate) fn wait_any(&self, tasks: &[Arc<Job>]) {
        loop {
            for job in tasks {
                if job.is_finished() {
                    return;
                }
            }
            if let Some(job) = get_job_from_queue(&self.thread_data, Self::MAIN_THREAD_INDEX) {
                run_job(&job)
            }
        }
    }

    pub(crate) fn wait_all(&self, tasks: &[Arc<Job>]) {
        loop {
            let mut finished_counter = 0usize;
            for job in tasks {
                if job.is_finished() {
                    finished_counter += 1;
                }
            }
            if finished_counter == tasks.len() {
                return;
            }
            if let Some(job) = get_job_from_queue(&self.thread_data, Self::MAIN_THREAD_INDEX) {
                run_job(&job)
            }
        }
    }
}

impl Drop for ThreadPool {
    fn drop(&mut self) {
        self.shutdown();
    }
}
