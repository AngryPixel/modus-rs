use crate::resource::{MergedResourceAccessSet, ResourceAccessSet, ResourceContainer};
use crate::task::Task;
use crate::task_container::{TaskContainer, TaskId, TaskScheduler};
use crate::thread_pool::ThreadPool;
use modus_core::{FixedBitSet, HashMap, HashSet};
use std::collections::hash_map::Entry;

#[derive(Debug)]
struct TaskExecutionState {
    access_set: ResourceAccessSet,
    total_dependencies: usize,
    dependency_counter: usize,
    dependent_tasks: Vec<TaskId>,
}

#[derive(Debug)]
struct ExecutorState {
    tasks: HashMap<TaskId, TaskExecutionState>,
    pending_tasks: Vec<TaskId>,
    active_access_set: MergedResourceAccessSet,
    active_tasks: HashSet<TaskId>,
    finished_tasks: HashSet<TaskId>,
}

impl ExecutorState {
    fn new() -> Self {
        Self {
            tasks: HashMap::new(),
            pending_tasks: Vec::new(),
            active_access_set: MergedResourceAccessSet::new(),
            active_tasks: HashSet::new(),
            finished_tasks: HashSet::new(),
        }
    }

    fn prepare(
        &mut self,
        task_scheduler: &TaskScheduler,
        task_container: &TaskContainer,
        resources: &ResourceContainer,
    ) {
        self.tasks.clear();
        self.finished_tasks.clear();
        self.active_tasks.clear();
        self.pending_tasks.clear();
        self.active_access_set.clear();

        self.tasks.reserve(task_container.len());
        self.pending_tasks.reserve(task_container.len());
        self.active_tasks.reserve(task_container.len());
        self.finished_tasks.reserve(task_container.len());

        for task_id in task_scheduler.task_order() {
            let task = task_container.get_task(*task_id).unwrap();
            for dependency in &task.dependencies {
                match self.tasks.entry(*dependency) {
                    Entry::Occupied(mut o) => {
                        let task_state = o.get_mut();
                        task_state.dependent_tasks.push(*task_id);
                    }
                    Entry::Vacant(v) => {
                        v.insert(TaskExecutionState {
                            access_set: build_resource_access_set(task.task.as_ref(), resources),
                            total_dependencies: 0,
                            dependency_counter: 0,
                            dependent_tasks: vec![*task_id],
                        });
                    }
                }
            }

            match self.tasks.entry(*task_id) {
                Entry::Vacant(v) => {
                    v.insert(TaskExecutionState {
                        access_set: build_resource_access_set(task.task.as_ref(), resources),
                        total_dependencies: task_container
                            .get_task(*task_id)
                            .unwrap()
                            .dependencies
                            .len(),
                        dependency_counter: 0,
                        dependent_tasks: vec![],
                    });
                }
                Entry::Occupied(mut o) => {
                    o.get_mut().total_dependencies = task_container
                        .get_task(*task_id)
                        .unwrap()
                        .dependencies
                        .len();
                }
            }
        }
    }
}

fn build_resource_access_set(task: &dyn Task, container: &ResourceContainer) -> ResourceAccessSet {
    let meta_data = task.meta_data();
    let mut read_set = FixedBitSet::with_capacity(container.len());
    let mut write_set = FixedBitSet::with_capacity(container.len());

    for read_type in &meta_data.type_access_list {
        read_set.set(container.get_index_by_type_id(*read_type).unwrap(), true);
    }

    for write_type in &meta_data.type_access_mut_list {
        write_set.set(container.get_index_by_type_id(*write_type).unwrap(), true);
    }

    ResourceAccessSet {
        read_set,
        write_set,
    }
}

#[derive(Debug)]
pub struct ParallelExecutor {
    state: ExecutorState,
    thread_pool: ThreadPool,
}

impl ParallelExecutor {
    pub fn new() -> Result<Self, std::io::Error> {
        let thread_pool = ThreadPool::new(3, Some("Job Thread"))?;
        Ok(Self {
            thread_pool,
            state: ExecutorState::new(),
        })
    }
}

impl ParallelExecutor {
    pub fn execute(
        &mut self,
        task_scheduler: &TaskScheduler,
        tasks: &TaskContainer,
        container: &ResourceContainer,
    ) {
        self.state.prepare(task_scheduler, tasks, container);

        for (id, task) in &mut self.state.tasks {
            task.dependency_counter = task.total_dependencies;
            if task.dependency_counter == 0 {
                self.state.pending_tasks.push(*id);
            }
        }

        // loop until we are out of pending & active tasks
        let mut tasks_to_run = Vec::with_capacity(8);
        let mut dependent_task_ids = Vec::new();
        let mut running_jobs = Vec::with_capacity(8);

        while !self.state.pending_tasks.is_empty() || !self.state.active_tasks.is_empty() {
            self.state.pending_tasks.retain(|task_id| {
                // skip already executing tasks
                if self.state.active_tasks.contains(task_id)
                    || self.state.finished_tasks.contains(task_id)
                {
                    return false;
                }

                let task_state = self.state.tasks.get(task_id).unwrap();

                if self
                    .state
                    .active_access_set
                    .is_compatible(&task_state.access_set)
                {
                    tasks_to_run.push(*task_id);
                    self.state.active_access_set.merge(&task_state.access_set);
                    self.state.active_tasks.insert(*task_id);
                    return false;
                }
                true
            });

            for task_id in tasks_to_run.drain(..) {
                let task_container = tasks.get_task(task_id).unwrap();
                let task_ref = task_container.task.as_ref();
                if !task_container.main_thread_only {
                    running_jobs.push(self.thread_pool.spawn(task_ref, container));
                } else {
                    running_jobs.push(self.thread_pool.run_in_place(task_ref, container));
                }
            }
            // Wait for at least one task to finish (attempt o execute a task while waiting?)

            self.thread_pool.wait_any(&running_jobs);
            running_jobs.retain(|job| {
                if !job.is_finished() {
                    return true;
                }
                let completed_task_id = job.task_id();
                //remove access_set
                let completed_task_state = self.state.tasks.get(&completed_task_id).unwrap();
                // remove from active
                self.state.active_tasks.remove(&completed_task_id);
                // push to finish
                self.state.finished_tasks.insert(completed_task_id);
                // update access set
                self.state
                    .active_access_set
                    .unmerge(&completed_task_state.access_set);
                dependent_task_ids.extend(&completed_task_state.dependent_tasks);
                false
            });

            {
                //push dependees to pending? NOTE: What to do about dependees which are still
                for dependee_id in dependent_task_ids.drain(..) {
                    let dependent_task = self.state.tasks.get_mut(&dependee_id).unwrap();
                    debug_assert!(dependent_task.dependency_counter > 0);
                    dependent_task.dependency_counter -= 1;
                    if dependent_task.dependency_counter == 0 {
                        self.state.pending_tasks.push(dependee_id);
                    }
                }
            }
        }
    }

    pub fn shutdown(&mut self) {
        self.thread_pool.shutdown();
    }
}

#[cfg(test)]
mod tests {
    use crate::executor::ParallelExecutor;
    use crate::extractors::{Reader, Writer};
    use crate::resource::ResourceContainer;
    use crate::task::FunctionTask;
    use crate::task_container::{TaskContainer, TaskId, TaskScheduler};
    use modus_core::sync::ThreadId;
    use std::ops::DerefMut;
    use std::time::Duration;

    #[test]
    fn check_everything_works() {
        const TASK_ID_0: TaskId = TaskId::new("foo");
        const TASK_ID_1: TaskId = TaskId::new("foo1");
        const TASK_ID_2: TaskId = TaskId::new("foo2");
        const TASK_ID_3: TaskId = TaskId::new("foo3");
        const TASK_ID_4: TaskId = TaskId::new("foo4");
        const TASK_ID_5: TaskId = TaskId::new("foo5");

        let mut container = ResourceContainer::new();
        let mut tasks = TaskContainer::new();
        let mut scheduler = TaskScheduler::new();
        let mut executor = ParallelExecutor::new().expect("Failed to create executor");

        container.add(20i32).unwrap();
        container.add(0.4f32).unwrap();

        tasks
            .add_task(
                FunctionTask::new(TASK_ID_0, |v1: Reader<i32>, v2: Reader<f32>| {
                    println!(
                        "T0 {:?} {} {}",
                        modus_core::sync::ThreadId::this_thread(),
                        *v1,
                        *v2
                    );
                }),
                &[],
            )
            .expect("Failed to add task");
        tasks
            .add_task(
                FunctionTask::new(TASK_ID_1, |mut v1: Writer<i32>| {
                    println!("T1 {:?} {}", modus_core::sync::ThreadId::this_thread(), *v1);
                    *(v1.deref_mut()) += 10;
                }),
                &[],
            )
            .expect("Failed to add task");
        tasks
            .add_task(
                FunctionTask::new(TASK_ID_2, |v1: Reader<i32>| {
                    println!("T2 {:?} {}", modus_core::sync::ThreadId::this_thread(), *v1);
                }),
                &[TASK_ID_1],
            )
            .expect("Failed to add task");
        tasks
            .add_task(
                FunctionTask::new(TASK_ID_3, |mut v1: Writer<i32>| {
                    println!("T3 {:?} {}", modus_core::sync::ThreadId::this_thread(), *v1);
                    *(v1.deref_mut()) += 10;
                }),
                &[],
            )
            .expect("Failed to add task");
        tasks
            .add_task(
                FunctionTask::new(TASK_ID_4, |mut v1: Writer<i32>| {
                    println!("T4 {:?} {}", modus_core::sync::ThreadId::this_thread(), *v1);
                    *(v1.deref_mut()) += 10;
                }),
                &[],
            )
            .expect("Failed to add task");
        tasks
            .add_task(
                FunctionTask::new(TASK_ID_5, |mut v1: Writer<i32>| {
                    println!("T5 {:?} {}", modus_core::sync::ThreadId::this_thread(), *v1);
                    *(v1.deref_mut()) += 10;
                }),
                &[],
            )
            .expect("Failed to add task");

        scheduler.add_enabled_tasks(&tasks);
        scheduler
            .validate(&tasks)
            .expect("Failed to validate tasks");
        executor.execute(&scheduler, &tasks, &container);
    }

    #[test]
    fn main_thread_job_execution() {
        let this_thread_id = ThreadId::this_thread();
        const TASK_ID_0: TaskId = TaskId::new("foo");
        const TASK_ID_1: TaskId = TaskId::new("foo1");
        const TASK_ID_2: TaskId = TaskId::new("foo2");
        const TASK_ID_3: TaskId = TaskId::new("foo3");
        const TASK_ID_4: TaskId = TaskId::new("foo4");

        let mut container = ResourceContainer::new();
        let mut tasks = TaskContainer::new();
        let mut executor = ParallelExecutor::new().expect("Failed to create executor");
        let mut scheduler = TaskScheduler::new();

        container.add(20i32).unwrap();
        container.add(0.4f32).unwrap();

        fn non_main_thread_task(_: Reader<i32>) {
            std::thread::sleep(Duration::from_secs(1));
        }

        tasks
            .add_task(FunctionTask::new(TASK_ID_0, non_main_thread_task), &[])
            .expect("Failed to add task");
        tasks
            .add_task(FunctionTask::new(TASK_ID_1, non_main_thread_task), &[])
            .expect("Failed to add task");
        tasks
            .add_task(FunctionTask::new(TASK_ID_2, non_main_thread_task), &[])
            .expect("Failed to add task");
        tasks
            .add_task(FunctionTask::new(TASK_ID_3, non_main_thread_task), &[])
            .expect("Failed to add task");
        tasks
            .add_main_thread_task(
                FunctionTask::new(TASK_ID_4, move |_: Reader<i32>| {
                    assert_eq!(this_thread_id, ThreadId::this_thread());
                }),
                &[],
            )
            .expect("Failed to add task");
        scheduler.add_enabled_tasks(&tasks);
        scheduler
            .validate(&tasks)
            .expect("Failed to validate tasks");
        executor.execute(&scheduler, &tasks, &container);
        executor.execute(&scheduler, &tasks, &container);
    }
}
