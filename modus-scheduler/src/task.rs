use crate::extractors::{FromResourceContainer, MutabilityMarkerExtractor};
use crate::resource::ResourceContainer;
use crate::task_container::TaskId;
use std::any::TypeId;
use std::fmt::{Debug, Formatter};
use std::marker::PhantomData;

pub trait ArgHandler<Args>: Clone + 'static {
    type Output;
    fn call(&self, args: Args) -> Self::Output;
}

#[derive(Debug)]
pub struct TaskMetaData {
    pub id: TaskId,
    pub type_access_list: Vec<TypeId>,
    pub type_access_mut_list: Vec<TypeId>,
}

/// Represents a unit of work which can be scheduled or not
pub trait Task: Send + Sync + 'static + Debug {
    fn meta_data(&self) -> &TaskMetaData;
    fn execute(&self, container: &ResourceContainer);
}

pub struct FunctionTask<F, Args>
where
    Args: MutabilityMarkerExtractor,
{
    meta_data: TaskMetaData,
    function: F,
    // Cheat to get get auto send and sync rules
    p: PhantomData<fn() -> Args>,
}

impl<F, Args> Debug for FunctionTask<F, Args>
where
    Args: MutabilityMarkerExtractor,
{
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        write!(f, "FunctionTask{{")?;
        Debug::fmt(&self.meta_data, f)?;
        write!(f, "}}")
    }
}

impl<F, Args> FunctionTask<F, Args>
where
    F: ArgHandler<Args, Output = ()> + Sync + Send + 'static,
    Args: MutabilityMarkerExtractor,
{
    pub fn new(task_id: TaskId, handler: F) -> Self {
        let mut type_access_list = Vec::new();
        let mut type_access_mut_list = Vec::new();

        Args::extract_mutability(&mut type_access_list, &mut type_access_mut_list);
        Self {
            meta_data: TaskMetaData {
                id: task_id,
                type_access_list,
                type_access_mut_list,
            },
            function: handler,
            p: PhantomData,
        }
    }
}

impl<F, Args> Task for FunctionTask<F, Args>
where
    F: ArgHandler<Args, Output = ()> + Send + Sync + 'static,
    Args: MutabilityMarkerExtractor + 'static + Debug,
{
    fn meta_data(&self) -> &TaskMetaData {
        &self.meta_data
    }

    fn execute(&self, container: &ResourceContainer) {
        let args = Args::extract_resource(&container)
            .expect("Failed to extract arguments from resource container");
        self.function.call(args)
    }
}

macro_rules! task_arg_handler_tuple ({$($param:ident)*} =>  {
   impl<Func, R, $($param:FromResourceContainer + 'static,)*> ArgHandler<($($param,)*)> for Func where
    Func: Fn($($param,)*) -> R + Clone + 'static, R:Clone {
       type Output =R;
        #[allow(non_snake_case)]
       fn call(&self, ($($param,)*):($($param,)*))-> Self::Output {
           (self)($($param,)*)
       }
   }
});

task_arg_handler_tuple! {}
task_arg_handler_tuple! {A}
task_arg_handler_tuple! {A B}
task_arg_handler_tuple! {A B C}
task_arg_handler_tuple! {A B C D}
task_arg_handler_tuple! {A B C D E}
task_arg_handler_tuple! {A B C D E F}
task_arg_handler_tuple! {A B C D E F G}
task_arg_handler_tuple! {A B C D E F G H }
task_arg_handler_tuple! {A B C D E F G H I}
task_arg_handler_tuple! {A B C D E F G H I J}
task_arg_handler_tuple! {A B C D E F G H I J K}

#[cfg(test)]
mod tests {
    use crate::extractors::{Reader, Writer};
    use crate::resource::ResourceContainer;
    use crate::task::{FunctionTask, Task};
    use crate::task_container::TaskId;
    use std::any::TypeId;

    fn empty_callable() {}

    const I32_VALUE: i32 = 20;
    const U32_VALUE: u32 = 40;
    fn one_arg_callable(v: Reader<i32>) {
        assert_eq!(*v, I32_VALUE);
    }

    fn two_arg_callable(v1: Reader<i32>, v2: Writer<u32>) {
        assert_eq!(*v1, I32_VALUE);
        assert_eq!(*v2, U32_VALUE);
    }

    #[test]
    fn check_task_conversion() {
        let mut container = ResourceContainer::new();
        container.add(I32_VALUE).expect("Failed to add resource");
        container.add(U32_VALUE).expect("Failed to add resource");

        {
            let callable = FunctionTask::new(TaskId::new("0"), empty_callable);
            callable.execute(&container);
            assert!(callable.meta_data().type_access_list.is_empty());
            assert!(callable.meta_data().type_access_mut_list.is_empty());
        }
        {
            let callable = FunctionTask::new(TaskId::new("0"), one_arg_callable);
            callable.execute(&container);
            assert_eq!(callable.meta_data().type_access_list.len(), 1);
            assert_eq!(
                callable.meta_data().type_access_list[0],
                TypeId::of::<i32>()
            );
            assert!(callable.meta_data().type_access_mut_list.is_empty());
        }
        {
            let callable = FunctionTask::new(TaskId::new("0"), two_arg_callable);
            callable.execute(&container);
            assert_eq!(callable.meta_data().type_access_list.len(), 1);
            assert_eq!(
                callable.meta_data().type_access_list[0],
                TypeId::of::<i32>()
            );
            assert_eq!(callable.meta_data().type_access_mut_list.len(), 1);
            assert_eq!(
                callable.meta_data().type_access_mut_list[0],
                TypeId::of::<u32>()
            );
        }
    }
}
