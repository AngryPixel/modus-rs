//! Simple function to generate a ppm image file for debugging purposes

#[derive(Debug, Eq, PartialEq)]
pub enum Colors {
    Gray,
    RGB,
}

/// Write ppm (RGB) or pgm (Gray) to a writer.
pub fn write_ppm_or_pgm(
    writer: &mut impl std::io::Write,
    width: usize,
    height: usize,
    colors: Colors,
    bytes: &[u8],
) -> std::io::Result<()> {
    let header = format!(
        "{}\n{} {}\n255\n",
        if colors == Colors::Gray { "P2" } else { "P3" },
        width,
        height
    );
    let color_channel_count = if colors == Colors::RGB { 3 } else { 1 };
    assert_eq!(color_channel_count * width * height, bytes.len());
    writer.write_all(header.as_bytes())?;
    for j in 0..height {
        for i in 0..width {
            let index = j * (width * color_channel_count) + (i * color_channel_count);
            if color_channel_count == 1 {
                writer.write_all(format!("{} ", bytes[index]).as_bytes())?;
            } else {
                writer.write_all(
                    format!(
                        "{} {} {} ",
                        bytes[index],
                        bytes[index + 1],
                        bytes[index + 2]
                    )
                    .as_bytes(),
                )?;
            }
        }
        writer.write_all("\n".as_bytes())?;
    }
    Ok(())
}
