use std::cell::{Ref, RefCell, RefMut};

#[derive(Debug, Copy, Clone, Eq, PartialEq)]
pub struct ThreadId(usize);

impl ThreadId {
    pub fn this_thread() -> Self {
        Self(Self::get_impl())
    }
    #[cfg(unix)]
    fn get_impl() -> usize {
        unsafe { libc::pthread_self() as usize }
    }

    #[cfg(windows)]
    fn get_impl() -> usize {
        unsafe { winapi::um::processthreadsapi::GetCurrentThreadId() as usize }
    }
}

#[test]
fn ensure_thread_ids_are_unique() {
    let this_thread_id = ThreadId::this_thread();
    std::thread::spawn(move || {
        let spawned_thread_id = ThreadId::this_thread();
        assert_ne!(this_thread_id, spawned_thread_id);
    });
}

/// Implements a thread safe accessor that only allows the owning thread to access the contents
#[derive(Debug)]
pub struct ThreadOwned<T>
where
    T: std::fmt::Debug,
{
    value: RefCell<T>,
    owning_thread_id: ThreadId,
}

#[derive(Debug)]
pub struct ThreadOwnedAccessError {}

impl<T: std::fmt::Debug> ThreadOwned<T> {
    pub fn new(value: T) -> Self {
        Self {
            value: RefCell::new(value),
            owning_thread_id: ThreadId::this_thread(),
        }
    }

    pub fn is_owning_thread(&self) -> bool {
        self.owning_thread_id == ThreadId::this_thread()
    }

    /// Immutable borrow of the owned value
    /// # Panic
    /// This will panic when accessed from any other thread other than the owning thread
    pub fn borrow(&self) -> Ref<'_, T> {
        assert!(
            self.is_owning_thread(),
            "Accessing data from non owning thread. This is not allowed"
        );
        // this is safe to call since this can only be access from the owning thread
        self.value.borrow()
    }

    /// Immutable borrow of the owned value, returning an error if the thread attempting to borrow is
    /// not the owning thread.
    pub fn try_borrow(&self) -> Result<Ref<'_, T>, ThreadOwnedAccessError> {
        if !self.is_owning_thread() {
            return Err(ThreadOwnedAccessError {});
        }
        // this is safe to call since this can only be access from the owning thread
        Ok(self.value.borrow())
    }

    /// Mutable borrow of the owned value
    /// # Panic
    /// This will panic when accessed from any other thread other than the owning thread
    pub fn borrow_mut(&self) -> RefMut<'_, T> {
        assert!(
            self.is_owning_thread(),
            "Accessing data from non owning thread. This is not allowed"
        );
        // this is safe to call since this can only be access from the owning thread
        self.value.borrow_mut()
    }

    /// Mutable borrow of the owned value, returning an error if the thread attempting to borrow is
    /// not the owning thread.
    pub fn try_borrow_mut(&self) -> Result<RefMut<'_, T>, ThreadOwnedAccessError> {
        if !self.is_owning_thread() {
            return Err(ThreadOwnedAccessError {});
        }
        // this is safe to call since this can only be access from the owning thread
        Ok(self.value.borrow_mut())
    }

    pub fn try_with<R, F: FnOnce(&T) -> R>(&self, f: F) -> Result<R, ThreadOwnedAccessError> {
        if !self.is_owning_thread() {
            return Err(ThreadOwnedAccessError {});
        }
        Ok(f(&self.borrow()))
    }

    pub fn try_with_mut<R, F: FnOnce(&mut T) -> R>(
        &self,
        f: F,
    ) -> Result<R, ThreadOwnedAccessError> {
        if !self.is_owning_thread() {
            return Err(ThreadOwnedAccessError {});
        }
        Ok(f(&mut self.borrow_mut()))
    }
}

unsafe impl<T: std::fmt::Debug + Send> std::marker::Send for ThreadOwned<T> {}
unsafe impl<T: std::fmt::Debug + Sync> std::marker::Sync for ThreadOwned<T> {}

pub use parking_lot::FairMutex;
pub use parking_lot::Mutex;
pub use parking_lot::ReentrantMutex;
pub use parking_lot::RwLock;

pub mod intrusiverc;
