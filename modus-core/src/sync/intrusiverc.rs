//! Intrusive Reference Counted Wrapper

use std::alloc::Layout;
use std::fmt::Formatter;
use std::marker::PhantomData;
use std::ops::Deref;
use std::ptr::NonNull;
use std::sync::atomic::AtomicI32;

/// Trait which expect a given type to increment it's internal reference count
pub trait RefCounted {
    /// Increment reference count
    fn increment_ref(&self);

    /// Return previous ref count before decrement
    fn decrement_ref(&self) -> i32;
}

/// Describe how memory should be controlled
pub trait Allocator {
    fn allocate(layout: Layout) -> NonNull<u8>;
    fn deallocate(ptr: NonNull<u8>, layout: Layout);
}

/// Implements the [Allocator] trait for the global allocator
pub struct GlobalAllocator {}

impl Allocator for GlobalAllocator {
    fn allocate(layout: Layout) -> NonNull<u8> {
        unsafe { NonNull::new(std::alloc::alloc(layout)).expect("Failed to allocate memory") }
    }

    fn deallocate(ptr: NonNull<u8>, layout: Layout) {
        unsafe {
            std::alloc::dealloc(ptr.as_ptr(), layout);
        }
    }
}

/// Wrapper around a type which adds reference counting to the type
pub struct IntrusiveRefCounted<T: Sized> {
    ref_count: AtomicI32,
    value: T,
}

impl<T: Sized> IntrusiveRefCounted<T> {
    pub fn new(value: T) -> Self {
        Self {
            value,
            ref_count: AtomicI32::new(1),
        }
    }
}

impl<T: Sized> Deref for IntrusiveRefCounted<T> {
    type Target = T;

    fn deref(&self) -> &Self::Target {
        &self.value
    }
}

impl<T> RefCounted for IntrusiveRefCounted<T> {
    fn increment_ref(&self) {
        self.ref_count
            .fetch_add(1, std::sync::atomic::Ordering::AcqRel);
    }

    fn decrement_ref(&self) -> i32 {
        self.ref_count
            .fetch_sub(1, std::sync::atomic::Ordering::AcqRel)
    }
}

/// Intrusive atomic reference counted ptr.
pub struct IntrusiveArc<T: RefCounted + Sized, Alloc: Allocator = GlobalAllocator> {
    ptr: NonNull<T>,
    p: PhantomData<T>,
    a: PhantomData<Alloc>,
}

impl<T: RefCounted, Alloc: Allocator> IntrusiveArc<T, Alloc> {
    pub fn new(value: T) -> Self {
        let ptr = Alloc::allocate(std::alloc::Layout::new::<T>());
        let ptr = unsafe {
            let ptr = NonNull::new_unchecked(ptr.as_ptr() as *mut T);
            std::ptr::write(ptr.as_ptr(), value);
            ptr
        };
        Self {
            ptr,
            p: PhantomData,
            a: PhantomData,
        }
    }

    /// # Safety
    /// You must guarantee that this pointer was allocated using the same allocator
    pub unsafe fn from_ptr(ptr: NonNull<T>) -> Self {
        Self {
            ptr,
            p: PhantomData,
            a: PhantomData,
        }
    }
}

impl<T: RefCounted + Sized, Alloc: Allocator> Drop for IntrusiveArc<T, Alloc> {
    fn drop(&mut self) {
        unsafe {
            if self.ptr.as_mut().decrement_ref() != 1 {
                return;
            }
            std::ptr::drop_in_place(self.ptr.as_ptr());
            let ptr = NonNull::new_unchecked(self.ptr.as_ptr() as *mut u8);
            Alloc::deallocate(ptr, std::alloc::Layout::new::<T>());
        }
    }
}

impl<T: std::fmt::Debug + RefCounted, Alloc: Allocator> std::fmt::Debug for IntrusiveArc<T, Alloc> {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        std::fmt::Debug::fmt(unsafe { self.ptr.as_ref() }, f)
    }
}

impl<T: RefCounted, Alloc: Allocator> Deref for IntrusiveArc<T, Alloc> {
    type Target = T;

    fn deref(&self) -> &Self::Target {
        unsafe { self.ptr.as_ref() }
    }
}

impl<T: RefCounted, Alloc: Allocator> Clone for IntrusiveArc<T, Alloc> {
    fn clone(&self) -> Self {
        unsafe {
            self.ptr.as_ref().increment_ref();
        }
        Self {
            ptr: self.ptr,
            p: Default::default(),
            a: Default::default(),
        }
    }
}

impl<T: RefCounted, Alloc: Allocator> AsRef<T> for IntrusiveArc<T, Alloc> {
    fn as_ref(&self) -> &T {
        unsafe { self.ptr.as_ref() }
    }
}

unsafe impl<T: RefCounted + Send, Alloc: Allocator> Send for IntrusiveArc<T, Alloc> {}
unsafe impl<T: RefCounted + Sync, Alloc: Allocator> Sync for IntrusiveArc<T, Alloc> {}

#[cfg(test)]
mod test {
    use crate::sync::intrusiverc::{GlobalAllocator, IntrusiveArc, IntrusiveRefCounted};
    use std::ops::Deref;

    type TestIARC = IntrusiveArc<IntrusiveRefCounted<i32>, GlobalAllocator>;

    #[test]
    fn test_basic_setup() {
        let ptr: TestIARC = IntrusiveArc::new(IntrusiveRefCounted::new(1024));
        assert_eq!(*(ptr.deref().deref()), 1024);
        let _copy = ptr.clone();
    }
}
