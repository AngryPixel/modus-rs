/// Hold the data related to one layer of the atlas
#[derive(Debug)]
pub struct AtlasLayer {
    pub data: Vec<u8>,
    pub dirty: bool,
}

impl AtlasLayer {
    fn new(data_size: usize) -> Self {
        Self {
            data: vec![0; data_size],
            dirty: false,
        }
    }

    fn clear(&mut self) {
        self.data.fill(0);
        self.dirty = true;
    }
}

#[derive(Debug, Copy, Clone)]
pub struct AllocResult {
    pub x: u32,
    pub y: u32,
    pub l: u32,
    pub uv_top_left: [f32; 2],
    pub ub_bottom_right: [f32; 2],
}

/// Texture atlas which has the option to use multiple layers to store content in the atlas. This
/// can map to rendering APIs which have support for 2d array textures.
#[derive(Debug)]
pub struct TextureAtlas {
    layers: Vec<AtlasLayer>,
    width: u32,
    height: u32,
    cursor: [u32; 3],
    color_channels: u32,
    max_height: u32,
    padding: u32,
}

impl TextureAtlas {
    pub fn new(width: u32, height: u32, layers: u32, gray_scale: bool, padding: u32) -> Self {
        let color_channels: u32 = if gray_scale { 1 } else { 4 };
        let mut r = Self {
            width: width.next_power_of_two(),
            height: height.next_power_of_two(),
            layers: Vec::with_capacity(layers as usize),
            color_channels,
            padding,
            cursor: [padding, padding, 0],
            max_height: 0,
        };

        for _ in 0..layers {
            r.layers
                .push(AtlasLayer::new((width * height * color_channels) as usize));
        }

        r
    }

    #[inline(always)]
    pub fn height(&self) -> u32 {
        self.height
    }

    #[inline(always)]
    pub fn width(&self) -> u32 {
        self.width
    }

    #[inline(always)]
    pub fn layers(&self) -> u32 {
        self.layers.len() as u32
    }

    pub fn is_dirty(&self) -> bool {
        let mut is_dirty = false;
        for layer in &self.layers {
            is_dirty = is_dirty || layer.dirty;
        }
        is_dirty
    }

    pub fn clear(&mut self) {
        self.cursor = [0; 3];
        for layer in &mut self.layers {
            layer.clear();
        }
    }

    #[inline(always)]
    pub fn color_channels(&self) -> u32 {
        self.color_channels
    }

    #[inline(always)]
    pub fn layer_data(&self, index: u32) -> &[u8] {
        &self.layers[index as usize].data
    }

    #[inline(always)]
    pub fn atlas_layers(&self) -> &[AtlasLayer] {
        &self.layers
    }

    pub fn allocate_slot(&mut self, width: u32, height: u32) -> Option<AllocResult> {
        if width >= self.width || height >= self.height {
            return None;
        }

        let padding_x2 = self.padding * 2;
        // update cursor info, check if we need to skip rows
        if self.cursor[0] + width + padding_x2 > self.width {
            self.cursor[0] = self.padding;
            self.cursor[1] += self.max_height + padding_x2;
            self.max_height = 0;
        }
        // update cursor info, check if we need to skip layers
        if self.cursor[1] + height + padding_x2 > self.height {
            self.cursor[0] = self.padding;
            self.cursor[1] = self.padding;
            self.cursor[2] += 1;
        }

        if self.cursor[2] >= self.layers() {
            //Exceeded all capacity
            return None;
        }

        self.layers[self.cursor[2] as usize].dirty = true;
        let result = AllocResult {
            x: self.cursor[0],
            y: self.cursor[1],
            l: self.cursor[2],
            uv_top_left: [
                self.cursor[0] as f32 / self.width as f32,
                self.cursor[1] as f32 / self.height as f32,
            ],
            ub_bottom_right: [
                (self.cursor[0] + width) as f32 / self.width as f32,
                (self.cursor[1] + height) as f32 / self.height as f32,
            ],
        };
        self.cursor[0] += width + padding_x2;
        self.max_height = self.max_height.max(height);
        Some(result)
    }

    pub fn write_at_gray_scale(&mut self, x: u32, y: u32, l: u32, pixel: u8) {
        self.write_at_color(x, y, l, [pixel, pixel, pixel, 255]);
    }

    pub fn write_at_color(&mut self, x: u32, y: u32, l: u32, pixel: [u8; 4]) {
        let stride = self.width * self.color_channels;
        let index = (y * stride + x) as usize;
        let layer_data = &mut self.layers[l as usize].data;
        layer_data[index] = pixel[0];
        if self.color_channels != 1 {
            layer_data[index + 1] = pixel[1];
            layer_data[index + 2] = pixel[2];
            layer_data[index + 3] = pixel[3];
        }
    }

    pub fn reset_dirty_state(&mut self) {
        for layer in &mut self.layers {
            layer.dirty = false;
        }
    }
}
