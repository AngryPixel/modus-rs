use super::*;
use crate::define_custom_handle_type;

#[test]
fn masked_counter_basic() {
    let mut mcounterer = MaskedCounter::new();
    assert!(!mcounterer.is_alive());
    assert_eq!(mcounterer.get_value(), 0);

    mcounterer.increment();
    assert!(!mcounterer.is_alive());
    assert_eq!(mcounterer.get_value(), 1);

    mcounterer.set_alive(true);
    assert!(mcounterer.is_alive());
    assert_eq!(mcounterer.get_value(), 1);

    mcounterer.increment();
    assert!(mcounterer.is_alive());
    assert_eq!(mcounterer.get_value(), 2);

    mcounterer.set_alive(false);
    assert!(!mcounterer.is_alive());
}

#[test]
fn masked_counter_wrapping_add() {
    let mut mcounterer = MaskedCounter::with_values(true, MASKED_COUNTER_COUNTER_MASK >> 1);
    assert!(mcounterer.is_alive());
    assert_eq!(mcounterer.get_value(), MASKED_COUNTER_COUNTER_MASK >> 1);

    mcounterer.increment();
    assert!(mcounterer.is_alive());
    assert_eq!(mcounterer.get_value(), 0);
}

struct TestType {
    _v: u64,
}

impl TestType {
    fn new() -> Self {
        println!("TestType CTOR");
        TestType { _v: 0xDEADBEAF }
    }
}

impl Drop for TestType {
    fn drop(&mut self) {
        println!("TestType DTOR");
    }
}
const MAX_STACK_CAPACITY: usize = 16;
type ArrayHandleMapType = ArrayHandleMap<Handle, TestType, MAX_STACK_CAPACITY>;

#[test]
fn array_handle_map_add() {
    let mut map = ArrayHandleMapType::new();
    {
        let h = map.insert(TestType::new());
        assert!(h.is_ok());
    }
    println!("After Insert");
}

#[test]
fn array_handle_map_add_remove() {
    let mut map = ArrayHandleMap::<Handle, u32, 4>::new();
    let v1 = 10_u32;
    let v2 = 20_u32;
    let v3 = 30_u32;
    let v4 = 40_u32;
    let v5 = 50_u32;

    // Add Values
    let r1 = map.insert(v1);
    assert!(r1.is_ok());
    let r2 = map.insert(v2);
    assert!(r2.is_ok());
    let r3 = map.insert(v3);
    assert!(r3.is_ok());
    let r4 = map.insert(v4);
    assert!(r4.is_ok());
    let r5 = map.insert(v5);
    assert!(!r5.is_ok());
    assert_eq!(map.len(), 4);

    // Check Values
    let gr1 = map.get(r1.unwrap());
    assert!(gr1.is_some());
    assert_eq!(*gr1.unwrap(), v1);

    let gr2 = map.get(r2.unwrap());
    assert!(gr2.is_some());
    assert_eq!(*gr2.unwrap(), v2);

    let gr3 = map.get(r3.unwrap());
    assert!(gr3.is_some());
    assert_eq!(*gr3.unwrap(), v3);

    let gr4 = map.get(r4.unwrap());
    assert!(gr4.is_some());
    assert_eq!(*gr4.unwrap(), v4);

    // Remove 2 & 3;
    let rem2 = map.remove(r2.unwrap());
    assert!(rem2.is_some());
    let rem3 = map.remove(r3.unwrap());
    assert!(rem3.is_some());
    assert_eq!(map.len(), 2);

    assert!(map.contains_handle(r1.unwrap()));
    assert!(map.contains_handle(r4.unwrap()));

    // Re add r2 and r3;
    let r2_v2 = map.insert(v2);
    assert!(r2.is_ok());
    let r3_v2 = map.insert(v3);
    assert!(r3.is_ok());

    // validate new r2 and r3
    let gr2_v2 = map.get(r2_v2.unwrap());
    assert!(gr2_v2.is_some());
    assert_eq!(*gr2_v2.unwrap(), v2);

    let gr3_v2 = map.get(r3_v2.unwrap());
    assert!(gr3_v2.is_some());
    assert_eq!(*gr3_v2.unwrap(), v3);

    // Make sure v1 r2 and r2 no longe exist
    assert!(!map.contains_handle(r2.unwrap()));
    assert!(!map.contains_handle(r3.unwrap()));

    // Remove everything
    let rem1 = map.remove(r1.unwrap());
    assert!(rem1.is_some());
    let rem4 = map.remove(r4.unwrap());
    assert!(rem4.is_some());
    let rem2_v2 = map.remove(r2_v2.unwrap());
    assert!(rem2_v2.is_some());
    let rem3_v2 = map.remove(r3_v2.unwrap());
    assert!(rem3_v2.is_some());
    assert_eq!(map.len(), 0);

    // Fill it up again
    let r1_v3 = map.insert(v1);
    assert!(r1_v3.is_ok());
    let r2_v3 = map.insert(v2);
    assert!(r2_v3.is_ok());
    let r3_v3 = map.insert(v3);
    assert!(r3_v3.is_ok());
    let r4_v3 = map.insert(v4);
    assert!(r4_v3.is_ok());
    let r5_v3 = map.insert(v5);
    assert!(r5_v3.is_err());

    // Check Values
    let gr1_v3 = map.get(r1_v3.unwrap());
    assert!(gr1_v3.is_some());
    assert_eq!(*gr1_v3.unwrap(), v1);

    let gr2_v3 = map.get(r2_v3.unwrap());
    assert!(gr2_v3.is_some());
    assert_eq!(*gr2_v3.unwrap(), v2);

    let gr3_v3 = map.get(r3_v3.unwrap());
    assert!(gr3_v3.is_some());
    assert_eq!(*gr3_v3.unwrap(), v3);

    let gr4_v3 = map.get(r4_v3.unwrap());
    assert!(gr4_v3.is_some());
    assert_eq!(*gr4_v3.unwrap(), v4);

    assert_eq!(map.len(), 4);

    map.clear();

    assert_eq!(map.len(), 0);
}

#[test]
fn array_handle_map_retain() {
    let mut map = ArrayHandleMap::<Handle, u32, 4>::new();
    let v1 = 1_u32;
    let v2 = 2_u32;
    let v3 = 3_u32;
    let v4 = 4_u32;

    // Add Values
    let r1 = map.insert(v1);
    assert!(r1.is_ok());
    let r2 = map.insert(v2);
    assert!(r2.is_ok());
    let r3 = map.insert(v3);
    assert!(r3.is_ok());
    let r4 = map.insert(v4);
    assert!(r4.is_ok());

    map.retain(|value| -> bool { value % 2u32 == 0 });

    assert!(!map.contains_handle(r1.unwrap()));
    assert!(map.contains_handle(r2.unwrap()));
    assert!(!map.contains_handle(r3.unwrap()));
    assert!(map.contains_handle(r4.unwrap()));
}

#[test]
fn array_handle_map_add_clear() {
    let mut map = ArrayHandleMap::<Handle, u32, 4>::new();
    let v1 = 10_u32;
    let v2 = 20_u32;
    let v3 = 30_u32;
    let v4 = 40_u32;
    let v5 = 50_u32;

    {
        // Add Values
        let r1 = map.insert(v1);
        assert!(r1.is_ok());
        let r2 = map.insert(v2);
        assert!(r2.is_ok());
        let r3 = map.insert(v3);
        assert!(r3.is_ok());
        let r4 = map.insert(v4);
        assert!(r4.is_ok());
        let r5 = map.insert(v5);
        assert!(!r5.is_ok());
        assert_eq!(map.len(), 4);

        map.clear();
        assert_eq!(map.count, 0);
        assert!(map.get(r1.unwrap()).is_none());
        assert!(map.get(r2.unwrap()).is_none());
        assert!(map.get(r3.unwrap()).is_none());
        assert!(map.get(r4.unwrap()).is_none());
    }

    // Add Values again
    let r1 = map.insert(v1);
    assert!(r1.is_ok());
    let r2 = map.insert(v2);
    assert!(r2.is_ok());
    let r3 = map.insert(v3);
    assert!(r3.is_ok());
    let r4 = map.insert(v4);
    assert!(r4.is_ok());
    let r5 = map.insert(v5);
    assert!(!r5.is_ok());
    assert_eq!(map.len(), 4);
}

#[derive(Debug)]
struct Foo {
    v: u32,
}

#[test]
fn array_handle_map_iter() {
    let mut map = ArrayHandleMap::<Handle, Foo, 4>::new();
    let val_2 = 2_u32;
    let val_4 = 4_u32;
    let v1 = Foo { v: 1_u32 };
    let v2 = Foo { v: val_2 };
    let v3 = Foo { v: 3_u32 };
    let v4 = Foo { v: val_4 };

    // Add Values
    let r1 = map.insert(v1);
    assert!(r1.is_ok());
    let r2 = map.insert(v2);
    assert!(r2.is_ok());
    let r3 = map.insert(v3);
    assert!(r3.is_ok());
    let r4 = map.insert(v4);
    assert!(r4.is_ok());
    map.retain(|value| -> bool { value.v % 2u32 == 0 });

    {
        let mut iter = map.iter();
        {
            let v = iter.next();
            assert!(v.is_some());
            assert_eq!(v.unwrap().v, val_2);
        }
        {
            let v = iter.next();
            assert!(v.is_some());
            assert_eq!(v.unwrap().v, val_4);
        }
        {
            let v = iter.next();
            assert!(v.is_none());
        }
    }

    {
        let mut iter = map.iter_mut();
        {
            let v = iter.next();
            assert!(v.is_some());
            assert_eq!(v.unwrap().v, val_2);
        }
        {
            let v = iter.next();
            assert!(v.is_some());
            assert_eq!(v.unwrap().v, val_4);
        }
        {
            let v = iter.next();
            assert!(v.is_none());
        }
    }

    for v in &map {
        println!("v={}", v.v);
    }

    for v in &mut map {
        println!("v={}", v.v);
    }
}

define_custom_handle_type!(MyHandle);

#[test]
fn custom_handle_type() {
    let custom2 = MyHandle::new();
    let mut map = ArrayHandleMap::<MyHandle, Foo, 4>::new();
    map.contains_handle(custom2.into());

    let r = map.insert(Foo { v: 32 });
    assert!(r.is_ok());
    assert!(map.contains_handle(r.unwrap().into()));
}

type HandleMapType = HandleMap<Handle, TestType>;
#[test]
fn handle_map_add() {
    let mut map = HandleMapType::new(2);
    {
        let h = map.insert(TestType::new());
        assert!(h.is_ok());
    }
    println!("After Insert");
}

#[test]
fn handle_map_add_remove() {
    let mut map = HandleMap::<Handle, u32>::new(2);
    let v1 = 10_u32;
    let v2 = 20_u32;
    let v3 = 30_u32;
    let v4 = 40_u32;
    let v5 = 50_u32;

    // Add Values
    let r1 = map.insert(v1);
    assert!(r1.is_ok());
    let r2 = map.insert(v2);
    assert!(r2.is_ok());
    let r3 = map.insert(v3);
    assert!(r3.is_ok());
    let r4 = map.insert(v4);
    assert!(r4.is_ok());
    let r5 = map.insert(v5);
    assert!(r5.is_ok());
    assert_eq!(map.len(), 5);

    // Check Values
    let gr1 = map.get(r1.unwrap());
    assert!(gr1.is_some());
    assert_eq!(*gr1.unwrap(), v1);

    let gr2 = map.get(r2.unwrap());
    assert!(gr2.is_some());
    assert_eq!(*gr2.unwrap(), v2);

    let gr3 = map.get(r3.unwrap());
    assert!(gr3.is_some());
    assert_eq!(*gr3.unwrap(), v3);

    let gr4 = map.get(r4.unwrap());
    assert!(gr4.is_some());
    assert_eq!(*gr4.unwrap(), v4);

    let rem5 = map.remove(r5.unwrap());
    assert!(rem5.is_some());
    // Remove 2 & 3;
    let rem2 = map.remove(r2.unwrap());
    assert!(rem2.is_some());
    let rem3 = map.remove(r3.unwrap());
    assert!(rem3.is_some());
    assert_eq!(map.len(), 2);

    assert!(map.contains_handle(r1.unwrap()));
    assert!(map.contains_handle(r4.unwrap()));

    // Re add r2 and r3;
    let r2_v2 = map.insert(v2);
    assert!(r2.is_ok());
    let r3_v2 = map.insert(v3);
    assert!(r3.is_ok());

    // validate new r2 and r3
    let gr2_v2 = map.get(r2_v2.unwrap());
    assert!(gr2_v2.is_some());
    assert_eq!(*gr2_v2.unwrap(), v2);

    let gr3_v2 = map.get(r3_v2.unwrap());
    assert!(gr3_v2.is_some());
    assert_eq!(*gr3_v2.unwrap(), v3);

    // Make sure v1 r2 and r2 no longe exist
    assert!(!map.contains_handle(r2.unwrap()));
    assert!(!map.contains_handle(r3.unwrap()));

    // Remove everything
    let rem1 = map.remove(r1.unwrap());
    assert!(rem1.is_some());
    let rem4 = map.remove(r4.unwrap());
    assert!(rem4.is_some());
    let rem2_v2 = map.remove(r2_v2.unwrap());
    assert!(rem2_v2.is_some());
    let rem3_v2 = map.remove(r3_v2.unwrap());
    assert!(rem3_v2.is_some());
    assert_eq!(map.len(), 0);

    // Fill it up again
    let r1_v3 = map.insert(v1);
    assert!(r1_v3.is_ok());
    let r2_v3 = map.insert(v2);
    assert!(r2_v3.is_ok());
    let r3_v3 = map.insert(v3);
    assert!(r3_v3.is_ok());
    let r4_v3 = map.insert(v4);
    assert!(r4_v3.is_ok());
    let r5_v3 = map.insert(v5);
    assert!(r5_v3.is_ok());

    // Check Values
    let gr1_v3 = map.get(r1_v3.unwrap());
    assert!(gr1_v3.is_some());
    assert_eq!(*gr1_v3.unwrap(), v1);

    let gr2_v3 = map.get(r2_v3.unwrap());
    assert!(gr2_v3.is_some());
    assert_eq!(*gr2_v3.unwrap(), v2);

    let gr3_v3 = map.get(r3_v3.unwrap());
    assert!(gr3_v3.is_some());
    assert_eq!(*gr3_v3.unwrap(), v3);

    let gr4_v3 = map.get(r4_v3.unwrap());
    assert!(gr4_v3.is_some());
    assert_eq!(*gr4_v3.unwrap(), v4);

    assert_eq!(map.len(), 5);

    map.clear();

    assert_eq!(map.len(), 0);
}

#[test]
fn handle_map_retain() {
    let mut map = HandleMap::<Handle, u32>::new(2);
    let v1 = 1_u32;
    let v2 = 2_u32;
    let v3 = 3_u32;
    let v4 = 4_u32;

    // Add Values
    let r1 = map.insert(v1);
    assert!(r1.is_ok());
    let r2 = map.insert(v2);
    assert!(r2.is_ok());
    let r3 = map.insert(v3);
    assert!(r3.is_ok());
    let r4 = map.insert(v4);
    assert!(r4.is_ok());

    map.retain(|value| -> bool { value % 2u32 == 0 });

    assert!(!map.contains_handle(r1.unwrap()));
    assert!(map.contains_handle(r2.unwrap()));
    assert!(!map.contains_handle(r3.unwrap()));
    assert!(map.contains_handle(r4.unwrap()));
}

#[test]
fn handle_map_add_clear() {
    let mut map = HandleMap::<Handle, u32>::new(2);
    let v1 = 10_u32;
    let v2 = 20_u32;
    let v3 = 30_u32;
    let v4 = 40_u32;
    let v5 = 50_u32;

    {
        // Add Values
        let r1 = map.insert(v1);
        assert!(r1.is_ok());
        let r2 = map.insert(v2);
        assert!(r2.is_ok());
        let r3 = map.insert(v3);
        assert!(r3.is_ok());
        let r4 = map.insert(v4);
        assert!(r4.is_ok());
        let r5 = map.insert(v5);
        assert!(r5.is_ok());
        assert_eq!(map.len(), 5);

        map.clear();
        assert_eq!(map.count, 0);
        assert!(map.get(r1.unwrap()).is_none());
        assert!(map.get(r2.unwrap()).is_none());
        assert!(map.get(r3.unwrap()).is_none());
        assert!(map.get(r4.unwrap()).is_none());
    }

    // Add Values again
    let r1 = map.insert(v1);
    assert!(r1.is_ok());
    let r2 = map.insert(v2);
    assert!(r2.is_ok());
    let r3 = map.insert(v3);
    assert!(r3.is_ok());
    let r4 = map.insert(v4);
    assert!(r4.is_ok());
    let r5 = map.insert(v5);
    assert!(r5.is_ok());
    assert_eq!(map.len(), 5);
}

#[test]
fn handle_map_iter() {
    let mut map = HandleMap::<Handle, Foo>::new(2);
    let val_2 = 2_u32;
    let val_4 = 4_u32;
    let v1 = Foo { v: 1_u32 };
    let v2 = Foo { v: val_2 };
    let v3 = Foo { v: 3_u32 };
    let v4 = Foo { v: val_4 };

    // Add Values
    let r1 = map.insert(v1);
    assert!(r1.is_ok());
    let r2 = map.insert(v2);
    assert!(r2.is_ok());
    let r3 = map.insert(v3);
    assert!(r3.is_ok());
    let r4 = map.insert(v4);
    assert!(r4.is_ok());
    map.retain(|value| -> bool { value.v % 2u32 == 0 });

    {
        let mut iter = map.iter();
        {
            let v = iter.next();
            assert!(v.is_some());
            assert_eq!(v.unwrap().v, val_2);
        }
        {
            let v = iter.next();
            assert!(v.is_some());
            assert_eq!(v.unwrap().v, val_4);
        }
        {
            let v = iter.next();
            assert!(v.is_none());
        }
    }

    {
        let mut iter = map.iter_mut();
        {
            let v = iter.next();
            assert!(v.is_some());
            assert_eq!(v.unwrap().v, val_2);
        }
        {
            let v = iter.next();
            assert!(v.is_some());
            assert_eq!(v.unwrap().v, val_4);
        }
        {
            let v = iter.next();
            assert!(v.is_none());
        }
    }

    for v in &map {
        println!("v={}", v.v);
    }

    for v in &mut map {
        println!("v={}", v.v);
    }
}

#[test]
fn handle_map_custom_handle_type() {
    let custom2 = MyHandle::new();
    let mut map = HandleMap::<MyHandle, Foo>::new(2);
    map.contains_handle(custom2.into());

    let r = map.insert(Foo { v: 32 });
    assert!(r.is_ok());
    assert!(map.contains_handle(r.unwrap().into()));
}

#[test]
fn shared_handle_map() {
    let mut map = SharedHandleMap::<MyHandle, i32>::new(2);
    let h = map.insert(20).unwrap();
    map.acquire(h);
    {
        let r = map.release(h);
        assert!(r.is_none());
    }
    {
        let r = map.release(h);
        assert!(r.is_some());
    }
}
