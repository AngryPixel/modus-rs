/// Simple fps counter calculator
use std::time::Duration;

#[derive(Debug, Default)]
pub struct FpsCounter {
    frame_counter: u32,
    fps: f64,
    frame_time_ms: f64,
    fps_tick: Duration,
}

impl FpsCounter {
    pub fn new() -> Self {
        Self {
            frame_counter: 0,
            fps: 0.0,
            frame_time_ms: 0.0,
            fps_tick: Duration::new(0, 0),
        }
    }

    pub fn update(&mut self, tick: Duration) {
        self.fps_tick += tick;
        self.frame_counter += 1;
        if self.fps_tick >= Duration::from_millis(1000) {
            self.fps = self.frame_counter as f64;
            self.frame_time_ms = 1000.0 / (self.frame_counter as f64);
            self.frame_counter = 0;
            self.fps_tick = Duration::new(0, 0);
        }
    }

    pub fn fps(&self) -> f64 {
        self.fps
    }

    pub fn frame_time_ms(&self) -> f64 {
        self.frame_time_ms
    }
}
