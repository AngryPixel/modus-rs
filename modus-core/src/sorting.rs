//! Implementation of different sorting algorithms

#[derive(Debug, Eq, PartialEq)]
pub enum TopologicalSortError<Key> {
    DuplicateNode(Key),
    DependencyCycle(Key, Key), // (node being processed, dependency where cycle was detected)
    DependencyNotFound(Key),
    EmtpyGraph,
}

#[derive(Debug, Copy, Clone, Eq, PartialEq)]
enum TopologicalSorterNodeState {
    None,
    Visiting,
    Visited,
}

#[derive(Debug)]
struct TopologicalSortDependency<Key>
where
    Key: Eq,
{
    key: Key,
    node: std::cell::Cell<Option<usize>>,
}

#[derive(Debug)]
struct TopologicalSorterNode<Key>
where
    Key: Eq + Copy + Clone,
{
    key: Key,
    dependencies: Vec<TopologicalSortDependency<Key>>,
    state: std::cell::Cell<TopologicalSorterNodeState>,
}

#[derive(Debug, Default)]
pub struct TopologicalSorter<Key>
where
    Key: Eq + Copy + Clone,
{
    nodes: Vec<TopologicalSorterNode<Key>>,
}

impl<Key> TopologicalSorter<Key>
where
    Key: Eq + Clone + Copy,
{
    pub fn new() -> Self {
        Self { nodes: vec![] }
    }

    pub fn with_capacity(capacity: usize) -> Self {
        Self {
            nodes: Vec::with_capacity(capacity),
        }
    }

    fn find_node(&self, key: Key) -> Option<usize> {
        for (i, v) in self.nodes.iter().enumerate() {
            if v.key == key {
                return Some(i);
            }
        }
        None
    }

    pub fn add(&mut self, key: Key, dependencies: &[Key]) -> Result<(), TopologicalSortError<Key>> {
        if self.find_node(key).is_some() {
            return Err(TopologicalSortError::DuplicateNode(key));
        }

        let node = TopologicalSorterNode {
            key,
            dependencies: dependencies
                .iter()
                .map(|k| TopologicalSortDependency {
                    key: *k,
                    node: std::cell::Cell::new(None),
                })
                .collect(),
            state: std::cell::Cell::new(TopologicalSorterNodeState::None),
        };
        self.nodes.push(node);
        Ok(())
    }

    pub fn build(&mut self) -> Result<Vec<Key>, TopologicalSortError<Key>> {
        let mut output = vec![];
        self.build_into(&mut output)?;
        Ok(output)
    }

    pub fn build_into(&mut self, output: &mut Vec<Key>) -> Result<(), TopologicalSortError<Key>> {
        if self.nodes.is_empty() {
            return Err(TopologicalSortError::EmtpyGraph);
        }

        output.clear();
        if self.nodes.len() == 1 {
            output.push(self.nodes[0].key);
            return Ok(());
        }

        // reset state and search for dependencies
        for node in &self.nodes {
            node.state.set(TopologicalSorterNodeState::None);
            for dep in &node.dependencies {
                if let Some(node_index) = self.find_node(dep.key) {
                    dep.node.set(Some(node_index));
                } else {
                    return Err(TopologicalSortError::DependencyNotFound(dep.key));
                }
            }
        }

        output.reserve(self.nodes.len());
        let mut queue = Vec::with_capacity(self.nodes.len());

        // start calculation
        for node in self.nodes.iter() {
            if node.state.get() == TopologicalSorterNodeState::None {
                queue.push(node);
            }

            while let Some(queue_node) = queue.pop() {
                // Already visited
                if queue_node.state.get() == TopologicalSorterNodeState::Visited {
                    continue;
                }

                // Visiting, finalize
                if queue_node.state.get() == TopologicalSorterNodeState::Visiting {
                    queue_node.state.set(TopologicalSorterNodeState::Visited);
                    output.push(queue_node.key);
                    continue;
                }

                // First visit
                queue_node.state.set(TopologicalSorterNodeState::Visiting);
                queue.push(queue_node);

                for dep in &queue_node.dependencies {
                    let dep_node = &self.nodes[dep.node.get().unwrap()];
                    if dep_node.state.get() == TopologicalSorterNodeState::Visiting {
                        return Err(TopologicalSortError::DependencyCycle(
                            queue_node.key,
                            dep_node.key,
                        ));
                    }
                    queue.push(dep_node);
                }
            }
        }

        Ok(())
    }

    pub fn clear(&mut self) {
        self.nodes.clear();
    }
}

#[cfg(test)]
mod tests {
    use crate::sorting::{TopologicalSortError, TopologicalSorter};
    #[test]
    fn simple_check() {
        let k1 = 0;
        let k2 = 1;
        let k3 = 2;
        let k4 = 3;

        let mut sorter = TopologicalSorter::<i32>::new();
        assert!(sorter.add(k1, &[]).is_ok());
        assert!(sorter.add(k2, &[k3, k4]).is_ok());
        assert!(sorter.add(k3, &[k1]).is_ok());
        assert!(sorter.add(k4, &[k3]).is_ok());

        let result = sorter.build();
        assert!(result.is_ok());
        let output = result.unwrap();

        let expected = [k1, k3, k4, k2];
        assert_eq!(output.as_slice(), expected);
    }

    #[test]
    fn simple_check_reverse_add() {
        let k1 = 0;
        let k2 = 1;
        let k3 = 2;
        let k4 = 3;

        let mut sorter = TopologicalSorter::<i32>::new();
        assert!(sorter.add(k4, &[k3]).is_ok());
        assert!(sorter.add(k3, &[k1]).is_ok());
        assert!(sorter.add(k2, &[k3, k4]).is_ok());
        assert!(sorter.add(k1, &[]).is_ok());

        let result = sorter.build();
        assert!(result.is_ok());
        let output = result.unwrap();

        let expected = [k1, k3, k4, k2];
        assert_eq!(output.as_slice(), expected);
    }

    #[test]
    fn cycle() {
        let k1 = 0;
        let k2 = 1;
        let k3 = 2;
        let k4 = 3;

        let mut sorter = TopologicalSorter::<i32>::new();
        assert!(sorter.add(k1, &[]).is_ok());
        assert!(sorter.add(k2, &[k3, k4]).is_ok());
        assert!(sorter.add(k3, &[k1]).is_ok());
        assert!(sorter.add(k4, &[k3, k2]).is_ok());

        let result = sorter.build();
        assert!(result.is_err());
        let error = result.expect_err("Should have been an error");
        assert_eq!(error, TopologicalSortError::DependencyCycle(k4, k2));
    }

    #[test]
    fn item_without_dependencies() {
        // Check location of items that have no dependencies, this is all dependent on the order
        // of addition
        let k1 = 0;
        let k2 = 1;
        let k3 = 2;
        let k4 = 3;

        let mut sorter = TopologicalSorter::<i32>::new();
        {
            assert!(sorter.add(k1, &[]).is_ok());
            assert!(sorter.add(k2, &[k3, k4]).is_ok());
            assert!(sorter.add(k3, &[]).is_ok());
            assert!(sorter.add(k4, &[k3]).is_ok());

            let result = sorter.build();
            assert!(result.is_ok());
            let output = result.unwrap();

            let expected = [k1, k3, k4, k2];
            assert_eq!(output.as_slice(), expected);
        }
        sorter.clear();
        {
            assert!(sorter.add(k4, &[k3]).is_ok());
            assert!(sorter.add(k2, &[k3, k4]).is_ok());
            assert!(sorter.add(k3, &[]).is_ok());
            assert!(sorter.add(k1, &[]).is_ok());

            let result = sorter.build();
            assert!(result.is_ok());
            let output = result.unwrap();

            let expected = [k3, k4, k2, k1];
            assert_eq!(output.as_slice(), expected);
        }
        sorter.clear();
        {
            assert!(sorter.add(k3, &[]).is_ok());
            assert!(sorter.add(k1, &[]).is_ok());
            assert!(sorter.add(k4, &[k3]).is_ok());
            assert!(sorter.add(k2, &[k3, k4]).is_ok());

            let result = sorter.build();
            assert!(result.is_ok());
            let output = result.unwrap();

            let expected = [k3, k1, k4, k2];
            assert_eq!(output.as_slice(), expected);
        }
    }
}
