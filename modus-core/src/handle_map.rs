//!
//! Generational handle map available in both stack and memory allocation variants
//!

use std::mem::MaybeUninit;

#[cfg(test)]
#[path = "handle_map_tests.rs"]
mod handle_map_tests;

#[cfg(target_pointer_width = "64")]
#[path = ""]
mod arch_types {
    pub(crate) type DataType = u32;
    pub(crate) const ALIVE_BIT_MASK: DataType = 0x1;
    pub(crate) const MASKED_COUNTER_COUNTER_BIT_SHIFT: DataType = 1;
    pub(crate) const MASKED_COUNTER_COUNTER_MASK: DataType = 0xFFFFFFFE;
}
#[cfg(target_pointer_width = "32")]
#[path = ""]
mod arch_types {
    pub(crate) type DataType = u16;
    pub(crate) const ALIVE_BIT_MASK: DataType = 0x1;
    pub(crate) const MASKED_COUNTER_COUNTER_BIT_SHIFT: DataType = 1;
    pub(crate) const MASKED_COUNTER_COUNTER_MASK: DataType = 0xFFFE;
}

use crate::allocators::pool_allocator::PoolAllocator;
use arch_types::*;
use std::marker::PhantomData;
use std::ops::{Deref, DerefMut};
use std::ptr::{null_mut, NonNull};

const MAX_ELEMENT_COUNT: DataType = DataType::MAX - 1;
const MASKED_COUNTER_SUM_COUNTER_MASK: DataType =
    MASKED_COUNTER_COUNTER_MASK >> MASKED_COUNTER_COUNTER_BIT_SHIFT;

#[derive(Debug, Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash)]
struct MaskedCounter {
    counter: DataType,
}

impl MaskedCounter {
    fn new() -> Self {
        Self { counter: 0 }
    }

    fn with_values(alive: bool, counter: DataType) -> Self {
        debug_assert!(counter <= MASKED_COUNTER_COUNTER_MASK);
        Self {
            counter: (alive as DataType)
                | ((counter << MASKED_COUNTER_COUNTER_BIT_SHIFT) & MASKED_COUNTER_COUNTER_MASK),
        }
    }

    fn is_alive(&self) -> bool {
        self.counter & ALIVE_BIT_MASK != 0
    }

    fn set_alive(&mut self, v: bool) {
        self.counter = (self.counter & MASKED_COUNTER_COUNTER_MASK) | (v as DataType);
    }

    #[cfg(test)]
    fn increment(&mut self) {
        let num = (self.counter & MASKED_COUNTER_COUNTER_MASK) >> 1;
        self.counter =
            (self.counter & ALIVE_BIT_MASK) | (((num + 1) & MASKED_COUNTER_SUM_COUNTER_MASK) << 1);
    }

    fn increment_and_set_dead(&mut self) {
        let num = (self.counter & MASKED_COUNTER_COUNTER_MASK) >> 1;
        self.counter = ((num + 1) & MASKED_COUNTER_SUM_COUNTER_MASK) << 1;
    }

    fn get_value(self) -> DataType {
        (self.counter & MASKED_COUNTER_COUNTER_MASK) >> MASKED_COUNTER_COUNTER_BIT_SHIFT
    }
}

impl std::fmt::Display for MaskedCounter {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        if self.is_alive() {
            write!(f, "counter:{}", self.get_value())
        } else {
            write!(f, "{{DEAD}}",)
        }
    }
}

#[derive(Debug, Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash)]
pub struct Handle {
    counter: MaskedCounter,
    index: DataType,
}

impl Handle {
    pub fn new() -> Self {
        Handle {
            counter: MaskedCounter::new(),
            index: DataType::MAX,
        }
    }
    /// # Safety
    /// It's up to the caller of this function to ensure that the data is valid and makes sense.
    pub unsafe fn new_unchecked_alive(counter: DataType, index: DataType) -> Self {
        Handle {
            counter: MaskedCounter::with_values(true, counter),
            index,
        }
    }

    pub fn is_valid(&self) -> bool {
        self.counter.is_alive()
    }

    pub fn reset(&mut self) {
        *self = Self::new()
    }
}

impl Default for Handle {
    fn default() -> Self {
        Handle::new()
    }
}

impl std::fmt::Display for Handle {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "{{{} index:{}}}", self.counter, self.index)
    }
}

pub trait HandleTrait: Copy + From<Handle> + Into<Handle> + Send {}

impl HandleTrait for Handle {}

#[macro_export]
macro_rules! define_custom_handle_type {
    ($name:ident) => {
        #[derive(Debug, Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash)]
        pub struct $name {
            h: $crate::handle_map::Handle,
        }

        impl $name {
            pub fn new() -> Self {
                Self {
                    h: $crate::handle_map::Handle::new(),
                }
            }

            pub fn is_valid(&self) -> bool {
                self.h.is_valid()
            }

            pub fn reset(&mut self) {
                self.h.reset();
            }
        }

        impl Default for $name {
            fn default() -> Self {
                Self::new()
            }
        }

        impl From<Handle> for $name {
            fn from(h: $crate::handle_map::Handle) -> Self {
                Self { h }
            }
        }

        impl From<$name> for $crate::handle_map::Handle {
            fn from(h: $name) -> Self {
                h.h
            }
        }

        impl From<&mut $name> for $crate::handle_map::Handle {
            fn from(h: &mut $name) -> Self {
                h.h
            }
        }

        impl From<&$name> for $crate::handle_map::Handle {
            fn from(h: &$name) -> Self {
                h.h
            }
        }

        impl std::fmt::Display for $name {
            fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
                write!(f, "{}{}", stringify!($name), self.h)
            }
        }

        impl $crate::handle_map::HandleTrait for $name {}
    };
}

// ---------- ArrayHandleMap --------------------------------------------------------------------

#[derive(Debug, Copy, Clone)]
struct MetaData {
    next_free_index: DataType,
    counter: MaskedCounter,
}

impl Default for MetaData {
    fn default() -> Self {
        MetaData {
            next_free_index: DataType::MAX,
            counter: MaskedCounter::new(),
        }
    }
}

#[derive(Debug)]
pub struct ArrayHandleMap<H, T, const CAPACITY: usize>
where
    H: HandleTrait,
{
    meta_data: [MetaData; CAPACITY],
    data: [MaybeUninit<T>; CAPACITY],
    count: usize,
    next_free_index: usize,
    p: PhantomData<H>,
}

impl<H, T, const CAPACITY: usize> ArrayHandleMap<H, T, CAPACITY>
where
    H: HandleTrait,
{
    pub fn new() -> Self {
        assert!(CAPACITY < MAX_ELEMENT_COUNT as usize);
        let mut instance = Self {
            meta_data: [MetaData::default(); CAPACITY],
            // Safety: We want uninitialized here as life time is controlled when creating and/or
            // removing handles
            data: unsafe { MaybeUninit::<[MaybeUninit<T>; CAPACITY]>::uninit().assume_init() },
            count: 0,
            next_free_index: CAPACITY as usize,
            p: PhantomData,
        };
        instance.init();
        instance
    }

    fn init(&mut self) {
        for i in 0..CAPACITY {
            self.meta_data[i].counter = MaskedCounter::with_values(false, 0);
            self.meta_data[i].next_free_index = (i + 1) as DataType;
        }
        self.next_free_index = 0;
    }

    fn on_drop(&mut self) {
        for i in 0..CAPACITY {
            if self.meta_data[i].counter.is_alive() {
                unsafe { std::ptr::drop_in_place(self.data[i].as_mut_ptr()) };
            }
        }
    }

    pub fn clear(&mut self) {
        for i in 0..CAPACITY {
            if self.meta_data[i].counter.is_alive() {
                unsafe { std::ptr::drop_in_place(self.data[i].as_mut_ptr()) };
            }
            self.meta_data[i].counter.increment_and_set_dead();
            self.next_free_index = i + 1;
        }
        self.count = 0;
        self.next_free_index = 0;
    }

    pub fn capacity(&self) -> usize {
        CAPACITY as usize
    }

    pub fn len(&self) -> usize {
        self.count
    }

    pub fn is_empty(&self) -> bool {
        self.count == 0
    }

    pub fn insert(&mut self, value: T) -> Result<H, T> {
        self.insert_internal(value).map(|h| h.into())
    }

    pub fn insert_with<F: FnOnce(H, &mut T)>(&mut self, value: T, f: F) -> Result<H, T> {
        let h = self.insert_internal(value)?;
        let index = h.index as usize;
        debug_assert!(index < CAPACITY);
        f(h.into(), unsafe { &mut *self.data[index].as_mut_ptr() });
        Ok(h.into())
    }

    #[inline(always)]
    fn is_valid_handle(&self, h: Handle) -> bool {
        let uindex = h.index as usize;
        uindex < CAPACITY && self.meta_data[uindex].counter == h.counter
    }

    pub fn get(&self, h: Handle) -> Option<&'_ T> {
        if !self.is_valid_handle(h) {
            return None;
        }
        // Safe since we verified that it exists and is alive
        unsafe { Some(&*(self.data[h.index as usize].as_ptr())) }
    }

    pub fn get_mut(&mut self, h: Handle) -> Option<&'_ mut T> {
        if !self.is_valid_handle(h) {
            return None;
        }
        // Safe since we verified that it exists and is alive
        unsafe { Some(&mut *(self.data[h.index as usize].as_mut_ptr())) }
    }

    pub fn contains_handle(&self, h: Handle) -> bool {
        self.is_valid_handle(h)
    }

    pub fn remove(&mut self, handle: H) -> Option<T> {
        let h: Handle = handle.into();
        if !self.is_valid_handle(h) {
            return None;
        }
        Some(self.remove_internal(h.index as usize))
    }

    pub fn retain<F>(&mut self, mut f: F)
    where
        F: FnMut(&T) -> bool,
    {
        for i in 0..CAPACITY {
            if self.meta_data[i].counter.is_alive() {
                // This is safe because we know that data is alive
                let value = unsafe { &*self.data[i].as_ptr() };
                if !f(value) {
                    self.remove_internal(i);
                }
            }
        }
    }

    fn insert_internal(&mut self, value: T) -> Result<Handle, T> {
        debug_assert!(self.next_free_index <= CAPACITY);
        if self.next_free_index >= CAPACITY {
            return Err(value);
        }
        let next_free = self.next_free_index;
        debug_assert!(next_free < CAPACITY);
        let meta_data = &mut self.meta_data[next_free];
        debug_assert!(!meta_data.counter.is_alive());
        // This is safe since we check whether this data has been initialized before hand
        unsafe { std::ptr::write(self.data[next_free].as_mut_ptr(), value) }
        self.next_free_index = meta_data.next_free_index as usize;
        meta_data.counter.set_alive(true);
        let handle = Handle {
            counter: meta_data.counter,
            index: next_free as DataType,
        };
        self.count += 1;
        Ok(handle)
    }

    pub fn iter(&self) -> ArrayHandleMapIter<'_, H, T, CAPACITY> {
        ArrayHandleMapIter {
            map: self,
            index: 0,
        }
    }

    pub fn iter_mut(&mut self) -> ArrayHandleMapIterMut<'_, H, T, CAPACITY> {
        ArrayHandleMapIterMut {
            map: self,
            index: 0,
        }
    }

    #[cfg(debug_assertions)]
    #[inline(always)]
    fn zero_data(data: &mut MaybeUninit<T>) {
        *data = MaybeUninit::<T>::zeroed();
    }

    #[cfg(not(debug_assertions))]
    #[inline(always)]
    fn zero_data(_data: &mut MaybeUninit<T>) {}

    fn remove_internal(&mut self, index: usize) -> T {
        let mut meta_data = &mut self.meta_data[index];
        let data = &mut self.data[index];
        debug_assert!(meta_data.counter.is_alive());
        meta_data.counter.increment_and_set_dead();
        debug_assert!(!meta_data.counter.is_alive());
        meta_data.next_free_index = self.next_free_index as DataType;
        self.next_free_index = index;
        self.count -= 1;
        // This is safe to do since we know this value is alive
        let val = unsafe { std::ptr::read(data.as_ptr()) };
        Self::zero_data(data);
        val
    }
}

impl<H, T, const CAPACITY: usize> Drop for ArrayHandleMap<H, T, CAPACITY>
where
    H: HandleTrait,
{
    fn drop(&mut self) {
        self.on_drop();
    }
}

impl<H, T, const CAPACITY: usize> Default for ArrayHandleMap<H, T, CAPACITY>
where
    H: HandleTrait,
{
    fn default() -> Self {
        Self::new()
    }
}

#[derive(Debug)]
pub struct ArrayHandleMapIter<'a, H, T, const CAPACITY: usize>
where
    H: HandleTrait,
{
    map: &'a ArrayHandleMap<H, T, CAPACITY>,
    index: usize,
}

impl<'a, H, T, const CAPACITY: usize> Iterator for ArrayHandleMapIter<'a, H, T, CAPACITY>
where
    H: HandleTrait,
{
    type Item = &'a T;
    fn next(&mut self) -> Option<Self::Item> {
        while self.index < CAPACITY && !self.map.meta_data[self.index].counter.is_alive() {
            self.index += 1;
        }
        if self.index >= CAPACITY {
            return None;
        }
        let lookup_index = self.index;
        self.index += 1;
        Some(unsafe { &*self.map.data[lookup_index].as_ptr() })
    }
}

#[derive(Debug)]
pub struct ArrayHandleMapIterMut<'a, H, T, const CAPACITY: usize>
where
    H: HandleTrait,
{
    map: &'a mut ArrayHandleMap<H, T, CAPACITY>,
    index: usize,
}

impl<'a, H, T, const CAPACITY: usize> Iterator for ArrayHandleMapIterMut<'a, H, T, CAPACITY>
where
    H: HandleTrait,
{
    type Item = &'a mut T;
    fn next(&mut self) -> Option<Self::Item> {
        while self.index < CAPACITY && !self.map.meta_data[self.index].counter.is_alive() {
            self.index += 1;
        }
        if self.index >= CAPACITY {
            return None;
        }
        let lookup_index = self.index;
        self.index += 1;
        Some(unsafe { &mut *self.map.data[lookup_index].as_mut_ptr() })
    }
}

impl<'a, H, T, const CAPACITY: usize> IntoIterator for &'a ArrayHandleMap<H, T, CAPACITY>
where
    H: HandleTrait,
{
    type Item = &'a T;
    type IntoIter = ArrayHandleMapIter<'a, H, T, CAPACITY>;
    fn into_iter(self) -> Self::IntoIter {
        self.iter()
    }
}

impl<'a, H, T, const CAPACITY: usize> IntoIterator for &'a mut ArrayHandleMap<H, T, CAPACITY>
where
    H: HandleTrait,
{
    type Item = &'a mut T;
    type IntoIter = ArrayHandleMapIterMut<'a, H, T, CAPACITY>;
    fn into_iter(self) -> Self::IntoIter {
        self.iter_mut()
    }
}

#[derive(Debug, Copy, Clone)]
struct MetaDataDynamic<T> {
    next_free_index: DataType,
    counter: MaskedCounter,
    ptr: *mut T,
}

impl<T> Default for MetaDataDynamic<T> {
    fn default() -> Self {
        MetaDataDynamic {
            next_free_index: DataType::MAX,
            counter: MaskedCounter::new(),
            ptr: null_mut(),
        }
    }
}

unsafe impl<T: Send> Send for MetaDataDynamic<T> {}
unsafe impl<T: Sync> Sync for MetaDataDynamic<T> {}

#[derive(Debug)]
pub struct HandleMap<H, T>
where
    H: HandleTrait,
{
    meta_data: Vec<MetaDataDynamic<T>>,
    allocator: PoolAllocator<T>,
    count: usize,
    next_free_index: usize,
    p: PhantomData<H>,
}

impl<T, H> HandleMap<H, T>
where
    H: HandleTrait,
{
    pub fn new(items_per_block: usize) -> Self {
        Self {
            meta_data: Vec::with_capacity(items_per_block),
            allocator: PoolAllocator::new(items_per_block),
            count: 0,
            next_free_index: 0,
            p: PhantomData,
        }
    }

    pub fn reserve(&mut self, size: usize) {
        assert!(self.expand(size), "Failed to reserve storage");
    }

    fn expand(&mut self, requested_size: usize) -> bool {
        if self.meta_data.len() == MAX_ELEMENT_COUNT as usize
            || requested_size >= MAX_ELEMENT_COUNT as usize
        {
            return false;
        }
        let next_size = if requested_size == 0 {
            self.meta_data.capacity()
        } else {
            requested_size
        };

        debug_assert!(next_size > self.meta_data.len());
        debug_assert!(self.next_free_index == self.meta_data.len());

        let current_size = self.meta_data.len();
        self.meta_data
            .resize_with(next_size, MetaDataDynamic::default);

        for i in current_size..self.meta_data.len() {
            let data = &mut self.meta_data[i];
            data.next_free_index = (i + 1) as DataType;
        }
        let last_index = self.meta_data.len() - 1;
        self.meta_data[last_index].next_free_index =
            if self.next_free_index as usize == current_size {
                self.meta_data.len() as DataType
            } else {
                self.next_free_index as DataType
            };
        self.next_free_index = current_size;
        true
    }

    fn on_drop(&mut self) {
        for meta_data in &mut self.meta_data {
            if meta_data.counter.is_alive() {
                debug_assert!(!meta_data.ptr.is_null());
                unsafe {
                    self.allocator
                        .drop_and_free(NonNull::new_unchecked(meta_data.ptr))
                }
            }
        }
    }

    pub fn clear(&mut self) {
        for i in 0..self.meta_data.len() {
            let meta_data = &mut self.meta_data[i];
            if meta_data.counter.is_alive() {
                debug_assert!(!meta_data.ptr.is_null());
                unsafe {
                    self.allocator
                        .drop_and_free(NonNull::new_unchecked(meta_data.ptr))
                }
            }
            meta_data.counter.increment_and_set_dead();
            meta_data.next_free_index = (i + 1) as DataType;
        }
        self.count = 0;
        self.next_free_index = 0;
    }

    pub fn len(&self) -> usize {
        self.count
    }

    pub fn is_empty(&self) -> bool {
        self.count == 0
    }

    pub fn insert(&mut self, value: T) -> Result<H, T> {
        let handle = self.insert_internal(value)?;
        Ok(handle.into())
    }

    pub fn insert_with<F: FnOnce(H, &mut T)>(&mut self, value: T, f: F) -> Result<H, T> {
        let handle = self.insert_internal(value)?;
        let index = handle.index as usize;
        debug_assert!(index < self.meta_data.len());
        // This is safe since we check whether this data has been initialized before hand
        f(handle.into(), unsafe { &mut *self.meta_data[index].ptr });
        Ok(handle.into())
    }

    #[inline(always)]
    fn is_valid_handle(&self, h: Handle) -> bool {
        let uindex = h.index as usize;
        uindex < self.meta_data.len() && self.meta_data[uindex].counter == h.counter
    }

    pub fn get(&self, handle: H) -> Option<&'_ T> {
        let h = handle.into();
        if !self.is_valid_handle(h) {
            return None;
        }
        debug_assert!(!self.meta_data[h.index as usize].ptr.is_null());
        // Safe since we verified that it exists and is alive
        unsafe { Some(&*(self.meta_data[h.index as usize].ptr)) }
    }

    pub fn get_mut(&mut self, handle: H) -> Option<&'_ mut T> {
        let h = handle.into();
        if !self.is_valid_handle(h) {
            return None;
        }
        debug_assert!(!self.meta_data[h.index as usize].ptr.is_null());
        // Safe since we verified that it exists and is alive
        unsafe { Some(&mut *(self.meta_data[h.index as usize].ptr)) }
    }

    pub fn contains_handle(&self, handle: H) -> bool {
        self.is_valid_handle(handle.into())
    }

    pub fn remove(&mut self, handle: H) -> Option<T> {
        let h = handle.into();
        if !self.is_valid_handle(h) {
            return None;
        }
        Some(self.remove_internal(h.index as usize))
    }

    pub fn retain<F>(&mut self, mut f: F)
    where
        F: FnMut(&T) -> bool,
    {
        for i in 0..self.meta_data.len() {
            let meta_data = &mut self.meta_data[i];
            if meta_data.counter.is_alive() {
                // This is safe because we know that data is alive
                let value = unsafe { &*meta_data.ptr };
                if !f(value) {
                    self.remove_internal(i);
                }
            }
        }
    }

    fn insert_internal(&mut self, value: T) -> Result<Handle, T> {
        if self.next_free_index >= self.meta_data.len() && !self.expand(self.meta_data.len() * 2) {
            return Err(value);
        }

        let memory = unsafe { self.allocator.try_allocate_with(value) }?.as_ptr();

        let next_free = self.next_free_index;
        let mut meta_data = &mut self.meta_data[next_free];
        debug_assert!(!meta_data.counter.is_alive());
        self.next_free_index = meta_data.next_free_index as usize;
        meta_data.ptr = memory;
        meta_data.counter.set_alive(true);
        let handle = Handle {
            counter: meta_data.counter,
            index: next_free as DataType,
        };
        self.count += 1;
        Ok(handle)
    }

    pub fn iter(&self) -> HandleMapIter<'_, H, T> {
        HandleMapIter {
            map: self,
            index: 0,
        }
    }

    pub fn iter_mut(&mut self) -> HandleMapIterMut<'_, H, T> {
        HandleMapIterMut {
            map: self,
            index: 0,
        }
    }

    fn remove_internal(&mut self, index: usize) -> T {
        let mut meta_data = &mut self.meta_data[index];
        debug_assert!(meta_data.counter.is_alive());
        meta_data.counter.increment_and_set_dead();
        debug_assert!(!meta_data.counter.is_alive());
        debug_assert!(!meta_data.ptr.is_null());
        meta_data.next_free_index = self.next_free_index as DataType;
        self.next_free_index = index;
        self.count -= 1;
        // This is safe to do since we know this value is alive
        let val = unsafe { std::ptr::read(meta_data.ptr) };
        unsafe {
            self.allocator.free(NonNull::new_unchecked(meta_data.ptr));
        }
        val
    }
}

impl<H, T> Drop for HandleMap<H, T>
where
    H: HandleTrait,
{
    fn drop(&mut self) {
        self.on_drop();
    }
}

#[derive(Debug)]
pub struct HandleMapIter<'a, H, T>
where
    H: HandleTrait,
{
    map: &'a HandleMap<H, T>,
    index: usize,
}

impl<'a, H, T> Iterator for HandleMapIter<'a, H, T>
where
    H: HandleTrait,
{
    type Item = &'a T;
    fn next(&mut self) -> Option<Self::Item> {
        while self.index < self.map.meta_data.len()
            && !self.map.meta_data[self.index].counter.is_alive()
        {
            self.index += 1;
        }
        if self.index >= self.map.meta_data.len() {
            return None;
        }
        let lookup_index = self.index;
        self.index += 1;
        Some(unsafe { &*self.map.meta_data[lookup_index].ptr })
    }
}

#[derive(Debug)]
pub struct HandleMapIterMut<'a, H, T>
where
    H: HandleTrait,
{
    map: &'a mut HandleMap<H, T>,
    index: usize,
}

impl<'a, H, T> Iterator for HandleMapIterMut<'a, H, T>
where
    H: HandleTrait,
{
    type Item = &'a mut T;
    fn next(&mut self) -> Option<Self::Item> {
        while self.index < self.map.meta_data.len()
            && !self.map.meta_data[self.index].counter.is_alive()
        {
            self.index += 1;
        }
        if self.index >= self.map.meta_data.len() {
            return None;
        }
        let lookup_index = self.index;
        self.index += 1;
        Some(unsafe { &mut *self.map.meta_data[lookup_index].ptr })
    }
}

impl<'a, H, T> IntoIterator for &'a HandleMap<H, T>
where
    H: HandleTrait,
{
    type Item = &'a T;
    type IntoIter = HandleMapIter<'a, H, T>;
    fn into_iter(self) -> Self::IntoIter {
        self.iter()
    }
}

impl<'a, H, T> IntoIterator for &'a mut HandleMap<H, T>
where
    H: HandleTrait,
{
    type Item = &'a mut T;
    type IntoIter = HandleMapIterMut<'a, H, T>;
    fn into_iter(self) -> Self::IntoIter {
        self.iter_mut()
    }
}

unsafe impl<T: Send, H: HandleTrait> Send for HandleMap<H, T> {}

#[derive(Debug)]
pub struct SharedData<T> {
    rc: i32,
    data: T,
}

impl<T> SharedData<T> {
    pub fn new(data: T) -> Self {
        Self { rc: 1, data }
    }

    pub fn release(self) -> T {
        self.data
    }
}

impl<T> Deref for SharedData<T> {
    type Target = T;
    fn deref(&self) -> &Self::Target {
        &self.data
    }
}

impl<T> DerefMut for SharedData<T> {
    fn deref_mut(&mut self) -> &mut Self::Target {
        &mut self.data
    }
}

/// Similar to HandleMap but it uses an intrusive reference count to ensure that items are only
/// removed when the reference count reaches 0. Use [acquire] to increase the reference count and
/// [release] to reduce the reference count.
#[derive(Debug)]
pub struct SharedHandleMap<H, T>
where
    H: HandleTrait,
{
    map: HandleMap<H, SharedData<T>>,
}

impl<H, T> SharedHandleMap<H, T>
where
    H: HandleTrait,
{
    pub fn new(items_per_block: usize) -> Self {
        Self {
            map: HandleMap::new(items_per_block),
        }
    }

    pub fn reserve(&mut self, size: usize) {
        self.map.reserve(size)
    }

    pub fn clear(&mut self) {
        self.map.clear();
    }

    pub fn len(&self) -> usize {
        self.map.count
    }

    pub fn is_empty(&self) -> bool {
        self.map.is_empty()
    }

    pub fn insert(&mut self, value: T) -> Result<H, SharedData<T>> {
        self.map.insert(SharedData::new(value))
    }

    pub fn insert_with<F: FnOnce(H, &mut T)>(
        &mut self,
        value: T,
        f: F,
    ) -> Result<H, SharedData<T>> {
        self.map
            .insert_with(SharedData::new(value), move |h, d| f(h, &mut d.data))
    }

    pub fn get(&self, handle: H) -> Option<&'_ SharedData<T>> {
        self.map.get(handle)
    }

    pub fn get_mut(&mut self, handle: H) -> Option<&'_ mut SharedData<T>> {
        self.map.get_mut(handle)
    }

    pub fn contains_handle(&self, handle: H) -> bool {
        self.map.contains_handle(handle)
    }

    pub fn acquire(&mut self, handle: H) {
        if let Some(d) = self.map.get_mut(handle) {
            d.rc += 1;
        }
    }

    pub fn get_and_acquire(&mut self, handle: H) -> Option<&'_ T> {
        self.map.get_mut(handle).map(|d| {
            d.rc += 1;
            &d.data
        })
    }

    pub fn get_mut_and_acquire(&mut self, handle: H) -> Option<&'_ mut T> {
        self.map.get_mut(handle).map(|d| {
            d.rc += 1;
            &mut d.data
        })
    }

    pub fn release(&mut self, handle: H) -> Option<T> {
        let should_remove = if let Some(d) = self.map.get_mut(handle) {
            d.rc -= 1;
            d.rc <= 0
        } else {
            false
        };
        if should_remove {
            self.map.remove(handle).map(|d| d.data)
        } else {
            None
        }
    }

    pub fn retain<F>(&mut self, mut f: F)
    where
        F: FnMut(&T) -> bool,
    {
        self.map.retain(move |v| f(&v.data))
    }

    pub fn iter(&self) -> HandleMapIter<'_, H, SharedData<T>> {
        HandleMapIter {
            map: &self.map,
            index: 0,
        }
    }

    pub fn iter_mut(&mut self) -> HandleMapIterMut<'_, H, SharedData<T>> {
        HandleMapIterMut {
            map: &mut self.map,
            index: 0,
        }
    }
}

impl<'a, H, T> IntoIterator for &'a SharedHandleMap<H, T>
where
    H: HandleTrait,
{
    type Item = &'a SharedData<T>;
    type IntoIter = HandleMapIter<'a, H, SharedData<T>>;
    fn into_iter(self) -> Self::IntoIter {
        self.iter()
    }
}

impl<'a, H, T> IntoIterator for &'a mut SharedHandleMap<H, T>
where
    H: HandleTrait,
{
    type Item = &'a mut SharedData<T>;
    type IntoIter = HandleMapIterMut<'a, H, SharedData<T>>;
    fn into_iter(self) -> Self::IntoIter {
        self.iter_mut()
    }
}

unsafe impl<T: Send, H: HandleTrait> Send for SharedHandleMap<H, T> {}
