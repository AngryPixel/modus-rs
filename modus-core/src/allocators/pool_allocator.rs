use crate::sync::intrusiverc::{Allocator, IntrusiveArc, RefCounted};
use crate::sync::Mutex;
use crate::sync::ThreadId;
use std::marker::PhantomData;
use std::ops::{Deref, DerefMut};
use std::ptr::{null_mut, NonNull};
use std::sync::atomic::{AtomicI32, AtomicI64};

#[derive(Debug)]
struct FreeListNode {
    next: *mut FreeListNode,
}
unsafe impl Send for FreeListNode {}
unsafe impl Sync for FreeListNode {}

#[derive(Debug)]
//Blocks are made up of : [header:BlockListNode][Item0][Item1]
struct BlockListNode {
    next: *mut BlockListNode,
}

unsafe impl Send for BlockListNode {}
unsafe impl Sync for BlockListNode {}

#[derive(Debug)]
struct PoolAllocatorBase {
    free_list: *mut FreeListNode,
    blocks: *mut BlockListNode,
    items_per_block: usize,
    type_size: usize,
    block_size: usize,
    type_alignment: usize,
}

impl Drop for PoolAllocatorBase {
    fn drop(&mut self) {
        unsafe {
            let alloc_layout =
                std::alloc::Layout::from_size_align(self.block_size, self.type_alignment).unwrap();
            while !self.blocks.is_null() {
                let next = (*self.blocks).next;
                std::alloc::dealloc(self.blocks as *mut u8, alloc_layout);
                self.blocks = next;
            }
        }
    }
}

unsafe impl Send for PoolAllocatorBase {}

impl PoolAllocatorBase {
    fn block_header_size_with_alignment(alignment: usize) -> usize {
        std::mem::size_of::<BlockListNode>()
            + (alignment - (std::mem::size_of::<BlockListNode>() % alignment))
    }

    fn new(type_size: usize, items_per_block: usize, type_alignment: usize) -> PoolAllocatorBase {
        debug_assert!(type_size > 0);
        debug_assert!(items_per_block > 0);
        let type_size_with_alignment = std::mem::size_of::<FreeListNode>()
            .max(type_size + (type_alignment - (type_size % type_alignment)));
        PoolAllocatorBase {
            free_list: null_mut(),
            blocks: null_mut(),
            items_per_block,
            type_size: type_size_with_alignment,
            block_size: Self::block_header_size_with_alignment(type_alignment)
                + (type_size_with_alignment * items_per_block),
            type_alignment,
        }
    }

    /// Allocate a new memory block to accommodate an item. Returns null on allocation failure.
    /// # Safety
    /// The returned pointer is only valid while the instance of this type is alive.
    unsafe fn allocate(&mut self) -> *mut u8 {
        if self.free_list.is_null() {
            self.free_list = self.allocate_block();
        }
        if self.free_list.is_null() {
            return null_mut();
        }
        let node = self.free_list;
        self.free_list = (*node).next;
        node as *mut u8
    }

    /// Free a previously allocate memory slot.
    /// # Safety
    /// This function is only safe to call with pointers originating from the [allocate:allocate]
    /// function.
    unsafe fn free(&mut self, ptr: *mut u8) {
        let mut free_node = ptr as *mut FreeListNode;
        (*free_node).next = self.free_list;
        self.free_list = free_node;
    }

    unsafe fn allocate_block(&mut self) -> *mut FreeListNode {
        // Allocate memory
        let alloc_layout =
            match std::alloc::Layout::from_size_align(self.block_size, self.type_alignment) {
                Err(_) => {
                    return null_mut();
                }
                Ok(l) => l,
            };

        let ptr = std::alloc::alloc(alloc_layout);
        if ptr.is_null() {
            return null_mut();
        }

        // Initialize nodes in block
        let mut block = ptr as *mut BlockListNode;
        (*block).next = null_mut();

        // Update Free list node pointers inside block
        let start_node = (block as *mut u8)
            .add(Self::block_header_size_with_alignment(self.type_alignment))
            as *mut FreeListNode;
        let mut node = start_node;
        debug_assert!(node as usize % self.type_alignment == 0);
        for _ in 0..self.items_per_block - 1 {
            (*node).next = (node as *mut u8).add(self.type_size) as *mut FreeListNode;
            node = (*node).next;
        }
        (*node).next = null_mut();

        // Update block list
        if !self.blocks.is_null() {
            (*block).next = self.blocks;
        }
        self.blocks = block;

        start_node
    }
}

/// Pooled allocator . Allocate memory blocks able to store up to N items. This allocator is
/// intended to be used by other primitives which can provide a safe lifetime abstraction.
#[derive(Debug)]
pub struct PoolAllocator<T> {
    allocator: PoolAllocatorBase,
    p: PhantomData<T>,
}

impl<T> PoolAllocator<T> {
    pub fn new(items_per_block: usize) -> Self {
        Self {
            allocator: PoolAllocatorBase::new(
                std::mem::size_of::<T>(),
                items_per_block,
                std::mem::align_of::<T>(),
            ),
            p: PhantomData::default(),
        }
    }

    /// Construct a new pointer to T with the given value
    /// # Safety
    /// The returned pointer is only valid for the duration of the lifetime of this allocator.
    /// # Panic
    /// This function panics on allocation failure
    pub unsafe fn allocate_with(&mut self, value: T) -> NonNull<T> {
        let ptr = self.allocator.allocate() as *mut T;
        assert!(!ptr.is_null(), "Failed to allocate memory");
        std::ptr::write(ptr, value);
        NonNull::new_unchecked(ptr)
    }

    /// Construct a new pointer capable of holding an element of size T
    /// # Safety
    /// The returned pointer is only valid for the duration of the lifetime of this allocator.
    /// # Panic
    /// This function panics on allocation failure
    pub unsafe fn allocate(&mut self) -> NonNull<T> {
        let ptr = self.allocator.allocate() as *mut T;
        assert!(!ptr.is_null(), "Failed to allocate memory");
        NonNull::new_unchecked(ptr)
    }

    /// Construct a new pointer capable of holding an element of size T
    /// # Safety
    /// The returned pointer is only valid for the duration of the lifetime of this allocator.
    pub unsafe fn try_allocate(&mut self) -> Option<NonNull<T>> {
        let ptr = self.allocator.allocate() as *mut T;
        if ptr.is_null() {
            return None;
        }
        Some(NonNull::new_unchecked(ptr))
    }

    /// Construct a new pointer to T with the given value
    /// # Safety
    /// The returned pointer is only valid for the duration of the lifetime of this allocator.
    pub unsafe fn try_allocate_with(&mut self, value: T) -> Result<NonNull<T>, T> {
        let ptr = self.allocator.allocate() as *mut T;
        if ptr.is_null() {
            return Err(value);
        }
        std::ptr::write(ptr, value);
        Ok(NonNull::new_unchecked(ptr))
    }

    /// Free the memory associated with a pointer allocated from this pool.
    /// # Safety
    /// The supplied pointer must have been allocate from this allocator
    pub unsafe fn free(&mut self, value: NonNull<T>) {
        self.allocator.free(value.as_ptr() as *mut u8);
    }

    /// Drop the value and free the pointer allocated from this pool.
    /// # Safety
    /// The supplied pointer must have been allocate from this allocator
    pub unsafe fn drop_and_free(&mut self, value: NonNull<T>) {
        value.as_ptr().drop_in_place();
        self.allocator.free(value.as_ptr() as *mut u8);
    }
}

unsafe impl<T: Send> Send for PoolAllocator<T> {}
unsafe impl<T: Sync> Sync for PoolAllocator<T> {}

#[cfg(test)]
mod tests {
    use super::*;
    struct Foo {
        y: u32,
    }

    #[test]
    fn test_block_allocation() {
        let mut allocator = PoolAllocator::<Foo>::new(4);
        const NUM_ALLOCATIONS: usize = 32;
        let mut ptrs = vec![];
        for i in 0..NUM_ALLOCATIONS {
            unsafe {
                let r = allocator.try_allocate_with(Foo { y: i as u32 });
                assert!(r.is_ok());
                let v = r.unwrap_or_else(|_| {
                    panic!("Not valid");
                });
                assert_eq!(v.as_ref().y, i as u32);
                ptrs.push(v);
            }
        }

        for ptr in ptrs {
            unsafe {
                allocator.free(ptr);
            }
        }
    }

    #[repr(align(32))]
    struct Bar {
        y: u32,
    }

    #[test]
    fn test_aligned_allocation() {
        let mut allocator = PoolAllocator::<Bar>::new(4);
        const NUM_ALLOCATIONS: usize = 32;
        let mut ptrs = vec![];
        for i in 0..NUM_ALLOCATIONS {
            unsafe {
                let r = allocator.try_allocate_with(Bar { y: i as u32 });
                assert!(r.is_ok());
                let v = r.unwrap_or_else(|_| panic!("Not valid"));
                assert_eq!(v.as_ref().y, i as u32);
                assert_eq!(v.as_ptr() as usize % 32, 0);
                ptrs.push(v);
            }
        }

        for ptr in ptrs {
            unsafe {
                allocator.free(ptr);
            }
        }
    }

    #[test]
    fn test_type_smaller_than_usize() {
        let mut allocator = PoolAllocator::<u8>::new(4);
        const NUM_ALLOCATIONS: usize = 32;
        let mut ptrs = vec![];
        for i in 0..NUM_ALLOCATIONS {
            unsafe {
                let r = allocator.try_allocate_with(i as u8);
                assert!(r.is_ok());
                let v = r.unwrap_or_else(|_| panic!("Not valid"));
                assert_eq!(*v.as_ref(), i as u8);
                ptrs.push(v);
            }
        }

        for ptr in ptrs {
            unsafe {
                allocator.free(ptr);
            }
        }
    }
}

// --- Global Pool Allocator --------------------------------------------------------------------

#[derive(Debug)]
struct PageData<T> {
    allocator: NonNull<TLSPoolAllocator<T>>,
    next_page: Option<NonNull<Page<T>>>,
    counter: AtomicI32,
}

const DEFAULT_PAGE_SIZE: usize = 1024;
const PAGE_SIZE: usize = DEFAULT_PAGE_SIZE - std::mem::size_of::<PageData<()>>();

/// Memory Page used by the allocator, it's only freed from memory when all the nodes
/// have been freed.
#[derive(Debug)]
#[repr(align(1024))]
struct Page<T> {
    page_data: PageData<T>,
    blocks: [u8; PAGE_SIZE],
    _p: PhantomData<T>,
}

union Node<T> {
    next_node: Option<NonNull<Node<T>>>,
    data: std::mem::ManuallyDrop<T>,
}

impl<T> Page<T> {
    const NODE_SIZE: usize = std::mem::size_of::<Node<T>>();
    const NUM_BLOCKS: usize = PAGE_SIZE / Self::NODE_SIZE;

    fn setup(&mut self, allocator: NonNull<TLSPoolAllocator<T>>) {
        self.page_data = PageData {
            allocator,
            next_page: None,
            counter: AtomicI32::new(1),
        };
    }

    unsafe fn first_block(&mut self) -> NonNull<Node<T>> {
        let block_ptr = self.blocks.as_mut_ptr();
        let alignment_offset = block_ptr.align_offset(std::mem::align_of::<T>());
        let block_ptr = block_ptr.add(alignment_offset);
        let node_ptr: *mut Node<T> = std::mem::transmute(block_ptr);
        debug_assert!(block_ptr as usize % std::mem::align_of::<T>() == 0);
        debug_assert!(
            ((*node_ptr).data.deref() as *const T as usize) % std::mem::align_of::<T>() == 0
        );
        NonNull::new_unchecked(node_ptr)
    }

    unsafe fn from_node(node: *mut u8) -> NonNull<Page<T>> {
        debug_assert!(DEFAULT_PAGE_SIZE.is_power_of_two());
        NonNull::new_unchecked(std::mem::transmute(
            node as usize & !(DEFAULT_PAGE_SIZE - 1),
        ))
    }

    unsafe fn alloc_page() -> Option<NonNull<Page<T>>> {
        let alloc_layout = std::alloc::Layout::new::<Page<T>>();
        let ptr = std::alloc::alloc(alloc_layout);
        debug_assert_eq!(
            ptr as usize & (DEFAULT_PAGE_SIZE - 1),
            0,
            "page is not aligned"
        );
        if ptr.is_null() {
            return None;
        }
        let page: *mut Page<T> = std::mem::transmute(ptr);
        Some(NonNull::new_unchecked(page))
    }

    unsafe fn acquire_page(mut page: NonNull<Page<T>>) {
        page.as_mut()
            .page_data
            .counter
            .fetch_add(1, std::sync::atomic::Ordering::AcqRel);
    }

    unsafe fn acquire_page_from_node(node: NonNull<Node<T>>) {
        let page = Self::from_node(node.as_ptr() as *mut u8);
        Self::acquire_page(page);
    }

    unsafe fn release_page(mut page: NonNull<Page<T>>) {
        let value = page
            .as_mut()
            .page_data
            .counter
            .fetch_sub(1, std::sync::atomic::Ordering::AcqRel);
        if value <= 1 {
            let alloc_layout = std::alloc::Layout::new::<Page<T>>();
            std::alloc::dealloc(page.as_ptr() as *mut u8, alloc_layout);
        }
    }
}

#[derive(Debug)]
struct NodeStack<T> {
    stack: Option<NonNull<Node<T>>>,
}

impl<T> NodeStack<T> {
    fn new() -> Self {
        Self { stack: None }
    }
    fn push(&mut self, mut node: NonNull<Node<T>>) {
        unsafe {
            node.as_mut().next_node = self.stack;
        }
        self.stack = Some(node);
    }

    fn pop(&mut self) -> Option<NonNull<Node<T>>> {
        match self.stack {
            None => None,
            Some(mut node) => unsafe {
                let next_node = node.as_mut().next_node;
                self.stack = next_node;
                Some(node)
            },
        }
    }

    fn clear(&mut self) {
        self.stack = None;
    }
}

/// This version of a Pool allocator is meant to be used as a thread local global allocator. It allocates
/// content on thread local memory blocks to avoid cross thread contention. To handle data
/// freed on different threads we push those pointers onto a separate stack guarded by a mutex.
/// # Usage
/// It's recommended to use the `declare_global_pool_allocator` macro to instantiate the correct
/// setup for each type.
/// # Safety
/// You must ensure all the allocations for a given thread scope are freed before the
/// allocator goes out of scope or it will lead to undefined behavior.
#[derive(Debug)]
pub struct TLSPoolAllocator<T> {
    thread_id: ThreadId,
    pages: Option<NonNull<Page<T>>>,
    free_nodes: NodeStack<T>,
    alloc_count: AtomicI64,
    thread_free_nodes: Mutex<NodeStack<T>>,
}

impl<T> TLSPoolAllocator<T> {
    pub fn new() -> Self {
        Self {
            thread_id: ThreadId::this_thread(),
            pages: None,
            free_nodes: NodeStack::new(),
            alloc_count: Default::default(),
            thread_free_nodes: Mutex::new(NodeStack::new()),
        }
    }

    fn grow(&mut self) -> bool {
        unsafe {
            if let Some(page) = Page::alloc_page() {
                self.init_page(page);
                return true;
            }
        }
        false
    }

    unsafe fn init_page(&mut self, mut page: NonNull<Page<T>>) {
        page.as_mut().setup(NonNull::new_unchecked(self));
        let mut next_node = page.as_mut().first_block();
        self.free_nodes.push(next_node);
        for _ in 0..Page::<T>::NUM_BLOCKS - 1 {
            next_node = NonNull::new_unchecked(next_node.as_ptr().add(1));
            self.free_nodes.push(next_node);
        }
        page.as_mut().page_data.next_page = self.pages;
        self.pages = Some(page);
    }

    fn free_pages(&mut self) {
        unsafe {
            while let Some(mut ptr) = self.pages {
                let next_page = ptr.as_mut().page_data.next_page;
                Page::release_page(ptr);
                self.pages = next_page;
            }
        }
        self.free_nodes.clear();
    }

    unsafe fn reclaim_memory(&mut self, ptr: NonNull<T>) {
        assert_eq!(self.thread_id, ThreadId::this_thread());
        let page = Page::<T>::from_node(ptr.as_ptr() as *mut u8);
        Page::release_page(page);
        let node = NonNull::new_unchecked(std::mem::transmute(ptr.as_ptr()));
        self.free_nodes.push(node);
    }

    unsafe fn reclaim_memory_to_thread(&self, ptr: NonNull<T>) {
        assert_ne!(self.thread_id, ThreadId::this_thread());
        let page = Page::<T>::from_node(ptr.as_ptr() as *mut u8);
        Page::release_page(page);
        let mut accessor = page
            .as_ref()
            .page_data
            .allocator
            .as_ref()
            .thread_free_nodes
            .lock();
        let node = NonNull::new_unchecked(std::mem::transmute(ptr.as_ptr()));
        accessor.push(node);
    }

    unsafe fn release(&mut self, ptr: NonNull<T>) {
        self.alloc_count
            .fetch_sub(1, std::sync::atomic::Ordering::AcqRel);
        if self.thread_id != ThreadId::this_thread() {
            self.reclaim_memory_to_thread(ptr);
        } else {
            self.reclaim_memory(ptr);
        }
    }

    unsafe fn acquire(&mut self) -> NonNull<T> {
        self.try_acquire().expect("Failed to allocate, OOM")
    }

    #[inline(always)]
    unsafe fn prepare_new_node(&self, node: NonNull<Node<T>>) -> NonNull<T> {
        self.alloc_count
            .fetch_add(1, std::sync::atomic::Ordering::AcqRel);
        Page::acquire_page_from_node(node);
        std::mem::transmute(node)
    }

    unsafe fn try_acquire(&mut self) -> Option<NonNull<T>> {
        assert_eq!(self.thread_id, ThreadId::this_thread());
        // try to allocate from local list
        if let Some(node) = self.free_nodes.pop() {
            return Some(self.prepare_new_node(node));
        }

        // check nodes released by other threads
        {
            let mut accessor = self.thread_free_nodes.lock();
            if let Some(node) = accessor.pop() {
                return Some(self.prepare_new_node(node));
            }
        }

        // Still no reusable nodes, time to grow
        if !self.grow() {
            return None;
        }

        // This is safe since we have succeeded in allocating memory with grow()
        let node = self.free_nodes.pop().unwrap();
        Some(self.prepare_new_node(node))
    }
}

#[derive(Debug)]
pub struct Box<T> {
    ptr: NonNull<T>,
}

impl<T> Box<T> {
    fn new(ptr: NonNull<T>) -> Self {
        Self { ptr }
    }
}

unsafe impl<T: Send> Send for Box<T> {}

impl<T> Drop for Box<T> {
    fn drop(&mut self) {
        unsafe {
            std::ptr::drop_in_place(self.ptr.as_ptr());
            let mut page = Page::from_node(self.ptr.as_ptr() as *mut u8);
            page.as_mut().page_data.allocator.as_mut().release(self.ptr);
        }
    }
}

impl<T> Deref for Box<T> {
    type Target = T;
    fn deref(&self) -> &Self::Target {
        unsafe { self.ptr.as_ref() }
    }
}

impl<T> DerefMut for Box<T> {
    fn deref_mut(&mut self) -> &mut Self::Target {
        unsafe { self.ptr.as_mut() }
    }
}

impl<T> TLSPoolAllocator<T> {
    pub fn allocate(&mut self, value: T) -> Box<T> {
        unsafe {
            let ptr = self.acquire();
            std::ptr::write(ptr.as_ptr(), value);
            Box::new(ptr)
        }
    }
}

impl<T: RefCounted> TLSPoolAllocator<T> {
    pub fn allocate_instrusiverc<Alloc: Allocator>(&mut self, value: T) -> IntrusiveArc<T, Alloc> {
        unsafe {
            let ptr = self.acquire();
            std::ptr::write(ptr.as_ptr(), value);
            IntrusiveArc::from_ptr(ptr)
        }
    }
}

impl<T> Drop for TLSPoolAllocator<T> {
    fn drop(&mut self) {
        use std::sync::atomic::Ordering;
        assert_eq!(
            self.alloc_count.load(Ordering::Acquire),
            0,
            "Not allocation were released!, this will result in invalid memory writes!"
        );
        self.free_pages();
    }
}

impl<T> Default for TLSPoolAllocator<T> {
    fn default() -> Self {
        Self::new()
    }
}

/// Help function for global allocator
pub fn tls_pool_allocator_free_page(ptr: NonNull<u8>) {
    unsafe {
        let mut page = Page::from_node(ptr.as_ptr());
        page.as_mut().page_data.allocator.as_mut().release(ptr);
    }
}

/// Declare a new global pool allocator which can then be accessed by the name.
/// ```
/// use modus_core::declare_tls_pool_allocator;
/// declare_tls_pool_allocator!(pub(crate), TestGlobalPoolAllocator, TEST_GLOBAL_ALLOCATOR, u64);
/// let v = TestGlobalPoolAllocator::allocate(20);
/// ```
///
#[macro_export]
macro_rules! declare_tls_pool_allocator {
    ($vis:vis , $name:ident, $var_name:ident, $type:tt) => {
        thread_local! {
            static $var_name: std::cell::RefCell<$crate::allocators::pool_allocator::TLSPoolAllocator<$type>> = std::cell::RefCell::new($crate::allocators::pool_allocator::TLSPoolAllocator::new());
        }

        $vis struct $name {}
        impl $name {
            $vis fn allocate(value: $type) -> $crate::allocators::pool_allocator::Box<$type> {
                $var_name.with(|allocator| allocator.borrow_mut().allocate(value))
            }
        }
    };
}

/// Declare a new global pool allocator, for types with use intrusive reference counts, which can
/// then be accessed by the name.
/// ```
/// use modus_core::declare_tls_pool_allocator_intrusiverc;
/// type RCType = modus_core::sync::intrusiverc::IntrusiveRefCounted<u64>;
/// declare_tls_pool_allocator_intrusiverc!(pub(crate), TestGlobalPoolAllocator, TEST_GLOBAL_ALLOCATOR, RCType);
/// let v = TestGlobalPoolAllocator::allocate(RCType::new(20));
/// ```
///
#[macro_export]
macro_rules! declare_tls_pool_allocator_intrusiverc {
    ($vis:vis , $name:ident, $var_name:ident, $type:tt) => {
        thread_local! {
            static $var_name: std::cell::RefCell<$crate::allocators::pool_allocator::TLSPoolAllocator<$type>> = std::cell::RefCell::new($crate::allocators::pool_allocator::TLSPoolAllocator::new());
        }

        $vis struct $name {}

        impl $crate::sync::intrusiverc::Allocator for $name {
            fn allocate(_ : std::alloc::Layout) -> std::ptr::NonNull<u8> {
               panic!("Should never be triggered!")
            }

            fn deallocate(ptr:std::ptr::NonNull<u8>, _: std::alloc::Layout) {
                $crate::allocators::pool_allocator::tls_pool_allocator_free_page(ptr);
            }
        }

        impl $name {
            $vis fn allocate(value: $type) -> $crate::sync::intrusiverc::IntrusiveArc<$type, $name> {
                $var_name.with(|allocator| allocator.borrow_mut().allocate_instrusiverc::<$name>(value))
            }
        }
    };
}

#[cfg(test)]
mod global_pool_allocator_test {
    use crate::allocators::pool_allocator::Page;

    declare_tls_pool_allocator!(pub(crate), TestGlobalPoolAllocator, TEST_GLOBAL_ALLOCATOR, u64);

    #[test]
    fn same_thread_alloc_free() {
        let alloc_count = Page::<u64>::NUM_BLOCKS * 2;

        let mut allocation = Vec::with_capacity(alloc_count);

        for i in 0..alloc_count as u64 {
            allocation.push(TestGlobalPoolAllocator::allocate(i));
        }

        for (i, v) in allocation.iter().enumerate() {
            assert_eq!(i as u64, *(*v));
        }
    }

    #[test]
    fn different_thread_alloc_free() {
        let alloc_count = Page::<u64>::NUM_BLOCKS * 2;

        let mut allocation = Vec::with_capacity(alloc_count);

        for i in 0..alloc_count as u64 {
            allocation.push(TestGlobalPoolAllocator::allocate(i));
        }

        for (i, v) in allocation.iter().enumerate() {
            assert_eq!(i as u64, *(*v));
        }

        std::thread::spawn(move || {
            allocation.clear();
        })
        .join()
        .unwrap();

        let mut allocation = Vec::with_capacity(alloc_count);
        for i in 0..alloc_count as u64 {
            allocation.push(TestGlobalPoolAllocator::allocate(i));
        }
    }

    type RCType = crate::sync::intrusiverc::IntrusiveRefCounted<u64>;
    declare_tls_pool_allocator_intrusiverc!(pub(crate), TestGlobalPoolAllocatorRC, TEST_GLOBAL_ALLOCATOR_RC, RCType);

    #[test]
    fn instrusive_rc() {
        let _ptr = TestGlobalPoolAllocatorRC::allocate(RCType::new(1024));
    }
}
