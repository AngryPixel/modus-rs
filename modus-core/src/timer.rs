//! High Perf timer using operating system specific primitives

use std::time::Duration;

#[derive(Debug)]
pub struct Timer {
    counter: i64,
}

#[cfg(unix)]
unsafe fn get_elapsed_nanoseconds() -> i64 {
    let mut now = libc::timespec {
        tv_sec: 0,
        tv_nsec: 0,
    };
    libc::clock_gettime(libc::CLOCK_MONOTONIC, &mut now);
    ((now.tv_sec as i64) * 1000000000) + (now.tv_nsec as i64)
}

impl Default for Timer {
    fn default() -> Self {
        Self::new()
    }
}

impl Timer {
    pub fn new() -> Self {
        Self {
            counter: unsafe { get_elapsed_nanoseconds() },
        }
    }

    pub fn reset(&mut self) {
        self.counter = unsafe { get_elapsed_nanoseconds() };
    }

    pub fn elapsed(&self) -> Duration {
        let diff = unsafe { get_elapsed_nanoseconds() } - self.counter;
        Duration::from_nanos(diff as u64)
    }

    pub fn reset_and_elapsed(&mut self) -> Duration {
        let new_counter = unsafe { get_elapsed_nanoseconds() };
        let diff = new_counter - self.counter;
        self.counter = new_counter;
        Duration::from_nanos(diff as u64)
    }
}
