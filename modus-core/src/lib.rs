//!
//! Core types that can be used anywhere in the negine
//!
pub type ArrayVec<T, const CAP: usize> = arrayvec::ArrayVec<T, CAP>;
pub type ArrayString<const CAP: usize> = arrayvec::ArrayString<CAP>;
pub type DefaultHasher = ahash::AHasher;
pub type DefaultRandomState = ahash::RandomState;

pub type HashMap<K, V, R = DefaultRandomState> = ahash::AHashMap<K, V, R>;
pub type HashSet<V, R = DefaultRandomState> = ahash::AHashSet<V, R>;
pub type FixedBitSet = fixedbitset::FixedBitSet;

pub mod allocators;
pub mod fps_counter;
pub mod handle_map;
pub mod ppm;
pub mod sorting;
pub mod sync;
pub mod texture_atlas;
pub mod timer;
