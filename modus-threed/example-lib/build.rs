use std::path::Path;

fn compile_shaders() {
    let shaders = [
        "shaders/hello_triangle/shader.vert",
        "shaders/hello_triangle/shader.frag",
        "shaders/hello_quad/shader.vert",
        "shaders/hello_quad/shader.frag",
    ];

    for entry in &shaders {
        let p = Path::new(*entry);
        modus_threed::build_utils::compile_shader(p);
    }
}

fn main() {
    compile_shaders();
    println!("cargo:rerun-if-changed=build.rs");
}
