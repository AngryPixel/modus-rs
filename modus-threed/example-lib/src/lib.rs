//! Helper lib for modus-threed examples and test projects, since it requires modus-app to be setup

use modus_app::app::{App, AppEventHandler, AppInitInfo, TickResult};
use modus_app::create_app;
use modus_app::events::WindowId;
use modus_app::window::{FullscreenMode, WindowCreateInfo};
use modus_core::fps_counter::FpsCounter;
use modus_core::timer::Timer;
use modus_log::*;
use modus_threed::device::{Device, DeviceError};
use modus_threed::render_graph::RenderGraphError;
use modus_threed::shader::ShaderCreateInfo;
use modus_threed::types::ShaderHandle;
use std::fmt::Formatter;
use std::io::Read;
use std::path::Path;
use std::sync::Arc;
use std::time::Duration;

#[derive(Debug)]
pub enum ExampleError {
    App(String),
    Device(DeviceError),
    RenderGraph(RenderGraphError),
    Misc(String),
}

impl From<DeviceError> for ExampleError {
    fn from(de: DeviceError) -> Self {
        ExampleError::Device(de)
    }
}

impl From<RenderGraphError> for ExampleError {
    fn from(re: RenderGraphError) -> Self {
        ExampleError::RenderGraph(re)
    }
}

impl std::fmt::Display for ExampleError {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        match self {
            ExampleError::App(s) => write!(f, "App init error: {}", s),
            ExampleError::Device(de) => write!(f, "DeviceError: {}", de),
            ExampleError::Misc(s) => write!(f, "Misc: {}", s),
            ExampleError::RenderGraph(g) => write!(f, "RenderGraph: {}", g),
        }
    }
}

pub trait ThreedExample {
    fn initialize(&mut self, runner: &mut ThreedExampleRunner) -> Result<(), ExampleError>;

    fn shutdown(&mut self, runner: &mut ThreedExampleRunner);

    fn tick(&mut self, runner: &mut ThreedExampleRunner) -> Result<bool, ExampleError>;
}

pub struct ThreedExampleRunner {
    app_name: String,
    pub device: Arc<dyn Device>,
    pub app: Box<dyn App>,
    pub window_id: WindowId,
    pub timer: Timer,
    pub frame_tick: Duration,
    pub fps_counter: FpsCounter,
}

impl ThreedExampleRunner {
    pub fn new(name: &str) -> Result<Self, ExampleError> {
        modus_profiler::register_thread!("Main");
        modus_profiler::scope!();
        if let Err(e) = init_log_system() {
            return Err(ExampleError::Misc(format!(
                "Failed to init log system: {}",
                e
            )));
        }

        let perf_name = format!("modus.thread_example.{}", name.to_lowercase());
        let app_init_info = AppInitInfo {
            name,
            name_preferences: &perf_name,
        };

        let mut args = std::env::args();

        let use_debug = !args.any(|x| x == "--disable-debug");
        let use_validation_layers = !args.any(|x| x == "--disable-validation");

        let instance_create_info = modus_threed::instance::InstanceCreateInfo {
            name,
            version_major: 0,
            version_minor: 0,
            version_patch: 0,
            debug: use_debug,
            api_validation: use_validation_layers,
        };

        let mut app = match create_app(&app_init_info, &instance_create_info) {
            Err(e) => {
                return Err(ExampleError::App(format!("Failed to init app: {}", e)));
            }
            Ok(a) => a,
        };

        let window_info = WindowCreateInfo {
            title: name,
            width: None,
            height: None,
            x: None,
            y: None,
            fullscreen: FullscreenMode::None,
            hidden: false,
        };

        let window_id = match app.window_system_mut().create_window(&window_info) {
            Ok(id) => id,
            Err(e) => return Err(ExampleError::App(e)),
        };

        let surface = app
            .window_system()
            .resole_window(window_id)
            .unwrap()
            .render_surface_interface();
        let device = app.threed_instance().clone().create_device(surface)?;

        Ok(Self {
            app,
            app_name: name.to_string(),
            device,
            window_id,
            timer: Timer::new(),
            frame_tick: Duration::new(0, 0),
            fps_counter: FpsCounter::new(),
        })
    }

    #[allow(clippy::result_unit_err)]
    pub fn run<T: ThreedExample + AppEventHandler>(&mut self, example: &mut T) -> Result<(), ()> {
        {
            modus_profiler::scope!("Example Init");
            log_debug!("Initializing example");
            if let Err(e) = example.initialize(self) {
                log_error!("Failed to init example: {}", e);
                return Err(());
            }
        }
        log_debug!("Starting main loop");
        let mut result = Ok(());
        // set the previous tick to nw to avoid high delta on first run
        self.timer.reset_and_elapsed();
        loop {
            modus_profiler::scope!("Tick");

            {
                modus_profiler::scope!("Example FPS + TITLE");
                self.frame_tick = self.timer.reset_and_elapsed();
                self.fps_counter.update(self.frame_tick);

                if let Some(window) = self
                    .app
                    .window_system_mut()
                    .resole_window_mut(self.window_id)
                {
                    window.set_title(&format!(
                        "{} - FPS:{:0.2} Frame:{:0.2} ms",
                        self.app_name,
                        self.fps_counter.fps(),
                        self.fps_counter.frame_time_ms(),
                    ));
                }
            }

            {
                modus_profiler::scope!("Example App Tick");
                let status = self.app.tick(example);
                if status == TickResult::Quit {
                    break;
                }
            }
            {
                modus_profiler::scope!("Example Tick");
                match example.tick(self) {
                    Ok(continue_tick) => {
                        if !continue_tick {
                            break;
                        }
                    }
                    Err(e) => {
                        log_debug!("Tick error: {}", e);
                        result = Err(());
                        break;
                    }
                }
            }
            modus_profiler::finish_frame!();
        }
        log_debug!("Terminating example");
        example.shutdown(self);
        log_debug!("Finished");
        result
    }
}

#[macro_export]
macro_rules! generate_main {
    ($example_name:ident) => {
        fn main() -> Result<(), ()> {
            let mut runner = match example_lib::ThreedExampleRunner::new(stringify!($example_name))
            {
                Ok(r) => r,
                Err(e) => {
                    eprintln!(
                        "Failed to init test runner for {}: {}",
                        stringify!($example_name),
                        e
                    );
                    return Err(());
                }
            };
            let mut example = $example_name::default();
            runner.run(&mut example)
        }
    };
}

#[allow(clippy::question_mark)]
pub fn load_shader(path: &str, device: &dyn Device) -> Result<ShaderHandle, DeviceError> {
    let shader_path = Path::new(env!("OUT_DIR")).join(path);
    log_debug!(
        "Loading shader: {}",
        shader_path.to_str().unwrap_or("<INVALID_PATH>")
    );
    let mut file = match std::fs::File::open(shader_path) {
        Ok(f) => f,
        Err(_) => return Err(DeviceError::Api("Failed to open file".to_string())),
    };
    let mut data = Vec::with_capacity(256);
    if file.read_to_end(&mut data).is_err() {
        return Err(DeviceError::Api("Failed to read file".to_string()));
    }
    let create_info = ShaderCreateInfo { data: &data };
    device.create_shader(&create_info)
}

pub const COLOR_TEXTURE_NAME: &str = "COLOR";
