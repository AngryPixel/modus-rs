#version 450

layout(location = 0) out vec4 outColor;

layout(location = 0) in vec3 inColor;
layout(location = 1) in vec2 inUV;

layout(set=0, binding=1) uniform sampler2D tex;

void main() {
    vec3 color = texture(tex, inUV).xyz;
    outColor = vec4(mix(inColor, color, 0.5), 1.0);
}
