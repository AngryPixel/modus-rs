#version 450

struct Vertex {
    float vx, vy, vz;
    float cx, cy, cz;
    float uvx, uvy;
};

layout(set = 1, binding = 0) uniform CamerBufer {
    mat4 vp;
    mat4 t;
};

layout(std430, set =0, binding = 0) readonly buffer ObjectBuffer {
    Vertex vertices[];
} objBuffer;

layout(location = 0) out vec3 outColor;
layout(location = 1) out vec2 outUV;

void main() {
    Vertex v = objBuffer.vertices[gl_VertexIndex];
    outColor = vec3(v.cx, v.cy, v.cz);
    outUV = vec2(v.uvx, v.uvy);
    vec4 vert = vec4(v.vx, v.vy, v.vz, 1.0);
    gl_Position = vp * t * vert;
}
