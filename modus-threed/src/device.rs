//! Device abstraction

use crate::app_interface::RenderSurfaceInterface;
use crate::buffer::BufferCreateInfo;
use crate::command_buffer::CommandBuffer;
use crate::descriptors::{
    DescriptorPoolCreateInfo, DescriptorSetLayoutCreateInfo, DescriptorSetUpdateInfo,
};
use crate::framebuffer::FramebufferCreateInfo;
use crate::pipeline::PipelineCreateInfo;
use crate::render_pass::RenderPassCreateInfo;
use crate::sampler::SamplerCreateInfo;
use crate::shader::ShaderCreateInfo;
use crate::texture::{TextureCreateInfo, TextureData};
use crate::types::{
    BufferHandle, DescriptorPoolHandle, DescriptorSetHandle, DescriptorSetLayoutHandle, DeviceSize,
    FramebufferHandle, PipelineHandle, RenderPassHandle, SamplerHandle, ShaderHandle,
    SwapChainHandle, TextureHandle,
};
use modus_core::sync::ThreadOwnedAccessError;
use std::fmt::Formatter;
use std::sync::PoisonError;

#[derive(Debug)]
pub struct DeviceCompressedTextureSupport {
    pub astc: bool,
    pub etc2: bool,
    pub s3tc: bool,
}

#[derive(Debug)]
pub struct DeviceInfo {
    pub min_cbuffer_size: u32,
    pub max_cbuffer_size: u32,
    pub max_texture_size: u32,
    pub max_texture_depth: u32,
    pub sbuffer_offset_alignment: usize,
    pub cbuffer_offset_alignment: usize,
    pub compressed_texture_support: DeviceCompressedTextureSupport,
}

#[derive(Debug, Eq, PartialEq)]
pub enum DeviceError {
    InvalidThreadAccess,
    Api(String),
    InvalidHandle,
    OutOfMemory,
}

pub type DeviceResult<T> = Result<T, DeviceError>;

#[derive(Debug, Eq, PartialEq, Copy, Clone)]
pub enum SwapChainAcquireResult {
    Ok,
    SwapChainResized,
}

impl std::fmt::Display for DeviceError {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        match self {
            DeviceError::InvalidThreadAccess => {
                write!(f, "Attempting to access device from invalid thread")
            }
            DeviceError::Api(e) => write!(f, "An API call failed > {}", e),
            DeviceError::InvalidHandle => write!(f, "Invalid handle provided"),
            DeviceError::OutOfMemory => write!(f, "Operation failed due to lack of memory"),
        }
    }
}

impl From<ThreadOwnedAccessError> for DeviceError {
    fn from(_: ThreadOwnedAccessError) -> Self {
        DeviceError::InvalidThreadAccess
    }
}

impl<T> From<PoisonError<T>> for DeviceError {
    fn from(_: PoisonError<T>) -> Self {
        DeviceError::Api("Failed to acquire lock".to_string())
    }
}

pub trait Device: std::fmt::Debug {
    fn device_info(&self) -> &DeviceInfo;

    fn begin_frame(&self) -> Result<(), DeviceError>;

    fn end_frame(&self, texture: TextureHandle, swap_chain: SwapChainHandle) -> DeviceResult<()>;

    fn create_swap_chain(
        &self,
        surface_interface: &dyn RenderSurfaceInterface,
    ) -> Result<SwapChainHandle, DeviceError>;

    fn destroy_swap_chain(&self, h: SwapChainHandle) -> Result<(), DeviceError>;

    fn swap_chain_size(&self, h: SwapChainHandle) -> Result<(u32, u32), DeviceError>;

    fn acquire_swap_chain_image(
        &self,
        h: SwapChainHandle,
        surface_interface: &dyn RenderSurfaceInterface,
    ) -> DeviceResult<SwapChainAcquireResult>;

    fn present_swap_chain(&self, h: SwapChainHandle) -> DeviceResult<()>;

    fn create_texture(
        &self,
        info: &TextureCreateInfo,
        data: &[TextureData],
    ) -> Result<TextureHandle, DeviceError>;

    fn destroy_texture(&self, h: TextureHandle) -> Result<(), DeviceError>;

    fn update_texture(&self, h: TextureHandle, data: &[TextureData]) -> Result<(), DeviceError>;

    fn create_framebuffer(
        &self,
        info: &FramebufferCreateInfo,
    ) -> Result<FramebufferHandle, DeviceError>;

    fn destroy_framebuffer(&self, h: FramebufferHandle) -> Result<(), DeviceError>;

    fn create_render_pass(
        &self,
        create_info: &RenderPassCreateInfo,
    ) -> Result<RenderPassHandle, DeviceError>;

    fn destroy_render_pass(&self, h: RenderPassHandle) -> Result<(), DeviceError>;

    fn create_shader(&self, info: &ShaderCreateInfo) -> Result<ShaderHandle, DeviceError>;

    fn destroy_shader(&self, h: ShaderHandle) -> Result<(), DeviceError>;

    fn create_pipeline(&self, info: &PipelineCreateInfo) -> Result<PipelineHandle, DeviceError>;

    fn destroy_pipeline(&self, h: PipelineHandle) -> Result<(), DeviceError>;

    fn create_descriptor_set_layout(
        &self,
        info: &DescriptorSetLayoutCreateInfo,
    ) -> Result<DescriptorSetLayoutHandle, DeviceError>;

    fn destroy_descriptor_set_layout(
        &self,
        h: DescriptorSetLayoutHandle,
    ) -> Result<(), DeviceError>;

    fn create_buffer(&self, info: &BufferCreateInfo) -> Result<BufferHandle, DeviceError>;

    fn destroy_buffer(&self, h: BufferHandle) -> Result<(), DeviceError>;

    fn create_sampler(&self, info: &SamplerCreateInfo) -> DeviceResult<SamplerHandle>;

    fn destroy_sampler(&self, h: SamplerHandle) -> DeviceResult<()>;

    fn create_descriptor_pool(
        &self,
        info: &DescriptorPoolCreateInfo,
    ) -> DeviceResult<DescriptorPoolHandle>;

    fn destroy_descriptor_pool(&self, h: DescriptorPoolHandle) -> DeviceResult<()>;

    /// Should DeviceError::OutOfMemory if the descriptor pool is full or fails to allocate
    /// the descriptor
    fn create_descriptor_set(
        &self,
        layout: DescriptorSetLayoutHandle,
        pool: DescriptorPoolHandle,
        values: Option<DescriptorSetUpdateInfo>,
    ) -> DeviceResult<DescriptorSetHandle>;

    fn update_descriptor_set(
        &self,
        h: DescriptorSetHandle,
        value: DescriptorSetUpdateInfo,
    ) -> DeviceResult<()>;

    fn destroy_descriptor_set(&self, h: DescriptorSetHandle) -> DeviceResult<()>;

    fn update_buffer(
        &self,
        h: BufferHandle,
        offset: DeviceSize,
        data: *const u8,
        size: DeviceSize,
    ) -> Result<(), DeviceError>;

    fn execute_commands(
        &self,
        f: &mut dyn FnMut(&'_ mut dyn CommandBuffer) -> Result<(), DeviceError>,
    ) -> Result<(), DeviceError>;
}

pub fn update_buffer_typed<T: Sized>(
    device: &dyn Device,
    h: BufferHandle,
    offset: DeviceSize,
    data: &T,
) -> DeviceResult<()> {
    let data_ptr = unsafe { std::mem::transmute(data) };
    device.update_buffer(h, offset, data_ptr, std::mem::size_of::<T>() as DeviceSize)
}
