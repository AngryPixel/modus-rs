//! Descriptor management

use crate::types::{BufferHandle, DescriptorSetHandle, DeviceSize, SamplerHandle, TextureHandle};

#[derive(Debug, Copy, Clone, Eq, PartialEq, Hash)]
pub struct PushConstantDesc {
    pub offset: u32,
    pub size: u32,
    pub stages: u8,
}

#[repr(u8)]
#[derive(Debug, Copy, Clone, Eq, PartialEq, Hash)]
pub enum DescriptorType {
    Constant,
    ConstantDynamic,
    Storage,
    StorageDynamic,
    Attachment,
    Texture,
}

#[derive(Debug, Copy, Clone, Eq, PartialEq, Hash)]
pub struct DescriptorDesc {
    pub binding_point: u32,
    pub descriptor_type: DescriptorType,
    pub stages: u8,
}

#[derive(Debug)]
pub struct DescriptorSetLayoutCreateInfo<'a, 'b> {
    pub push_constants: &'a [PushConstantDesc],
    pub descriptors: &'b [DescriptorDesc],
}

#[derive(Debug, Copy, Clone, Hash, PartialEq, Eq)]
pub struct DescriptorBufferInput {
    pub buffer: BufferHandle,
    pub buffer_offset: DeviceSize,
    pub buffer_size: Option<DeviceSize>,
    pub binding_point: u32,
    pub dtype: DescriptorType,
}

#[derive(Debug, Copy, Clone, Hash, PartialEq, Eq)]
pub struct DescriptorTextureInput {
    pub texture: TextureHandle,
    pub sampler: SamplerHandle,
    pub binding_point: u32,
}

#[derive(Debug)]
pub struct DescriptorSetUpdateInfo<'a, 'c> {
    pub buffers: &'a [DescriptorBufferInput],
    pub textures: &'c [DescriptorTextureInput],
}

#[derive(Debug)]
pub struct DescriptorSetBindInfo<'b> {
    pub handle: DescriptorSetHandle,
    pub set_index: u32,
    pub dynamic_offsets: &'b [u32],
}

#[derive(Debug)]
pub struct DescriptorPoolCreateInfo {
    pub constant_count: u32,
    pub constant_dynamic_count: u32,
    pub storage_count: u32,
    pub storage_dynamic_count: u32,
    pub input_attachment_count: u32,
    pub texture_count: u32,
    pub max_set_count: u32,
}
