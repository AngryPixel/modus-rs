//! Render Pass abstractions

use crate::types::TextureFormat;

#[derive(Debug, Copy, Clone, Eq, PartialEq)]
pub enum AttachmentLoadOp {
    None,
    Load,
    Clear,
}

#[derive(Debug, Copy, Clone, Eq, PartialEq)]
pub enum AttachmentStoreOp {
    None,
    Store,
}

#[derive(Debug, Copy, Clone, Eq, PartialEq)]
pub enum AttachmentAccess {
    Read,
    Write,
}

#[derive(Debug, Copy, Clone)]
pub struct RenderPassAttachmentDesc {
    pub load_op: AttachmentLoadOp,
    pub store_op: AttachmentStoreOp,
    pub access: AttachmentAccess,
    pub format: TextureFormat,
    pub samples: u32,
}

#[derive(Debug, Copy, Clone, Eq, PartialEq)]
pub enum PipelineStage {
    Graphics,
    Compute,
}

pub const MAX_ATTACHMENT_COUNT: usize = 8;
#[derive(Debug)]
pub struct RenderPassCreateInfo<'a> {
    pub stage: PipelineStage,
    pub attachments: &'a [RenderPassAttachmentDesc],
    pub input_attachments: &'a [usize],
    pub color_attachments: &'a [usize],
    pub depth_stencil_attachment: Option<usize>,
}

#[derive(Debug, Copy, Clone)]
pub enum ClearValue {
    Color([f32; 4]),
    DepthStencil(f32, u8),
}
#[derive(Debug, Copy, Clone)]
pub struct RenderPassClearInfo {
    pub values: [ClearValue; MAX_ATTACHMENT_COUNT],
}

impl Default for RenderPassClearInfo {
    fn default() -> Self {
        Self {
            values: [
                ClearValue::Color([0.0; 4]),
                ClearValue::Color([0.0; 4]),
                ClearValue::Color([0.0; 4]),
                ClearValue::Color([0.0; 4]),
                ClearValue::Color([0.0; 4]),
                ClearValue::Color([0.0; 4]),
                ClearValue::Color([0.0; 4]),
                ClearValue::Color([0.0; 4]),
            ],
        }
    }
}
