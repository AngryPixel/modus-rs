//! Pipeline management

use crate::types::{
    BlendFactor, BlendOp, CompareOp, CullMode, DescriptorSetLayoutHandle, FrontFace, LogicOp,
    PolygonMode, Primitive, RenderPassHandle, ShaderHandle, ShaderType, StencilOp,
};

#[derive(Debug)]
pub struct InputAssemblyState {
    pub primitive: Primitive,
    pub primitive_restart: bool,
}

#[derive(Debug)]
pub struct ShaderState {
    pub shader: ShaderHandle,
    pub stage: ShaderType,
    pub entry: &'static str,
}

#[derive(Debug, Copy, Clone)]
pub struct DepthBias {
    pub constant_factor: f32,
    pub clamp: f32,
    pub slope_factor: f32,
}

#[derive(Debug)]
pub struct RasterizationState {
    pub depth_clamp: bool,
    pub rasterizer_discard: bool,
    pub polygon_mode: PolygonMode,
    pub line_width: f32,
    pub cull_mode: CullMode,
    pub front_face: FrontFace,
    pub depth_bias: Option<DepthBias>,
}

#[derive(Debug)]
pub struct MultisampleState {
    pub sample_shading: bool,
    pub samples: u32,
    pub min_sample_shading: f32,
    pub alpha_to_coverage: bool,
    pub alpha_to_one: bool,
}

#[derive(Debug, Copy, Clone)]
pub struct StencilFaceState {
    pub fail_op: StencilOp,
    pub pass_op: StencilOp,
    pub depth_fail_op: StencilOp,
    pub compare_op: CompareOp,
    pub compare_mask: u32,
    pub write_mask: u32,
    pub reference: u32,
}

#[derive(Debug, Copy, Clone)]
pub struct StencilState {
    pub front: StencilFaceState,
    pub back: StencilFaceState,
}

#[derive(Debug, Copy, Clone)]
pub struct DepthState {
    pub write: bool,
    pub compare_op: CompareOp,
    pub depth_bounds: Option<(f32, f32)>,
}

#[derive(Debug)]
pub struct DepthStencilState {
    pub depth: Option<DepthState>,
    pub stencil: Option<StencilState>,
}

pub const COLOR_MASK_R: u32 = 0x1;
pub const COLOR_MASK_G: u32 = 0x2;
pub const COLOR_MASK_B: u32 = 0x4;
pub const COLOR_MASK_A: u32 = 0x8;
pub const COLOR_MASK_ALL: u32 = COLOR_MASK_A | COLOR_MASK_B | COLOR_MASK_G | COLOR_MASK_R;

#[derive(Debug, Copy, Clone)]
pub struct ColorBlendState {
    pub src_color_blend_factor: BlendFactor,
    pub dst_color_blend_factor: BlendFactor,
    pub color_blend_op: BlendOp,
    pub src_alpha_blend_factor: BlendFactor,
    pub dst_alpha_blend_factor: BlendFactor,
    pub alpha_blend_op: BlendOp,
}

#[derive(Debug)]
pub struct ColorAttachmentBlendState {
    pub state: Option<ColorBlendState>,
    pub color_mask: u32,
}

#[derive(Debug)]
pub struct BlendState<'a> {
    pub attachments: &'a [ColorAttachmentBlendState],
    pub logic_op: Option<LogicOp>,
    pub blend_constants: [f32; 4],
}

#[derive(Debug)]
pub struct PipelineCreateInfo<'a, 'b, 'c> {
    pub descriptor_set_layouts: Option<&'c [DescriptorSetLayoutHandle]>,
    pub depth_stencil_state: DepthStencilState,
    pub blend_state: BlendState<'a>,
    pub rasterization_state: RasterizationState,
    pub input_assembly_state: InputAssemblyState,
    pub shader_states: &'b [ShaderState],
    pub multisample_state: MultisampleState,
    pub render_pass: RenderPassHandle,
}
