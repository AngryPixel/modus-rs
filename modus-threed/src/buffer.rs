//! GPU Buffers

use crate::types::{BufferType, DeviceSize, MemoryType};

#[derive(Debug)]
pub struct BufferCreateInfo<'a> {
    pub btype: BufferType,
    pub size: DeviceSize,
    pub memory_type: MemoryType,
    pub data: Option<&'a [u8]>,
}
