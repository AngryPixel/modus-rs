//! Sampler Description

use crate::types::{Filter, SamplerAddressMode, SamplerMipmapMode};

#[derive(Debug, Eq, PartialEq, Hash, Copy, Clone)]
pub struct SamplerCreateInfo {
    pub mag_filter: Filter,
    pub min_filter: Filter,
    pub mipmap_mode: SamplerMipmapMode,
    pub address_mode_u: SamplerAddressMode,
    pub address_mode_v: SamplerAddressMode,
    pub address_mode_w: SamplerAddressMode,
    pub anisotropy: bool,
    pub max_anisotropy: u32,
}
