//! App Interface required to setup vulkan
use std::os::raw::c_char;

#[cfg(target_pointer_width = "64")]
pub type NativeSurfacePtr = *mut std::os::raw::c_void;

#[cfg(target_pointer_width = "32")]
pub type NativeSurfacePtr = u64;

pub trait RenderSurfaceInterface: std::fmt::Debug {
    /// Get the underlying representation of the surface
    /// # Safety
    /// Only safe to access if the implementor of this trait is alive!
    unsafe fn native_surface_handle(&self) -> NativeSurfacePtr;
    fn surface_size(&self) -> (u32, u32);
}

pub trait VkAppInterface: std::fmt::Debug {
    fn required_extension_list(&self) -> Result<Vec<*const c_char>, String>;
}
