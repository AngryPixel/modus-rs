use super::*;
use crate::render_pass::{AttachmentLoadOp, AttachmentStoreOp};
use crate::types::TextureFormat;

#[allow(unused_assignments)]
#[derive(Debug)]
struct EmptyPass {}

impl RenderPass<()> for EmptyPass {
    fn build(
        &self,
        _user_data: &mut (),
        _render_graph: &RenderGraph<()>,
        _render_pass_data: &RenderPassBuildState,
        _cmd_buffer: &mut dyn CommandBuffer,
    ) -> Result<(), DeviceError> {
        Ok(())
    }
}

#[test]
fn basic_checks() {
    let instance = crate::create_null_instance();
    let surface_interface = crate::null::RenderSurfaceInterface {};
    let device = instance.create_device(&surface_interface).unwrap();
    let mut graph = RenderGraph::new(device);
    let post_process_pass_handle = {
        let inout = [RenderPassColorOutputDesc {
            name: "post_process",
            input: Some("color"),
            size: SizeType::SwapChainRelative(1.0, 1.0, 1.0),
            format: TextureFormat::R8G8B8A8,
            samples: 1,
            transient: false,
            load_op: AttachmentLoadOp::Clear,
            store_op: AttachmentStoreOp::Store,
        }];

        let create_info = RenderPassCreateInfo {
            name: "post_process_pass",
            stage: PipelineStage::Graphics,
            color_inputs: &[],
            color_outputs: &inout,
            size: SizeType::SwapChainRelative(1.0, 1.0, 1.0),
            build_pass: true,
        };

        let r = graph.add_pass(&create_info, Box::new(EmptyPass {}));
        assert!(r.is_ok());
        r.unwrap()
    };

    let color_pass_handle = {
        let outputs = [RenderPassColorOutputDesc {
            name: "color",
            size: SizeType::SwapChainRelative(1.0, 1.0, 1.0),
            format: TextureFormat::R8G8B8A8,
            samples: 1,
            transient: false,
            input: None,
            load_op: AttachmentLoadOp::Clear,
            store_op: AttachmentStoreOp::Store,
        }];

        let create_info = RenderPassCreateInfo {
            name: "color_pass",
            stage: PipelineStage::Graphics,
            color_inputs: &[],
            color_outputs: &outputs,
            size: SizeType::SwapChainRelative(1.0, 1.0, 1.0),
            build_pass: true,
        };

        let r = graph.add_pass(&create_info, Box::new(EmptyPass {}));
        assert!(r.is_ok());
        r.unwrap()
    };

    assert!(graph.validate().is_ok());
    let order = graph.build_pass_graph();
    assert!(order.is_ok());
    if let Ok(order) = order {
        assert_eq!(order.len(), 2);
        assert_eq!(order[0], color_pass_handle);
        assert_eq!(order[1], post_process_pass_handle);
    }
}
