//! Definitions for the render graph system
use crate::app_interface::RenderSurfaceInterface;
use crate::command_buffer::CommandBuffer;
use crate::device::{Device, DeviceError, SwapChainAcquireResult};
use crate::render_pass::{PipelineStage, RenderPassAttachmentDesc, MAX_ATTACHMENT_COUNT};
use crate::texture::{
    TextureCreateInfo, TEXTURE_USAGE_FLAG_ATTACHMENT, TEXTURE_USAGE_FLAG_SHADER_INPUT,
};
use crate::types::{FramebufferHandle, SwapChainHandle, TextureFormat, TextureHandle, TextureType};
use modus_core::handle_map::Handle;
use modus_core::handle_map::HandleMap;
use modus_core::sorting::{TopologicalSortError, TopologicalSorter};
use modus_core::HashMap;
use modus_core::{define_custom_handle_type, ArrayVec};
use std::cell::Cell;
use std::collections::hash_map::Entry::{Occupied, Vacant};
use std::fmt::Formatter;
use std::sync::Arc;

define_custom_handle_type!(RenderPassHandle);
define_custom_handle_type!(TextureResourceHandle);
define_custom_handle_type!(BufferResourceHandle);
const MAX_COLOR_SLOTS: usize = 8;

#[derive(Debug, Copy, Clone, PartialEq)]
pub enum SizeType {
    InputRelative(f32, f32, f32),
    SwapChainRelative(f32, f32, f32),
    Absolute(f32, f32, f32),
}

#[derive(Debug, Copy, Clone)]
pub struct RenderPassColorOutputDesc {
    pub name: &'static str,
    pub size: SizeType,
    pub format: TextureFormat,
    pub samples: u32,
    pub transient: bool,
    pub input: Option<&'static str>,
    pub load_op: crate::render_pass::AttachmentLoadOp,
    pub store_op: crate::render_pass::AttachmentStoreOp,
}

#[derive(Debug)]
pub struct RenderPassCreateInfo<'a> {
    pub name: &'static str,
    pub stage: PipelineStage,
    pub size: SizeType,
    pub color_inputs: &'a [&'static str],
    pub color_outputs: &'a [RenderPassColorOutputDesc],
    pub build_pass: bool,
}

#[derive(Debug)]
pub struct RenderPassBuildState {
    pub render_pass: crate::types::RenderPassHandle,
    pub framebuffer: FramebufferHandle,
    pub dimensions: [u32; 3],
}

#[derive(Copy, Clone, Debug, Eq, PartialEq)]
enum ResourceState {
    Undefined,
    Created,
}

#[derive(Debug, PartialEq)]
pub struct TextureResource {
    pub name: &'static str,
    pub size: SizeType,
    pub format: TextureFormat,
    pub samples: u32,
    pub(crate) device_resource: DeviceResource<TextureHandle>,
    pub transient: bool,
    load_op: crate::render_pass::AttachmentLoadOp,
    store_op: crate::render_pass::AttachmentStoreOp,
    state: ResourceState,
}

impl TextureResource {
    fn new_undefined(name: &'static str) -> TextureResource {
        Self {
            name,
            size: SizeType::SwapChainRelative(1.0, 1.0, 1.0),
            format: TextureFormat::Total,
            samples: 1,
            device_resource: DeviceResource::Uninitialized,
            transient: false,
            load_op: crate::render_pass::AttachmentLoadOp::None,
            store_op: crate::render_pass::AttachmentStoreOp::None,
            state: ResourceState::Undefined,
        }
    }
}

pub trait RenderPass<UserData>: std::fmt::Debug {
    fn build(
        &self,
        user_data: &mut UserData,
        render_graph: &RenderGraph<UserData>,
        render_pass_state: &RenderPassBuildState,
        cmd_buffer: &mut dyn CommandBuffer,
    ) -> Result<(), DeviceError>;
}

pub(crate) struct RenderPassData<UserData> {
    pub(crate) handle: RenderPassHandle,
    pub(crate) name: &'static str,
    pub(crate) stage: PipelineStage,
    pub(crate) color_inputs: ArrayVec<TextureResourceHandle, MAX_COLOR_SLOTS>,
    pub(crate) color_outputs: ArrayVec<TextureResourceHandle, MAX_COLOR_SLOTS>,
    pub(crate) device_pass: Cell<crate::types::RenderPassHandle>,
    pub(crate) device_framebuffer: Cell<crate::types::FramebufferHandle>,
    pub(crate) size: SizeType,
    pub(crate) pass: Box<dyn RenderPass<UserData>>,
    pub(crate) build_pass: bool,
    calculated_size: [u32; 3],
}

impl<UserData> std::fmt::Debug for RenderPassData<UserData> {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        write!(f, "RenderPass{{")?;
        write!(f, "handle:{:?} ", self.handle)?;
        write!(f, "name:{:?} ", self.name)?;
        write!(f, "color_inputs:{:?} ", self.color_inputs)?;
        write!(f, "color_outputs:{:?} ", self.color_outputs)?;
        write!(f, "device_pass:{:?} ", self.device_pass)?;
        write!(f, "device_framebuffer:{:?} ", self.device_framebuffer)?;
        write!(f, "size:{:?} ", self.size)?;
        write!(f, "}}")
    }
}

impl<UserData> RenderPassData<UserData> {
    fn new(
        name: &'static str,
        stage: PipelineStage,
        size: SizeType,
        pass: Box<dyn RenderPass<UserData>>,
        build_pass: bool,
    ) -> Self {
        RenderPassData {
            handle: Default::default(),
            name,
            stage,
            color_outputs: Default::default(),
            color_inputs: Default::default(),
            device_pass: Default::default(),
            device_framebuffer: Default::default(),
            size,
            pass,
            build_pass,
            calculated_size: [0; 3],
        }
    }
}

#[derive(Debug)]
enum ResourceHandle {
    Texture(TextureResourceHandle),
    Buffer(BufferResourceHandle),
}

#[derive(Debug, Copy, Clone, Eq, PartialEq)]
pub(crate) enum DeviceResource<T>
where
    T: Copy + Clone + Eq + PartialEq,
{
    Uninitialized,
    Owned(T),
    Aliased(T),
}

#[derive(Debug)]
pub enum RenderGraphValidationError {
    UndefinedTextureResource,
    InputOutputTextureSizeMismatch,
    InputOutputTextureFormatMismatch,
    InputOutputColorCountMismatch,
}

impl std::fmt::Display for RenderGraphValidationError {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        match self {
            RenderGraphValidationError::UndefinedTextureResource => {
                write!(f, "Undefined Texture resource")
            }
            RenderGraphValidationError::InputOutputColorCountMismatch => {
                write!(f, "Color inputs and outputs count doesn't match")
            }
            RenderGraphValidationError::InputOutputTextureFormatMismatch => {
                write!(f, "Color input and output texture formats don't match")
            }
            RenderGraphValidationError::InputOutputTextureSizeMismatch => {
                write!(f, "Color input and output texture size don't match")
            }
        }
    }
}

#[derive(Debug)]
pub enum RenderGraphError {
    DuplicatePass,
    FailedToResolveTexture(&'static str),
    ExceedsMaxColorSlots,
    FailedToStoreRenderPass,
    FailedToLocateTextureProducer,
    DependencyCycle(RenderPassHandle, RenderPassHandle),
    EmptyGraph,
    DeviceError(DeviceError),
    ValidationError(RenderGraphValidationError),
}

impl std::fmt::Display for RenderGraphError {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        match self {
            RenderGraphError::DuplicatePass => write!(f, "Duplicate Pass"),
            RenderGraphError::FailedToResolveTexture(name) => {
                write!(f, "Failed to resolve texture: {}", name)
            }
            RenderGraphError::ExceedsMaxColorSlots => write!(f, "Pass exceeds max color slots"),
            RenderGraphError::FailedToStoreRenderPass => write!(f, "Failed to store render pass"),
            RenderGraphError::FailedToLocateTextureProducer => {
                write!(f, "Failed to locate texture producer")
            }
            RenderGraphError::DependencyCycle(p1, p2) => write!(
                f,
                "Dependency cycle detected between pass {} and pass {}",
                p1, p2
            ),
            RenderGraphError::EmptyGraph => write!(f, "Graph is empty"),
            RenderGraphError::DeviceError(d) => write!(f, "{}", d),
            RenderGraphError::ValidationError(ve) => write!(f, "{}", ve),
        }
    }
}

impl From<TopologicalSortError<RenderPassHandle>> for RenderGraphError {
    fn from(e: TopologicalSortError<RenderPassHandle>) -> Self {
        match e {
            TopologicalSortError::DuplicateNode(_) => RenderGraphError::DuplicatePass,
            TopologicalSortError::DependencyCycle(k, v) => RenderGraphError::DependencyCycle(k, v),
            TopologicalSortError::DependencyNotFound(_) => {
                panic!("Failed to resolve graph dependency, this should not happen");
            }
            TopologicalSortError::EmtpyGraph => RenderGraphError::EmptyGraph,
        }
    }
}

impl From<DeviceError> for RenderGraphError {
    fn from(e: DeviceError) -> Self {
        RenderGraphError::DeviceError(e)
    }
}

impl From<RenderGraphValidationError> for RenderGraphError {
    fn from(e: RenderGraphValidationError) -> Self {
        RenderGraphError::ValidationError(e)
    }
}

#[derive(Debug)]
pub struct RenderGraph<UserData> {
    pub device: Arc<dyn Device>,
    pub(crate) render_passes: HandleMap<RenderPassHandle, RenderPassData<UserData>>,
    pub(crate) texture_resources: HandleMap<TextureResourceHandle, TextureResource>,
    name_to_resource: HashMap<&'static str, ResourceHandle>,
    swap_chain_dimensions: (u32, u32, u32),
    pass_sorter: TopologicalSorter<RenderPassHandle>,
}

impl<UserData> Drop for RenderGraph<UserData> {
    fn drop(&mut self) {
        self.clear_resources(false);
    }
}

impl<UserData> RenderGraph<UserData> {
    pub fn new(device: Arc<dyn Device>) -> Self {
        Self {
            device,
            render_passes: HandleMap::new(16),
            texture_resources: HandleMap::new(32),
            name_to_resource: HashMap::with_capacity(64),
            swap_chain_dimensions: (0, 0, 0),
            pass_sorter: TopologicalSorter::with_capacity(16),
        }
    }

    pub fn contains_pass(&self, name: &'static str) -> bool {
        for pass in &self.render_passes {
            if pass.name == name {
                return true;
            }
        }
        false
    }

    pub fn add_pass<'a>(
        &mut self,
        create_info: &RenderPassCreateInfo<'a>,
        pass: Box<dyn RenderPass<UserData>>,
    ) -> Result<RenderPassHandle, RenderGraphError> {
        if self.contains_pass(create_info.name) {
            return Err(RenderGraphError::DuplicatePass);
        }

        if create_info.color_inputs.len() > MAX_COLOR_SLOTS {
            return Err(RenderGraphError::ExceedsMaxColorSlots);
        }

        let mut pass = RenderPassData::new(
            create_info.name,
            create_info.stage,
            create_info.size,
            pass,
            create_info.build_pass,
        );

        // Resolve all outputs
        for output in create_info.color_outputs.iter() {
            if let Some(input) = output.input {
                let input_handle = match self.get_or_create_undefined_texture(input) {
                    Some(h) => h,
                    None => return Err(RenderGraphError::FailedToResolveTexture(input)),
                };
                pass.color_inputs.push(input_handle);
            } else {
                pass.color_inputs.push(TextureResourceHandle::new());
            }

            let texture = TextureResource {
                name: output.name,
                size: output.size,
                format: output.format,
                samples: output.samples,
                device_resource: DeviceResource::Uninitialized,
                transient: output.transient,
                state: ResourceState::Created,
                load_op: output.load_op,
                store_op: output.store_op,
            };

            let output_handle = match self.get_or_create_texture(texture) {
                Some(h) => h,
                None => return Err(RenderGraphError::FailedToResolveTexture(output.name)),
            };
            pass.color_outputs.push(output_handle);
        }

        // Resolve inputs
        for input_name in create_info.color_inputs.iter() {
            let input_handle = match self.get_or_create_undefined_texture(*input_name) {
                Some(h) => h,
                None => return Err(RenderGraphError::FailedToResolveTexture(*input_name)),
            };
            pass.color_inputs.push(input_handle);
            pass.color_outputs.push(TextureResourceHandle::new());
        }

        let handle = match self.render_passes.insert_with(pass, |h, p| {
            p.handle = h;
        }) {
            Ok(h) => h,
            Err(_) => {
                return Err(RenderGraphError::FailedToStoreRenderPass);
            }
        };
        Ok(handle)
    }

    /// Use this to create a place holder for a texture resource that is not defined by the
    /// a render pass
    fn get_or_create_undefined_texture(
        &mut self,
        name: &'static str,
    ) -> Option<TextureResourceHandle> {
        match self.name_to_resource.entry(name) {
            Occupied(o) => match o.get() {
                ResourceHandle::Texture(t) => Some(*t),
                _ => None,
            },
            Vacant(v) => {
                let handle = match self
                    .texture_resources
                    .insert(TextureResource::new_undefined(name))
                {
                    Ok(h) => h,
                    Err(_) => {
                        return None;
                    }
                };
                v.insert(ResourceHandle::Texture(handle));
                Some(handle)
            }
        }
    }

    fn get_or_create_texture(&mut self, texture: TextureResource) -> Option<TextureResourceHandle> {
        match self.name_to_resource.entry(texture.name) {
            Occupied(o) => match o.get() {
                ResourceHandle::Texture(t) => {
                    let resource = self.texture_resources.get_mut(*t).unwrap();
                    if resource.state == ResourceState::Undefined {
                        *resource = texture;
                    }
                    Some(*t)
                }
                _ => None,
            },
            Vacant(v) => {
                let handle = match self.texture_resources.insert(texture) {
                    Ok(h) => h,
                    Err(_) => {
                        return None;
                    }
                };
                v.insert(ResourceHandle::Texture(handle));
                Some(handle)
            }
        }
    }

    pub fn get_named_texture(&self, name: &'static str) -> Option<TextureHandle> {
        if let Some(resource_handle) = self.name_to_resource.get(name) {
            match resource_handle {
                ResourceHandle::Texture(h) => {
                    if let Some(t) = self.texture_resources.get(*h) {
                        match t.device_resource {
                            DeviceResource::Owned(handle) | DeviceResource::Aliased(handle) => {
                                Some(handle)
                            }
                            _ => None,
                        }
                    } else {
                        None
                    }
                }
                _ => None,
            }
        } else {
            None
        }
    }

    pub(crate) fn calculate_size(
        size: SizeType,
        swap_chain_dimensions: (u32, u32, u32),
        relative_size: Option<(u32, u32, u32)>,
    ) -> (u32, u32, u32) {
        match size {
            SizeType::Absolute(x, y, z) => (x as u32, y as u32, z as u32),
            SizeType::InputRelative(w, h, d) => {
                if let Some((rw, rh, rd)) = relative_size {
                    (
                        ((rw as f32 * w).ceil() as u32).max(1),
                        ((rh as f32 * h).ceil() as u32).max(1),
                        ((rd as f32 * d).ceil() as u32).max(1),
                    )
                } else {
                    panic!("No relative size provided");
                }
            }
            SizeType::SwapChainRelative(w, h, d) => (
                ((swap_chain_dimensions.0 as f32 * w).ceil() as u32).max(1),
                ((swap_chain_dimensions.1 as f32 * h).ceil() as u32).max(1),
                ((swap_chain_dimensions.2 as f32 * d).ceil() as u32).max(1),
            ),
        }
    }

    fn validate(&self) -> Result<(), RenderGraphValidationError> {
        crate::profile_scope!();
        for pass in &self.render_passes {
            if pass.color_inputs.len() != pass.color_outputs.len() {
                return Err(RenderGraphValidationError::InputOutputColorCountMismatch);
            }

            for i in 0..pass.color_outputs.len() {
                let input_handle = pass.color_inputs[i];
                let output_handle = pass.color_outputs[i];

                if input_handle.is_valid() && output_handle.is_valid() {
                    let input = match self.texture_resources.get(input_handle) {
                        Some(h) => h,
                        _ => return Err(RenderGraphValidationError::UndefinedTextureResource),
                    };

                    let output = match self.texture_resources.get(output_handle) {
                        Some(h) => h,
                        _ => return Err(RenderGraphValidationError::UndefinedTextureResource),
                    };

                    if output.format != input.format {
                        return Err(RenderGraphValidationError::InputOutputTextureFormatMismatch);
                    }

                    let input_size =
                        Self::calculate_size(input.size, self.swap_chain_dimensions, None);
                    let output_size =
                        Self::calculate_size(output.size, self.swap_chain_dimensions, None);
                    if input_size != output_size {
                        return Err(RenderGraphValidationError::InputOutputTextureSizeMismatch);
                    }
                } else if let Some(t) = self.texture_resources.get(input_handle) {
                    if t.state == ResourceState::Undefined {
                        return Err(RenderGraphValidationError::UndefinedTextureResource);
                    }
                    /*
                    Is this even necessary?
                    match self.texture_resources.get(output_handle.into()) {
                        Some(_) => {}
                        _ => return Err(RenderGraphValidationError::UndefinedTextureResource),
                    };*/
                }
            }
        }
        Ok(())
    }

    fn find_texture_producer(
        &self,
        texture_handle: TextureResourceHandle,
    ) -> Option<RenderPassHandle> {
        //TODO : optimize
        for pass in &self.render_passes {
            for output in &pass.color_outputs {
                if *output == texture_handle {
                    return Some(pass.handle);
                }
            }
        }
        None
    }

    fn build_pass_graph(&mut self) -> Result<Vec<RenderPassHandle>, RenderGraphError> {
        crate::profile_scope!();
        self.pass_sorter.clear();
        for pass in &self.render_passes {
            let mut dependencies = ArrayVec::<RenderPassHandle, 8>::new();
            for input in &pass.color_inputs {
                if input.is_valid() {
                    let producer_pass = match self.find_texture_producer(*input) {
                        Some(h) => h,
                        None => return Err(RenderGraphError::FailedToLocateTextureProducer),
                    };
                    dependencies.push(producer_pass);
                }
            }
            self.pass_sorter.add(pass.handle, &dependencies)?;
        }
        let order = self.pass_sorter.build()?;
        Ok(order)
    }

    fn create_device_texture(
        device: &dyn Device,
        size: (u32, u32, u32),
        texture: &TextureResource,
    ) -> Result<TextureHandle, DeviceError> {
        let texture_create_info = TextureCreateInfo {
            width: size.0,
            height: size.1,
            depth: size.2,
            mip_map_levels: 1,
            sample_count: texture.samples as u8,
            array_count: 1,
            ttype: TextureType::T2D,
            format: texture.format,
            transient: texture.transient,
            usage_flags: TEXTURE_USAGE_FLAG_ATTACHMENT | TEXTURE_USAGE_FLAG_SHADER_INPUT,
        };
        device.create_texture(&texture_create_info, &[])
    }

    fn build_pass_resources(
        &mut self,
        pass_handle: RenderPassHandle,
    ) -> Result<(), RenderGraphError> {
        // for each inputs/outputs create a new texture
        // for each inout check if dimensions and format matches, assign output as alias

        let pass = self.render_passes.get_mut(pass_handle).unwrap();

        let pass_size = Self::calculate_size(pass.size, self.swap_chain_dimensions, None);
        pass.calculated_size = [pass_size.0, pass_size.1, pass_size.2];

        let mut pass_attachments =
            ArrayVec::<RenderPassAttachmentDesc, MAX_ATTACHMENT_COUNT>::new();
        let mut color_attachment_indices = ArrayVec::<usize, MAX_ATTACHMENT_COUNT>::new();
        let mut input_attachment_indices = ArrayVec::<usize, MAX_ATTACHMENT_COUNT>::new();

        let mut framebuffer_textures = ArrayVec::<TextureHandle, MAX_ATTACHMENT_COUNT>::new();
        for i in 0..pass.color_outputs.len() {
            let input_device_resource = match self.texture_resources.get_mut(pass.color_inputs[i]) {
                Some(texture) => {
                    if let DeviceResource::Uninitialized = texture.device_resource {
                        let size =
                            Self::calculate_size(texture.size, self.swap_chain_dimensions, None);
                        let handle =
                            Self::create_device_texture(self.device.as_ref(), size, texture)?;
                        texture.device_resource = DeviceResource::Owned(handle);
                    }
                    let input_attachment_desc = crate::render_pass::RenderPassAttachmentDesc {
                        load_op: crate::render_pass::AttachmentLoadOp::Load,
                        store_op: crate::render_pass::AttachmentStoreOp::None,
                        access: crate::render_pass::AttachmentAccess::Read,
                        format: texture.format,
                        samples: texture.samples,
                    };
                    framebuffer_textures.push(match texture.device_resource {
                        DeviceResource::Owned(h) | DeviceResource::Aliased(h) => h,
                        _ => panic!("This should be unreachable"),
                    });
                    pass_attachments.push(input_attachment_desc);
                    input_attachment_indices.push(pass_attachments.len() - 1);
                    texture.device_resource
                }
                None => DeviceResource::Uninitialized,
            };

            if let Some(texture) = self.texture_resources.get_mut(pass.color_outputs[i]) {
                if texture.device_resource == DeviceResource::Uninitialized {
                    texture.device_resource = match input_device_resource {
                        DeviceResource::Uninitialized => {
                            let size = Self::calculate_size(
                                texture.size,
                                self.swap_chain_dimensions,
                                None,
                            );
                            let handle =
                                Self::create_device_texture(self.device.as_ref(), size, texture)?;
                            DeviceResource::Owned(handle)
                        }
                        DeviceResource::Owned(h) => DeviceResource::Aliased(h),
                        DeviceResource::Aliased(h) => DeviceResource::Aliased(h),
                    }
                }

                let color_attachment_desc = crate::render_pass::RenderPassAttachmentDesc {
                    load_op: texture.load_op,
                    store_op: texture.store_op,
                    access: crate::render_pass::AttachmentAccess::Write,
                    format: texture.format,
                    samples: texture.samples,
                };
                framebuffer_textures.push(match texture.device_resource {
                    DeviceResource::Owned(h) | DeviceResource::Aliased(h) => h,
                    _ => panic!("This should be unreachable"),
                });
                pass_attachments.push(color_attachment_desc);
                color_attachment_indices.push(pass_attachments.len() - 1);
            }

            if pass.build_pass && !pass.device_pass.get().is_valid() {
                let pass_create_info = crate::render_pass::RenderPassCreateInfo {
                    stage: pass.stage,
                    attachments: &pass_attachments,
                    input_attachments: &input_attachment_indices,
                    color_attachments: &color_attachment_indices,
                    depth_stencil_attachment: None,
                };

                pass.device_pass
                    .set(self.device.create_render_pass(&pass_create_info)?);
            }

            if pass.device_pass.get().is_valid() && !pass.device_framebuffer.get().is_valid() {
                let framebuffer_create_info = crate::framebuffer::FramebufferCreateInfo {
                    width: pass_size.0,
                    height: pass_size.1,
                    depth: pass_size.2,
                    render_pass: pass.device_pass.get(),
                    textures: &framebuffer_textures,
                };
                pass.device_framebuffer
                    .set(self.device.create_framebuffer(&framebuffer_create_info)?);
            }
        }
        Ok(())
    }

    pub fn execute(
        &mut self,
        user_data: &mut UserData,
        texture_name: &'static str,
        swap_chain: SwapChainHandle,
        surface_interface: &dyn RenderSurfaceInterface,
    ) -> Result<(), RenderGraphError> {
        crate::profile_scope!();
        {
            let swap_chain_size = self.device.swap_chain_size(swap_chain)?;
            self.swap_chain_dimensions.0 = swap_chain_size.0;
            self.swap_chain_dimensions.1 = swap_chain_size.1;
            self.swap_chain_dimensions.2 = 1;
        }

        // validate graph
        self.validate()?;

        // build pass order
        let pass_order = self.build_pass_graph()?;

        self.device.begin_frame()?;
        if let SwapChainAcquireResult::SwapChainResized = self
            .device
            .acquire_swap_chain_image(swap_chain, surface_interface)?
        {
            self.clear_resources(true);
            return Ok(());
        }

        {
            crate::profile_scope!("build_pass_resources");
            for p in &pass_order {
                // Should be fine to unwrap here since we have validated ahead of time that this handle
                // is safe

                // Build device resources, check for alias types (input -> read, write-> output)
                self.build_pass_resources(*p)?;
            }
        }

        let texture = if let Some(t) = self.get_named_texture(texture_name) {
            t
        } else {
            return Err(RenderGraphError::FailedToLocateTextureProducer);
        };

        // execute graph
        {
            crate::profile_scope!("execute_passes");
            for p in &pass_order {
                let pass = self.render_passes.get(*p).unwrap();
                let build_state = RenderPassBuildState {
                    render_pass: pass.device_pass.get(),
                    framebuffer: pass.device_framebuffer.get(),
                    dimensions: pass.calculated_size,
                };
                self.device.execute_commands(
                    &mut |cmd_buffer: &mut dyn CommandBuffer| -> Result<(), DeviceError> {
                        pass.pass.build(user_data, self, &build_state, cmd_buffer)?;
                        Ok(())
                    },
                )?;
            }
        }
        self.device.end_frame(texture, swap_chain)?;
        self.device.present_swap_chain(swap_chain)?;
        Ok(())
    }

    fn clear_resources(&mut self, is_resize: bool) {
        crate::profile_scope!();
        for texture in &mut self.texture_resources {
            if let DeviceResource::Owned(h) = texture.device_resource {
                self.device.destroy_texture(h).unwrap();
            }
            texture.device_resource = DeviceResource::Uninitialized;
        }

        for pass in &mut self.render_passes {
            self.device
                .destroy_framebuffer(pass.device_framebuffer.get())
                .unwrap();
            pass.device_framebuffer
                .set(crate::types::FramebufferHandle::new());
            if !is_resize {
                self.device
                    .destroy_render_pass(pass.device_pass.get())
                    .unwrap();
                pass.device_pass.set(crate::types::RenderPassHandle::new());
            }
        }
    }
}

#[cfg(test)]
#[path = "render_graph_tests.rs"]
mod render_graph_tests;
