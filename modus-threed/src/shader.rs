#[derive(Debug)]
pub struct ShaderCreateInfo<'a> {
    pub data: &'a [u8],
}
