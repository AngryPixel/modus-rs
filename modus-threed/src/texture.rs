use crate::types::{TextureFormat, TextureType};

pub const TEXTURE_USAGE_FLAG_ATTACHMENT: u32 = 1 << 0;
pub const TEXTURE_USAGE_FLAG_SHADER_INPUT: u32 = 1 << 1;

#[derive(Debug, Copy, Clone, Eq, PartialEq)]
pub struct TextureCreateInfo {
    pub width: u32,
    pub height: u32,
    pub depth: u32,
    pub mip_map_levels: u8,
    pub sample_count: u8,
    pub array_count: u8,
    pub ttype: TextureType,
    pub format: TextureFormat,
    pub transient: bool,
    pub usage_flags: u32,
}

#[derive(Debug)]
pub struct TextureData<'a> {
    pub width: u32,
    pub height: u32,
    pub depth: u32,
    pub array_index: u32,
    pub mip_map_level: u8,
    pub data: &'a [u8],
}
