//! null implementation

use crate::app_interface::NativeSurfacePtr;
use crate::buffer::BufferCreateInfo;
use crate::command_buffer::CommandBuffer;
use crate::descriptors::{
    DescriptorPoolCreateInfo, DescriptorSetLayoutCreateInfo, DescriptorSetUpdateInfo,
};
use crate::device::{
    DeviceCompressedTextureSupport, DeviceError, DeviceInfo, DeviceResult, SwapChainAcquireResult,
};
use crate::framebuffer::FramebufferCreateInfo;
use crate::instance::NativeInstancePtr;
use crate::pipeline::PipelineCreateInfo;
use crate::render_pass::RenderPassCreateInfo;
use crate::sampler::SamplerCreateInfo;
use crate::shader::ShaderCreateInfo;
use crate::texture::{TextureCreateInfo, TextureData};
use crate::types::{
    BufferHandle, DescriptorPoolHandle, DescriptorSetHandle, DescriptorSetLayoutHandle, DeviceSize,
    FramebufferHandle, PipelineHandle, SamplerHandle, ShaderHandle, SwapChainHandle, TextureHandle,
};
use modus_core::handle_map::Handle;
use modus_core::sync::ThreadOwned;
use std::sync::Arc;

#[derive(Debug)]
pub(crate) struct Instance {}

impl crate::instance::Instance for Instance {
    fn create_device(
        self: Arc<Self>,
        _render_surface: &dyn crate::app_interface::RenderSurfaceInterface,
    ) -> Result<Arc<dyn crate::device::Device>, DeviceError> {
        Ok(Arc::new(Device::new(self)))
    }

    unsafe fn native_ptr(&self) -> NativeInstancePtr {
        0xDEAD as NativeInstancePtr
    }

    unsafe fn destroy_render_surface(&self, _surface: NativeSurfacePtr) {}
}

#[derive(Debug)]
pub(crate) struct Device {
    _instance: Arc<Instance>,
    device_info: DeviceInfo,
    thread_owned: ThreadOwned<()>,
}

impl Device {
    pub(crate) fn new(instance: Arc<Instance>) -> Self {
        Self {
            device_info: DeviceInfo {
                min_cbuffer_size: 0,
                max_cbuffer_size: u32::MAX,
                max_texture_size: 8192,
                max_texture_depth: 256,
                sbuffer_offset_alignment: 16,
                cbuffer_offset_alignment: 16,
                compressed_texture_support: DeviceCompressedTextureSupport {
                    astc: true,
                    etc2: true,
                    s3tc: true,
                },
            },
            thread_owned: ThreadOwned::new(()),
            _instance: instance,
        }
    }
}

#[derive(Debug)]
pub struct RenderSurfaceInterface {}

impl crate::app_interface::RenderSurfaceInterface for RenderSurfaceInterface {
    unsafe fn native_surface_handle(&self) -> NativeSurfacePtr {
        0x0 as NativeInstancePtr
    }

    fn surface_size(&self) -> (u32, u32) {
        (1280, 720)
    }
}

#[allow(unused_variables)]
impl crate::device::Device for Device {
    fn device_info(&self) -> &DeviceInfo {
        &self.device_info
    }

    fn begin_frame(&self) -> DeviceResult<()> {
        self.thread_owned.try_borrow()?;
        Ok(())
    }

    fn end_frame(&self, _tex: TextureHandle, _swap_chain: SwapChainHandle) -> DeviceResult<()> {
        self.thread_owned.try_borrow()?;
        Ok(())
    }

    fn create_swap_chain(
        &self,
        _surface_interface: &dyn crate::app_interface::RenderSurfaceInterface,
    ) -> Result<SwapChainHandle, DeviceError> {
        self.thread_owned.try_borrow()?;
        // SAFETY: Is okay since we will never use this handle for anything
        Ok(unsafe { Handle::new_unchecked_alive(1245, 12).into() })
    }

    fn destroy_swap_chain(&self, _h: SwapChainHandle) -> Result<(), DeviceError> {
        self.thread_owned.try_borrow()?;
        Ok(())
    }

    fn swap_chain_size(&self, _h: SwapChainHandle) -> Result<(u32, u32), DeviceError> {
        self.thread_owned.try_borrow()?;
        Ok((1280, 720))
    }

    fn acquire_swap_chain_image(
        &self,
        h: SwapChainHandle,
        surface_interface: &dyn crate::app_interface::RenderSurfaceInterface,
    ) -> DeviceResult<SwapChainAcquireResult> {
        Ok(SwapChainAcquireResult::Ok)
    }

    fn present_swap_chain(&self, h: SwapChainHandle) -> DeviceResult<()> {
        Ok(())
    }

    fn create_texture(
        &self,
        _info: &TextureCreateInfo,
        _d: &[TextureData],
    ) -> Result<TextureHandle, DeviceError> {
        self.thread_owned.try_borrow()?;
        Ok(unsafe { Handle::new_unchecked_alive(1245, 12).into() })
    }

    fn destroy_texture(&self, _h: TextureHandle) -> Result<(), DeviceError> {
        self.thread_owned.try_borrow()?;
        Ok(())
    }

    fn update_texture(&self, h: TextureHandle, data: &[TextureData]) -> Result<(), DeviceError> {
        Ok(())
    }

    fn create_framebuffer(
        &self,
        _info: &FramebufferCreateInfo,
    ) -> Result<FramebufferHandle, DeviceError> {
        self.thread_owned.try_borrow()?;
        Ok(unsafe { Handle::new_unchecked_alive(1245, 12).into() })
    }

    fn destroy_framebuffer(&self, _h: FramebufferHandle) -> Result<(), DeviceError> {
        self.thread_owned.try_borrow()?;
        Ok(())
    }

    fn create_render_pass(
        &self,
        _h: &RenderPassCreateInfo,
    ) -> Result<crate::types::RenderPassHandle, DeviceError> {
        self.thread_owned.try_borrow()?;
        Ok(unsafe { Handle::new_unchecked_alive(1246, 12).into() })
    }

    fn destroy_render_pass(&self, _h: crate::types::RenderPassHandle) -> Result<(), DeviceError> {
        self.thread_owned.try_borrow()?;
        Ok(())
    }

    fn create_shader(&self, _info: &ShaderCreateInfo) -> Result<ShaderHandle, DeviceError> {
        self.thread_owned.try_borrow()?;
        Ok(unsafe { Handle::new_unchecked_alive(1246, 12).into() })
    }

    fn destroy_shader(&self, _h: ShaderHandle) -> Result<(), DeviceError> {
        self.thread_owned.try_borrow()?;
        Ok(())
    }

    fn create_pipeline(&self, _info: &PipelineCreateInfo) -> Result<PipelineHandle, DeviceError> {
        self.thread_owned.try_borrow()?;
        Ok(unsafe { Handle::new_unchecked_alive(1246, 12).into() })
    }

    fn destroy_pipeline(&self, _h: PipelineHandle) -> Result<(), DeviceError> {
        self.thread_owned.try_borrow()?;
        Ok(())
    }

    fn create_descriptor_set_layout(
        &self,
        _info: &DescriptorSetLayoutCreateInfo,
    ) -> Result<DescriptorSetLayoutHandle, DeviceError> {
        self.thread_owned.try_borrow()?;
        Ok(unsafe { Handle::new_unchecked_alive(1246, 12).into() })
    }

    fn destroy_descriptor_set_layout(
        &self,
        _h: DescriptorSetLayoutHandle,
    ) -> Result<(), DeviceError> {
        self.thread_owned.try_borrow()?;
        Ok(())
    }

    fn create_buffer(&self, _info: &BufferCreateInfo) -> Result<BufferHandle, DeviceError> {
        self.thread_owned.try_borrow()?;
        Ok(unsafe { Handle::new_unchecked_alive(1246, 12).into() })
    }

    fn destroy_buffer(&self, _h: BufferHandle) -> Result<(), DeviceError> {
        self.thread_owned.try_borrow()?;
        Ok(())
    }

    fn create_sampler(&self, _info: &SamplerCreateInfo) -> DeviceResult<SamplerHandle> {
        self.thread_owned.try_borrow()?;
        Ok(unsafe { Handle::new_unchecked_alive(1246, 12).into() })
    }

    fn destroy_sampler(&self, _h: SamplerHandle) -> DeviceResult<()> {
        self.thread_owned.try_borrow()?;
        Ok(())
    }

    fn create_descriptor_pool(
        &self,
        _info: &DescriptorPoolCreateInfo,
    ) -> DeviceResult<DescriptorPoolHandle> {
        self.thread_owned.try_borrow()?;
        Ok(unsafe { Handle::new_unchecked_alive(1246, 12).into() })
    }

    fn destroy_descriptor_pool(&self, _h: DescriptorPoolHandle) -> DeviceResult<()> {
        self.thread_owned.try_borrow()?;
        Ok(())
    }

    fn create_descriptor_set(
        &self,
        _layout: DescriptorSetLayoutHandle,
        _pool: DescriptorPoolHandle,
        _values: Option<DescriptorSetUpdateInfo>,
    ) -> DeviceResult<DescriptorSetHandle> {
        self.thread_owned.try_borrow()?;
        Ok(unsafe { Handle::new_unchecked_alive(1246, 12).into() })
    }

    fn update_descriptor_set(
        &self,
        _h: DescriptorSetHandle,
        _value: DescriptorSetUpdateInfo,
    ) -> DeviceResult<()> {
        self.thread_owned.try_borrow()?;
        Ok(())
    }

    fn destroy_descriptor_set(&self, _h: DescriptorSetHandle) -> DeviceResult<()> {
        self.thread_owned.try_borrow()?;
        Ok(())
    }

    fn update_buffer(
        &self,
        _h: BufferHandle,
        _offset: DeviceSize,
        _data: *const u8,
        _size: DeviceSize,
    ) -> Result<(), DeviceError> {
        Ok(())
    }

    fn execute_commands(
        &self,
        _f: &mut dyn FnMut(&'_ mut dyn CommandBuffer) -> Result<(), DeviceError>,
    ) -> Result<(), DeviceError> {
        Ok(())
    }
}
