use crate::device::{DeviceError, DeviceResult};
use crate::sampler::SamplerCreateInfo;
use crate::types::SamplerHandle;
use crate::vulkan::device::Device;
use crate::vulkan::types::{filter_to_vk, sampler_address_mode_to_vk, sampler_mipmap_mode_to_vk};
use crate::vulkan::utils::{
    vk_result_to_str, DeferredResourceDeleter, DeferredResourceDeleterBuilder, HashedResourceMap,
    VkSamplerHandle,
};
use std::ptr::null_mut;
use vulkan_sys::*;

//TODO: Hash sampler create info

#[derive(Debug)]
pub(crate) struct Sampler {
    pub(crate) handle: VkSamplerHandle,
}

impl Sampler {
    pub(crate) fn create(device: &Device, info: &SamplerCreateInfo) -> DeviceResult<Self> {
        let vk_info = VkSamplerCreateInfo {
            sType: VkStructureType::VK_STRUCTURE_TYPE_SAMPLER_CREATE_INFO,
            pNext: null_mut(),
            flags: 0,
            magFilter: filter_to_vk(info.mag_filter),
            minFilter: filter_to_vk(info.min_filter),
            mipmapMode: sampler_mipmap_mode_to_vk(info.mipmap_mode),
            addressModeU: sampler_address_mode_to_vk(info.address_mode_u),
            addressModeV: sampler_address_mode_to_vk(info.address_mode_v),
            addressModeW: sampler_address_mode_to_vk(info.address_mode_w),
            mipLodBias: 0.0,
            anisotropyEnable: info.anisotropy as u32,
            maxAnisotropy: info.max_anisotropy as f32,
            compareEnable: 0,
            compareOp: VkCompareOp::VK_COMPARE_OP_NEVER,
            minLod: 0.0,
            maxLod: VK_LOD_CLAMP_NONE as f32,
            borderColor: VkBorderColor::VK_BORDER_COLOR_FLOAT_TRANSPARENT_BLACK,
            unnormalizedCoordinates: 0,
        };

        let mut handle = VkSamplerHandle::null();
        unsafe {
            let r = vkCreateSampler(
                device.state.logical_device.value(),
                &vk_info,
                null_mut(),
                handle.ptr_mut(),
            );
            if r != VkResult::VK_SUCCESS {
                return Err(DeviceError::Api(format!(
                    "Failed to create sampler:{}",
                    vk_result_to_str(r)
                )));
            }
        }
        Ok(Sampler { handle })
    }

    fn destroy(&mut self, device: VkDevice) {
        unsafe {
            self.handle.destroy_with(|s| {
                vkDestroySampler(device, s, null_mut());
            });
        }
    }
}

pub(crate) type SamplerMap = HashedResourceMap<SamplerHandle, SamplerCreateInfo, Sampler>;

pub(crate) struct SamplerDeleter {
    device: VkDevice,
}

impl SamplerDeleter {
    pub(crate) fn new(device: &Device) -> Self {
        Self {
            device: device.state.logical_device.value(),
        }
    }
}

impl DeferredResourceDeleter<Sampler> for SamplerDeleter {
    fn delete(&mut self, sampler: &mut Sampler) {
        sampler.destroy(self.device);
    }
}

impl DeferredResourceDeleterBuilder<'_, Sampler> for Sampler {
    type Deleter = SamplerDeleter;
    fn create_deleter(device: &Device) -> Self::Deleter {
        Self::Deleter::new(device)
    }
}
