use crate::buffer::BufferCreateInfo;
use crate::device::DeviceError;
use crate::types::{BufferHandle, BufferType, DeviceSize};
use crate::vulkan::device::Device;
use crate::vulkan::types::memory_type_to_vma;
use crate::vulkan::utils::vk_result_to_str;
use crate::vulkan::utils::{
    vk_zero_init, DeferredResourceDeleter, DeferredResourceDeleterBuilder, ResourceMap,
    VkBufferHandle, VmaAllocationHandle,
};
use std::ffi::c_void;
use std::ptr::null_mut;
use vulkan_sys::*;

pub(crate) struct BufferDropScope<'a> {
    pub(crate) buffer: Buffer,
    pub(crate) device: &'a Device,
}

impl<'a> Drop for BufferDropScope<'a> {
    fn drop(&mut self) {
        self.buffer.destroy(
            self.device.state.logical_device.value(),
            self.device.state.vma.value(),
        );
    }
}

#[derive(Debug)]
pub(crate) struct Buffer {
    pub(crate) handle: VkBufferHandle,
    allocation: VmaAllocationHandle,
    pub(crate) size: DeviceSize,
}

fn buffer_type_to_vk_usage(t: BufferType) -> u32 {
    match t {
        BufferType::Uniform => VkBufferUsageFlagBits::VK_BUFFER_USAGE_UNIFORM_BUFFER_BIT as u32,
        BufferType::Storage => VkBufferUsageFlagBits::VK_BUFFER_USAGE_STORAGE_BUFFER_BIT as u32,
        BufferType::Index => VkBufferUsageFlagBits::VK_BUFFER_USAGE_INDEX_BUFFER_BIT as u32,
        BufferType::Vertex => VkBufferUsageFlagBits::VK_BUFFER_USAGE_VERTEX_BUFFER_BIT as u32,
    }
}

impl Buffer {
    pub(crate) fn create_vk(
        device: &Device,
        size: DeviceSize,
        usage_flags: u32,
        memory_flags: VmaMemoryUsage,
        sharing_mode: VkSharingMode,
    ) -> Result<Buffer, DeviceError> {
        let mut buffer_info = vk_zero_init::<VkBufferCreateInfo>();
        buffer_info.sType = VkStructureType::VK_STRUCTURE_TYPE_BUFFER_CREATE_INFO;
        buffer_info.size = size;
        buffer_info.usage = usage_flags;
        buffer_info.sharingMode = sharing_mode;

        if memory_flags == VmaMemoryUsage::VMA_MEMORY_USAGE_GPU_ONLY {
            buffer_info.usage |= VkBufferUsageFlagBits::VK_BUFFER_USAGE_TRANSFER_DST_BIT as u32;
        }

        let mut alloc_info = vk_zero_init::<VmaAllocationCreateInfo>();
        alloc_info.usage = memory_flags;
        if memory_flags == VmaMemoryUsage::VMA_MEMORY_USAGE_CPU_TO_GPU {
            alloc_info.flags |=
                VmaAllocationCreateFlagBits::VMA_ALLOCATION_CREATE_MAPPED_BIT as u32;
        }

        let mut buffer_handle = VkBufferHandle::null();
        let mut alloc_handle = VmaAllocationHandle::null();
        unsafe {
            let alloc_result = vmaCreateBuffer(
                device.state.vma.value(),
                &buffer_info,
                &alloc_info,
                buffer_handle.ptr_mut(),
                alloc_handle.ptr_mut(),
                null_mut(),
            );
            if alloc_result != VkResult::VK_SUCCESS {
                return Err(DeviceError::Api(format!(
                    "Failed to create buffer: {}",
                    vk_result_to_str(alloc_result)
                )));
            }
        }

        let buffer = Buffer {
            handle: buffer_handle,
            allocation: alloc_handle,
            size,
        };

        Ok(buffer)
    }

    pub(crate) fn create(device: &Device, info: &BufferCreateInfo) -> Result<Buffer, DeviceError> {
        Self::create_vk(
            device,
            info.size,
            buffer_type_to_vk_usage(info.btype),
            memory_type_to_vma(info.memory_type),
            VkSharingMode::VK_SHARING_MODE_EXCLUSIVE,
        )
    }

    pub(crate) fn destroy(&mut self, device: VkDevice, vma: VmaAllocator) {
        unsafe {
            self.handle.destroy_with(|b| {
                vkDestroyBuffer(device, b, null_mut());
            });
            self.allocation.destroy_with(|a| {
                vmaFreeMemory(vma, a);
            });
        }
    }

    pub(crate) fn update(
        &mut self,
        device: &Device,
        offset: DeviceSize,
        data: *const u8,
        size: DeviceSize,
    ) -> Result<(), DeviceError> {
        crate::profile_scope!();

        if size > self.size || offset + size > self.size {
            return Err(DeviceError::Api(
                "Offset + size or size exceeds buffer dimensions".to_string(),
            ));
        }

        let vma = device.state.vma.value();
        let mut alloc_info = vk_zero_init::<VmaAllocationInfo>();
        let mut mem_flags = 0_u32;
        unsafe {
            vmaGetAllocationInfo(vma, self.allocation.value(), &mut alloc_info);
            vmaGetMemoryTypeProperties(vma, alloc_info.memoryType, &mut mem_flags);
        }
        if (mem_flags & VkMemoryPropertyFlagBits::VK_MEMORY_PROPERTY_HOST_VISIBLE_BIT as u32) != 0 {
            // cpu visible we cam map
            if !alloc_info.pMappedData.is_null() {
                // memory is persitently mapped, no mapping required
                unsafe {
                    let u8_ptr = (alloc_info.pMappedData as *mut u8).offset(offset as isize);
                    std::ptr::copy_nonoverlapping(data, u8_ptr, size as usize);
                    // flush range if we are not host coherent
                    if (mem_flags
                        & VkMemoryPropertyFlagBits::VK_MEMORY_PROPERTY_HOST_COHERENT_BIT as u32)
                        == 0
                    {
                        vmaFlushAllocation(vma, self.allocation.value(), offset, size);
                    }
                }
            } else {
                // Default mapping
                let mut buffer_ptr: *mut c_void = null_mut();
                unsafe {
                    let r = vmaMapMemory(vma, self.allocation.value(), &mut buffer_ptr);
                    if r != VkResult::VK_SUCCESS {
                        return Err(DeviceError::Api(format!(
                            "Failed to map buffer: {}",
                            vk_result_to_str(r)
                        )));
                    }
                    let u8_ptr = (buffer_ptr as *mut u8).offset(offset as isize);
                    std::ptr::copy_nonoverlapping(data, u8_ptr, size as usize);
                    vmaUnmapMemory(vma, self.allocation.value());
                }
            }
        } else {
            // perform transfer to GPU memory
            let mut uploader = device.resources.uploader.try_borrow_mut()?;
            let dst_buffer = self.handle.value();
            uploader.upload(
                device,
                unsafe { std::slice::from_raw_parts(data, size as usize) },
                |_, src_buffer, src_offset, cmd_buffer| {
                    unsafe {
                        let copy_info = VkBufferCopy {
                            srcOffset: src_offset,
                            dstOffset: offset,
                            size,
                        };
                        vkCmdCopyBuffer(cmd_buffer, src_buffer, dst_buffer, 1, &copy_info);
                    }
                    Ok(())
                },
            )?;
        }
        Ok(())
    }
}

pub(crate) type BufferMap = ResourceMap<BufferHandle, Buffer>;

pub(crate) struct BufferDeleter {
    device: VkDevice,
    vma: VmaAllocator,
}

impl BufferDeleter {
    pub(crate) fn new(device: &Device) -> Self {
        Self {
            device: device.state.logical_device.value(),
            vma: device.state.vma.value(),
        }
    }
}

impl DeferredResourceDeleter<Buffer> for BufferDeleter {
    fn delete(&mut self, buffer: &mut Buffer) {
        buffer.destroy(self.device, self.vma);
    }
}

impl DeferredResourceDeleterBuilder<'_, Buffer> for Buffer {
    type Deleter = BufferDeleter;
    fn create_deleter(device: &Device) -> Self::Deleter {
        Self::Deleter::new(device)
    }
}
