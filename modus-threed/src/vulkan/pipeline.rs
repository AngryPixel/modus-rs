use crate::device::DeviceError;
use crate::pipeline::{
    ColorBlendState, DepthBias, DepthState, PipelineCreateInfo, StencilFaceState, StencilState,
};
use crate::render_pass::MAX_ATTACHMENT_COUNT;
use crate::types::{
    BlendFactor, BlendOp, CompareOp, DescriptorSetLayoutHandle, LogicOp, PipelineHandle,
    RenderPassHandle, ShaderHandle, StencilOp,
};
use crate::vulkan::descriptors::DescriptorSetLayoutMap;
use crate::vulkan::device::Device;
use crate::vulkan::render_pass::RenderPassMap;
use crate::vulkan::shader::ShaderMap;
use crate::vulkan::texture::sample_count_to_vk_sample;
use crate::vulkan::types::{
    blend_factor_to_vk, blend_op_to_vk, compare_op_to_vk, cull_mode_to_vk, front_face_to_vk,
    logic_op_to_vk, pipeline_stage_flags_to_vk, polygon_mode_to_vk, primitive_to_vk,
    shader_type_to_vk, stencil_op_to_vk,
};
use crate::vulkan::utils::{
    vk_result_to_str, vk_zero_init, DeferredResourceDeleter, DeferredResourceDeleterBuilder,
    ResourceMap, VkPipelineHandle, VkPipelineLayoutHandle,
};
use modus_core::ArrayVec;
use std::cell::RefMut;
use std::ffi::CString;
use std::ptr::null_mut;
use vulkan_sys::*;

const MAX_SHADER_COUNT: usize = 4;
const MAX_DESCRIPTOR_SET_LAYOUTS_COUNT: usize = 8;
#[derive(Debug)]
pub(crate) struct Pipeline {
    pub(crate) pipeline: VkPipelineHandle,
    pub(crate) layout: VkPipelineLayoutHandle,
    render_pass: RenderPassHandle,
    shaders: ArrayVec<ShaderHandle, MAX_SHADER_COUNT>,
    descriptor_set_layouts: ArrayVec<DescriptorSetLayoutHandle, MAX_DESCRIPTOR_SET_LAYOUTS_COUNT>,
}

impl Pipeline {
    pub(crate) fn create(
        info: &PipelineCreateInfo,
        device: &Device,
        render_passes: &mut RenderPassMap,
        shaders: &mut ShaderMap,
        descriptor_set_layouts: &mut DescriptorSetLayoutMap,
    ) -> Result<Pipeline, DeviceError> {
        let render_pass = if let Some(r) = render_passes.get(info.render_pass) {
            r.handle.value()
        } else {
            return Err(DeviceError::Api(
                "Failed to resolve render pass handle".to_string(),
            ));
        };

        // --- Shader States -----------------------------------------------------------------------
        let mut shader_handles = ArrayVec::<ShaderHandle, MAX_SHADER_COUNT>::new();
        let mut shader_states =
            ArrayVec::<VkPipelineShaderStageCreateInfo, MAX_SHADER_COUNT>::new();
        let mut shader_entry_points = ArrayVec::<CString, MAX_SHADER_COUNT>::new();
        let mut dsl_handles =
            ArrayVec::<DescriptorSetLayoutHandle, MAX_DESCRIPTOR_SET_LAYOUTS_COUNT>::new();

        for shader_state in info.shader_states.iter() {
            shader_entry_points.push(CString::new(shader_state.entry).unwrap_or_default());
        }

        for (index, shader_state) in info.shader_states.iter().enumerate() {
            let shader_module = if let Some(r) = shaders.get(shader_state.shader) {
                r.module.value()
            } else {
                return Err(DeviceError::Api(format!(
                    "Failed to resolve shader handle{}",
                    shader_state.shader
                )));
            };
            shader_handles.push(shader_state.shader);
            shader_states.push(VkPipelineShaderStageCreateInfo {
                sType: VkStructureType::VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO,
                pNext: null_mut(),
                flags: 0,
                stage: shader_type_to_vk(shader_state.stage),
                module: shader_module,
                pName: shader_entry_points[index].as_ptr(),
                pSpecializationInfo: null_mut(),
            });
        }
        let shader_stages = shader_states;

        // --- Vertex Input -----------------------------------------------------------------------
        // Disabled for now since we want to use shader buffer instead
        let vertex_input_state = VkPipelineVertexInputStateCreateInfo {
            sType: VkStructureType::VK_STRUCTURE_TYPE_PIPELINE_VERTEX_INPUT_STATE_CREATE_INFO,
            pNext: null_mut(),
            flags: 0,
            vertexBindingDescriptionCount: 0,
            pVertexBindingDescriptions: null_mut(),
            vertexAttributeDescriptionCount: 0,
            pVertexAttributeDescriptions: null_mut(),
        };

        // --- Input Assembly ---------------------------------------------------------------------
        let input_assembly_state = VkPipelineInputAssemblyStateCreateInfo {
            sType: VkStructureType::VK_STRUCTURE_TYPE_PIPELINE_INPUT_ASSEMBLY_STATE_CREATE_INFO,
            pNext: null_mut(),
            flags: 0,
            topology: primitive_to_vk(info.input_assembly_state.primitive),
            primitiveRestartEnable: info.input_assembly_state.primitive_restart as u32,
        };

        // --- ViewPort & Scissors ----------------------------------------------------------------
        // These are always set to be dynamic but still need an initial value
        // TODO: Does this need to be declared?
        let view_port = VkViewport {
            x: 0.0,
            y: 0.0,
            width: 1280.0,
            height: 720.0,
            minDepth: 0.0,
            maxDepth: 1.0,
        };

        let scissor = VkRect2D {
            offset: VkOffset2D { x: 0, y: 0 },
            extent: VkExtent2D {
                width: 1280,
                height: 720,
            },
        };

        let viewport_state = VkPipelineViewportStateCreateInfo {
            sType: VkStructureType::VK_STRUCTURE_TYPE_PIPELINE_VIEWPORT_STATE_CREATE_INFO,
            pNext: null_mut(),
            flags: 0,
            viewportCount: 1,
            pViewports: &view_port,
            scissorCount: 1,
            pScissors: &scissor,
        };

        // --- Rasterizer ------------------------------------------------------------------------

        let depth_bias = info.rasterization_state.depth_bias.unwrap_or(DepthBias {
            constant_factor: 0.0,
            clamp: 0.0,
            slope_factor: 0.0,
        });

        let rasterizer_state = VkPipelineRasterizationStateCreateInfo {
            sType: VkStructureType::VK_STRUCTURE_TYPE_PIPELINE_RASTERIZATION_STATE_CREATE_INFO,
            pNext: null_mut(),
            flags: 0,
            depthClampEnable: info.rasterization_state.depth_clamp as u32,
            rasterizerDiscardEnable: info.rasterization_state.rasterizer_discard as u32,
            polygonMode: polygon_mode_to_vk(info.rasterization_state.polygon_mode),
            cullMode: cull_mode_to_vk(info.rasterization_state.cull_mode),
            frontFace: front_face_to_vk(info.rasterization_state.front_face),
            depthBiasEnable: info.rasterization_state.depth_bias.is_some() as u32,
            depthBiasConstantFactor: depth_bias.constant_factor,
            depthBiasClamp: depth_bias.clamp,
            depthBiasSlopeFactor: depth_bias.slope_factor,
            lineWidth: info.rasterization_state.line_width,
        };

        // --- Multi Sample -----------------------------------------------------------------------
        let multisample_state = VkPipelineMultisampleStateCreateInfo {
            sType: VkStructureType::VK_STRUCTURE_TYPE_PIPELINE_MULTISAMPLE_STATE_CREATE_INFO,
            pNext: null_mut(),
            flags: 0,
            rasterizationSamples: sample_count_to_vk_sample(info.multisample_state.samples),
            sampleShadingEnable: info.multisample_state.sample_shading as u32,
            minSampleShading: info.multisample_state.min_sample_shading,
            pSampleMask: null_mut(),
            alphaToCoverageEnable: info.multisample_state.alpha_to_coverage as u32,
            alphaToOneEnable: info.multisample_state.alpha_to_one as u32,
        };

        // --- Depth Stencil ----------------------------------------------------------------------

        let depth_state = info.depth_stencil_state.depth.unwrap_or(DepthState {
            write: false,
            compare_op: CompareOp::Always,
            depth_bounds: None,
        });
        let depth_bounds = depth_state.depth_bounds.unwrap_or((0.0, 1.0));

        let stencil_state = info.depth_stencil_state.stencil.unwrap_or(StencilState {
            front: StencilFaceState {
                fail_op: StencilOp::Keep,
                pass_op: StencilOp::Keep,
                depth_fail_op: StencilOp::Keep,
                compare_op: CompareOp::Never,
                compare_mask: 0,
                write_mask: 0,
                reference: 0,
            },
            back: StencilFaceState {
                fail_op: StencilOp::Keep,
                pass_op: StencilOp::Keep,
                depth_fail_op: StencilOp::Keep,
                compare_op: CompareOp::Never,
                compare_mask: 0,
                write_mask: 0,
                reference: 0,
            },
        });

        let depth_stencil_state = VkPipelineDepthStencilStateCreateInfo {
            sType: VkStructureType::VK_STRUCTURE_TYPE_PIPELINE_DEPTH_STENCIL_STATE_CREATE_INFO,
            pNext: null_mut(),
            flags: 0,
            depthTestEnable: info.depth_stencil_state.depth.is_some() as u32,
            depthWriteEnable: depth_state.write as u32,
            depthCompareOp: compare_op_to_vk(depth_state.compare_op),
            depthBoundsTestEnable: depth_state.depth_bounds.is_some() as u32,
            stencilTestEnable: 0,
            front: VkStencilOpState {
                failOp: stencil_op_to_vk(stencil_state.front.fail_op),
                passOp: stencil_op_to_vk(stencil_state.front.pass_op),
                depthFailOp: stencil_op_to_vk(stencil_state.front.depth_fail_op),
                compareOp: compare_op_to_vk(stencil_state.front.compare_op),
                compareMask: stencil_state.front.compare_mask,
                writeMask: stencil_state.front.write_mask,
                reference: stencil_state.front.reference,
            },
            back: VkStencilOpState {
                failOp: stencil_op_to_vk(stencil_state.back.fail_op),
                passOp: stencil_op_to_vk(stencil_state.back.pass_op),
                depthFailOp: stencil_op_to_vk(stencil_state.back.depth_fail_op),
                compareOp: compare_op_to_vk(stencil_state.back.compare_op),
                compareMask: stencil_state.back.compare_mask,
                writeMask: stencil_state.back.write_mask,
                reference: stencil_state.back.reference,
            },
            minDepthBounds: depth_bounds.0,
            maxDepthBounds: depth_bounds.1,
        };

        // --- Blending ---------------------------------------------------------------------------

        let mut attachment_blend_states =
            ArrayVec::<VkPipelineColorBlendAttachmentState, MAX_ATTACHMENT_COUNT>::new();

        for state in info.blend_state.attachments {
            let bs = state.state.unwrap_or(ColorBlendState {
                src_color_blend_factor: BlendFactor::Zero,
                dst_color_blend_factor: BlendFactor::Zero,
                color_blend_op: BlendOp::Add,
                src_alpha_blend_factor: BlendFactor::Zero,
                dst_alpha_blend_factor: BlendFactor::Zero,
                alpha_blend_op: BlendOp::Add,
            });
            attachment_blend_states.push(VkPipelineColorBlendAttachmentState {
                blendEnable: state.state.is_some() as u32,
                srcColorBlendFactor: blend_factor_to_vk(bs.src_color_blend_factor),
                dstColorBlendFactor: blend_factor_to_vk(bs.dst_color_blend_factor),
                colorBlendOp: blend_op_to_vk(bs.color_blend_op),
                srcAlphaBlendFactor: blend_factor_to_vk(bs.dst_color_blend_factor),
                dstAlphaBlendFactor: blend_factor_to_vk(bs.dst_alpha_blend_factor),
                alphaBlendOp: blend_op_to_vk(bs.alpha_blend_op),
                colorWriteMask: state.color_mask,
            });
        }

        let logic_op = info.blend_state.logic_op.unwrap_or(LogicOp::Clear);
        let blend_state = VkPipelineColorBlendStateCreateInfo {
            sType: VkStructureType::VK_STRUCTURE_TYPE_PIPELINE_COLOR_BLEND_STATE_CREATE_INFO,
            pNext: null_mut(),
            flags: 0,
            logicOpEnable: info.blend_state.logic_op.is_some() as u32,
            logicOp: logic_op_to_vk(logic_op),
            attachmentCount: attachment_blend_states.len() as u32,
            pAttachments: attachment_blend_states.as_ptr(),
            blendConstants: info.blend_state.blend_constants,
        };

        // --- Dynamic State -----------------------------------------------------------------------
        let dynamic_states = [
            VkDynamicState::VK_DYNAMIC_STATE_VIEWPORT,
            VkDynamicState::VK_DYNAMIC_STATE_SCISSOR,
        ];
        let dynamic_state = VkPipelineDynamicStateCreateInfo {
            sType: VkStructureType::VK_STRUCTURE_TYPE_PIPELINE_DYNAMIC_STATE_CREATE_INFO,
            pNext: null_mut(),
            flags: 0,
            dynamicStateCount: dynamic_states.len() as u32,
            pDynamicStates: dynamic_states.as_ptr(),
        };

        // --- Pipeline Layout ---------------------------------------------------------------------
        let mut pipeline_layout = VkPipelineLayoutHandle::null();
        let pipeline_layout_create_info = if let Some(dsl) = info.descriptor_set_layouts {
            let mut vk_dsls =
                ArrayVec::<VkDescriptorSetLayout, MAX_DESCRIPTOR_SET_LAYOUTS_COUNT>::new();
            let mut vk_push_constants =
                ArrayVec::<VkPushConstantRange, MAX_DESCRIPTOR_SET_LAYOUTS_COUNT>::new();

            for handle in dsl {
                if let Some(h) = descriptor_set_layouts.get(*handle) {
                    vk_dsls.push(h.handle.value());
                    dsl_handles.push(*handle);

                    for push_constant in &h.push_constants {
                        vk_push_constants.push(VkPushConstantRange {
                            stageFlags: pipeline_stage_flags_to_vk(push_constant.stages),
                            offset: push_constant.offset,
                            size: push_constant.size,
                        });
                    }
                } else {
                    return Err(DeviceError::Api(format!(
                        "Failed to resolve descriptor set handle: {}",
                        *handle
                    )));
                }
            }

            VkPipelineLayoutCreateInfo {
                sType: VkStructureType::VK_STRUCTURE_TYPE_PIPELINE_LAYOUT_CREATE_INFO,
                pNext: null_mut(),
                flags: 0,
                setLayoutCount: vk_dsls.len() as u32,
                pSetLayouts: vk_dsls.as_ptr(),
                pushConstantRangeCount: vk_push_constants.len() as u32,
                pPushConstantRanges: vk_push_constants.as_ptr(),
            }
        } else {
            VkPipelineLayoutCreateInfo {
                sType: VkStructureType::VK_STRUCTURE_TYPE_PIPELINE_LAYOUT_CREATE_INFO,
                pNext: null_mut(),
                flags: 0,
                setLayoutCount: 0,
                pSetLayouts: null_mut(),
                pushConstantRangeCount: 0,
                pPushConstantRanges: null_mut(),
            }
        };

        unsafe {
            let r = vkCreatePipelineLayout(
                device.state.logical_device.value(),
                &pipeline_layout_create_info,
                null_mut(),
                pipeline_layout.ptr_mut(),
            );
            if r != VkResult::VK_SUCCESS {
                return Err(DeviceError::Api(format!(
                    "Failed to create pipeline layout:{}",
                    vk_result_to_str(r)
                )));
            }
        }

        // --- Pipeline --------------------------------------------------------------------------

        let pipeline_create_info = VkGraphicsPipelineCreateInfo {
            sType: VkStructureType::VK_STRUCTURE_TYPE_GRAPHICS_PIPELINE_CREATE_INFO,
            pNext: null_mut(),
            flags: 0,
            stageCount: shader_stages.len() as u32,
            pStages: shader_stages.as_ptr(),
            pVertexInputState: &vertex_input_state,
            pInputAssemblyState: &input_assembly_state,
            pTessellationState: null_mut(),
            pViewportState: &viewport_state,
            pRasterizationState: &rasterizer_state,
            pMultisampleState: &multisample_state,
            pDepthStencilState: &depth_stencil_state,
            pColorBlendState: &blend_state,
            pDynamicState: &dynamic_state,
            layout: pipeline_layout.value(),
            renderPass: render_pass,
            subpass: 0,
            basePipelineHandle: vk_zero_init::<VkPipeline>(),
            basePipelineIndex: -1,
        };

        let mut pipeline = VkPipelineHandle::null();
        unsafe {
            //TODO: pipeline cache
            let device = device.state.logical_device.value();
            let r = vkCreateGraphicsPipelines(
                device,
                vk_zero_init::<VkPipelineCache>(),
                1,
                &pipeline_create_info,
                null_mut(),
                pipeline.ptr_mut(),
            );
            if r != VkResult::VK_SUCCESS {
                pipeline_layout.destroy_with(|p| {
                    vkDestroyPipelineLayout(device, p, null_mut());
                });
                return Err(DeviceError::Api(format!(
                    "Failed to create graphics pipeline: {}",
                    vk_result_to_str(r)
                )));
            }
        }

        render_passes.acquire(info.render_pass);
        for shader in &shader_handles {
            shaders.acquire(*shader);
        }
        for dsl in &dsl_handles {
            descriptor_set_layouts.acquire(*dsl);
        }

        Ok(Pipeline {
            pipeline,
            layout: pipeline_layout,
            render_pass: info.render_pass,
            shaders: shader_handles,
            descriptor_set_layouts: dsl_handles,
        })
    }

    fn destroy(
        &mut self,
        device: VkDevice,
        render_passes: &mut RenderPassMap,
        shaders: &mut ShaderMap,
        descriptor_set_layouts: &mut DescriptorSetLayoutMap,
    ) {
        render_passes.release(self.render_pass);
        for shader in &self.shaders {
            shaders.release(*shader);
        }
        for dsl in &self.descriptor_set_layouts {
            descriptor_set_layouts.release(*dsl);
        }
        unsafe {
            self.pipeline.destroy_with(|p| {
                vkDestroyPipeline(device, p, null_mut());
            });
            self.layout.destroy_with(|l| {
                vkDestroyPipelineLayout(device, l, null_mut());
            });
        }
    }
}

pub(crate) type PipelineMap = ResourceMap<PipelineHandle, Pipeline>;

#[derive(Debug)]
pub(crate) struct PipelineDeleter<'a> {
    shaders: RefMut<'a, ShaderMap>,
    render_passes: RefMut<'a, RenderPassMap>,
    descriptor_set_layouts: RefMut<'a, DescriptorSetLayoutMap>,
    device: VkDevice,
}

impl<'a> PipelineDeleter<'a> {
    pub(crate) fn new(device: &'a Device) -> Self {
        Self {
            shaders: device.resources.shaders.borrow_mut(),
            render_passes: device.resources.render_passes.borrow_mut(),
            descriptor_set_layouts: device.resources.descriptor_set_layouts.borrow_mut(),
            device: device.state.logical_device.value(),
        }
    }
}

impl<'a> DeferredResourceDeleter<Pipeline> for PipelineDeleter<'a> {
    fn delete(&mut self, pipeline: &mut Pipeline) {
        pipeline.destroy(
            self.device,
            &mut self.render_passes,
            &mut self.shaders,
            &mut self.descriptor_set_layouts,
        );
    }
}

impl<'a> DeferredResourceDeleterBuilder<'a, Pipeline> for Pipeline {
    type Deleter = PipelineDeleter<'a>;
    fn create_deleter(device: &'a Device) -> Self::Deleter {
        Self::Deleter::new(device)
    }
}
