use crate::app_interface::RenderSurfaceInterface;
use crate::buffer::BufferCreateInfo;
use crate::command_buffer::CommandBuffer;
use crate::descriptors::{
    DescriptorPoolCreateInfo, DescriptorSetLayoutCreateInfo, DescriptorSetUpdateInfo,
};
use crate::device::{
    DeviceCompressedTextureSupport, DeviceError, DeviceInfo, SwapChainAcquireResult,
};
use crate::device::{DeviceError as DevError, DeviceResult};
use crate::framebuffer::FramebufferCreateInfo;
use crate::pipeline::PipelineCreateInfo;
use crate::sampler::SamplerCreateInfo;
use crate::shader::ShaderCreateInfo;
use crate::texture::{TextureCreateInfo, TextureData};
use crate::types::{
    BufferHandle, DescriptorPoolHandle, DescriptorSetHandle, DescriptorSetLayoutHandle, DeviceSize,
    FramebufferHandle, PipelineHandle, SamplerHandle, ShaderHandle, SwapChainHandle, TextureHandle,
};
use crate::vulkan::buffer::{Buffer, BufferMap};
use crate::vulkan::command_buffer::CommandBuffer as CmdBuffer;
use crate::vulkan::descriptors::{
    DescriptorPoolMap, DescriptorPoolV2, DescriptorSetLayout, DescriptorSetLayoutKey,
    DescriptorSetLayoutMap, DescriptorSetV2, DescriptorSetV2Map,
};
use crate::vulkan::device_uploader::DeviceUploader;
use crate::vulkan::framebuffer::{Framebuffer, FramebufferMap};
use crate::vulkan::instance::Instance;
use crate::vulkan::physical_device::{build_required_device_extension_list, PhysicalDevice};
use crate::vulkan::pipeline::{Pipeline, PipelineMap};
use crate::vulkan::render_pass::{RenderPass, RenderPassMap};
use crate::vulkan::sampler::{Sampler, SamplerMap};
use crate::vulkan::shader::{Shader, ShaderMap};
use crate::vulkan::swap_chain::{SwapChainMap, SwapChainState};
use crate::vulkan::sync::{Fence, FrameSyncObjects};
use crate::vulkan::texture::{Texture, TextureMap};
use crate::vulkan::utils::{
    vk_api_version_1_0, vk_result_to_str, vk_zero_init, VkCommandBufferHandle, VkCommandPoolHandle,
    VkDeviceHandle, VkQueueHandle, VkSemaphoreHandle, VkVmaAllocHandle, MAX_FRAMES_IN_FLIGHT,
};
use modus_core::sync::Mutex;
use modus_core::sync::ThreadOwned;
use modus_core::ArrayVec;
use modus_log::prelude::*;
use std::ffi::CStr;
use std::ptr::null_mut;
use std::sync::Arc;
use vulkan_sys::*;

const MAX_QUEUE_STORAGE: usize = 4;

#[derive(Debug)]
pub(crate) struct DeviceState {
    pub(crate) vma: VkVmaAllocHandle,
    pub(crate) logical_device: VkDeviceHandle,
    pub(crate) _instance: Arc<crate::vulkan::instance::Instance>,
    pub(crate) physical_device: PhysicalDevice,
    device_queues: [Mutex<VkQueueHandle>; MAX_QUEUE_STORAGE],
}

impl DeviceState {
    pub(crate) fn graphics_queue(&self) -> &Mutex<VkQueueHandle> {
        &self.device_queues[self.physical_device.graphics_queue_index as usize]
    }

    pub(crate) fn presentation_queue(&self) -> &Mutex<VkQueueHandle> {
        &self.device_queues[self.physical_device.graphics_queue_index as usize]
    }
}

#[derive(Debug)]
pub(crate) struct FrameData {
    sync_objects: FrameSyncObjects,
    command_buffers: [VkCommandBufferHandle; MAX_FRAMES_IN_FLIGHT],
    command_pool: [VkCommandPoolHandle; MAX_FRAMES_IN_FLIGHT],
    active_command_buffer: VkCommandBuffer,
    active_index: usize,
}

#[derive(Debug)]
pub(crate) struct DeviceResources {
    pub(crate) uploader: ThreadOwned<DeviceUploader>,
    pub(crate) swap_chains: ThreadOwned<SwapChainMap>,
    pub(crate) textures: ThreadOwned<TextureMap>,
    pub(crate) render_passes: ThreadOwned<RenderPassMap>,
    pub(crate) framebuffers: ThreadOwned<FramebufferMap>,
    pub(crate) shaders: ThreadOwned<ShaderMap>,
    pub(crate) pipelines: ThreadOwned<PipelineMap>,
    pub(crate) descriptor_set_layouts: ThreadOwned<DescriptorSetLayoutMap>,
    pub(crate) descriptor_pools: ThreadOwned<DescriptorPoolMap>,
    pub(crate) descriptor_sets_v2: ThreadOwned<DescriptorSetV2Map>,
    pub(crate) buffers: ThreadOwned<BufferMap>,
    pub(crate) samplers: ThreadOwned<SamplerMap>,
}

#[derive(Debug)]
pub(crate) struct Device {
    pub(crate) frame_data: ThreadOwned<FrameData>,
    pub(crate) resources: DeviceResources,
    pub(crate) state: DeviceState,
    pub(crate) device_info: DeviceInfo,
}

impl FrameData {
    fn init(&mut self, device: &Device) -> DeviceResult<()> {
        self.create_command_pools(&device.state)?;
        self.sync_objects.init(device)?;
        Ok(())
    }

    fn destroy(&mut self, device: &Device) {
        self.sync_objects.destroy(device);
        self.destroy_command_pools(&device.state);
    }

    fn rebuild(&mut self, device: &Device) -> DeviceResult<()> {
        self.sync_objects.rebuild(device)?;
        self.reset_command_pools(&device.state)?;
        Ok(())
    }

    fn destroy_command_pools(&mut self, device_state: &DeviceState) {
        for i in 0..MAX_FRAMES_IN_FLIGHT {
            let cmd_buffer = &mut self.command_buffers[i];
            let cmd_pool = &mut self.command_pool[i];
            unsafe {
                let device = device_state.logical_device.value();
                cmd_buffer.destroy_with(|b| {
                    vkFreeCommandBuffers(device, cmd_pool.value(), 1, &b);
                });
                cmd_pool.destroy_with(|p| {
                    vkDestroyCommandPool(device, p, null_mut());
                });
            }
        }
    }

    fn reset_command_pools(&mut self, device_state: &DeviceState) -> DeviceResult<()> {
        for cmd_pool in &mut self.command_pool {
            unsafe {
                let device = device_state.logical_device.value();
                let r = vkResetCommandPool(device, cmd_pool.value(), 0);
                if r != VkResult::VK_SUCCESS {
                    return Err(DeviceError::Api(format!(
                        "Failed to reset command pool: {}",
                        vk_result_to_str(r)
                    )));
                }
            }
        }
        Ok(())
    }

    fn reset_and_swap_command_pool(&mut self, device_state: &DeviceState) -> DeviceResult<()> {
        self.active_index = (self.active_index + 1) % MAX_FRAMES_IN_FLIGHT;
        self.active_command_buffer = self.command_buffers[self.active_index].value();

        {
            crate::profile_scope!("Wait cmd fence");
            self.sync_objects.fences[self.active_index]
                .wait(device_state.logical_device.value(), u64::MAX)?;
            // self.sync_objects.fences[self.active_index].reset(device_state.logical_device.value())?;
        }

        let cmd_pool = &mut self.command_pool[self.active_index];
        unsafe {
            let device = device_state.logical_device.value();
            let r = vkResetCommandPool(device, cmd_pool.value(), 0);
            if r != VkResult::VK_SUCCESS {
                return Err(DeviceError::Api(format!(
                    "Failed to reset command pool {}",
                    vk_result_to_str(r)
                )));
            }
        }
        Ok(())
    }

    fn create_command_pools(&mut self, device_state: &DeviceState) -> Result<(), DevError> {
        let mut cmd_pool_create_info = vk_zero_init::<VkCommandPoolCreateInfo>();
        cmd_pool_create_info.sType = VkStructureType::VK_STRUCTURE_TYPE_COMMAND_POOL_CREATE_INFO;
        cmd_pool_create_info.queueFamilyIndex = device_state.physical_device.graphics_queue_index;
        cmd_pool_create_info.flags = 0;

        for (index, command_pool) in self.command_pool.iter_mut().enumerate() {
            unsafe {
                let cmd_pool_result = vkCreateCommandPool(
                    device_state.logical_device.value(),
                    &cmd_pool_create_info,
                    null_mut(),
                    command_pool.ptr_mut(),
                );
                if cmd_pool_result != VkResult::VK_SUCCESS {
                    return Err(DevError::Api(format!(
                        "Failed to create  command pool {}",
                        vk_result_to_str(cmd_pool_result)
                    )));
                }
                let cmd_buffer_create_info = VkCommandBufferAllocateInfo {
                    sType: VkStructureType::VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO,
                    pNext: null_mut(),
                    commandPool: command_pool.value(),
                    level: VkCommandBufferLevel::VK_COMMAND_BUFFER_LEVEL_PRIMARY,
                    commandBufferCount: 1,
                };

                let cmd_buffer_result = vkAllocateCommandBuffers(
                    device_state.logical_device.value(),
                    &cmd_buffer_create_info,
                    self.command_buffers[index].ptr_mut(),
                );
                if cmd_buffer_result != VkResult::VK_SUCCESS {
                    return Err(DevError::Api(format!(
                        "Failed to crate command buffer {}",
                        vk_result_to_str(cmd_buffer_result)
                    )));
                }
            }
        }
        Ok(())
    }

    fn current_acquire_semaphore(&self) -> &VkSemaphoreHandle {
        &self.sync_objects.image_available[self.active_index as usize]
    }

    fn current_render_finished_semaphore(&self) -> &VkSemaphoreHandle {
        &self.sync_objects.image_available[self.active_index as usize]
    }

    fn current_render_fence(&self) -> &Fence {
        &self.sync_objects.fences[self.active_index as usize]
    }
}

fn create_logical_device(
    instance: &Instance,
    physical_device: &PhysicalDevice,
) -> Result<VkDeviceHandle, VkResult> {
    const MAX_QUEUE_COUNT: usize = 2;
    let mut queue_create_info = [vk_zero_init::<VkDeviceQueueCreateInfo>(); MAX_QUEUE_COUNT];
    let mut queue_indices = ArrayVec::<u32, MAX_QUEUE_COUNT>::new();
    queue_indices.push(physical_device.graphics_queue_index);
    if physical_device.graphics_queue_index != physical_device.presentation_queue_index {
        queue_indices.push(physical_device.presentation_queue_index);
    }
    let queue_priority: f32 = 1.0;
    for (index, value) in queue_indices.iter().enumerate() {
        let info = &mut queue_create_info[index];
        info.sType = VkStructureType::VK_STRUCTURE_TYPE_DEVICE_QUEUE_CREATE_INFO;
        info.queueFamilyIndex = *value;
        info.queueCount = 1;
        info.pQueuePriorities = &queue_priority;
    }

    let physical_device_features = physical_device.logical_device_create_features();

    let validation_layers = if instance.use_validation_layers {
        Instance::build_validation_layer_list()
    } else {
        vec![]
    };

    let required_extensions = build_required_device_extension_list();
    if !required_extensions.is_empty() {
        log_debug!("Attempting to create logical device with the following extensions:");
        for ext in &required_extensions {
            log_debug!("     {}", unsafe { CStr::from_ptr(*ext) }.to_string_lossy());
        }
    }
    if !validation_layers.is_empty() {
        log_debug!("Attempting to create logical device with the following validation layers:");
        for ext in &validation_layers {
            log_debug!("     {}", unsafe { CStr::from_ptr(*ext) }.to_string_lossy());
        }
    }

    let mut create_info = vk_zero_init::<VkDeviceCreateInfo>();
    create_info.sType = VkStructureType::VK_STRUCTURE_TYPE_DEVICE_CREATE_INFO;
    create_info.pQueueCreateInfos = queue_create_info.as_ptr();
    create_info.queueCreateInfoCount = queue_indices.len() as u32;
    create_info.pEnabledFeatures = &physical_device_features;
    create_info.enabledLayerCount = validation_layers.len() as u32;
    create_info.ppEnabledLayerNames = validation_layers.as_ptr();
    create_info.enabledExtensionCount = required_extensions.len() as u32;
    create_info.ppEnabledExtensionNames = required_extensions.as_ptr();

    let mut device = VkDeviceHandle::default();
    let result = unsafe {
        vkCreateDevice(
            physical_device.device,
            &create_info,
            null_mut(),
            device.ptr_mut(),
        )
    };
    if result != VkResult::VK_SUCCESS {
        return Err(result);
    }
    Ok(device)
}

impl Device {
    pub(crate) fn create(
        instance: Arc<Instance>,
        render_surface: &dyn RenderSurfaceInterface,
    ) -> Result<Arc<dyn crate::device::Device>, DevError> {
        crate::profile_scope!();
        let physical_device = match PhysicalDevice::create(instance.as_ref(), render_surface) {
            Err(_) => {
                return Err(DevError::Api(
                    "Failed to create physical device".to_string(),
                ))
            }
            Ok(p) => p,
        };

        let device = match create_logical_device(instance.as_ref(), &physical_device) {
            Err(_) => return Err(DevError::Api("Failed to create logical device".to_string())),
            Ok(d) => d,
        };

        let device_queues = [
            Mutex::new(VkQueueHandle::null()),
            Mutex::new(VkQueueHandle::null()),
            Mutex::new(VkQueueHandle::null()),
            Mutex::new(VkQueueHandle::null()),
        ];

        // get graphics queue
        {
            assert!((physical_device.graphics_queue_index as usize) < MAX_QUEUE_STORAGE);
            let mut queue = VkQueueHandle::null();
            unsafe {
                vkGetDeviceQueue(
                    device.value(),
                    physical_device.graphics_queue_index,
                    0,
                    queue.ptr_mut(),
                );
            }
            *device_queues[physical_device.graphics_queue_index as usize].lock() = queue;
        }

        if physical_device.presentation_queue_index != physical_device.graphics_queue_index {
            assert!((physical_device.presentation_queue_index as usize) < MAX_QUEUE_STORAGE);
            let mut queue = VkQueueHandle::null();
            unsafe {
                vkGetDeviceQueue(
                    device.value(),
                    physical_device.presentation_queue_index,
                    0,
                    queue.ptr_mut(),
                );
            }
            *device_queues[physical_device.presentation_queue_index as usize].lock() = queue;
        }

        let device_info = DeviceInfo {
            min_cbuffer_size: 0,
            max_cbuffer_size: physical_device.properties.limits.maxUniformBufferRange,
            max_texture_size: physical_device.properties.limits.maxImageDimension2D,
            max_texture_depth: physical_device.properties.limits.maxImageDimension3D,
            cbuffer_offset_alignment: physical_device
                .properties
                .limits
                .minUniformBufferOffsetAlignment as usize,
            sbuffer_offset_alignment: physical_device
                .properties
                .limits
                .minStorageBufferOffsetAlignment as usize,
            compressed_texture_support: DeviceCompressedTextureSupport {
                astc: physical_device.features.textureCompressionASTC_LDR != 0,
                etc2: physical_device.features.textureCompressionETC2 != 0,
                s3tc: physical_device.features.textureCompressionBC != 0,
            },
        };

        let mut vma: VkVmaAllocHandle = Default::default();

        let mut vma_create_info = vk_zero_init::<VmaAllocatorCreateInfo>();
        vma_create_info.vulkanApiVersion = vk_api_version_1_0();
        vma_create_info.physicalDevice = physical_device.device;
        vma_create_info.device = device.value();
        vma_create_info.instance = instance.vk_instance.value();

        unsafe {
            let result = vmaCreateAllocator(&vma_create_info, vma.ptr_mut());
            if result != VkResult::VK_SUCCESS {
                return Err(DevError::Api(
                    "Failed to create vulkan allocator instance".to_string(),
                ));
            }
        }

        let device = Device {
            state: DeviceState {
                logical_device: device,
                _instance: instance,
                physical_device,
                vma,
                device_queues,
            },
            frame_data: ThreadOwned::new(FrameData {
                command_buffers: [
                    VkCommandBufferHandle::null(),
                    VkCommandBufferHandle::null(),
                    VkCommandBufferHandle::null(),
                ],
                command_pool: [
                    VkCommandPoolHandle::null(),
                    VkCommandPoolHandle::null(),
                    VkCommandPoolHandle::null(),
                ],
                active_command_buffer: vk_zero_init::<VkCommandBuffer>(),
                active_index: 0,
                sync_objects: FrameSyncObjects::new(),
            }),
            resources: DeviceResources {
                uploader: ThreadOwned::new(DeviceUploader::new()),
                swap_chains: ThreadOwned::new(SwapChainMap::new(8)),
                textures: ThreadOwned::new(TextureMap::new(64)),
                render_passes: ThreadOwned::new(RenderPassMap::new(32)),
                framebuffers: ThreadOwned::new(FramebufferMap::new(32)),
                shaders: ThreadOwned::new(ShaderMap::new(64)),
                pipelines: ThreadOwned::new(PipelineMap::new(64)),
                descriptor_set_layouts: ThreadOwned::new(DescriptorSetLayoutMap::new(64)),
                buffers: ThreadOwned::new(BufferMap::new(64)),
                samplers: ThreadOwned::new(SamplerMap::new(32)),
                descriptor_pools: ThreadOwned::new(DescriptorPoolMap::new(32)),
                descriptor_sets_v2: ThreadOwned::new(DescriptorSetV2Map::new(128)),
            },
            device_info,
        };

        device.frame_data.borrow_mut().init(&device)?;

        device.resources.uploader.borrow_mut().initialize(&device)?;

        Ok(Arc::new(device))
    }

    fn shutdown(&mut self) {
        {
            unsafe {
                vkDeviceWaitIdle(self.state.logical_device.value());
            }

            let mut accessor = self.frame_data.borrow_mut();
            accessor.destroy(self);
        }

        self.resources.uploader.borrow_mut().destroy(self);
        self.resources.pipelines.borrow_mut().clear(self);
        self.resources.framebuffers.borrow_mut().clear(self);
        self.resources.render_passes.borrow_mut().clear(self);
        self.resources.samplers.borrow_mut().clear(self);
        self.resources.textures.borrow_mut().clear(self);
        self.resources.shaders.borrow_mut().clear(self);
        self.resources.descriptor_sets_v2.borrow_mut().clear(self);
        self.resources
            .descriptor_set_layouts
            .borrow_mut()
            .clear(self);
        self.resources.descriptor_pools.borrow_mut().clear(self);
        self.resources.buffers.borrow_mut().clear(self);
        self.resources.swap_chains.borrow_mut().clear();
    }
}

impl Drop for Device {
    fn drop(&mut self) {
        self.shutdown();
    }
}

impl crate::device::Device for Device {
    fn device_info(&self) -> &DeviceInfo {
        &self.device_info
    }

    fn begin_frame(&self) -> DeviceResult<()> {
        crate::profile_scope!();
        let shared_state = &self.state;

        self.frame_data.try_with_mut(move |accessor| {
            accessor.reset_and_swap_command_pool(shared_state)?;
            // Cleanup past resources queued for deletion
            {
                crate::profile_scope!("clear deferred");
                self.resources.pipelines.borrow_mut().clear_deferred(self);
                self.resources
                    .framebuffers
                    .borrow_mut()
                    .clear_deferred(self);
                self.resources
                    .render_passes
                    .borrow_mut()
                    .clear_deferred(self);
                self.resources.samplers.borrow_mut().clear_deferred(self);
                self.resources.textures.borrow_mut().clear_deferred(self);
                self.resources.shaders.borrow_mut().clear_deferred(self);
                self.resources
                    .descriptor_set_layouts
                    .borrow_mut()
                    .clear_deferred(self);
                self.resources
                    .descriptor_sets_v2
                    .borrow_mut()
                    .clear_deferred(self);
                self.resources
                    .descriptor_pools
                    .borrow_mut()
                    .clear_deferred(self);
                self.resources.buffers.borrow_mut().clear_deferred(self);
            }

            unsafe {
                // begin new command buffer
                let begin_info = VkCommandBufferBeginInfo {
                    sType: VkStructureType::VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO,
                    pNext: null_mut(),
                    flags: VkCommandBufferUsageFlagBits::VK_COMMAND_BUFFER_USAGE_ONE_TIME_SUBMIT_BIT
                        as u32,
                    pInheritanceInfo: null_mut(),
                };

                {
                    let r = vkBeginCommandBuffer(accessor.active_command_buffer, &begin_info);
                    if r != VkResult::VK_SUCCESS {
                        return Err(DevError::Api(format!(
                            "Failed to begin command buffer recording: {}",
                            vk_result_to_str(r)
                        )));
                    }
                }
            }
            Ok(())
        })?
    }

    fn end_frame(
        &self,
        texture_handle: TextureHandle,
        swap_chain_handle: SwapChainHandle,
    ) -> DeviceResult<()> {
        crate::profile_scope!();

        let uploader_semaphore = {
            let mut uploader = self.resources.uploader.try_borrow_mut()?;
            if uploader.has_pending_submit() {
                uploader.submit(self)?;
                let sem = uploader.upload_complete_semaphore();
                uploader.end_frame(self)?;
                Some(sem)
            } else {
                None
            }
        };

        let accessor = self.frame_data.try_borrow_mut()?;
        {
            // record copy
            let mut cmd_buffer = CmdBuffer {
                device: self,
                cmd_buffer: accessor.active_command_buffer,
            };
            cmd_buffer.blit_texture_to_swap_chain(texture_handle, swap_chain_handle)?;
        }

        // finish command buffer
        unsafe {
            // record copy
            let r = vkEndCommandBuffer(accessor.active_command_buffer);
            if r != VkResult::VK_SUCCESS {
                return Err(DevError::Api("Failed to end command buffer".to_string()));
            }
        }

        // Submit command buffer
        {
            crate::profile_scope!("Submit Command");
            let signal_semaphores = [accessor.current_render_finished_semaphore().value()];
            let mut wait_semaphores = ArrayVec::<VkSemaphore, 4>::new();
            let mut wait_stages = ArrayVec::<u32, 4>::new();
            wait_semaphores.push(accessor.current_acquire_semaphore().value());
            wait_stages.push(
                VkPipelineStageFlagBits::VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT as u32,
            );
            if let Some(upload_sem) = uploader_semaphore {
                wait_semaphores.push(upload_sem);
                wait_stages.push(VkPipelineStageFlagBits::VK_PIPELINE_STAGE_TRANSFER_BIT as u32);
            }

            let cmd_buffers = [accessor.active_command_buffer];

            let submit_info = VkSubmitInfo {
                sType: VkStructureType::VK_STRUCTURE_TYPE_SUBMIT_INFO,
                pNext: null_mut(),
                waitSemaphoreCount: wait_semaphores.len() as u32,
                pWaitSemaphores: wait_semaphores.as_ptr(),
                pWaitDstStageMask: wait_stages.as_ptr(),
                commandBufferCount: cmd_buffers.len() as u32,
                pCommandBuffers: cmd_buffers.as_ptr(),
                signalSemaphoreCount: signal_semaphores.len() as u32,
                pSignalSemaphores: signal_semaphores.as_ptr(),
            };
            // Fence can only be reset here or there will be issues when the swap chain
            // acquires an image we are still rendering to.

            if let Err(e) = accessor
                .current_render_fence()
                .reset(self.state.logical_device.value())
            {
                log_error!("Failed to reset render submission fence: {}", e);
            }

            {
                crate::profile_scope!("queue_lock");
                let queue_accessor = self.state.graphics_queue().lock();
                let submit_result = unsafe {
                    vkQueueSubmit(
                        queue_accessor.value(),
                        1,
                        &submit_info,
                        accessor.current_render_fence().vk_handle(),
                    )
                };
                if submit_result != VkResult::VK_SUCCESS {
                    return Err(DeviceError::Api(format!(
                        "Failed to submit graphics queue: {}",
                        vk_result_to_str(submit_result)
                    )));
                }
            }
        }
        Ok(())
    }

    fn create_swap_chain(
        &self,
        surface_interface: &dyn RenderSurfaceInterface,
    ) -> Result<SwapChainHandle, DevError> {
        let mut swap_chains = self.resources.swap_chains.try_borrow_mut()?;
        let swap_chain = crate::vulkan::swap_chain::SwapChain::create(self, surface_interface)?;
        if let Ok(h) = swap_chains.insert(swap_chain) {
            Ok(h)
        } else {
            Err(DevError::Api(
                "Failed to manage created swap_chain".to_string(),
            ))
        }
    }

    fn destroy_swap_chain(&self, h: SwapChainHandle) -> Result<(), DevError> {
        let mut resource_guard = self.resources.swap_chains.try_borrow_mut()?;
        if let Some(mut sw) = resource_guard.remove(h) {
            sw.destroy(self);
        }
        Ok(())
    }

    fn swap_chain_size(&self, h: SwapChainHandle) -> Result<(u32, u32), DevError> {
        let resource_guard = self.resources.swap_chains.try_borrow()?;
        if let Some(swap_chain) = resource_guard.get(h) {
            let extent = swap_chain.get_extent();
            return Ok((extent.width, extent.height));
        }
        Err(DevError::InvalidHandle)
    }

    fn acquire_swap_chain_image(
        &self,
        h: SwapChainHandle,
        surface_interface: &dyn RenderSurfaceInterface,
    ) -> DeviceResult<SwapChainAcquireResult> {
        let mut swap_chains = self.resources.swap_chains.try_borrow_mut()?;
        let swap_chain = if let Some(s) = swap_chains.get_mut(h) {
            s
        } else {
            return Err(DeviceError::InvalidHandle);
        };
        let mut accessor = self.frame_data.try_borrow_mut()?;
        {
            crate::profile_scope!("Acquire Image");
            let fences = [
                accessor.sync_objects.fences[0].vk_handle(),
                accessor.sync_objects.fences[1].vk_handle(),
                accessor.sync_objects.fences[2].vk_handle(),
            ];
            let result = swap_chain.begin_frame(
                self.state.logical_device.value(),
                &fences,
                accessor.current_acquire_semaphore().value(),
            )?;

            if matches!(result, SwapChainState::RebuildRequired) {
                unsafe {
                    vkDeviceWaitIdle(self.state.logical_device.value());
                }
                accessor.rebuild(self).expect(
                    "Failed to rebuild frame sync objects, device is now in an unusable state",
                );
                self.resources
                    .uploader
                    .borrow_mut()
                    .rebuild_sync_objects(self).expect("Failed to update device uploader sync objects, device is now in an unusable state");
                swap_chain
                    .rebuild(self, surface_interface)
                    .expect("Failed to rebuild swap chain, we are now in an invalid state");
                return Ok(crate::device::SwapChainAcquireResult::SwapChainResized);
            }
        }
        Ok(SwapChainAcquireResult::Ok)
    }

    fn present_swap_chain(&self, h: SwapChainHandle) -> DeviceResult<()> {
        let mut swap_chains = self.resources.swap_chains.try_borrow_mut()?;
        let swap_chain = if let Some(s) = swap_chains.get_mut(h) {
            s
        } else {
            return Err(DeviceError::InvalidHandle);
        };
        let accessor = self.frame_data.try_borrow()?;
        swap_chain.present(
            self.state.presentation_queue(),
            accessor.current_render_finished_semaphore().value(),
        )
    }

    fn create_texture(
        &self,
        info: &TextureCreateInfo,
        data: &[TextureData],
    ) -> Result<TextureHandle, DevError> {
        let mut textures = self.resources.textures.try_borrow_mut()?;
        let texture = Texture::create(self, info, data)?;
        textures.insert(texture)
    }

    fn destroy_texture(&self, h: TextureHandle) -> Result<(), DevError> {
        let mut resource_guard = self.resources.textures.try_borrow_mut()?;
        resource_guard.release(h);
        Ok(())
    }

    fn update_texture(&self, h: TextureHandle, data: &[TextureData]) -> Result<(), DeviceError> {
        let mut textures = self.resources.textures.try_borrow_mut()?;
        if let Some(texture) = textures.get_mut(h) {
            for tdata in data {
                texture.set_data(self, tdata)?;
            }
            Ok(())
        } else {
            Err(DeviceError::InvalidHandle)
        }
    }

    fn create_framebuffer(
        &self,
        info: &FramebufferCreateInfo,
    ) -> Result<FramebufferHandle, DevError> {
        let mut framebuffers = self.resources.framebuffers.try_borrow_mut()?;
        let mut textures = self.resources.textures.borrow_mut();
        let mut render_passes = self.resources.render_passes.borrow_mut();
        let framebuffer = Framebuffer::create(info, self, &mut textures, &mut render_passes)?;
        framebuffers.insert(framebuffer)
    }

    fn destroy_framebuffer(&self, h: FramebufferHandle) -> Result<(), DevError> {
        let mut resource_guard = self.resources.framebuffers.try_borrow_mut()?;
        resource_guard.release(h);
        Ok(())
    }

    fn create_render_pass(
        &self,
        create_info: &crate::render_pass::RenderPassCreateInfo,
    ) -> Result<crate::types::RenderPassHandle, DevError> {
        let mut render_passes = self.resources.render_passes.try_borrow_mut()?;
        let pass = RenderPass::create(self, create_info)?;
        render_passes.insert(pass)
    }

    fn destroy_render_pass(&self, h: crate::types::RenderPassHandle) -> Result<(), DevError> {
        let mut resource_guard = self.resources.render_passes.try_borrow_mut()?;
        resource_guard.release(h);
        Ok(())
    }

    fn create_shader(&self, info: &ShaderCreateInfo) -> Result<ShaderHandle, DevError> {
        let mut shaders = self.resources.shaders.try_borrow_mut()?;
        let shader = Shader::create(self, info)?;
        shaders.insert(shader)
    }

    fn destroy_shader(&self, h: ShaderHandle) -> Result<(), DevError> {
        let mut shaders = self.resources.shaders.try_borrow_mut()?;
        shaders.release(h);
        Ok(())
    }

    fn create_pipeline(&self, info: &PipelineCreateInfo) -> Result<PipelineHandle, DevError> {
        let mut pipelines = self.resources.pipelines.try_borrow_mut()?;
        let mut shaders = self.resources.shaders.try_borrow_mut()?;
        let mut render_passes = self.resources.render_passes.try_borrow_mut()?;
        let mut descriptor_set_layouts = self.resources.descriptor_set_layouts.try_borrow_mut()?;
        let pipeline = Pipeline::create(
            info,
            self,
            &mut render_passes,
            &mut shaders,
            &mut descriptor_set_layouts,
        )?;
        pipelines.insert(pipeline)
    }

    fn destroy_pipeline(&self, h: PipelineHandle) -> Result<(), DevError> {
        let mut pipelines = self.resources.pipelines.try_borrow_mut()?;
        pipelines.release(h);
        Ok(())
    }

    fn create_descriptor_set_layout(
        &self,
        info: &DescriptorSetLayoutCreateInfo,
    ) -> Result<DescriptorSetLayoutHandle, DevError> {
        let key = DescriptorSetLayoutKey::from(info);
        let mut descriptor_set_layouts = self.resources.descriptor_set_layouts.try_borrow_mut()?;
        descriptor_set_layouts.insert(key, || DescriptorSetLayout::create(info, self))
    }

    fn destroy_descriptor_set_layout(&self, h: DescriptorSetLayoutHandle) -> Result<(), DevError> {
        self.resources
            .descriptor_set_layouts
            .try_borrow_mut()?
            .release(h);
        Ok(())
    }

    fn create_buffer(&self, info: &BufferCreateInfo) -> Result<BufferHandle, DevError> {
        let mut buffer = Buffer::create(self, info)?;
        let mut buffers = self.resources.buffers.try_borrow_mut()?;

        if let Some(data) = info.data {
            if let Err(r) = buffer.update(self, 0, data.as_ptr(), data.len() as DeviceSize) {
                buffer.destroy(self.state.logical_device.value(), self.state.vma.value());
                return Err(r);
            };
        }
        buffers.insert(buffer)
    }

    fn destroy_buffer(&self, h: BufferHandle) -> Result<(), DevError> {
        self.resources.buffers.try_borrow_mut()?.release(h);
        Ok(())
    }

    fn create_sampler(&self, info: &SamplerCreateInfo) -> DeviceResult<SamplerHandle> {
        let mut samplers = self.resources.samplers.try_borrow_mut()?;
        samplers.insert(*info, || Sampler::create(self, info))
    }

    fn destroy_sampler(&self, h: SamplerHandle) -> DeviceResult<()> {
        self.resources.samplers.try_borrow_mut()?.release(h);
        Ok(())
    }

    fn create_descriptor_pool(
        &self,
        info: &DescriptorPoolCreateInfo,
    ) -> DeviceResult<DescriptorPoolHandle> {
        let descriptor_pool = DescriptorPoolV2::create(self, info)?;
        let mut pools = self.resources.descriptor_pools.try_borrow_mut()?;
        pools.insert(descriptor_pool)
    }

    fn destroy_descriptor_pool(&self, h: DescriptorPoolHandle) -> DeviceResult<()> {
        self.resources.descriptor_pools.try_borrow_mut()?.release(h);
        Ok(())
    }

    fn create_descriptor_set(
        &self,
        layout: DescriptorSetLayoutHandle,
        pool: DescriptorPoolHandle,
        values: Option<DescriptorSetUpdateInfo>,
    ) -> DeviceResult<DescriptorSetHandle> {
        let mut layouts = self.resources.descriptor_set_layouts.try_borrow_mut()?;
        let mut pools = self.resources.descriptor_pools.try_borrow_mut()?;
        let mut descriptor_set =
            DescriptorSetV2::create(self, layout, &mut layouts, pool, &mut pools)?;
        if let Some(data) = values {
            if let Err(e) = descriptor_set.update(self, &data) {
                descriptor_set.destroy(&mut layouts, &mut pools);
                return Err(e);
            }
        }
        let mut descriptor_sets = self.resources.descriptor_sets_v2.borrow_mut();
        descriptor_sets.insert(descriptor_set)
    }

    fn update_descriptor_set(
        &self,
        h: DescriptorSetHandle,
        value: DescriptorSetUpdateInfo,
    ) -> DeviceResult<()> {
        match self
            .resources
            .descriptor_sets_v2
            .try_borrow_mut()?
            .get_mut(h)
        {
            Some(set) => set.update(self, &value),
            None => Err(DevError::InvalidHandle),
        }
    }

    fn destroy_descriptor_set(&self, h: DescriptorSetHandle) -> DeviceResult<()> {
        self.resources
            .descriptor_sets_v2
            .try_borrow_mut()?
            .release(h);
        Ok(())
    }

    fn update_buffer(
        &self,
        h: BufferHandle,
        offset: DeviceSize,
        data: *const u8,
        size: DeviceSize,
    ) -> Result<(), DevError> {
        let mut buffers = self.resources.buffers.try_borrow_mut()?;
        if let Some(b) = buffers.get_mut(h) {
            b.update(self, offset, data, size)
        } else {
            Err(DevError::InvalidHandle)
        }
    }

    fn execute_commands(
        &self,
        f: &mut dyn FnMut(&'_ mut dyn crate::command_buffer::CommandBuffer) -> Result<(), DevError>,
    ) -> Result<(), DevError> {
        let mut cmd_buffer = CmdBuffer {
            device: self,
            cmd_buffer: self.frame_data.try_borrow()?.active_command_buffer,
        };
        f(&mut cmd_buffer)?;
        Ok(())
    }
}
