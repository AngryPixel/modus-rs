//! Manage uploading of data to the device to GPU only memory

use crate::device::{DeviceError, DeviceResult};
use crate::types::DeviceSize;
use crate::vulkan::buffer::Buffer;
use crate::vulkan::device::Device;
use crate::vulkan::sync::{Fence, Semaphore};
use crate::vulkan::utils::{
    vk_result_to_str, VkCommandBufferHandle, VkCommandPoolHandle, MAX_FRAMES_IN_FLIGHT,
};
use std::ptr::null_mut;
use vulkan_sys::*;

#[derive(Debug)]
struct UploaderBuffer {
    buffer: Buffer,
    offset: DeviceSize,
}

const UPLOAD_BUFFER_SIZE: DeviceSize = 4 * 1024 * 1024;
impl UploaderBuffer {
    fn new(device: &Device) -> DeviceResult<Self> {
        let buffer = Buffer::create_vk(
            device,
            UPLOAD_BUFFER_SIZE,
            VkBufferUsageFlagBits::VK_BUFFER_USAGE_TRANSFER_SRC_BIT as u32,
            VmaMemoryUsage::VMA_MEMORY_USAGE_CPU_ONLY,
            VkSharingMode::VK_SHARING_MODE_EXCLUSIVE,
        )?;
        Ok(Self { buffer, offset: 0 })
    }

    fn write(&mut self, device: &Device, data: &[u8]) -> DeviceResult<DeviceSize> {
        self.buffer
            .update(device, self.offset, data.as_ptr(), data.len() as DeviceSize)?;
        let prev_offset = self.offset;
        self.offset += data.len() as DeviceSize;
        Ok(prev_offset)
    }

    #[inline(always)]
    fn can_accommodate(&self, size: DeviceSize) -> bool {
        self.offset + size <= self.buffer.size
    }

    fn reset(&mut self) {
        self.offset = 0;
    }
}

#[derive(Debug)]
pub(crate) struct DeviceUploader {
    fence: [Fence; MAX_FRAMES_IN_FLIGHT],
    semaphore: [Semaphore; MAX_FRAMES_IN_FLIGHT],
    command_pool: [VkCommandPoolHandle; MAX_FRAMES_IN_FLIGHT],
    command_buffer: [VkCommandBufferHandle; MAX_FRAMES_IN_FLIGHT],
    upload_buffers: [Vec<UploaderBuffer>; MAX_FRAMES_IN_FLIGHT],
    free_upload_buffers: Vec<UploaderBuffer>,
    pending: [bool; MAX_FRAMES_IN_FLIGHT],
    frame_counter: usize,
}

impl DeviceUploader {
    pub(crate) fn new() -> Self {
        Self {
            fence: [Fence::new(), Fence::new(), Fence::new()],
            semaphore: [Semaphore::new(), Semaphore::new(), Semaphore::new()],
            command_pool: [Default::default(), Default::default(), Default::default()],
            command_buffer: [Default::default(), Default::default(), Default::default()],
            upload_buffers: [vec![], vec![], vec![]],
            pending: [false; MAX_FRAMES_IN_FLIGHT],
            free_upload_buffers: Vec::with_capacity(4),
            frame_counter: 0,
        }
    }

    pub(crate) fn initialize(&mut self, device: &Device) -> DeviceResult<()> {
        for i in 0..MAX_FRAMES_IN_FLIGHT {
            self.fence[i].init(device, true)?;
            self.semaphore[i].init(device)?;
            unsafe {
                let create_info = VkCommandPoolCreateInfo {
                    sType: VkStructureType::VK_STRUCTURE_TYPE_COMMAND_POOL_CREATE_INFO,
                    pNext: null_mut(),
                    flags: 0,
                    queueFamilyIndex: device.state.physical_device.graphics_queue_index,
                };

                let r = vkCreateCommandPool(
                    device.state.logical_device.value(),
                    &create_info,
                    null_mut(),
                    self.command_pool[i].ptr_mut(),
                );
                if r != VkResult::VK_SUCCESS {
                    return Err(DeviceError::Api(
                        format! {"Failed to create uploader command pool: {}", vk_result_to_str(r)},
                    ));
                }
            }

            unsafe {
                let create_info = VkCommandBufferAllocateInfo {
                    sType: VkStructureType::VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO,
                    pNext: null_mut(),
                    commandPool: self.command_pool[i].value(),
                    level: VkCommandBufferLevel::VK_COMMAND_BUFFER_LEVEL_PRIMARY,
                    commandBufferCount: 1,
                };

                let r = vkAllocateCommandBuffers(
                    device.state.logical_device.value(),
                    &create_info,
                    self.command_buffer[i].ptr_mut(),
                );
                if r != VkResult::VK_SUCCESS {
                    return Err(DeviceError::Api(
                        format! {"Failed to create uploader command buffer: {}", vk_result_to_str(r)},
                    ));
                }
            }
        }

        self.swap_pools(device)?;

        let upload_buffer = UploaderBuffer::new(device)?;
        self.upload_buffers[self.frame_counter].push(upload_buffer);
        Ok(())
    }

    fn swap_pools(&mut self, device: &Device) -> DeviceResult<()> {
        self.frame_counter = (self.frame_counter + 1) % MAX_FRAMES_IN_FLIGHT;
        unsafe {
            self.fence[self.frame_counter].wait(device.state.logical_device.value(), u64::MAX)?;
            self.pending[self.frame_counter] = false;
            let upload_buffers = &mut self.upload_buffers[self.frame_counter];
            while let Some(mut buffer) = upload_buffers.pop() {
                buffer.reset();
                self.free_upload_buffers.push(buffer);
            }

            let r = vkResetCommandPool(
                device.state.logical_device.value(),
                self.command_pool[self.frame_counter].value(),
                0,
            );
            if r != VkResult::VK_SUCCESS {
                return Err(DeviceError::Api(
                    format! {"Failed to reset command pool: {}", vk_result_to_str(r)},
                ));
            }
            let begin_info = VkCommandBufferBeginInfo {
                sType: VkStructureType::VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO,
                pNext: null_mut(),
                flags: VkCommandBufferUsageFlagBits::VK_COMMAND_BUFFER_USAGE_ONE_TIME_SUBMIT_BIT
                    as u32,
                pInheritanceInfo: null_mut(),
            };
            let r =
                vkBeginCommandBuffer(self.command_buffer[self.frame_counter].value(), &begin_info);
            if r != VkResult::VK_SUCCESS {
                return Err(DeviceError::Api(
                    format! {"Failed to begin command buffer recording: {}", vk_result_to_str(r)},
                ));
            }
        }
        Ok(())
    }

    pub(crate) fn destroy(&mut self, device: &Device) {
        for buffer in &mut self.free_upload_buffers {
            buffer.buffer.destroy(
                device.state.logical_device.value(),
                device.state.vma.value(),
            );
        }
        self.free_upload_buffers.clear();
        for i in 0..MAX_FRAMES_IN_FLIGHT {
            {
                for buffer in &mut self.upload_buffers[i] {
                    buffer.buffer.destroy(
                        device.state.logical_device.value(),
                        device.state.vma.value(),
                    );
                }
                self.upload_buffers[i].clear()
            }
            self.fence[i].destroy(device);
            self.semaphore[i].destroy(device);
            let device = device.state.logical_device.value();
            unsafe {
                let vk_command_pool = self.command_pool[i].value();
                self.command_buffer[i].destroy_with(|c| {
                    vkFreeCommandBuffers(device, vk_command_pool, 1, &c);
                });
                self.command_pool[i].destroy_with(|c| {
                    vkDestroyCommandPool(device, c, null_mut());
                });
            }
        }
    }

    pub(crate) fn rebuild_sync_objects(&mut self, device: &Device) -> DeviceResult<()> {
        for semaphore in &mut self.semaphore {
            semaphore.destroy(device);
            semaphore.init(device)?;
        }
        Ok(())
    }

    fn find_or_allocate_upload_buffer(
        &mut self,
        device: &Device,
        size: DeviceSize,
    ) -> DeviceResult<usize> {
        assert!(size < UPLOAD_BUFFER_SIZE);
        let upload_buffers = &mut self.upload_buffers[self.frame_counter];
        for (index, upload_buffer) in upload_buffers.iter().enumerate() {
            if upload_buffer.can_accommodate(size) {
                return Ok(index);
            }
        }

        if let Some(buffer) = self.free_upload_buffers.pop() {
            // use an existing one
            upload_buffers.push(buffer);
        } else {
            // no more buffers need to allocate a new one
            upload_buffers.push(UploaderBuffer::new(device)?);
        }
        Ok(upload_buffers.len() - 1)
    }

    pub(crate) fn upload(
        &mut self,
        device: &Device,
        data: &[u8],
        function: impl FnOnce(&Device, VkBuffer, DeviceSize, VkCommandBuffer) -> DeviceResult<()>,
    ) -> DeviceResult<()> {
        let upload_buffer_index =
            self.find_or_allocate_upload_buffer(device, data.len() as DeviceSize)?;
        let upload_buffer = &mut self.upload_buffers[self.frame_counter][upload_buffer_index];
        let storage_offset = upload_buffer.write(device, data)?;
        self.pending[self.frame_counter] = true;
        (function)(
            device,
            upload_buffer.buffer.handle.value(),
            storage_offset,
            self.command_buffer[self.frame_counter].value(),
        )
    }

    pub(crate) fn record(
        &mut self,
        device: &Device,
        function: impl FnOnce(&Device, VkCommandBuffer) -> DeviceResult<()>,
    ) -> DeviceResult<()> {
        self.pending[self.frame_counter] = true;
        (function)(device, self.command_buffer[self.frame_counter].value())
    }

    pub(crate) fn end_frame(&mut self, device: &Device) -> DeviceResult<()> {
        self.swap_pools(device)
    }

    #[inline(always)]
    pub(crate) fn has_pending_submit(&self) -> bool {
        self.pending[self.frame_counter]
    }

    pub(crate) fn submit(&self, device: &Device) -> DeviceResult<()> {
        debug_assert!(self.has_pending_submit());
        unsafe {
            let r = vkEndCommandBuffer(self.command_buffer[self.frame_counter].value());
            if r != VkResult::VK_SUCCESS {
                return Err(DeviceError::Api(
                    format! {"Failed to end command buffer recording: {}", vk_result_to_str(r)},
                ));
            }
        }

        self.fence[self.frame_counter].reset(device.state.logical_device.value())?;
        let wait_cmd_buffers = [self.command_buffer[self.frame_counter].value()];
        let signal_semaphores = [self.semaphore[self.frame_counter].vk_handle()];
        let submit_info = VkSubmitInfo {
            sType: VkStructureType::VK_STRUCTURE_TYPE_SUBMIT_INFO,
            pNext: null_mut(),
            waitSemaphoreCount: 0,
            pWaitSemaphores: null_mut(),
            pWaitDstStageMask: null_mut(),
            commandBufferCount: wait_cmd_buffers.len() as u32,
            pCommandBuffers: wait_cmd_buffers.as_ptr(),
            signalSemaphoreCount: signal_semaphores.len() as u32,
            pSignalSemaphores: signal_semaphores.as_ptr(),
        };
        unsafe {
            crate::profile_scope!("Uploader queue_lock");
            let queue_lock = device.state.graphics_queue().lock();
            let r = vkQueueSubmit(
                queue_lock.value(),
                1,
                &submit_info,
                self.fence[self.frame_counter].vk_handle(),
            );
            if r != VkResult::VK_SUCCESS {
                return Err(DeviceError::Api(
                    format! {"Failed to submit upload data to queue: {}", vk_result_to_str(r)},
                ));
            }
        }
        Ok(())
    }

    #[inline(always)]
    pub(crate) fn upload_complete_semaphore(&self) -> VkSemaphore {
        self.semaphore[self.frame_counter].vk_handle()
    }
}
