//! Render Pass implementation

use crate::device::DeviceError;
use crate::render_pass::{
    AttachmentLoadOp, AttachmentStoreOp, PipelineStage, MAX_ATTACHMENT_COUNT,
};
use crate::types::RenderPassHandle;
use crate::vulkan::device::Device;
use crate::vulkan::texture::{sample_count_to_vk_sample, texture_format_to_vk_format};
use crate::vulkan::utils::{
    vk_result_to_str, DeferredResourceDeleter, DeferredResourceDeleterBuilder, ResourceMap,
    VkRenderPassHandle,
};
use modus_core::ArrayVec;
use std::ptr::{null, null_mut};
use vulkan_sys::*;

#[derive(Debug)]
pub(crate) struct RenderPass {
    pub(crate) handle: VkRenderPassHandle,
}

fn load_op_to_vk_op(op: AttachmentLoadOp) -> VkAttachmentLoadOp {
    match op {
        AttachmentLoadOp::None => VkAttachmentLoadOp::VK_ATTACHMENT_LOAD_OP_DONT_CARE,
        AttachmentLoadOp::Load => VkAttachmentLoadOp::VK_ATTACHMENT_LOAD_OP_LOAD,
        AttachmentLoadOp::Clear => VkAttachmentLoadOp::VK_ATTACHMENT_LOAD_OP_CLEAR,
    }
}

fn store_op_to_vk_op(op: AttachmentStoreOp) -> VkAttachmentStoreOp {
    match op {
        AttachmentStoreOp::None => VkAttachmentStoreOp::VK_ATTACHMENT_STORE_OP_DONT_CARE,
        AttachmentStoreOp::Store => VkAttachmentStoreOp::VK_ATTACHMENT_STORE_OP_STORE,
    }
}

impl RenderPass {
    pub(crate) fn create(
        device: &Device,
        create_info: &crate::render_pass::RenderPassCreateInfo,
    ) -> Result<RenderPass, DeviceError> {
        if create_info.attachments.len() >= MAX_ATTACHMENT_COUNT
            || create_info.color_attachments.len() >= MAX_ATTACHMENT_COUNT
            || create_info.input_attachments.len() >= MAX_ATTACHMENT_COUNT
        {
            return Err(DeviceError::Api(format!(
                "Attachment count exceeds {}",
                MAX_ATTACHMENT_COUNT
            )));
        }

        let mut attachment_descs = ArrayVec::<VkAttachmentDescription, MAX_ATTACHMENT_COUNT>::new();
        let mut input_attachment_refs =
            ArrayVec::<VkAttachmentReference, MAX_ATTACHMENT_COUNT>::new();
        let mut color_attachment_refs =
            ArrayVec::<VkAttachmentReference, MAX_ATTACHMENT_COUNT>::new();
        let mut depth_stencil_attachment_ref = None;

        //TODO: This needs to be improved
        let color_reads = !input_attachment_refs.is_empty();
        let color_writes = !color_attachment_refs.is_empty();
        for attachment in create_info.attachments {
            let desc = VkAttachmentDescription {
                flags: 0,
                format: texture_format_to_vk_format(attachment.format),
                samples: sample_count_to_vk_sample(attachment.samples),
                loadOp: load_op_to_vk_op(attachment.load_op),
                storeOp: store_op_to_vk_op(attachment.store_op),
                stencilLoadOp: VkAttachmentLoadOp::VK_ATTACHMENT_LOAD_OP_DONT_CARE,
                stencilStoreOp: VkAttachmentStoreOp::VK_ATTACHMENT_STORE_OP_DONT_CARE,
                initialLayout: VkImageLayout::VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL,
                finalLayout: VkImageLayout::VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL,
            };
            attachment_descs.push(desc);
        }

        for index in create_info.color_attachments {
            let attachment_ref = VkAttachmentReference {
                attachment: *index as u32,
                layout: VkImageLayout::VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL,
            };
            color_attachment_refs.push(attachment_ref);
        }

        for index in create_info.input_attachments {
            let attachment_ref = VkAttachmentReference {
                attachment: *index as u32,
                layout: VkImageLayout::VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL,
            };
            input_attachment_refs.push(attachment_ref);
        }

        if let Some(index) = create_info.depth_stencil_attachment {
            let attachment_ref = VkAttachmentReference {
                attachment: index as u32,
                layout: VkImageLayout::VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL,
            };
            depth_stencil_attachment_ref = Some(attachment_ref);
        }

        /*
        match attachment.access {
            AttachmentAccess::Read => color_reads = true,
            AttachmentAccess::Write => color_writes = true,
        };*/

        let sub_pass_desc = VkSubpassDescription {
            flags: 0,
            pipelineBindPoint: if create_info.stage == PipelineStage::Graphics {
                VkPipelineBindPoint::VK_PIPELINE_BIND_POINT_GRAPHICS
            } else {
                VkPipelineBindPoint::VK_PIPELINE_BIND_POINT_COMPUTE
            },
            inputAttachmentCount: input_attachment_refs.len() as u32,
            pInputAttachments: input_attachment_refs.as_ptr(),
            colorAttachmentCount: color_attachment_refs.len() as u32,
            pColorAttachments: color_attachment_refs.as_ptr(),
            pResolveAttachments: null_mut(),
            pDepthStencilAttachment: if let Some(desc) = &depth_stencil_attachment_ref {
                desc
            } else {
                null()
            },
            preserveAttachmentCount: 0,
            pPreserveAttachments: null_mut(),
        };

        let sub_pass_dep = VkSubpassDependency {
            srcSubpass: VK_SUBPASS_EXTERNAL as u32,
            dstSubpass: 0,
            srcStageMask: VkPipelineStageFlagBits::VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT
                as u32,
            dstStageMask: VkPipelineStageFlagBits::VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT
                as u32,
            srcAccessMask: if !color_reads {
                0
            } else {
                VkAccessFlagBits::VK_ACCESS_COLOR_ATTACHMENT_READ_BIT as u32
            },
            dstAccessMask: if !color_writes {
                0
            } else {
                VkAccessFlagBits::VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT as u32
            },
            dependencyFlags: 0,
        };

        let render_pass_info = VkRenderPassCreateInfo {
            sType: VkStructureType::VK_STRUCTURE_TYPE_RENDER_PASS_CREATE_INFO,
            pNext: null_mut(),
            flags: 0,
            attachmentCount: attachment_descs.len() as u32,
            pAttachments: attachment_descs.as_ptr(),
            subpassCount: 1,
            pSubpasses: &sub_pass_desc,
            dependencyCount: 1,
            pDependencies: &sub_pass_dep,
        };

        let mut render_pass = VkRenderPassHandle::null();
        unsafe {
            let r = vkCreateRenderPass(
                device.state.logical_device.value(),
                &render_pass_info,
                null_mut(),
                render_pass.ptr_mut(),
            );
            if r != VkResult::VK_SUCCESS {
                return Err(DeviceError::Api(format!(
                    "Failed to create render pass: {}",
                    vk_result_to_str(r)
                )));
            }
        }

        let render_pass = RenderPass {
            handle: render_pass,
        };
        Ok(render_pass)
    }
}

pub(crate) type RenderPassMap = ResourceMap<RenderPassHandle, RenderPass>;

#[derive(Debug)]
pub(crate) struct RenderPassDeleter {
    pub(crate) device: VkDevice,
}

impl RenderPassDeleter {
    pub(crate) fn new(device: &Device) -> Self {
        Self {
            device: device.state.logical_device.value(),
        }
    }
}

impl DeferredResourceDeleter<RenderPass> for RenderPassDeleter {
    fn delete(&mut self, render_pass: &mut RenderPass) {
        unsafe {
            render_pass.handle.destroy_with(|h| {
                vkDestroyRenderPass(self.device, h, null_mut());
            })
        }
    }
}

impl DeferredResourceDeleterBuilder<'_, RenderPass> for RenderPass {
    type Deleter = RenderPassDeleter;
    fn create_deleter(device: &Device) -> Self::Deleter {
        Self::Deleter::new(device)
    }
}
