use crate::app_interface::{NativeSurfacePtr, RenderSurfaceInterface};
use crate::vulkan::instance::Instance;
use crate::vulkan::physical_device::PhysicalDeviceError::NoVkDeviceFound;
use crate::vulkan::utils::{vk_uninit_vec, vk_zero_init};
use modus_log::*;
use std::ffi::CStr;
use std::ptr::null_mut;
use vulkan_sys as vk;
use vulkan_sys::VkQueueFamilyProperties;

#[derive(Debug)]
pub(crate) struct PhysicalDevice {
    pub(crate) device: vk::VkPhysicalDevice,
    pub(crate) graphics_queue_index: u32,
    pub(crate) presentation_queue_index: u32,
    pub(crate) properties: vk::VkPhysicalDeviceProperties,
    pub(crate) features: vk::VkPhysicalDeviceFeatures,
}

#[derive(Debug, Copy, Clone)]
pub(crate) enum PhysicalDeviceError {
    NoVkDeviceFound,
    NoSuitableDeviceFound,
}

pub(crate) fn build_required_device_extension_list() -> Vec<*const std::os::raw::c_char> {
    vec![
        CStr::from_bytes_with_nul(vk::VK_KHR_SWAPCHAIN_EXTENSION_NAME)
            .unwrap()
            .as_ptr() as *const std::os::raw::c_char,
        CStr::from_bytes_with_nul(vk::VK_KHR_MAINTENANCE1_EXTENSION_NAME)
            .unwrap()
            .as_ptr() as *const std::os::raw::c_char,
    ]
}

fn physical_device_type_to_str(t: vk::VkPhysicalDeviceType) -> &'static str {
    use vk::VkPhysicalDeviceType::*;
    match t {
        VK_PHYSICAL_DEVICE_TYPE_DISCRETE_GPU => "Discrete",
        VK_PHYSICAL_DEVICE_TYPE_INTEGRATED_GPU => "Integrated",
        VK_PHYSICAL_DEVICE_TYPE_VIRTUAL_GPU => "Virtual",
        VK_PHYSICAL_DEVICE_TYPE_CPU => "CPU",
        _ => "Unknown",
    }
}

fn check_device_features(features: &vk::VkPhysicalDeviceFeatures) -> bool {
    features.samplerAnisotropy != 0
}

#[derive(Debug, Copy, Clone)]
struct QueuesIndices {
    graphics: Option<u32>,
    presentation: Option<u32>,
}

impl QueuesIndices {
    fn is_complete(&self) -> bool {
        self.graphics.is_some() && self.presentation.is_some()
    }
}

//noinspection ALL
fn find_device_queues_indices(
    device: vk::VkPhysicalDevice,
    surface: vk::VkSurfaceKHR,
) -> QueuesIndices {
    use vk::VkQueueFlagBits::*;
    let mut indices = QueuesIndices {
        graphics: None,
        presentation: None,
    };
    let mut queue_family_count = 0_u32;

    unsafe {
        vk::vkGetPhysicalDeviceQueueFamilyProperties(device, &mut queue_family_count, null_mut())
    };

    if queue_family_count == 0 {
        return indices;
    }

    let mut queue_families = vk_uninit_vec::<VkQueueFamilyProperties>(queue_family_count as usize);
    unsafe {
        vk::vkGetPhysicalDeviceQueueFamilyProperties(
            device,
            &mut queue_family_count,
            queue_families.as_mut_ptr(),
        )
    };

    for (index, queue_family) in queue_families.iter().enumerate() {
        if queue_family.queueFlags & (VK_QUEUE_GRAPHICS_BIT as u32) != 0 {
            indices.graphics = Some(index as u32);
        }

        let mut present_support = 0_u32;
        unsafe {
            vk::vkGetPhysicalDeviceSurfaceSupportKHR(
                device,
                index as u32,
                surface,
                &mut present_support,
            )
        };
        if present_support != 0 {
            indices.presentation = Some(index as u32)
        }

        if indices.is_complete() {
            break;
        }
    }

    indices
}

fn check_device_extensions(device: vk::VkPhysicalDevice) -> bool {
    let mut extension_count = 0_u32;
    unsafe {
        vk::vkEnumerateDeviceExtensionProperties(
            device,
            null_mut(),
            &mut extension_count,
            null_mut(),
        )
    };

    if extension_count == 0 {
        return false;
    }

    let mut available_extensions =
        vk_uninit_vec::<vk::VkExtensionProperties>(extension_count as usize);
    unsafe {
        vk::vkEnumerateDeviceExtensionProperties(
            device,
            null_mut(),
            &mut extension_count,
            available_extensions.as_mut_ptr(),
        )
    };

    let required_extensions = build_required_device_extension_list();

    log_debug!("Available device extensions:");
    for ext in &available_extensions {
        log_debug!(
            "    {}",
            unsafe { CStr::from_ptr(ext.extensionName.as_ptr()) }.to_string_lossy()
        );
    }

    let mut extension_found_count = 0_usize;
    for ext in &available_extensions {
        for required_ext in &required_extensions {
            if unsafe {
                CStr::from_ptr(ext.extensionName.as_ptr()) == CStr::from_ptr(*required_ext)
            } {
                extension_found_count += 1;
                break;
            }
        }
    }
    extension_found_count == required_extensions.len()
}

fn check_device_swap_chain_support(
    device: vk::VkPhysicalDevice,
    surface: vk::VkSurfaceKHR,
) -> bool {
    let mut format_count = 0_u32;
    let mut present_mode_count = 0_u32;
    unsafe {
        vk::vkGetPhysicalDeviceSurfaceFormatsKHR(device, surface, &mut format_count, null_mut());
        vk::vkGetPhysicalDeviceSurfacePresentModesKHR(
            device,
            surface,
            &mut present_mode_count,
            null_mut(),
        );
    }
    format_count != 0 && present_mode_count != 0
}

impl PhysicalDevice {
    pub(crate) fn create(
        instance: &Instance,
        render_surface: &dyn RenderSurfaceInterface,
    ) -> Result<PhysicalDevice, PhysicalDeviceError> {
        let vk_instance = &instance.vk_instance;

        let mut device_count = 0_u32;
        unsafe {
            vk::vkEnumeratePhysicalDevices(vk_instance.value(), &mut device_count, null_mut())
        };
        if device_count == 0 {
            log_error!("No available GPUs with Vulkan support");
            return Err(NoVkDeviceFound);
        }

        let mut physical_devices = vk_uninit_vec::<vk::VkPhysicalDevice>(device_count as usize);
        unsafe {
            vk::vkEnumeratePhysicalDevices(
                vk_instance.value(),
                &mut device_count,
                physical_devices.as_mut_ptr(),
            )
        };

        // Try to find an usable device
        log_debug!("Available Vulkan Devices:");
        for pdevice in &mut physical_devices {
            let mut device_properties = vk_zero_init::<vk::VkPhysicalDeviceProperties>();
            unsafe {
                vk::vkGetPhysicalDeviceProperties(*pdevice, &mut device_properties);
            }
            log_debug!(
                "   name:{} type:{}",
                unsafe { CStr::from_ptr(device_properties.deviceName.as_ptr()) }.to_string_lossy(),
                physical_device_type_to_str(device_properties.deviceType)
            )
        }

        let vk_surface = unsafe {
            std::mem::transmute::<NativeSurfacePtr, vk::VkSurfaceKHR>(
                render_surface.native_surface_handle(),
            )
        };

        let check_device = |device: vk::VkPhysicalDevice,
                            device_type: vk::VkPhysicalDeviceType|
         -> Option<PhysicalDevice> {
            let mut device_properties = vk_zero_init::<vk::VkPhysicalDeviceProperties>();
            let mut device_features = vk_zero_init::<vk::VkPhysicalDeviceFeatures>();
            unsafe {
                vk::vkGetPhysicalDeviceProperties(device, &mut device_properties);
                vk::vkGetPhysicalDeviceFeatures(device, &mut device_features);
            }
            log_debug!(
                "Analyzing device name:{} type:{}",
                unsafe { CStr::from_ptr(device_properties.deviceName.as_ptr()) }.to_string_lossy(),
                physical_device_type_to_str(device_properties.deviceType)
            );

            if device_properties.deviceType == device_type
                && check_device_features(&device_features)
            {
                let indices = find_device_queues_indices(device, vk_surface);
                let extensions_supported = check_device_extensions(device);
                let swap_chain_adequate = if extensions_supported {
                    check_device_swap_chain_support(device, vk_surface)
                } else {
                    false
                };

                if indices.is_complete() && extensions_supported && swap_chain_adequate {
                    log_debug!(
                        "Found suitable device name:{} type:{}",
                        unsafe { CStr::from_ptr(device_properties.deviceName.as_ptr()) }
                            .to_string_lossy(),
                        physical_device_type_to_str(device_properties.deviceType)
                    );
                    return Some(PhysicalDevice {
                        device,
                        graphics_queue_index: indices.graphics.unwrap(),
                        presentation_queue_index: indices.presentation.unwrap(),
                        properties: device_properties,
                        features: device_features,
                    });
                }
            }
            None
        };

        // check for dedicated device first
        log_debug!("Checking for discrete vulkan gpus");
        for pdevice in &mut physical_devices {
            if let Some(device) = check_device(
                *pdevice,
                vk::VkPhysicalDeviceType::VK_PHYSICAL_DEVICE_TYPE_DISCRETE_GPU,
            ) {
                return Ok(device);
            }
        }
        log_debug!("Checking for integrated vulkan gpus");
        for pdevice in &mut physical_devices {
            if let Some(device) = check_device(
                *pdevice,
                vk::VkPhysicalDeviceType::VK_PHYSICAL_DEVICE_TYPE_INTEGRATED_GPU,
            ) {
                return Ok(device);
            }
        }
        log_error!("Failed to find a suitable device");
        Err(PhysicalDeviceError::NoSuitableDeviceFound)
    }

    pub(crate) fn logical_device_create_features(&self) -> vk::VkPhysicalDeviceFeatures {
        let mut features = vk_zero_init::<vk::VkPhysicalDeviceFeatures>();
        features.samplerAnisotropy = 1;
        features
    }
}
