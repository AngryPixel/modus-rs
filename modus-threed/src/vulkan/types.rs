use crate::types::*;
use vulkan_sys::*;

pub(crate) fn shader_type_to_vk(t: ShaderType) -> VkShaderStageFlagBits {
    match t {
        ShaderType::Vertex => VkShaderStageFlagBits::VK_SHADER_STAGE_VERTEX_BIT,
        ShaderType::Fragment => VkShaderStageFlagBits::VK_SHADER_STAGE_FRAGMENT_BIT,
        ShaderType::Compute => VkShaderStageFlagBits::VK_SHADER_STAGE_COMPUTE_BIT,
    }
}

pub(crate) fn primitive_to_vk(t: Primitive) -> VkPrimitiveTopology {
    match t {
        Primitive::Points => VkPrimitiveTopology::VK_PRIMITIVE_TOPOLOGY_POINT_LIST,
        Primitive::Lines => VkPrimitiveTopology::VK_PRIMITIVE_TOPOLOGY_LINE_LIST,
        Primitive::LineStrip => VkPrimitiveTopology::VK_PRIMITIVE_TOPOLOGY_LINE_STRIP,
        Primitive::Triangles => VkPrimitiveTopology::VK_PRIMITIVE_TOPOLOGY_TRIANGLE_LIST,
        Primitive::TriangleStrip => VkPrimitiveTopology::VK_PRIMITIVE_TOPOLOGY_TRIANGLE_STRIP,
        Primitive::TriangleFan => VkPrimitiveTopology::VK_PRIMITIVE_TOPOLOGY_TRIANGLE_FAN,
    }
}

pub(crate) fn polygon_mode_to_vk(t: PolygonMode) -> VkPolygonMode {
    match t {
        PolygonMode::Fill => VkPolygonMode::VK_POLYGON_MODE_FILL,
        PolygonMode::Point => VkPolygonMode::VK_POLYGON_MODE_POINT,
        PolygonMode::Line => VkPolygonMode::VK_POLYGON_MODE_LINE,
    }
}

pub(crate) fn cull_mode_to_vk(t: CullMode) -> VkCullModeFlags {
    match t {
        CullMode::None => VkCullModeFlagBits::VK_CULL_MODE_NONE as u32,
        CullMode::Front => VkCullModeFlagBits::VK_CULL_MODE_FRONT_BIT as u32,
        CullMode::Back => VkCullModeFlagBits::VK_CULL_MODE_BACK_BIT as u32,
        CullMode::FrontAndBack => VkCullModeFlagBits::VK_CULL_MODE_FRONT_AND_BACK as u32,
    }
}

pub(crate) fn front_face_to_vk(t: FrontFace) -> VkFrontFace {
    match t {
        FrontFace::ClockWise => VkFrontFace::VK_FRONT_FACE_CLOCKWISE,
        FrontFace::CounterClockwise => VkFrontFace::VK_FRONT_FACE_COUNTER_CLOCKWISE,
    }
}

pub(crate) fn compare_op_to_vk(t: CompareOp) -> VkCompareOp {
    match t {
        CompareOp::Never => VkCompareOp::VK_COMPARE_OP_NEVER,
        CompareOp::Less => VkCompareOp::VK_COMPARE_OP_LESS,
        CompareOp::LessOrEqual => VkCompareOp::VK_COMPARE_OP_LESS_OR_EQUAL,
        CompareOp::Greater => VkCompareOp::VK_COMPARE_OP_GREATER,
        CompareOp::GreaterOrEqual => VkCompareOp::VK_COMPARE_OP_GREATER_OR_EQUAL,
        CompareOp::Equal => VkCompareOp::VK_COMPARE_OP_EQUAL,
        CompareOp::NotEqual => VkCompareOp::VK_COMPARE_OP_NOT_EQUAL,
        CompareOp::Always => VkCompareOp::VK_COMPARE_OP_ALWAYS,
    }
}

pub(crate) fn stencil_op_to_vk(t: StencilOp) -> VkStencilOp {
    match t {
        StencilOp::Keep => VkStencilOp::VK_STENCIL_OP_KEEP,
        StencilOp::Zero => VkStencilOp::VK_STENCIL_OP_ZERO,
        StencilOp::Replace => VkStencilOp::VK_STENCIL_OP_REPLACE,
        StencilOp::IncrementAndClamp => VkStencilOp::VK_STENCIL_OP_INCREMENT_AND_CLAMP,
        StencilOp::DecrementAndClamp => VkStencilOp::VK_STENCIL_OP_DECREMENT_AND_CLAMP,
        StencilOp::Invert => VkStencilOp::VK_STENCIL_OP_INVERT,
        StencilOp::IncrementAndWrap => VkStencilOp::VK_STENCIL_OP_INCREMENT_AND_WRAP,
        StencilOp::DecrementAndWrap => VkStencilOp::VK_STENCIL_OP_DECREMENT_AND_WRAP,
    }
}

pub(crate) fn blend_op_to_vk(t: BlendOp) -> VkBlendOp {
    match t {
        BlendOp::Add => VkBlendOp::VK_BLEND_OP_ADD,
        BlendOp::Subtract => VkBlendOp::VK_BLEND_OP_SUBTRACT,
        BlendOp::SubtractReverse => VkBlendOp::VK_BLEND_OP_REVERSE_SUBTRACT,
        BlendOp::Min => VkBlendOp::VK_BLEND_OP_MIN,
        BlendOp::Max => VkBlendOp::VK_BLEND_OP_MAX,
    }
}

pub(crate) fn blend_factor_to_vk(t: BlendFactor) -> VkBlendFactor {
    match t {
        BlendFactor::Zero => VkBlendFactor::VK_BLEND_FACTOR_ZERO,
        BlendFactor::One => VkBlendFactor::VK_BLEND_FACTOR_ONE,
        BlendFactor::SourceColor => VkBlendFactor::VK_BLEND_FACTOR_SRC_COLOR,
        BlendFactor::OneMinusSourceColor => VkBlendFactor::VK_BLEND_FACTOR_ONE_MINUS_SRC_COLOR,
        BlendFactor::SourceAlpha => VkBlendFactor::VK_BLEND_FACTOR_SRC_ALPHA,
        BlendFactor::OneMinusSourceAlpha => VkBlendFactor::VK_BLEND_FACTOR_ONE_MINUS_SRC_ALPHA,
        BlendFactor::DestinationColor => VkBlendFactor::VK_BLEND_FACTOR_DST_COLOR,
        BlendFactor::OneMinusDestinationColor => VkBlendFactor::VK_BLEND_FACTOR_ONE_MINUS_DST_COLOR,
        BlendFactor::DestinationAlpha => VkBlendFactor::VK_BLEND_FACTOR_DST_ALPHA,
        BlendFactor::OneMinusDestinationAlpha => VkBlendFactor::VK_BLEND_FACTOR_ONE_MINUS_DST_ALPHA,
        BlendFactor::ConstantColor => VkBlendFactor::VK_BLEND_FACTOR_CONSTANT_COLOR,
        BlendFactor::OneMinusConstantColor => {
            VkBlendFactor::VK_BLEND_FACTOR_ONE_MINUS_CONSTANT_COLOR
        }
        BlendFactor::ConstantAlpha => VkBlendFactor::VK_BLEND_FACTOR_CONSTANT_ALPHA,
        BlendFactor::OneMinusConstantAlpha => {
            VkBlendFactor::VK_BLEND_FACTOR_ONE_MINUS_CONSTANT_ALPHA
        }
        BlendFactor::SourceAlphaSaturate => VkBlendFactor::VK_BLEND_FACTOR_SRC_ALPHA_SATURATE,
    }
}

pub(crate) fn logic_op_to_vk(t: LogicOp) -> VkLogicOp {
    match t {
        LogicOp::Clear => VkLogicOp::VK_LOGIC_OP_CLEAR,
        LogicOp::And => VkLogicOp::VK_LOGIC_OP_AND,
        LogicOp::AndReverse => VkLogicOp::VK_LOGIC_OP_AND_REVERSE,
        LogicOp::Copy => VkLogicOp::VK_LOGIC_OP_COPY,
        LogicOp::AndInverted => VkLogicOp::VK_LOGIC_OP_AND_INVERTED,
        LogicOp::NoOp => VkLogicOp::VK_LOGIC_OP_NO_OP,
        LogicOp::Xor => VkLogicOp::VK_LOGIC_OP_XOR,
        LogicOp::Or => VkLogicOp::VK_LOGIC_OP_OR,
        LogicOp::Nor => VkLogicOp::VK_LOGIC_OP_NOR,
        LogicOp::Equivalent => VkLogicOp::VK_LOGIC_OP_EQUIVALENT,
        LogicOp::Invert => VkLogicOp::VK_LOGIC_OP_INVERT,
        LogicOp::Reverse => VkLogicOp::VK_LOGIC_OP_OR_REVERSE,
        LogicOp::CopyInverted => VkLogicOp::VK_LOGIC_OP_COPY_INVERTED,
        LogicOp::Nand => VkLogicOp::VK_LOGIC_OP_NAND,
        LogicOp::Set => VkLogicOp::VK_LOGIC_OP_SET,
    }
}

pub(crate) fn pipeline_stage_flags_to_vk(flags: u8) -> u32 {
    let mut vk_stages = 0u32;
    if flags & PIPELINE_STAGE_FLAG_VERTEX == PIPELINE_STAGE_FLAG_VERTEX {
        vk_stages |= VkShaderStageFlagBits::VK_SHADER_STAGE_VERTEX_BIT as u32;
    }
    if flags & PIPELINE_STAGE_FLAG_FRAGMENT == PIPELINE_STAGE_FLAG_FRAGMENT {
        vk_stages |= VkShaderStageFlagBits::VK_SHADER_STAGE_FRAGMENT_BIT as u32;
    }
    vk_stages
}

pub(crate) fn memory_type_to_vma(t: MemoryType) -> VmaMemoryUsage {
    match t {
        MemoryType::CpuOnly => VmaMemoryUsage::VMA_MEMORY_USAGE_CPU_ONLY,
        MemoryType::GpuOnly => VmaMemoryUsage::VMA_MEMORY_USAGE_GPU_ONLY,
        MemoryType::CpuToGpu => VmaMemoryUsage::VMA_MEMORY_USAGE_CPU_TO_GPU,
        MemoryType::GpuToCpu => VmaMemoryUsage::VMA_MEMORY_USAGE_GPU_TO_CPU,
        MemoryType::CpuCopy => VmaMemoryUsage::VMA_MEMORY_USAGE_CPU_COPY,
        MemoryType::GpuLazyAlloc => VmaMemoryUsage::VMA_MEMORY_USAGE_GPU_LAZILY_ALLOCATED,
    }
}

pub(crate) fn index_type_to_vk(t: IndexType) -> VkIndexType {
    match t {
        IndexType::U16 => VkIndexType::VK_INDEX_TYPE_UINT16,
        IndexType::U32 => VkIndexType::VK_INDEX_TYPE_UINT32,
        IndexType::None => VkIndexType::VK_INDEX_TYPE_MAX_ENUM,
    }
}

pub(crate) fn filter_to_vk(t: Filter) -> VkFilter {
    match t {
        Filter::Nearest => VkFilter::VK_FILTER_NEAREST,
        Filter::Linear => VkFilter::VK_FILTER_LINEAR,
    }
}

pub(crate) fn sampler_mipmap_mode_to_vk(t: SamplerMipmapMode) -> VkSamplerMipmapMode {
    match t {
        SamplerMipmapMode::Nearest => VkSamplerMipmapMode::VK_SAMPLER_MIPMAP_MODE_NEAREST,
        SamplerMipmapMode::Linear => VkSamplerMipmapMode::VK_SAMPLER_MIPMAP_MODE_LINEAR,
    }
}

pub(crate) fn sampler_address_mode_to_vk(t: SamplerAddressMode) -> VkSamplerAddressMode {
    match t {
        SamplerAddressMode::Repeat => VkSamplerAddressMode::VK_SAMPLER_ADDRESS_MODE_REPEAT,
        SamplerAddressMode::MirroredRepeat => {
            VkSamplerAddressMode::VK_SAMPLER_ADDRESS_MODE_MIRRORED_REPEAT
        }
        SamplerAddressMode::ClampToEdge => {
            VkSamplerAddressMode::VK_SAMPLER_ADDRESS_MODE_CLAMP_TO_EDGE
        }
        SamplerAddressMode::ClampToBorder => {
            VkSamplerAddressMode::VK_SAMPLER_ADDRESS_MODE_CLAMP_TO_BORDER
        }
    }
}
