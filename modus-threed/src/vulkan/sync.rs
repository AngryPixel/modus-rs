use crate::vulkan::utils::{
    vk_result_to_str, VkFenceHandle, VkSemaphoreHandle, MAX_FRAMES_IN_FLIGHT,
};
use modus_core::ArrayVec;
use std::ptr::null_mut;

use crate::device::{DeviceError, DeviceResult};
use crate::vulkan::device::Device;
use vulkan_sys::*;

#[derive(Debug)]
pub(crate) struct Fence {
    fence: VkFenceHandle,
}

#[allow(unused)]
impl Fence {
    pub(crate) fn new() -> Self {
        Self {
            fence: VkFenceHandle::null(),
        }
    }

    pub(crate) fn create(device: &Device, signaled: bool) -> DeviceResult<Fence> {
        let mut fence = Fence::new();
        fence.init(device, signaled)?;
        Ok(fence)
    }

    pub(crate) fn init(&mut self, device: &Device, signaled: bool) -> DeviceResult<()> {
        assert!(!self.fence.is_valid());
        let fence_info = VkFenceCreateInfo {
            sType: VkStructureType::VK_STRUCTURE_TYPE_FENCE_CREATE_INFO,
            pNext: null_mut(),
            flags: if signaled {
                VkFenceCreateFlagBits::VK_FENCE_CREATE_SIGNALED_BIT as u32
            } else {
                0
            },
        };

        unsafe {
            let r3 = vkCreateFence(
                device.state.logical_device.value(),
                &fence_info,
                null_mut(),
                self.fence.ptr_mut(),
            );
            if r3 != VkResult::VK_SUCCESS {
                return Err(DeviceError::Api(format!(
                    "Failed to create fence: {}",
                    vk_result_to_str(r3)
                )));
            }
        }
        Ok(())
    }

    pub(crate) fn destroy(&mut self, device: &Device) {
        let device = device.state.logical_device.value();
        unsafe {
            self.fence.destroy_with(|f| {
                vkDestroyFence(device, f, null_mut());
            });
        }
    }

    #[inline(always)]
    pub(crate) fn vk_handle(&self) -> VkFence {
        self.fence.value()
    }

    pub(crate) fn reset(&self, device: VkDevice) -> DeviceResult<()> {
        unsafe {
            let r = vkResetFences(device, 1, &self.fence.value());
            if r != VkResult::VK_SUCCESS {
                return Err(DeviceError::Api(format!(
                    "Failed to reset fence: {}",
                    vk_result_to_str(r)
                )));
            }
        }
        Ok(())
    }

    pub(crate) fn wait(&self, device: VkDevice, timeout_nano_seconds: u64) -> DeviceResult<()> {
        unsafe {
            let r = vkWaitForFences(
                device,
                1,
                &self.fence.value(),
                VK_TRUE,
                timeout_nano_seconds,
            );
            if r != VkResult::VK_SUCCESS {
                return Err(DeviceError::Api(format!(
                    "Failed to wait on fence: {}",
                    vk_result_to_str(r)
                )));
            }
        }
        Ok(())
    }

    pub(crate) fn wait_multiple(
        device: VkDevice,
        fences: &[Fence],
        wait_all: bool,
        timeout_nano_seconds: u64,
    ) -> DeviceResult<()> {
        const MAX_FENCES: usize = 36;
        assert!(fences.len() < MAX_FENCES);
        let mut vk_fences = ArrayVec::<VkFence, MAX_FENCES>::new();
        for fence in fences {
            vk_fences.push(fence.vk_handle());
        }

        unsafe {
            let r = vkWaitForFences(
                device,
                vk_fences.len() as u32,
                vk_fences.as_ptr(),
                if wait_all { VK_TRUE } else { 0 },
                timeout_nano_seconds,
            );
            if r != VkResult::VK_SUCCESS {
                return Err(DeviceError::Api(format!(
                    "Failed to wait on fences: {}",
                    vk_result_to_str(r)
                )));
            }
        }
        Ok(())
    }
}

#[derive(Debug)]
pub(crate) struct Semaphore {
    h: VkSemaphoreHandle,
}

impl Semaphore {
    pub(crate) fn new() -> Semaphore {
        Self {
            h: Default::default(),
        }
    }

    pub(crate) fn init(&mut self, device: &Device) -> DeviceResult<()> {
        let info = VkSemaphoreCreateInfo {
            sType: VkStructureType::VK_STRUCTURE_TYPE_SEMAPHORE_CREATE_INFO,
            pNext: null_mut(),
            flags: 0,
        };
        unsafe {
            let r = vkCreateSemaphore(
                device.state.logical_device.value(),
                &info,
                null_mut(),
                self.h.ptr_mut(),
            );
            if r != VkResult::VK_SUCCESS {
                return Err(DeviceError::Api(format!(
                    "Failed to create semaphore: {}",
                    vk_result_to_str(r)
                )));
            }
        }
        Ok(())
    }

    pub(crate) fn destroy(&mut self, device: &Device) {
        let device = device.state.logical_device.value();
        unsafe {
            self.h.destroy_with(|s| {
                vkDestroySemaphore(device, s, null_mut());
            });
        }
    }

    #[inline(always)]
    pub(crate) fn vk_handle(&self) -> VkSemaphore {
        self.h.value()
    }
}

#[derive(Debug)]
pub(crate) struct FrameSyncObjects {
    pub(crate) fences: [Fence; MAX_FRAMES_IN_FLIGHT],
    pub(crate) image_available: [VkSemaphoreHandle; MAX_FRAMES_IN_FLIGHT],
    pub(crate) render_finished: [VkSemaphoreHandle; MAX_FRAMES_IN_FLIGHT],
}

impl FrameSyncObjects {
    pub(crate) fn new() -> Self {
        Self {
            fences: [Fence::new(), Fence::new(), Fence::new()],
            image_available: [
                VkSemaphoreHandle::null(),
                VkSemaphoreHandle::null(),
                VkSemaphoreHandle::null(),
            ],
            render_finished: [
                VkSemaphoreHandle::null(),
                VkSemaphoreHandle::null(),
                VkSemaphoreHandle::null(),
            ],
        }
    }

    pub(crate) fn init(&mut self, device: &Device) -> DeviceResult<()> {
        let semaphore_info = VkSemaphoreCreateInfo {
            sType: VkStructureType::VK_STRUCTURE_TYPE_SEMAPHORE_CREATE_INFO,
            pNext: null_mut(),
            flags: 0,
        };

        for i in 0..MAX_FRAMES_IN_FLIGHT {
            unsafe {
                let r1 = vkCreateSemaphore(
                    device.state.logical_device.value(),
                    &semaphore_info,
                    null_mut(),
                    self.render_finished[i].ptr_mut(),
                );
                if r1 != VkResult::VK_SUCCESS {
                    return Err(DeviceError::Api(format!(
                        "Failed to create semaphore: {}",
                        vk_result_to_str(r1)
                    )));
                }
                let r2 = vkCreateSemaphore(
                    device.state.logical_device.value(),
                    &semaphore_info,
                    null_mut(),
                    self.image_available[i].ptr_mut(),
                );
                if r2 != VkResult::VK_SUCCESS {
                    return Err(DeviceError::Api(format!(
                        "Failed to create semaphore: {}",
                        vk_result_to_str(r2)
                    )));
                }
                self.fences[i].init(device, true)?;
            }
        }
        Ok(())
    }

    pub(crate) fn destroy(&mut self, device: &Device) {
        for fence in &mut self.fences {
            fence.destroy(device);
        }
        unsafe {
            let device = device.state.logical_device.value();
            for semaphore in &mut self.image_available {
                semaphore.destroy_with(|s| {
                    vkDestroySemaphore(device, s, null_mut());
                });
            }

            for semaphore in &mut self.render_finished {
                semaphore.destroy_with(|s| {
                    vkDestroySemaphore(device, s, null_mut());
                });
            }
        }
    }

    pub(crate) fn rebuild(&mut self, device: &Device) -> DeviceResult<()> {
        self.destroy(device);
        self.init(device)
    }
}
