use crate::device::{DeviceError, DeviceResult};
use crate::vulkan::device::Device;
use modus_core::handle_map::{
    HandleMapIter, HandleMapIterMut, HandleTrait, SharedData, SharedHandleMap,
};
use modus_core::HashMap;
use std::collections::hash_map::Entry;
use std::hash::Hash;
use std::mem::MaybeUninit;
use std::ptr::null_mut;

pub(crate) const MAX_FRAMES_IN_FLIGHT: usize = 3;

pub(crate) fn vk_zero_init<T>() -> T {
    unsafe { MaybeUninit::<T>::zeroed().assume_init() }
}

#[allow(clippy::uninit_vec)]
pub(crate) fn vk_uninit_vec<T>(size: usize) -> Vec<T> {
    let mut v = Vec::<T>::with_capacity(size);
    unsafe { v.set_len(size) };
    v
}

fn make_vk_api_version(variant: u32, major: u32, minor: u32, patch: u32) -> u32 {
    (variant << 29) | (major << 22) | (minor << 12) | patch
}

pub(crate) fn vk_api_version_1_0() -> u32 {
    make_vk_api_version(0, 1, 0, 0)
}

pub(crate) fn vk_make_version(major: u32, minor: u32, patch: u32) -> u32 {
    (major << 22) | (minor << 12) | patch
}

pub(crate) fn vk_result_to_str(result: vulkan_sys::VkResult) -> &'static str {
    use vulkan_sys::VkResult::*;
    match result {
        VK_SUCCESS => "Success",
        VK_NOT_READY => "Not ready",
        VK_TIMEOUT => "Timeout",
        VK_EVENT_SET => "Event set",
        VK_EVENT_RESET => "Event reset",
        VK_INCOMPLETE => "Incomplete",
        VK_ERROR_OUT_OF_HOST_MEMORY => "Out of host memory",
        VK_ERROR_OUT_OF_DEVICE_MEMORY => "Out of device memory",
        VK_ERROR_INITIALIZATION_FAILED => "Initialization failed",
        VK_ERROR_DEVICE_LOST => "Device Lost",
        VK_ERROR_MEMORY_MAP_FAILED => "Memory map failed",
        VK_ERROR_LAYER_NOT_PRESENT => "Layer not present",
        VK_ERROR_EXTENSION_NOT_PRESENT => "Extension not present",
        VK_ERROR_FEATURE_NOT_PRESENT => "Feature not present",
        VK_ERROR_INCOMPATIBLE_DRIVER => "Incompatible driver",
        VK_ERROR_TOO_MANY_OBJECTS => "Too many objects",
        VK_ERROR_FORMAT_NOT_SUPPORTED => "Format not supported",
        VK_ERROR_FRAGMENTED_POOL => "Fragmented Pool",
        VK_ERROR_UNKNOWN => "Uknown error",
        VK_ERROR_OUT_OF_POOL_MEMORY => "Out of pool memory",
        VK_ERROR_INVALID_EXTERNAL_HANDLE => "Invalid external handle",
        VK_ERROR_FRAGMENTATION => "Fragmentation",
        VK_ERROR_INVALID_OPAQUE_CAPTURE_ADDRESS => "Invalid opaque capture address",
        VK_ERROR_SURFACE_LOST_KHR => "Surface lost (KHR)",
        VK_ERROR_NATIVE_WINDOW_IN_USE_KHR => "Window in use (KHR)",
        VK_SUBOPTIMAL_KHR => "Suboptimal (KHR)",
        VK_ERROR_OUT_OF_DATE_KHR => "Out of date (KHR)",
        VK_ERROR_INCOMPATIBLE_DISPLAY_KHR => "Incompatible display (KHR)",
        VK_ERROR_VALIDATION_FAILED_EXT => "Validation Failed",
        VK_ERROR_INVALID_SHADER_NV => "Invalid Shader (NV)",
        VK_ERROR_INVALID_DRM_FORMAT_MODIFIER_PLANE_LAYOUT_EXT => {
            "Invalid drm format modifier plane layout (EXT)"
        }
        VK_ERROR_NOT_PERMITTED_EXT => "Not permitted (EXT)",
        VK_ERROR_FULL_SCREEN_EXCLUSIVE_MODE_LOST_EXT => {
            "Full screen mode exclusive mode lost (EXT)"
        }
        VK_THREAD_IDLE_KHR => "Thread idle (KHR)",
        VK_THREAD_DONE_KHR => "Thread done (KHR)",
        VK_OPERATION_DEFERRED_KHR => "Operation deferred (KHR)",
        VK_OPERATION_NOT_DEFERRED_KHR => "Operation not deferred (KHR)",
        VK_PIPELINE_COMPILE_REQUIRED_EXT => "Pipeline compile required (EXT)",
        _ => "Unhandled error",
    }
}

macro_rules! gen_vk_handle {
    ($name:ident, $t:ty) => {
        #[derive(Debug, Eq, PartialEq)]
        pub(crate) struct $name {
            h: $t,
        }
        #[allow(unused)]
        impl $name {
            pub(crate) fn null() -> Self {
                $name { h: null_mut() }
            }
            pub(crate) fn is_valid(&self) -> bool {
                !self.h.is_null()
            }
            pub(crate) unsafe fn ptr_mut(&mut self) -> &mut $t {
                &mut self.h
            }
            pub(crate) fn value(&self) -> $t {
                self.h
            }
            pub(crate) fn destroy_with<F: FnOnce($t)>(&mut self, f: F) {
                if self.is_valid() {
                    f(self.h);
                    self.h = null_mut();
                }
            }
        }
        impl Default for $name {
            fn default() -> Self {
                Self::null()
            }
        }
        unsafe impl Send for $name {}
    };
    ($name:ident, $t:ty, $dtor:expr) => {
        gen_vk_handle!($name, $t);
        #[allow(unused_unsafe)]
        impl Drop for $name {
            fn drop(&mut self) {
                if self.is_valid() {
                    unsafe { $dtor(self.h) }
                    self.h = null_mut();
                }
            }
        }
    };
}

macro_rules! gen_vk_ndhandle {
    ($name:ident, $t:ty) => {
        #[derive(Debug, Eq, PartialEq)]
        pub(crate) struct $name {
            h: $t,
        }
        #[allow(unused)]
        impl $name {
            #[cfg(target_pointer_width = "64")]
            pub(crate) fn null() -> Self {
                $name { h: null_mut() }
            }
            #[cfg(target_pointer_width = "32")]
            pub(crate) fn null() -> Self {
                $name { h: 0_u64 }
            }
            #[cfg(target_pointer_width = "32")]
            pub(crate) fn is_valid(&self) -> bool {
                self.h != 0_u64
            }
            #[cfg(target_pointer_width = "64")]
            pub(crate) fn is_valid(&self) -> bool {
                !self.h.is_null()
            }
            pub(crate) unsafe fn ptr_mut(&mut self) -> &mut $t {
                &mut self.h
            }
            pub(crate) fn value(&self) -> $t {
                self.h
            }
            #[cfg(target_pointer_width = "32")]
            pub(crate) fn destroy_with<F: FnOnce($t)>(&mut self, f: F) {
                if self.is_valid() {
                    f(self.h);
                    self.h = 0_u64;
                }
            }
            #[cfg(target_pointer_width = "64")]
            pub(crate) fn destroy_with<F: FnOnce($t)>(&mut self, f: F) {
                if self.is_valid() {
                    f(self.h);
                    self.h = null_mut();
                }
            }
        }
        impl Drop for $name {
            fn drop(&mut self) {
                assert!(!self.is_valid(), "Did not release this vulkan handle");
            }
        }
        impl Default for $name {
            fn default() -> Self {
                Self::null()
            }
        }
        unsafe impl Send for $name {}
    };
}

gen_vk_handle!(
    VkDeviceHandle,
    vulkan_sys::VkDevice,
    |v: vulkan_sys::VkDevice| {
        vulkan_sys::vkDestroyDevice(v, null_mut());
    }
);

gen_vk_handle!(
    VkInstanceHandle,
    vulkan_sys::VkInstance,
    |v: vulkan_sys::VkInstance| {
        vulkan_sys::vkDestroyInstance(v, null_mut());
    }
);

gen_vk_handle!(
    VkVmaAllocHandle,
    vulkan_sys::VmaAllocator,
    |vma: vulkan_sys::VmaAllocator| {
        vulkan_sys::vmaDestroyAllocator(vma);
    }
);

gen_vk_handle!(VkQueueHandle, vulkan_sys::VkQueue, |_| {});

gen_vk_ndhandle!(
    VkDebugUtilsMessengerHandle,
    vulkan_sys::VkDebugUtilsMessengerEXT
);

gen_vk_ndhandle!(VkSwapChainHandle, vulkan_sys::VkSwapchainKHR);
gen_vk_ndhandle!(VkImageHandle, vulkan_sys::VkImage);
gen_vk_ndhandle!(VkImageViewHandle, vulkan_sys::VkImageView);
gen_vk_ndhandle!(VkFramebufferHandle, vulkan_sys::VkFramebuffer);
gen_vk_ndhandle!(VkRenderPassHandle, vulkan_sys::VkRenderPass);
gen_vk_ndhandle!(VkFenceHandle, vulkan_sys::VkFence);
gen_vk_ndhandle!(VkSemaphoreHandle, vulkan_sys::VkSemaphore);
gen_vk_ndhandle!(VkCommandPoolHandle, vulkan_sys::VkCommandPool);
gen_vk_handle!(VkCommandBufferHandle, vulkan_sys::VkCommandBuffer);
gen_vk_handle!(VmaAllocationHandle, vulkan_sys::VmaAllocation, |_| {});
gen_vk_ndhandle!(VkBufferHandle, vulkan_sys::VkBuffer);
gen_vk_ndhandle!(VkShaderHandle, vulkan_sys::VkShaderModule);
gen_vk_ndhandle!(VkPipelineHandle, vulkan_sys::VkPipeline);
gen_vk_ndhandle!(VkPipelineLayoutHandle, vulkan_sys::VkPipelineLayout);
gen_vk_ndhandle!(
    VkDescriptorSetLayoutHandle,
    vulkan_sys::VkDescriptorSetLayout
);
gen_vk_ndhandle!(VkDescriptorPoolHandle, vulkan_sys::VkDescriptorPool);
gen_vk_ndhandle!(VkDescriptorSetHandle, vulkan_sys::VkDescriptorSet);
gen_vk_ndhandle!(VkSamplerHandle, vulkan_sys::VkSampler);

/// Trait which is required to ensure that when a resource is being deleted it can be cleaned up
/// properly.
pub(crate) trait DeferredResourceDeleter<T> {
    fn delete(&mut self, value: &mut T);
}

/// Create a new DeferredResourceDeleter for the current type
pub(crate) trait DeferredResourceDeleterBuilder<'a, T> {
    type Deleter: DeferredResourceDeleter<T>;
    fn create_deleter(device: &'a Device) -> Self::Deleter;
}

#[derive(Debug)]
struct DeferredDeleteQueue<T> {
    index: usize,
    queues: [Vec<T>; MAX_FRAMES_IN_FLIGHT],
}

impl<T> DeferredDeleteQueue<T> {
    fn push(&mut self, value: T) {
        self.queues[self.index].push(value);
    }

    fn clear(&mut self, deleter: &mut impl DeferredResourceDeleter<T>) {
        self.index = (self.index + 1) % MAX_FRAMES_IN_FLIGHT;
        for item in &mut self.queues[self.index] {
            deleter.delete(item);
        }
        self.queues[self.index].clear();
    }

    fn clear_all(&mut self, deleter: &mut impl DeferredResourceDeleter<T>) {
        for queue in &mut self.queues {
            for item in queue.iter_mut() {
                deleter.delete(item);
            }
            queue.clear();
        }
    }
}

/// Intrusive Reference Tracked resource map for vulkan resources. To indicate the resource is used
/// somewhere else, use the [acquire] function. When you a resource needs to be destroyed use the
/// [release]. When the resource's references reach 0, it gets added to the deletion queue, which
/// be cleaned on frame N + MAX_FRAMES_IN_FLIGHT, assuming [clear_deferred()] is called on every
/// frame.
#[derive(Debug)]
pub(crate) struct ResourceMap<H, T>
where
    H: HandleTrait,
{
    map: SharedHandleMap<H, T>,
    deferred_deletes: DeferredDeleteQueue<T>,
}

#[allow(unused)]
impl<H: HandleTrait, T> ResourceMap<H, T> {
    pub(crate) fn new(elems_per_block: usize) -> Self {
        Self {
            map: SharedHandleMap::new(elems_per_block),
            deferred_deletes: DeferredDeleteQueue {
                index: 0,
                queues: [
                    Vec::with_capacity(8),
                    Vec::with_capacity(8),
                    Vec::with_capacity(8),
                ],
            },
        }
    }

    pub(crate) fn len(&self) -> usize {
        self.map.len()
    }

    pub(crate) fn is_empty(&self) -> bool {
        self.map.is_empty()
    }

    pub(crate) fn insert(&mut self, value: T) -> Result<H, DeviceError> {
        match self.map.insert(value) {
            Ok(h) => Ok(h),
            Err(v) => {
                self.deferred_deletes.push(v.release());
                Err(DeviceError::Api(
                    "Failed to take ownership of the resource".to_string(),
                ))
            }
        }
    }

    pub(crate) fn insert_with<F: FnOnce(H, &mut T)>(
        &mut self,
        value: T,
        f: F,
    ) -> Result<H, DeviceError> {
        match self.map.insert_with(value, f) {
            Ok(h) => Ok(h),
            Err(v) => {
                self.deferred_deletes.push(v.release());
                Err(DeviceError::Api(
                    "Failed to take ownership of the resource".to_string(),
                ))
            }
        }
    }

    pub(crate) fn get(&self, handle: H) -> Option<&'_ SharedData<T>> {
        self.map.get(handle)
    }

    pub(crate) fn get_mut(&mut self, handle: H) -> Option<&'_ mut SharedData<T>> {
        self.map.get_mut(handle)
    }

    pub(crate) fn contains_handle(&self, handle: H) -> bool {
        self.map.contains_handle(handle)
    }

    pub(crate) fn acquire(&mut self, handle: H) {
        self.map.acquire(handle)
    }

    pub(crate) fn get_and_acquire(&mut self, handle: H) -> Option<&'_ T> {
        self.map.get_and_acquire(handle)
    }

    pub(crate) fn get_mut_and_acquire(&mut self, handle: H) -> Option<&'_ mut T> {
        self.map.get_mut_and_acquire(handle)
    }

    pub(crate) fn release(&mut self, handle: H) {
        if let Some(t) = self.map.release(handle) {
            self.deferred_deletes.push(t);
        }
    }

    pub(crate) fn retain<F>(&mut self, mut f: F)
    where
        F: FnMut(&T) -> bool,
    {
        self.map.retain(f)
    }

    pub(crate) fn iter(&self) -> HandleMapIter<'_, H, SharedData<T>> {
        self.map.iter()
    }

    pub(crate) fn iter_mut(&mut self) -> HandleMapIterMut<'_, H, SharedData<T>> {
        self.map.iter_mut()
    }
}

#[allow(unused)]
impl<'a, H: HandleTrait, T: DeferredResourceDeleterBuilder<'a, T>> ResourceMap<H, T> {
    pub(crate) fn clear_deferred(&mut self, device: &'a Device) {
        let mut deleter = T::create_deleter(device);
        self.deferred_deletes.clear(&mut deleter);
    }

    pub(crate) fn clear(&mut self, device: &'a Device) {
        let mut deleter = T::create_deleter(device);
        self.deferred_deletes.clear_all(&mut deleter);
        for mut item in self.map.iter_mut() {
            deleter.delete(item);
        }
        self.map.clear();
    }
}

impl<H: HandleTrait, T> Drop for ResourceMap<H, T> {
    fn drop(&mut self) {
        // If you trigger this assert, it means the resources weren't properly cleaned up
        debug_assert!(self.is_empty());
    }
}

/// Hashed resource map which ensures that unique resource are never duplicated but re-used. When
/// we find a a resource that matches an existing one we increment the reference count rather
/// than creating a new one.
#[derive(Debug)]
pub(crate) struct HashedResourceMap<H: HandleTrait, K: Hash + Eq, T> {
    hash_map: HashMap<K, H>,
    handle_map: ResourceMap<H, T>,
}

#[allow(unused)]
impl<H: HandleTrait, K: Hash + Eq, T> HashedResourceMap<H, K, T> {
    pub(crate) fn new(elems_per_block: usize) -> Self {
        Self {
            hash_map: HashMap::with_capacity(8),
            handle_map: ResourceMap::new(elems_per_block),
        }
    }

    pub(crate) fn len(&self) -> usize {
        self.handle_map.len()
    }

    pub(crate) fn is_empty(&self) -> bool {
        self.handle_map.is_empty()
    }

    pub(crate) fn insert(
        &mut self,
        key: K,
        create_fn: impl FnOnce() -> DeviceResult<T>,
    ) -> DeviceResult<H> {
        match self.hash_map.entry(key) {
            Entry::Occupied(mut o) => {
                let current_handle = *o.get();
                if self.handle_map.get_and_acquire(current_handle).is_some() {
                    Ok(current_handle)
                } else {
                    match self.handle_map.insert(create_fn()?) {
                        Ok(h) => {
                            o.insert(h);
                            Ok(h)
                        }
                        Err(e) => Err(e),
                    }
                }
            }
            Entry::Vacant(v) => match self.handle_map.insert(create_fn()?) {
                Ok(h) => {
                    v.insert(h);
                    Ok(h)
                }
                Err(e) => Err(e),
            },
        }
    }

    pub(crate) fn insert_with<F: FnOnce(H, &mut T)>(
        &mut self,
        key: K,
        create_fn: impl FnOnce() -> DeviceResult<T>,
        f: F,
    ) -> Result<H, DeviceError> {
        match self.hash_map.entry(key) {
            Entry::Occupied(mut o) => {
                let current_handle = *o.get();
                if self.handle_map.get_and_acquire(current_handle).is_some() {
                    Ok(current_handle)
                } else {
                    match self.handle_map.insert_with(create_fn()?, f) {
                        Ok(h) => {
                            o.insert(h);
                            Ok(h)
                        }
                        Err(e) => Err(e),
                    }
                }
            }
            Entry::Vacant(v) => match self.handle_map.insert_with(create_fn()?, f) {
                Ok(h) => {
                    v.insert(h);
                    Ok(h)
                }
                Err(e) => Err(e),
            },
        }
    }

    pub(crate) fn get(&self, handle: H) -> Option<&'_ SharedData<T>> {
        self.handle_map.get(handle)
    }

    pub(crate) fn get_mut(&mut self, handle: H) -> Option<&'_ mut SharedData<T>> {
        self.handle_map.get_mut(handle)
    }

    pub(crate) fn contains_handle(&self, handle: H) -> bool {
        self.handle_map.contains_handle(handle)
    }

    pub(crate) fn acquire(&mut self, handle: H) {
        self.handle_map.acquire(handle)
    }

    pub(crate) fn get_and_acquire(&mut self, handle: H) -> Option<&'_ T> {
        self.handle_map.get_and_acquire(handle)
    }

    pub(crate) fn get_mut_and_acquire(&mut self, handle: H) -> Option<&'_ mut T> {
        self.handle_map.get_mut_and_acquire(handle)
    }

    pub(crate) fn release(&mut self, handle: H) {
        self.handle_map.release(handle)
    }

    pub(crate) fn retain<F>(&mut self, f: F)
    where
        F: FnMut(&T) -> bool,
    {
        self.handle_map.retain(f)
    }

    pub(crate) fn iter(&self) -> HandleMapIter<'_, H, SharedData<T>> {
        self.handle_map.iter()
    }

    pub(crate) fn iter_mut(&mut self) -> HandleMapIterMut<'_, H, SharedData<T>> {
        self.handle_map.iter_mut()
    }
}

#[allow(unused)]
impl<'a, H: HandleTrait, K: Hash + Eq, T: DeferredResourceDeleterBuilder<'a, T>>
    HashedResourceMap<H, K, T>
{
    pub(crate) fn clear_deferred(&mut self, device: &'a Device) {
        self.handle_map.clear_deferred(device);
    }

    pub(crate) fn clear(&mut self, device: &'a Device) {
        self.handle_map.clear(device);
        self.hash_map.clear();
    }
}
