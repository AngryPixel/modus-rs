//! Vulcan Instance setup
use crate::app_interface::VkAppInterface;
use crate::device::DeviceError;
use crate::instance::NativeInstancePtr;
use crate::vulkan::utils::{
    vk_api_version_1_0, vk_make_version, vk_uninit_vec, VkDebugUtilsMessengerHandle,
    VkInstanceHandle,
};
use modus_log::{log_debug, log_error};
use std::ffi::{c_void, CStr, CString};
use std::mem::{transmute, MaybeUninit};
use std::os::raw::c_char;
use std::ptr::null_mut;
use std::sync::Arc;
use vulkan_sys as vk;

extern "C" fn vk_debug_callback(
    severity: vk::VkDebugUtilsMessageSeverityFlagBitsEXT,
    _message_type: vk::VkDebugUtilsMessageTypeFlagsEXT,
    callback_data: *const vk::VkDebugUtilsMessengerCallbackDataEXT,
    _: *mut c_void,
) -> vk::VkBool32 {
    use vk::VkDebugUtilsMessageSeverityFlagBitsEXT::*;
    if severity as u32 >= VK_DEBUG_UTILS_MESSAGE_SEVERITY_WARNING_BIT_EXT as u32 {
        let cb = unsafe { &*callback_data };
        let message = unsafe { CStr::from_ptr(cb.pMessage) };
        let message_str = message.to_str().unwrap();
        log_error!(target:"Vulkan Validation Layer", "Vukan Error:{}", message_str);
    }
    vk::VK_FALSE
}

#[derive(Debug)]
struct DebugUtilsMessenger {
    instance: vk::VkInstance,
    handle: VkDebugUtilsMessengerHandle,
}

impl DebugUtilsMessenger {
    fn create(instance: vk::VkInstance) -> Result<Self, vk::VkResult> {
        use vk::VkDebugUtilsMessageSeverityFlagBitsEXT::*;
        use vk::VkDebugUtilsMessageTypeFlagBitsEXT::*;
        let create_info = vk::VkDebugUtilsMessengerCreateInfoEXT {
            sType: vk::VkStructureType::VK_STRUCTURE_TYPE_DEBUG_UTILS_MESSENGER_CREATE_INFO_EXT,
            pNext: std::ptr::null_mut(),
            messageSeverity: VK_DEBUG_UTILS_MESSAGE_SEVERITY_VERBOSE_BIT_EXT as u32
                | VK_DEBUG_UTILS_MESSAGE_SEVERITY_WARNING_BIT_EXT as u32
                | VK_DEBUG_UTILS_MESSAGE_SEVERITY_ERROR_BIT_EXT as u32,
            messageType: VK_DEBUG_UTILS_MESSAGE_TYPE_GENERAL_BIT_EXT as u32
                | VK_DEBUG_UTILS_MESSAGE_TYPE_VALIDATION_BIT_EXT as u32,
            pUserData: std::ptr::null_mut(),
            flags: 0,
            pfnUserCallback: Some(vk_debug_callback),
        };
        let mut debug_messenger = VkDebugUtilsMessengerHandle::null();
        let result = if let Some(func) = unsafe {
            transmute::<vk::PFN_vkVoidFunction, vk::PFN_vkCreateDebugUtilsMessengerEXT>(
                vk::vkGetInstanceProcAddr(
                    instance,
                    CStr::from_bytes_with_nul_unchecked(
                        "vkCreateDebugUtilsMessengerEXT\0".as_bytes(),
                    )
                    .as_ptr(),
                ),
            )
        } {
            unsafe {
                func(
                    instance,
                    &create_info,
                    std::ptr::null_mut(),
                    debug_messenger.ptr_mut(),
                )
            }
        } else {
            return Err(vk::VkResult::VK_ERROR_EXTENSION_NOT_PRESENT);
        };

        if result == vk::VkResult::VK_SUCCESS {
            Ok(Self {
                instance,
                handle: debug_messenger,
            })
        } else {
            Err(result)
        }
    }
}

impl Drop for DebugUtilsMessenger {
    fn drop(&mut self) {
        unsafe {
            if let Some(func) = transmute::<
                vk::PFN_vkVoidFunction,
                vk::PFN_vkDestroyDebugUtilsMessengerEXT,
            >(vk::vkGetInstanceProcAddr(
                self.instance,
                CStr::from_bytes_with_nul_unchecked("vkDestroyDebugUtilsMessengerEXT\0".as_bytes())
                    .as_ptr(),
            )) {
                let inst = self.instance;
                self.handle.destroy_with(|h| {
                    func(inst, h, std::ptr::null_mut());
                });
            }
        }
    }
}

#[derive(Debug)]
pub(crate) struct Instance {
    _debug_messenger: Option<DebugUtilsMessenger>,
    pub(crate) vk_instance: VkInstanceHandle,
    pub(crate) use_validation_layers: bool,
}

fn check_validation_layer_support(debug: bool) -> bool {
    let mut layer_count = 0u32;
    unsafe { vk::vkEnumerateInstanceLayerProperties(&mut layer_count, std::ptr::null_mut()) };

    let mut available_layers = vk_uninit_vec::<vk::VkLayerProperties>(layer_count as usize);
    unsafe {
        vk::vkEnumerateInstanceLayerProperties(&mut layer_count, available_layers.as_mut_ptr());
    }

    if debug {
        if layer_count != 0 {
            log_debug!("Available Vulkan Layers:");
            for layer in &available_layers {
                log_debug!("   {:?}", unsafe {
                    CStr::from_ptr(layer.layerName.as_ptr())
                });
            }
        } else {
            log_debug!("No available Vulkan Layers");
        }
    }

    let required_layers = ["VK_LAYER_KHRONOS_validation"];
    for required_layer in required_layers {
        let mut layer_found = false;
        for layer in &available_layers {
            let layer_name_cstr = unsafe { CStr::from_ptr(layer.layerName.as_ptr()) };
            match layer_name_cstr.to_str() {
                Ok(str) => {
                    if required_layer == str {
                        layer_found = true;
                        break;
                    }
                }
                Err(_) => {
                    log_error!("Failed to convert layer name to utf8 {:?}", layer.layerName);
                    return false;
                }
            }
        }

        if !layer_found {
            log_error!("Failed to find required vk layer: {}", required_layer);
            return false;
        }
    }
    true
}

fn log_instance_extensions() {
    let mut ext_count = 0u32;
    unsafe {
        vk::vkEnumerateInstanceExtensionProperties(
            std::ptr::null_mut(),
            &mut ext_count,
            std::ptr::null_mut(),
        )
    };
    if ext_count != 0 {
        let mut extensions = vk_uninit_vec::<vk::VkExtensionProperties>(ext_count as usize);
        unsafe {
            vk::vkEnumerateInstanceExtensionProperties(
                std::ptr::null_mut(),
                &mut ext_count,
                extensions.as_mut_ptr(),
            );
            extensions.set_len(ext_count as usize);
        }
        log_debug!("Available Instance extensions:");
        for ext in &extensions {
            let name_cstr = unsafe { CStr::from_ptr(ext.extensionName.as_ptr()) };
            if let Ok(ext) = name_cstr.to_str() {
                log_debug!("   {}", ext);
            } else {
                log_debug!("    Failed to convert ext to utf8 {:?}", ext.extensionName);
            }
        }
    } else {
        log_debug!("No instance extensions available");
    }
}

impl Instance {
    pub(crate) fn build_validation_layer_list() -> Vec<*const std::os::raw::c_char> {
        vec![
            CStr::from_bytes_with_nul("VK_LAYER_KHRONOS_validation\0".as_bytes())
                .unwrap()
                .as_ptr() as *const std::os::raw::c_char,
        ]
    }

    pub(crate) fn new(
        info: &crate::instance::InstanceCreateInfo,
        app_interface: &dyn VkAppInterface,
    ) -> Result<Self, crate::instance::InstanceError> {
        crate::profile_scope!();
        if info.api_validation && !check_validation_layer_support(info.debug) {
            return Err(crate::instance::InstanceError::MissingValidationLayers);
        }

        if info.debug {
            log_instance_extensions();
        }

        let mut required_extension_list = match app_interface.required_extension_list() {
            Ok(ext) => ext,
            Err(e) => {
                log_error!("Failed to get app extension:{}", e);
                return Err(crate::instance::InstanceError::AppInterface);
            }
        };

        if info.debug {
            required_extension_list
                .push(vk::VK_EXT_DEBUG_UTILS_EXTENSION_NAME.as_ptr() as *const c_char);
        }

        if !required_extension_list.is_empty() {
            log_debug!("Attempting to create vulkan instance with the following extensions:");
            for ext in &required_extension_list {
                let name_cstr = unsafe { CStr::from_ptr(*ext) };
                if let Ok(ext_str) = name_cstr.to_str() {
                    log_debug!("   {}", ext_str);
                } else {
                    log_debug!("    Failed to convert ext to utf8 {:?}", ext);
                }
            }
        }

        let app_name_cstr = CString::new(info.name).unwrap_or_default();
        let engine_name_cstr = CString::new("threed").unwrap_or_default();
        let version_major = 0_u32;
        let version_minor = 1_u32;
        let version_patch = 0_u32;

        let mut app_info = unsafe { MaybeUninit::<vk::VkApplicationInfo>::zeroed().assume_init() };
        app_info.sType = vk::VkStructureType::VK_STRUCTURE_TYPE_APPLICATION_INFO;
        app_info.pApplicationName = app_name_cstr.as_ptr();
        app_info.applicationVersion = vk_make_version(version_major, version_minor, version_patch);
        app_info.pEngineName = engine_name_cstr.as_ptr();
        app_info.engineVersion = app_info.applicationVersion;
        app_info.apiVersion = vk_api_version_1_0();

        let validation_layers = if info.api_validation {
            Self::build_validation_layer_list()
        } else {
            vec![]
        };

        let create_info = vk::VkInstanceCreateInfo {
            sType: vk::VkStructureType::VK_STRUCTURE_TYPE_INSTANCE_CREATE_INFO,
            pNext: std::ptr::null(),
            pApplicationInfo: &mut app_info,
            enabledExtensionCount: required_extension_list.len() as u32,
            ppEnabledExtensionNames: required_extension_list.as_ptr(),
            flags: 0,
            enabledLayerCount: validation_layers.len() as u32,
            ppEnabledLayerNames: validation_layers.as_ptr(),
        };

        let mut vk_instance = VkInstanceHandle::null();
        if unsafe {
            vk::vkCreateInstance(&create_info, std::ptr::null_mut(), vk_instance.ptr_mut())
        } != vk::VkResult::VK_SUCCESS
        {
            return Err(crate::instance::InstanceError::Setup);
        }

        let debug_messenger = if info.debug {
            match DebugUtilsMessenger::create(vk_instance.value()) {
                Err(_) => {
                    return Err(crate::instance::InstanceError::Setup);
                }
                Ok(d) => Some(d),
            }
        } else {
            None
        };

        debug_assert!(vk_instance.is_valid());
        let instance = Self {
            vk_instance,
            _debug_messenger: debug_messenger,
            use_validation_layers: info.api_validation,
        };

        Ok(instance)
    }
}

impl Drop for Instance {
    fn drop(&mut self) {}
}

impl crate::instance::Instance for Instance {
    fn create_device(
        self: Arc<Self>,
        render_surface: &dyn crate::app_interface::RenderSurfaceInterface,
    ) -> Result<Arc<dyn crate::device::Device>, DeviceError> {
        crate::vulkan::device::Device::create(self, render_surface)
    }

    unsafe fn native_ptr(&self) -> NativeInstancePtr {
        self.vk_instance.value() as NativeInstancePtr
    }

    unsafe fn destroy_render_surface(&self, surface: crate::app_interface::NativeSurfacePtr) {
        #[cfg(target_pointer_width = "64")]
        let is_null = surface.is_null();
        #[cfg(target_pointer_width = "32")]
        let is_null = surface == 0;
        if !is_null {
            vk::vkDestroySurfaceKHR(
                self.vk_instance.value(),
                surface as vk::VkSurfaceKHR,
                null_mut(),
            );
        }
    }
}
