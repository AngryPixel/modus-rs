use crate::device::DeviceError;
use crate::shader::ShaderCreateInfo;
use crate::types::ShaderHandle;
use crate::vulkan::device::Device;
use crate::vulkan::utils::{
    vk_result_to_str, DeferredResourceDeleter, DeferredResourceDeleterBuilder, ResourceMap,
    VkShaderHandle,
};
use std::ptr::null_mut;
use vulkan_sys::*;

#[derive(Debug)]
pub(crate) struct Shader {
    pub(crate) module: VkShaderHandle,
}

impl Shader {
    pub(crate) fn create(device: &Device, info: &ShaderCreateInfo) -> Result<Shader, DeviceError> {
        let mut shader = VkShaderHandle::null();
        unsafe {
            let create_info = VkShaderModuleCreateInfo {
                sType: VkStructureType::VK_STRUCTURE_TYPE_SHADER_MODULE_CREATE_INFO,
                pNext: null_mut(),
                flags: 0,
                codeSize: info.data.len() as size_t,
                pCode: std::mem::transmute(info.data.as_ptr()),
            };

            let r = vkCreateShaderModule(
                device.state.logical_device.value(),
                &create_info,
                null_mut(),
                shader.ptr_mut(),
            );
            if r != VkResult::VK_SUCCESS {
                return Err(DeviceError::Api(format!(
                    "Failed to create shader: {}",
                    vk_result_to_str(r)
                )));
            }
        }

        Ok(Shader { module: shader })
    }

    fn destroy(&mut self, device: VkDevice) {
        unsafe {
            self.module.destroy_with(|s| {
                vkDestroyShaderModule(device, s, null_mut());
            });
        }
    }
}

pub(crate) type ShaderMap = ResourceMap<ShaderHandle, Shader>;

pub(crate) struct ShaderDeleter {
    device: VkDevice,
}

impl ShaderDeleter {
    pub(crate) fn new(device: &Device) -> Self {
        Self {
            device: device.state.logical_device.value(),
        }
    }
}

impl DeferredResourceDeleter<Shader> for ShaderDeleter {
    fn delete(&mut self, shader: &mut Shader) {
        shader.destroy(self.device);
    }
}

impl DeferredResourceDeleterBuilder<'_, Shader> for Shader {
    type Deleter = ShaderDeleter;
    fn create_deleter(device: &Device) -> Self::Deleter {
        Self::Deleter::new(device)
    }
}
