use crate::descriptors::*;
use crate::device::{DeviceError, DeviceResult};
use crate::types::{
    DescriptorPoolHandle, DescriptorSetHandle, DescriptorSetLayoutHandle, DeviceSize,
};
use crate::vulkan::device::Device;
use crate::vulkan::types::pipeline_stage_flags_to_vk;
use crate::vulkan::utils::{
    vk_result_to_str, vk_zero_init, DeferredResourceDeleter, DeferredResourceDeleterBuilder,
    HashedResourceMap, ResourceMap, VkDescriptorPoolHandle, VkDescriptorSetLayoutHandle,
};
use modus_core::ArrayVec;
use std::cell::RefMut;
use std::ptr::null_mut;
use vulkan_sys::*;

const MAX_DESCRIPTOR_COUNT: usize = 16;
const MAX_PUSH_CONSTANT_COUNT: usize = 4;

const MAX_DESCRIPTORS_IN_KEY: usize = 8;
const MAX_PUSH_CONSTANTS_IN_KEY: usize = 4;
#[derive(Debug, Eq, PartialEq, Hash)]
pub(crate) struct DescriptorSetLayoutKey {
    descriptors: ArrayVec<DescriptorDesc, MAX_DESCRIPTORS_IN_KEY>,
    push_constants: ArrayVec<PushConstantDesc, MAX_PUSH_CONSTANTS_IN_KEY>,
}

impl<'a, 'b> From<&DescriptorSetLayoutCreateInfo<'a, 'b>> for DescriptorSetLayoutKey {
    fn from(info: &DescriptorSetLayoutCreateInfo<'a, 'b>) -> Self {
        assert!(info.descriptors.len() <= MAX_DESCRIPTORS_IN_KEY);
        assert!(info.push_constants.len() <= MAX_PUSH_CONSTANTS_IN_KEY);
        let mut descriptors = ArrayVec::<DescriptorDesc, MAX_DESCRIPTORS_IN_KEY>::new();
        let mut push_constants = ArrayVec::<PushConstantDesc, MAX_PUSH_CONSTANTS_IN_KEY>::new();
        for d in info.descriptors {
            descriptors.push(*d);
        }
        for p in info.push_constants {
            push_constants.push(*p);
        }
        Self {
            descriptors,
            push_constants,
        }
    }
}

#[derive(Debug)]
pub(crate) struct DescriptorSetLayout {
    pub(crate) handle: VkDescriptorSetLayoutHandle,
    pub(crate) push_constants: ArrayVec<PushConstantDesc, MAX_PUSH_CONSTANT_COUNT>,
}

fn descriptor_type_to_vk(dtype: DescriptorType) -> VkDescriptorType {
    match dtype {
        DescriptorType::Constant => VkDescriptorType::VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER,
        DescriptorType::ConstantDynamic => {
            VkDescriptorType::VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER_DYNAMIC
        }
        DescriptorType::Storage => VkDescriptorType::VK_DESCRIPTOR_TYPE_STORAGE_BUFFER,
        DescriptorType::StorageDynamic => {
            VkDescriptorType::VK_DESCRIPTOR_TYPE_STORAGE_BUFFER_DYNAMIC
        }
        DescriptorType::Attachment => VkDescriptorType::VK_DESCRIPTOR_TYPE_INPUT_ATTACHMENT,
        DescriptorType::Texture => VkDescriptorType::VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER,
    }
}

impl DescriptorSetLayout {
    pub(crate) fn create(
        info: &DescriptorSetLayoutCreateInfo,
        device: &Device,
    ) -> Result<DescriptorSetLayout, DeviceError> {
        let mut bindings = ArrayVec::<VkDescriptorSetLayoutBinding, MAX_DESCRIPTOR_COUNT>::new();
        let mut push_constants = ArrayVec::<PushConstantDesc, MAX_PUSH_CONSTANT_COUNT>::new();

        for push_constant in info.push_constants {
            push_constants.push(*push_constant);
        }

        for descriptor in info.descriptors {
            let vk_descriptor_type = descriptor_type_to_vk(descriptor.descriptor_type);
            bindings.push(VkDescriptorSetLayoutBinding {
                binding: descriptor.binding_point,
                descriptorType: vk_descriptor_type,
                descriptorCount: 1,
                stageFlags: pipeline_stage_flags_to_vk(descriptor.stages),
                pImmutableSamplers: null_mut(),
            });
        }

        let create_info = VkDescriptorSetLayoutCreateInfo {
            sType: VkStructureType::VK_STRUCTURE_TYPE_DESCRIPTOR_SET_LAYOUT_CREATE_INFO,
            pNext: null_mut(),
            flags: 0,
            bindingCount: bindings.len() as u32,
            pBindings: bindings.as_ptr(),
        };

        let mut handle = VkDescriptorSetLayoutHandle::null();
        unsafe {
            let r = vkCreateDescriptorSetLayout(
                device.state.logical_device.value(),
                &create_info,
                null_mut(),
                handle.ptr_mut(),
            );
            if r != VkResult::VK_SUCCESS {
                return Err(DeviceError::Api(format!(
                    "Failed to crate descriptor set layout:{}",
                    vk_result_to_str(r)
                )));
            }
        }

        Ok(DescriptorSetLayout {
            handle,
            push_constants,
        })
    }

    fn destroy(&mut self, device: VkDevice) {
        unsafe {
            self.handle.destroy_with(|l| {
                vkDestroyDescriptorSetLayout(device, l, null_mut());
            });
        }
    }
}

pub(crate) type DescriptorSetLayoutMap =
    HashedResourceMap<DescriptorSetLayoutHandle, DescriptorSetLayoutKey, DescriptorSetLayout>;

pub(crate) struct DescriptorSetLayoutDeleter {
    device: VkDevice,
}

impl DescriptorSetLayoutDeleter {
    pub(crate) fn new(device: &Device) -> Self {
        Self {
            device: device.state.logical_device.value(),
        }
    }
}

impl DeferredResourceDeleter<DescriptorSetLayout> for DescriptorSetLayoutDeleter {
    fn delete(&mut self, texture: &mut DescriptorSetLayout) {
        texture.destroy(self.device);
    }
}

impl DeferredResourceDeleterBuilder<'_, DescriptorSetLayout> for DescriptorSetLayout {
    type Deleter = DescriptorSetLayoutDeleter;
    fn create_deleter(device: &Device) -> Self::Deleter {
        Self::Deleter::new(device)
    }
}
// --- Descriptor Pooling -----------------------------------------------------------------------

#[derive(Debug)]
pub(crate) struct DescriptorPoolV2 {
    handle: VkDescriptorPoolHandle,
}

impl DescriptorPoolV2 {
    pub(crate) fn create(
        device: &Device,
        info: &DescriptorPoolCreateInfo,
    ) -> DeviceResult<DescriptorPoolV2> {
        let mut pool_sizes = ArrayVec::<VkDescriptorPoolSize, 8>::new();

        if info.constant_count != 0 {
            pool_sizes.push(VkDescriptorPoolSize {
                type_: VkDescriptorType::VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER,
                descriptorCount: info.constant_count,
            });
        }
        if info.constant_dynamic_count != 0 {
            pool_sizes.push(VkDescriptorPoolSize {
                type_: VkDescriptorType::VK_DESCRIPTOR_TYPE_UNIFORM_BUFFER_DYNAMIC,
                descriptorCount: info.constant_dynamic_count,
            });
        }
        if info.storage_count != 0 {
            pool_sizes.push(VkDescriptorPoolSize {
                type_: VkDescriptorType::VK_DESCRIPTOR_TYPE_STORAGE_BUFFER,
                descriptorCount: info.storage_count,
            });
        }
        if info.storage_dynamic_count != 0 {
            pool_sizes.push(VkDescriptorPoolSize {
                type_: VkDescriptorType::VK_DESCRIPTOR_TYPE_STORAGE_BUFFER_DYNAMIC,
                descriptorCount: info.storage_dynamic_count,
            });
        }
        if info.texture_count != 0 {
            pool_sizes.push(VkDescriptorPoolSize {
                type_: VkDescriptorType::VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER,
                descriptorCount: info.texture_count,
            });
        }
        if info.input_attachment_count != 0 {
            pool_sizes.push(VkDescriptorPoolSize {
                type_: VkDescriptorType::VK_DESCRIPTOR_TYPE_INPUT_ATTACHMENT,
                descriptorCount: info.input_attachment_count,
            });
        }

        let pool_info = VkDescriptorPoolCreateInfo {
            sType: VkStructureType::VK_STRUCTURE_TYPE_DESCRIPTOR_POOL_CREATE_INFO,
            pNext: null_mut(),
            flags: 0,
            maxSets: info.max_set_count,
            poolSizeCount: pool_sizes.len() as u32,
            pPoolSizes: pool_sizes.as_ptr(),
        };

        let mut pool_handle = VkDescriptorPoolHandle::null();
        unsafe {
            let r = vkCreateDescriptorPool(
                device.state.logical_device.value(),
                &pool_info,
                null_mut(),
                pool_handle.ptr_mut(),
            );
            if r != VkResult::VK_SUCCESS {
                return Err(DeviceError::Api(format!(
                    "Failed to create descriptor pool: {}",
                    vk_result_to_str(r)
                )));
            }
        }
        Ok(DescriptorPoolV2 {
            handle: pool_handle,
        })
    }

    pub(crate) fn destroy(&mut self, device: VkDevice) {
        unsafe {
            self.handle.destroy_with(|dp| {
                vkDestroyDescriptorPool(device, dp, null_mut());
            });
        }
    }
}

pub(crate) type DescriptorPoolMap = ResourceMap<DescriptorPoolHandle, DescriptorPoolV2>;

pub(crate) struct DescriptorPoolDeleter {
    device: VkDevice,
}

impl DescriptorPoolDeleter {
    pub(crate) fn new(device: &Device) -> Self {
        Self {
            device: device.state.logical_device.value(),
        }
    }
}

impl DeferredResourceDeleter<DescriptorPoolV2> for DescriptorPoolDeleter {
    fn delete(&mut self, dp: &mut DescriptorPoolV2) {
        dp.destroy(self.device);
    }
}

impl DeferredResourceDeleterBuilder<'_, DescriptorPoolV2> for DescriptorPoolV2 {
    type Deleter = DescriptorPoolDeleter;
    fn create_deleter(device: &Device) -> Self::Deleter {
        Self::Deleter::new(device)
    }
}

#[derive(Debug)]
pub(crate) struct DescriptorSetV2 {
    pub(crate) handle: VkDescriptorSet,
    pool: DescriptorPoolHandle,
    layout: DescriptorSetLayoutHandle,
}

impl DescriptorSetV2 {
    pub(crate) fn create(
        device: &Device,
        layout_handle: DescriptorSetLayoutHandle,
        layouts: &mut DescriptorSetLayoutMap,
        pool_handle: DescriptorPoolHandle,
        pools: &mut DescriptorPoolMap,
    ) -> DeviceResult<DescriptorSetV2> {
        let layout = if let Some(l) = layouts.get(layout_handle) {
            l.handle.value()
        } else {
            return Err(DeviceError::Api(format!(
                "Failed to resolve descriptor layout:{}",
                layout_handle
            )));
        };

        let pool = if let Some(p) = pools.get(pool_handle) {
            p.handle.value()
        } else {
            return Err(DeviceError::Api(format!(
                "Failed to resolve descriptor pool:{}",
                pool_handle
            )));
        };

        let mut descriptor_set = vk_zero_init::<VkDescriptorSet>();
        let alloc_info = VkDescriptorSetAllocateInfo {
            sType: VkStructureType::VK_STRUCTURE_TYPE_DESCRIPTOR_SET_ALLOCATE_INFO,
            pNext: null_mut(),
            descriptorPool: pool,
            descriptorSetCount: 1,
            pSetLayouts: &layout,
        };
        unsafe {
            let r = vkAllocateDescriptorSets(
                device.state.logical_device.value(),
                &alloc_info,
                &mut descriptor_set,
            );
            if r != VkResult::VK_SUCCESS {
                if r == VkResult::VK_ERROR_FRAGMENTED_POOL
                    || r == VkResult::VK_ERROR_OUT_OF_POOL_MEMORY
                {
                    return Err(DeviceError::OutOfMemory);
                } else {
                    return Err(DeviceError::Api(format!(
                        "Failed to allocate descriptor set:{}",
                        vk_result_to_str(r)
                    )));
                }
            }
        }

        pools.acquire(pool_handle);
        layouts.acquire(layout_handle);

        Ok(DescriptorSetV2 {
            handle: descriptor_set,
            pool: pool_handle,
            layout: layout_handle,
        })
    }

    pub(crate) fn destroy(
        &mut self,
        layouts: &mut DescriptorSetLayoutMap,
        pools: &mut DescriptorPoolMap,
    ) {
        layouts.release(self.layout);
        pools.release(self.pool);
    }

    pub(crate) fn update(
        &mut self,
        device: &Device,
        info: &DescriptorSetUpdateInfo,
    ) -> Result<(), DeviceError> {
        // write data to descriptor set

        let mut descriptor_writes = ArrayVec::<VkWriteDescriptorSet, MAX_DESCRIPTOR_COUNT>::new();
        let mut buffer_writes = ArrayVec::<VkDescriptorBufferInfo, MAX_DESCRIPTOR_COUNT>::new();
        let mut texture_writes = ArrayVec::<VkDescriptorImageInfo, MAX_DESCRIPTOR_COUNT>::new();

        if !info.buffers.is_empty() {
            let buffers = device.resources.buffers.try_borrow()?;
            for buffer in info.buffers {
                let vk_buffer = if let Some(b) = buffers.get(buffer.buffer) {
                    b.handle.value()
                } else {
                    return Err(DeviceError::Api(format!(
                        "Failed to resolve buffer handle for descriptor {}",
                        buffer.buffer
                    )));
                };
                buffer_writes.push(VkDescriptorBufferInfo {
                    buffer: vk_buffer,
                    offset: buffer.buffer_offset as VkDeviceSize,
                    range: buffer.buffer_size.unwrap_or(VK_WHOLE_SIZE as DeviceSize)
                        as VkDeviceSize,
                });
                let write_descriptor_set = VkWriteDescriptorSet {
                    sType: VkStructureType::VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET,
                    pNext: null_mut(),
                    dstSet: self.handle,
                    dstBinding: buffer.binding_point,
                    dstArrayElement: 0,
                    descriptorCount: 1,
                    descriptorType: descriptor_type_to_vk(buffer.dtype),
                    pImageInfo: null_mut(),
                    pBufferInfo: unsafe { buffer_writes.as_ptr().add(buffer_writes.len() - 1) },
                    pTexelBufferView: null_mut(),
                };
                descriptor_writes.push(write_descriptor_set);
            }
        }

        if !info.textures.is_empty() {
            let textures = device.resources.textures.try_borrow()?;
            let samplers = device.resources.samplers.try_borrow()?;
            for texture in info.textures {
                let vk_texture = if let Some(b) = textures.get(texture.texture) {
                    b.view.value()
                } else {
                    return Err(DeviceError::Api(format!(
                        "Failed to resolve texture handle for descriptor {}",
                        texture.texture
                    )));
                };
                let vk_sampler = if let Some(b) = samplers.get(texture.sampler) {
                    b.handle.value()
                } else {
                    return Err(DeviceError::Api(format!(
                        "Failed to resolve sampler handle for descriptor {}",
                        texture.sampler
                    )));
                };
                texture_writes.push(VkDescriptorImageInfo {
                    sampler: vk_sampler,
                    imageView: vk_texture,
                    imageLayout: VkImageLayout::VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL,
                });
                let write_descriptor_set = VkWriteDescriptorSet {
                    sType: VkStructureType::VK_STRUCTURE_TYPE_WRITE_DESCRIPTOR_SET,
                    pNext: null_mut(),
                    dstSet: self.handle,
                    dstBinding: texture.binding_point,
                    dstArrayElement: 0,
                    descriptorCount: 1,
                    descriptorType: VkDescriptorType::VK_DESCRIPTOR_TYPE_COMBINED_IMAGE_SAMPLER,
                    pImageInfo: unsafe { texture_writes.as_ptr().add(texture_writes.len() - 1) },
                    pBufferInfo: null_mut(),
                    pTexelBufferView: null_mut(),
                };
                descriptor_writes.push(write_descriptor_set);
            }
        }

        unsafe {
            vkUpdateDescriptorSets(
                device.state.logical_device.value(),
                descriptor_writes.len() as u32,
                descriptor_writes.as_ptr(),
                0,
                null_mut(),
            );
        }

        Ok(())
    }
}

pub(crate) type DescriptorSetV2Map = ResourceMap<DescriptorSetHandle, DescriptorSetV2>;

pub(crate) struct DescriptorSetV2Deleter<'a> {
    layouts: RefMut<'a, DescriptorSetLayoutMap>,
    pools: RefMut<'a, DescriptorPoolMap>,
}

impl<'a> DescriptorSetV2Deleter<'a> {
    pub(crate) fn new(device: &'a Device) -> Self {
        Self {
            layouts: device.resources.descriptor_set_layouts.borrow_mut(),
            pools: device.resources.descriptor_pools.borrow_mut(),
        }
    }
}

impl<'a> DeferredResourceDeleter<DescriptorSetV2> for DescriptorSetV2Deleter<'a> {
    fn delete(&mut self, dp: &mut DescriptorSetV2) {
        dp.destroy(&mut self.layouts, &mut self.pools)
    }
}

impl<'a> DeferredResourceDeleterBuilder<'a, DescriptorSetV2> for DescriptorSetV2 {
    type Deleter = DescriptorSetV2Deleter<'a>;
    fn create_deleter(device: &'a Device) -> Self::Deleter {
        Self::Deleter::new(device)
    }
}
