use crate::device::DeviceError;
use crate::framebuffer::FramebufferCreateInfo;
use crate::render_pass::MAX_ATTACHMENT_COUNT;
use crate::types::{FramebufferHandle, RenderPassHandle, TextureHandle};
use crate::vulkan::device::Device;
use crate::vulkan::render_pass::RenderPassMap;
use crate::vulkan::texture::TextureMap;
use crate::vulkan::utils::{
    vk_result_to_str, vk_zero_init, DeferredResourceDeleter, DeferredResourceDeleterBuilder,
    ResourceMap, VkFramebufferHandle,
};
use modus_core::ArrayVec;
use std::cell::RefMut;
use std::ptr::null_mut;
use vulkan_sys::*;

#[derive(Debug)]
pub(crate) struct Framebuffer {
    pub(crate) handle: VkFramebufferHandle,
    pub(crate) width: u32,
    pub(crate) height: u32,
    pub(crate) _depth: u32,
    render_pass: RenderPassHandle,
    textures: ArrayVec<TextureHandle, MAX_ATTACHMENT_COUNT>,
}
impl Framebuffer {
    pub(crate) fn create(
        info: &FramebufferCreateInfo,
        device: &Device,
        textures: &mut TextureMap,
        render_passes: &mut RenderPassMap,
    ) -> Result<Framebuffer, DeviceError> {
        let mut images = ArrayVec::<VkImageView, 16>::new();

        let render_pass = if let Some(r) = render_passes.get(info.render_pass) {
            r.handle.value()
        } else {
            return Err(DeviceError::Api(
                "Failed to resolve render pass handle".to_string(),
            ));
        };

        for texture in info.textures {
            if let Some(t) = textures.get(*texture) {
                images.push(t.view.value());
            } else {
                return Err(DeviceError::Api(
                    "Failed to resolve texture handle".to_string(),
                ));
            }
        }

        let mut framebuffer_info = vk_zero_init::<VkFramebufferCreateInfo>();
        framebuffer_info.sType = VkStructureType::VK_STRUCTURE_TYPE_FRAMEBUFFER_CREATE_INFO;
        framebuffer_info.renderPass = render_pass;
        framebuffer_info.attachmentCount = images.len() as u32;
        framebuffer_info.pAttachments = images.as_ptr();
        framebuffer_info.width = info.width;
        framebuffer_info.height = info.height;
        framebuffer_info.layers = info.depth;

        let mut framebuffer = VkFramebufferHandle::null();
        unsafe {
            let r = vkCreateFramebuffer(
                device.state.logical_device.value(),
                &framebuffer_info,
                null_mut(),
                framebuffer.ptr_mut(),
            );
            if r != VkResult::VK_SUCCESS {
                return Err(DeviceError::Api(format!(
                    "Failed to create framebuffer {}",
                    vk_result_to_str(r)
                )));
            }
        }

        let mut texture_vec = ArrayVec::<TextureHandle, MAX_ATTACHMENT_COUNT>::new();
        for texture in info.textures {
            textures.acquire(*texture);
            texture_vec.push(*texture);
        }
        render_passes.acquire(info.render_pass);

        Ok(Framebuffer {
            handle: framebuffer,
            width: info.width,
            height: info.height,
            _depth: info.depth,
            render_pass: info.render_pass,
            textures: texture_vec,
        })
    }
}

pub(crate) type FramebufferMap = ResourceMap<FramebufferHandle, Framebuffer>;

#[derive(Debug)]
pub(crate) struct FramebufferDeleter<'a> {
    textures: RefMut<'a, TextureMap>,
    render_passes: RefMut<'a, RenderPassMap>,
    device: VkDevice,
}

impl<'a> FramebufferDeleter<'a> {
    pub(crate) fn new(device: &'a Device) -> Self {
        Self {
            textures: device.resources.textures.borrow_mut(),
            render_passes: device.resources.render_passes.borrow_mut(),
            device: device.state.logical_device.value(),
        }
    }
}

impl<'a> DeferredResourceDeleter<Framebuffer> for FramebufferDeleter<'a> {
    fn delete(&mut self, framebuffer: &mut Framebuffer) {
        for texture in &framebuffer.textures {
            self.textures.release(*texture);
        }
        self.render_passes.release(framebuffer.render_pass);
        unsafe {
            framebuffer.handle.destroy_with(|f| {
                vkDestroyFramebuffer(self.device, f, null_mut());
            })
        }
    }
}

impl<'a> DeferredResourceDeleterBuilder<'a, Framebuffer> for Framebuffer {
    type Deleter = FramebufferDeleter<'a>;
    fn create_deleter(device: &'a Device) -> Self::Deleter {
        Self::Deleter::new(device)
    }
}
