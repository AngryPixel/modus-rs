//! Vulkan Command buffer implementation

use crate::descriptors::DescriptorSetBindInfo;
use crate::device::{DeviceError, DeviceResult};
use crate::render_pass::{ClearValue, RenderPassClearInfo};
use crate::types::{
    BufferHandle, FramebufferHandle, IndexType, PipelineHandle, Rect, RenderPassHandle,
    SwapChainHandle, TextureHandle, Viewport,
};
use crate::vulkan::device::Device;
use crate::vulkan::types::index_type_to_vk;
use crate::vulkan::utils::vk_zero_init;
use modus_core::ArrayVec;
use std::ptr::null_mut;
use vulkan_sys::*;

#[derive(Debug)]
pub(crate) struct CommandBuffer<'a> {
    pub(crate) device: &'a Device,
    pub(crate) cmd_buffer: VkCommandBuffer,
}

impl<'a> CommandBuffer<'a> {
    fn begin_render_pass(
        &mut self,
        render_pass: VkRenderPass,
        framebuffer: VkFramebuffer,
        dimensions: [u32; 2],
        clear_data: Option<RenderPassClearInfo>,
    ) -> Result<(), DeviceError> {
        let clear_info = if let Some(clear_data) = clear_data {
            clear_data
        } else {
            vk_zero_init::<RenderPassClearInfo>()
        };

        let mut clear_values = ArrayVec::<VkClearValue, 20>::new();
        for color in &clear_info.values {
            match color {
                ClearValue::Color(c) => clear_values.push(VkClearValue {
                    color: VkClearColorValue { float32: *c },
                }),
                ClearValue::DepthStencil(depth, stencil) => clear_values.push(VkClearValue {
                    depthStencil: VkClearDepthStencilValue {
                        depth: *depth,
                        stencil: *stencil as u32,
                    },
                }),
            }
        }

        let render_pass_begin_info = VkRenderPassBeginInfo {
            sType: VkStructureType::VK_STRUCTURE_TYPE_RENDER_PASS_BEGIN_INFO,
            pNext: null_mut(),
            renderPass: render_pass,
            framebuffer,
            renderArea: VkRect2D {
                offset: VkOffset2D { x: 0, y: 0 },
                extent: VkExtent2D {
                    width: dimensions[0],
                    height: dimensions[1],
                },
            },
            clearValueCount: clear_values.len() as u32,
            pClearValues: clear_values.as_ptr(),
        };
        unsafe {
            vkCmdBeginRenderPass(
                self.cmd_buffer,
                &render_pass_begin_info,
                VkSubpassContents::VK_SUBPASS_CONTENTS_INLINE,
            );
        }
        Ok(())
    }
}

impl<'a> crate::command_buffer::CommandBuffer for CommandBuffer<'a> {
    fn blit_texture_to_swap_chain(
        &mut self,
        texture: TextureHandle,
        swap_chain: SwapChainHandle,
    ) -> Result<(), DeviceError> {
        let swap_chains = self.device.resources.swap_chains.try_borrow()?;
        let swap_chain = match swap_chains.get(swap_chain) {
            Some(s) => s,
            None => return Err(DeviceError::Api("Invalid swap chain handle".to_string())),
        };
        let textures = self.device.resources.textures.try_borrow()?;
        let texture = match textures.get(texture) {
            Some(t) => t,
            None => return Err(DeviceError::Api("Invalid texture handle".to_string())),
        };

        // TODO: This will not work for other formats or items that are not attachments
        // transition images to copy src/dst first
        let image_src_barrier = VkImageMemoryBarrier {
            sType: VkStructureType::VK_STRUCTURE_TYPE_IMAGE_MEMORY_BARRIER,
            pNext: null_mut(),
            srcAccessMask: VkAccessFlagBits::VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT as u32,
            dstAccessMask: VkAccessFlagBits::VK_ACCESS_TRANSFER_READ_BIT as u32,
            oldLayout: VkImageLayout::VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL,
            newLayout: VkImageLayout::VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL,
            srcQueueFamilyIndex: VK_QUEUE_FAMILY_IGNORED as u32,
            dstQueueFamilyIndex: VK_QUEUE_FAMILY_IGNORED as u32,
            image: texture.image.value(),
            subresourceRange: VkImageSubresourceRange {
                aspectMask: VkImageAspectFlagBits::VK_IMAGE_ASPECT_COLOR_BIT as u32,
                baseMipLevel: 0,
                levelCount: 1,
                baseArrayLayer: 0,
                layerCount: 1,
            },
        };

        let image_dst_barrier = VkImageMemoryBarrier {
            sType: VkStructureType::VK_STRUCTURE_TYPE_IMAGE_MEMORY_BARRIER,
            pNext: null_mut(),
            srcAccessMask: 0,
            dstAccessMask: VkAccessFlagBits::VK_ACCESS_TRANSFER_WRITE_BIT as u32,
            oldLayout: VkImageLayout::VK_IMAGE_LAYOUT_PRESENT_SRC_KHR,
            newLayout: VkImageLayout::VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL,
            srcQueueFamilyIndex: VK_QUEUE_FAMILY_IGNORED as u32,
            dstQueueFamilyIndex: VK_QUEUE_FAMILY_IGNORED as u32,
            image: swap_chain.get_active_image(),
            subresourceRange: VkImageSubresourceRange {
                aspectMask: VkImageAspectFlagBits::VK_IMAGE_ASPECT_COLOR_BIT as u32,
                baseMipLevel: 0,
                levelCount: 1,
                baseArrayLayer: 0,
                layerCount: 1,
            },
        };

        unsafe {
            vkCmdPipelineBarrier(
                self.cmd_buffer,
                VkPipelineStageFlagBits::VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT as u32,
                VkPipelineStageFlagBits::VK_PIPELINE_STAGE_TRANSFER_BIT as u32,
                VkDependencyFlagBits::VK_DEPENDENCY_BY_REGION_BIT as u32,
                0,
                null_mut(),
                0,
                null_mut(),
                2,
                [image_src_barrier, image_dst_barrier].as_ptr(),
            );
        }

        let src_layout = VkImageLayout::VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL;
        let dst_layout = VkImageLayout::VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL;
        let sw_extent = swap_chain.get_extent();

        if texture.create_info.width != sw_extent.width
            || texture.create_info.height != sw_extent.height
        {
            let blit_region = VkImageBlit {
                srcSubresource: VkImageSubresourceLayers {
                    aspectMask: VkImageAspectFlagBits::VK_IMAGE_ASPECT_COLOR_BIT as u32,
                    mipLevel: 0,
                    baseArrayLayer: 0,
                    layerCount: 1,
                },
                srcOffsets: [
                    VkOffset3D { x: 0, y: 0, z: 0 },
                    VkOffset3D {
                        x: texture.create_info.width as i32,
                        y: texture.create_info.height as i32,
                        z: 1,
                    },
                ],
                dstSubresource: VkImageSubresourceLayers {
                    aspectMask: VkImageAspectFlagBits::VK_IMAGE_ASPECT_COLOR_BIT as u32,
                    mipLevel: 0,
                    baseArrayLayer: 0,
                    layerCount: 1,
                },
                dstOffsets: [
                    VkOffset3D { x: 0, y: 0, z: 0 },
                    VkOffset3D {
                        x: sw_extent.width as i32,
                        y: sw_extent.height as i32,
                        z: 1,
                    },
                ],
            };
            unsafe {
                vkCmdBlitImage(
                    self.cmd_buffer,
                    texture.image.value(),
                    src_layout,
                    swap_chain.get_active_image(),
                    dst_layout,
                    1,
                    &blit_region,
                    VkFilter::VK_FILTER_NEAREST,
                );
            }
        } else {
            let copy_region = VkImageCopy {
                srcSubresource: VkImageSubresourceLayers {
                    aspectMask: VkImageAspectFlagBits::VK_IMAGE_ASPECT_COLOR_BIT as u32,
                    mipLevel: 0,
                    baseArrayLayer: 0,
                    layerCount: 1,
                },
                srcOffset: VkOffset3D { x: 0, y: 0, z: 0 },
                dstSubresource: VkImageSubresourceLayers {
                    aspectMask: VkImageAspectFlagBits::VK_IMAGE_ASPECT_COLOR_BIT as u32,
                    mipLevel: 0,
                    baseArrayLayer: 0,
                    layerCount: 1,
                },
                dstOffset: VkOffset3D { x: 0, y: 0, z: 0 },
                extent: VkExtent3D {
                    width: sw_extent.width,
                    height: sw_extent.height,
                    depth: 1,
                },
            };

            unsafe {
                vkCmdCopyImage(
                    self.cmd_buffer,
                    texture.image.value(),
                    src_layout,
                    swap_chain.get_active_image(),
                    dst_layout,
                    1,
                    &copy_region,
                );
            }
        }

        let image_src_restore_barrier = VkImageMemoryBarrier {
            sType: VkStructureType::VK_STRUCTURE_TYPE_IMAGE_MEMORY_BARRIER,
            pNext: null_mut(),
            srcAccessMask: VkAccessFlagBits::VK_ACCESS_TRANSFER_READ_BIT as u32,
            dstAccessMask: 0,
            oldLayout: VkImageLayout::VK_IMAGE_LAYOUT_TRANSFER_SRC_OPTIMAL,
            newLayout: VkImageLayout::VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL,
            srcQueueFamilyIndex: VK_QUEUE_FAMILY_IGNORED as u32,
            dstQueueFamilyIndex: VK_QUEUE_FAMILY_IGNORED as u32,
            image: texture.image.value(),
            subresourceRange: VkImageSubresourceRange {
                aspectMask: VkImageAspectFlagBits::VK_IMAGE_ASPECT_COLOR_BIT as u32,
                baseMipLevel: 0,
                levelCount: 1,
                baseArrayLayer: 0,
                layerCount: 1,
            },
        };

        let image_dst_restore_barrier = VkImageMemoryBarrier {
            sType: VkStructureType::VK_STRUCTURE_TYPE_IMAGE_MEMORY_BARRIER,
            pNext: null_mut(),
            srcAccessMask: VkAccessFlagBits::VK_ACCESS_TRANSFER_WRITE_BIT as u32,
            dstAccessMask: 0,
            oldLayout: VkImageLayout::VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL,
            newLayout: VkImageLayout::VK_IMAGE_LAYOUT_PRESENT_SRC_KHR,
            srcQueueFamilyIndex: VK_QUEUE_FAMILY_IGNORED as u32,
            dstQueueFamilyIndex: VK_QUEUE_FAMILY_IGNORED as u32,
            image: swap_chain.get_active_image(),
            subresourceRange: VkImageSubresourceRange {
                aspectMask: VkImageAspectFlagBits::VK_IMAGE_ASPECT_COLOR_BIT as u32,
                baseMipLevel: 0,
                levelCount: 1,
                baseArrayLayer: 0,
                layerCount: 1,
            },
        };

        unsafe {
            vkCmdPipelineBarrier(
                self.cmd_buffer,
                VkPipelineStageFlagBits::VK_PIPELINE_STAGE_TRANSFER_BIT as u32,
                VkPipelineStageFlagBits::VK_PIPELINE_STAGE_TOP_OF_PIPE_BIT as u32,
                VkDependencyFlagBits::VK_DEPENDENCY_BY_REGION_BIT as u32,
                0,
                null_mut(),
                0,
                null_mut(),
                2,
                [image_src_restore_barrier, image_dst_restore_barrier].as_ptr(),
            );
        }

        Ok(())
    }

    fn begin_render_pass(
        &mut self,
        render_pass: RenderPassHandle,
        framebuffer: FramebufferHandle,
        clear_data: Option<RenderPassClearInfo>,
    ) -> Result<(), DeviceError> {
        let render_passes = self.device.resources.render_passes.try_borrow()?;
        let pass = match render_passes.get(render_pass) {
            Some(p) => p,
            None => return Err(DeviceError::Api("Invalid render pass handle".to_string())),
        };
        let framebuffers = self.device.resources.framebuffers.try_borrow()?;
        let fbo = match framebuffers.get(framebuffer) {
            Some(p) => p,
            None => return Err(DeviceError::Api("Invalid framebuffer handle".to_string())),
        };
        self.begin_render_pass(
            pass.handle.value(),
            fbo.handle.value(),
            [fbo.width, fbo.height],
            clear_data,
        )
    }

    fn begin_render_pass_with_swap_chain(
        &mut self,
        render_pass: RenderPassHandle,
        swap_chain: SwapChainHandle,
        clear_info: Option<RenderPassClearInfo>,
    ) -> Result<(), DeviceError> {
        let render_passes = self.device.resources.render_passes.try_borrow()?;
        let pass = match render_passes.get(render_pass) {
            Some(p) => p,
            None => return Err(DeviceError::Api("Invalid render pass handle".to_string())),
        };
        let swap_chains = self.device.resources.swap_chains.try_borrow()?;
        let swap_chain = match swap_chains.get(swap_chain) {
            Some(p) => p,
            None => return Err(DeviceError::Api("Invalid swap chain handle".to_string())),
        };
        let extent = swap_chain.get_extent();
        self.begin_render_pass(
            pass.handle.value(),
            swap_chain.fbo_handle(),
            [extent.width, extent.height],
            clear_info,
        )
    }

    fn end_render_pass(&mut self) -> Result<(), DeviceError> {
        unsafe {
            vkCmdEndRenderPass(self.cmd_buffer);
        }
        Ok(())
    }

    fn bind_pipeline(&mut self, h: PipelineHandle) -> Result<(), DeviceError> {
        let pipeline = if let Some(p) = self.device.resources.pipelines.try_borrow()?.get(h) {
            p.pipeline.value()
        } else {
            return Err(DeviceError::InvalidHandle);
        };
        unsafe {
            vkCmdBindPipeline(
                self.cmd_buffer,
                VkPipelineBindPoint::VK_PIPELINE_BIND_POINT_GRAPHICS,
                pipeline,
            );
        }
        Ok(())
    }

    fn draw(
        &mut self,
        vertex_count: u32,
        instance_count: u32,
        first_vertex: u32,
        first_instance: u32,
    ) -> Result<(), DeviceError> {
        unsafe {
            vkCmdDraw(
                self.cmd_buffer,
                vertex_count,
                instance_count,
                first_vertex,
                first_instance,
            );
        }
        Ok(())
    }

    fn draw_indexed(
        &mut self,
        index_count: u32,
        instance_count: u32,
        first_index: u32,
        vertex_offset: i32,
        first_instance: u32,
    ) -> Result<(), DeviceError> {
        unsafe {
            vkCmdDrawIndexed(
                self.cmd_buffer,
                index_count,
                instance_count,
                first_index,
                vertex_offset,
                first_instance,
            );
        }
        Ok(())
    }

    fn bind_index_buffer(
        &mut self,
        h: BufferHandle,
        offset: usize,
        index_type: IndexType,
    ) -> Result<(), DeviceError> {
        let buffers = self.device.resources.buffers.try_borrow()?;
        if let Some(b) = buffers.get(h) {
            unsafe {
                vkCmdBindIndexBuffer(
                    self.cmd_buffer,
                    b.handle.value(),
                    offset as VkDeviceSize,
                    index_type_to_vk(index_type),
                );
            }
        } else {
            return Err(DeviceError::InvalidHandle);
        }
        Ok(())
    }

    fn set_viewport(&mut self, viewport: &Viewport, inverted: bool) -> Result<(), DeviceError> {
        // Use VK_KHR_MAINTENANCE_1 extension to allow flipping the viewport
        // rather than inverting Y in the shader.
        let vk_viewport = VkViewport {
            x: viewport.x,
            y: viewport.y + if inverted { viewport.h } else { 0.0 },
            width: viewport.w,
            height: if inverted { -viewport.h } else { viewport.h },
            minDepth: 0.0,
            maxDepth: 1.0,
        };
        unsafe {
            vkCmdSetViewport(self.cmd_buffer, 0, 1, &vk_viewport);
        }
        Ok(())
    }

    fn set_scissor_rect(&mut self, rect: &Rect) -> Result<(), DeviceError> {
        let rect = VkRect2D {
            offset: VkOffset2D {
                x: rect.x,
                y: rect.y,
            },
            extent: VkExtent2D {
                width: rect.w,
                height: rect.h,
            },
        };
        unsafe {
            vkCmdSetScissor(self.cmd_buffer, 0, 1, &rect);
        }
        Ok(())
    }

    fn bind_descriptors(
        &mut self,
        pipeline: PipelineHandle,
        descriptors: &[DescriptorSetBindInfo],
    ) -> DeviceResult<()> {
        let vk_pipeline_layout =
            if let Some(p) = self.device.resources.pipelines.try_borrow()?.get(pipeline) {
                p.layout.value()
            } else {
                return Err(DeviceError::Api(format!(
                    "Failed to resolve pipeline: {}",
                    pipeline
                )));
            };
        let descriptor_sets = self.device.resources.descriptor_sets_v2.try_borrow()?;
        for info in descriptors {
            let descriptor_set = if let Some(d) = descriptor_sets.get(info.handle) {
                d.handle
            } else {
                return Err(DeviceError::Api(format!(
                    "Failed to resolve descriptor set: {}",
                    info.handle
                )));
            };
            unsafe {
                vkCmdBindDescriptorSets(
                    self.cmd_buffer,
                    VkPipelineBindPoint::VK_PIPELINE_BIND_POINT_GRAPHICS,
                    vk_pipeline_layout,
                    info.set_index,
                    1,
                    &descriptor_set,
                    info.dynamic_offsets.len() as u32,
                    info.dynamic_offsets.as_ptr(),
                );
            }
        }
        Ok(())
    }
}
