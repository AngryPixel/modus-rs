use crate::device::{DeviceError, DeviceResult};
use crate::texture::{
    TextureCreateInfo, TextureData, TEXTURE_USAGE_FLAG_ATTACHMENT, TEXTURE_USAGE_FLAG_SHADER_INPUT,
};
use crate::types::{TextureFormat, TextureHandle, TextureType};
use crate::vulkan::device::Device;
use crate::vulkan::utils::{
    vk_result_to_str, vk_zero_init, DeferredResourceDeleter, DeferredResourceDeleterBuilder,
    ResourceMap, VkImageHandle, VkImageViewHandle, VmaAllocationHandle,
};
use std::ptr::null_mut;
use vulkan_sys::*;

pub(crate) fn texture_format_to_vk_format(format: TextureFormat) -> VkFormat {
    use VkFormat::*;
    match format {
        TextureFormat::R8G8B8A8 => VK_FORMAT_R8G8B8A8_UNORM,
        TextureFormat::R8G8B8 => VK_FORMAT_R8G8B8_UNORM,
        TextureFormat::R5G6B5 => VK_FORMAT_R5G6B5_UNORM_PACK16,
        TextureFormat::R4G4B4A4 => VK_FORMAT_R4G4B4A4_UNORM_PACK16,
        TextureFormat::R5G5B5A1 => VK_FORMAT_R5G5B5A1_UNORM_PACK16,
        TextureFormat::Depth32F => VK_FORMAT_D32_SFLOAT,
        TextureFormat::Depth24Stencil8 => VK_FORMAT_D24_UNORM_S8_UINT,
        TextureFormat::Depth32Stencil8 => VK_FORMAT_D32_SFLOAT_S8_UINT,
        TextureFormat::RGBA_ASTC4x4 => VK_FORMAT_ASTC_4x4_UNORM_BLOCK,
        TextureFormat::RGBA_ASTC5x4 => VK_FORMAT_ASTC_5x4_UNORM_BLOCK,
        TextureFormat::RGBA_ASTC5x5 => VK_FORMAT_ASTC_5x5_UNORM_BLOCK,
        TextureFormat::RGBA_ASTC6x5 => VK_FORMAT_ASTC_6x5_UNORM_BLOCK,
        TextureFormat::RGBA_ASTC6x6 => VK_FORMAT_ASTC_6x6_UNORM_BLOCK,
        TextureFormat::RGBA_ASTC8x5 => VK_FORMAT_ASTC_8x5_UNORM_BLOCK,
        TextureFormat::RGBA_ASTC8x6 => VK_FORMAT_ASTC_8x6_UNORM_BLOCK,
        TextureFormat::RGBA_ASTC8x8 => VK_FORMAT_ASTC_8x8_UNORM_BLOCK,
        TextureFormat::RGBA_ASTC10x5 => VK_FORMAT_ASTC_10x5_UNORM_BLOCK,
        TextureFormat::RGBA_ASTC10x6 => VK_FORMAT_ASTC_10x6_UNORM_BLOCK,
        TextureFormat::RGBA_ASTC10x8 => VK_FORMAT_ASTC_10x8_UNORM_BLOCK,
        TextureFormat::RGBA_ASTC10x10 => VK_FORMAT_ASTC_10x10_UNORM_BLOCK,
        TextureFormat::RGBA_ASTC12x10 => VK_FORMAT_ASTC_12x12_UNORM_BLOCK,
        TextureFormat::RGBA_ASTC12x12 => VK_FORMAT_ASTC_12x12_UNORM_BLOCK,
        TextureFormat::R8 => VK_FORMAT_R8_UNORM,
        TextureFormat::RGBA_ASTC4x4_SRGB => VK_FORMAT_ASTC_4x4_SRGB_BLOCK,
        TextureFormat::RGBA_ASTC5x4_SRGB => VK_FORMAT_ASTC_5x4_SRGB_BLOCK,
        TextureFormat::RGBA_ASTC5x5_SRGB => VK_FORMAT_ASTC_5x5_SRGB_BLOCK,
        TextureFormat::RGBA_ASTC6x5_SRGB => VK_FORMAT_ASTC_6x5_SRGB_BLOCK,
        TextureFormat::RGBA_ASTC6x6_SRGB => VK_FORMAT_ASTC_6x6_SRGB_BLOCK,
        TextureFormat::RGBA_ASTC8x5_SRGB => VK_FORMAT_ASTC_8x5_SRGB_BLOCK,
        TextureFormat::RGBA_ASTC8x6_SRGB => VK_FORMAT_ASTC_8x6_SRGB_BLOCK,
        TextureFormat::RGBA_ASTC8x8_SRGB => VK_FORMAT_ASTC_8x8_SRGB_BLOCK,
        TextureFormat::RGBA_ASTC10x5_SRGB => VK_FORMAT_ASTC_10x5_SRGB_BLOCK,
        TextureFormat::RGBA_ASTC10x6_SRGB => VK_FORMAT_ASTC_10x6_SRGB_BLOCK,
        TextureFormat::RGBA_ASTC10x8_SRGB => VK_FORMAT_ASTC_10x8_SRGB_BLOCK,
        TextureFormat::RGBA_ASTC10x10_SRGB => VK_FORMAT_ASTC_10x10_SRGB_BLOCK,
        TextureFormat::RGBA_ASTC12x10_SRGB => VK_FORMAT_ASTC_12x10_SRGB_BLOCK,
        TextureFormat::RGBA_ASTC12x12_SRGB => VK_FORMAT_ASTC_12x12_SRGB_BLOCK,
        TextureFormat::R8G8B8_SRGB => VK_FORMAT_R8G8B8A8_SRGB,
        TextureFormat::R8G8B8A8_SRGB => VK_FORMAT_R8G8B8_SRGB,
        TextureFormat::R_EAC11 => VK_FORMAT_EAC_R11_UNORM_BLOCK,
        TextureFormat::R_EAC11_SIGNED => VK_FORMAT_EAC_R11_SNORM_BLOCK,
        TextureFormat::RG_EAC11 => VK_FORMAT_EAC_R11G11_UNORM_BLOCK,
        TextureFormat::RG_EAC11_SIGNED => VK_FORMAT_EAC_R11G11_SNORM_BLOCK,
        TextureFormat::RGB_ETC2 => VK_FORMAT_ETC2_R8G8B8_UNORM_BLOCK,
        TextureFormat::RGB_ETC2_SRGB => VK_FORMAT_ETC2_R8G8B8_SRGB_BLOCK,
        TextureFormat::RGBA_ETC2_A1 => VK_FORMAT_ETC2_R8G8B8A1_UNORM_BLOCK,
        TextureFormat::RGBA_ETC2_A1_SRGB => VK_FORMAT_ETC2_R8G8B8A1_SRGB_BLOCK,
        TextureFormat::RGBA_ETC2 => VK_FORMAT_ETC2_R8G8B8A8_UNORM_BLOCK,
        TextureFormat::RGBA_ETC2_SRGB => VK_FORMAT_ETC2_R8G8B8A8_SRGB_BLOCK,
        TextureFormat::R10G10B10A2_I32 => VK_FORMAT_A2B10G10R10_SINT_PACK32,
        TextureFormat::R10G10B10A2_U32 => VK_FORMAT_A2B10G10R10_UINT_PACK32,
        TextureFormat::R16G16B16_F16 => VK_FORMAT_R16G16B16_SFLOAT,
        TextureFormat::R16G16B16A_F16 => VK_FORMAT_R16G16B16A16_SFLOAT,
        TextureFormat::R16G16_F16 => VK_FORMAT_R16G16_SFLOAT,
        TextureFormat::R16_U16 => VK_FORMAT_R16_UINT,
        TextureFormat::R32G32_U32 => VK_FORMAT_R32G32_UINT,
        TextureFormat::R16G16_U16 => VK_FORMAT_R16G16_UINT,
        TextureFormat::Depth16F => VK_FORMAT_D16_UNORM,
        TextureFormat::B8G8R8A8 => VK_FORMAT_B8G8R8A8_UNORM,
        TextureFormat::B8G8R8A8_SRGB => VK_FORMAT_B8G8R8A8_SRGB,
        _ => {
            debug_assert!(false, "Unknown format");
            VK_FORMAT_UNDEFINED
        }
    }
}

pub(crate) fn vk_format_to_texture_format(format: VkFormat) -> TextureFormat {
    use VkFormat::*;
    match format {
        VK_FORMAT_R8G8B8A8_UNORM => TextureFormat::R8G8B8A8,
        VK_FORMAT_R8G8B8_UNORM => TextureFormat::R8G8B8,
        VK_FORMAT_R5G6B5_UNORM_PACK16 => TextureFormat::R5G6B5,
        VK_FORMAT_R4G4B4A4_UNORM_PACK16 => TextureFormat::R4G4B4A4,
        VK_FORMAT_R5G5B5A1_UNORM_PACK16 => TextureFormat::R5G5B5A1,
        VK_FORMAT_D32_SFLOAT => TextureFormat::Depth32F,
        VK_FORMAT_D24_UNORM_S8_UINT => TextureFormat::Depth24Stencil8,
        VK_FORMAT_D32_SFLOAT_S8_UINT => TextureFormat::Depth32Stencil8,
        VK_FORMAT_ASTC_4x4_UNORM_BLOCK => TextureFormat::RGBA_ASTC4x4,
        VK_FORMAT_ASTC_5x4_UNORM_BLOCK => TextureFormat::RGBA_ASTC5x4,
        VK_FORMAT_ASTC_5x5_UNORM_BLOCK => TextureFormat::RGBA_ASTC5x5,
        VK_FORMAT_ASTC_6x5_UNORM_BLOCK => TextureFormat::RGBA_ASTC6x5,
        VK_FORMAT_ASTC_6x6_UNORM_BLOCK => TextureFormat::RGBA_ASTC6x6,
        VK_FORMAT_ASTC_8x5_UNORM_BLOCK => TextureFormat::RGBA_ASTC8x5,
        VK_FORMAT_ASTC_8x6_UNORM_BLOCK => TextureFormat::RGBA_ASTC8x6,
        VK_FORMAT_ASTC_8x8_UNORM_BLOCK => TextureFormat::RGBA_ASTC8x8,
        VK_FORMAT_ASTC_10x5_UNORM_BLOCK => TextureFormat::RGBA_ASTC10x5,
        VK_FORMAT_ASTC_10x6_UNORM_BLOCK => TextureFormat::RGBA_ASTC10x6,
        VK_FORMAT_ASTC_10x8_UNORM_BLOCK => TextureFormat::RGBA_ASTC10x8,
        VK_FORMAT_ASTC_10x10_UNORM_BLOCK => TextureFormat::RGBA_ASTC10x10,
        VK_FORMAT_ASTC_12x10_UNORM_BLOCK => TextureFormat::RGBA_ASTC12x10,
        VK_FORMAT_ASTC_12x12_UNORM_BLOCK => TextureFormat::RGBA_ASTC12x12,
        VK_FORMAT_R8_UNORM => TextureFormat::R8,
        VK_FORMAT_ASTC_4x4_SRGB_BLOCK => TextureFormat::RGBA_ASTC4x4_SRGB,
        VK_FORMAT_ASTC_5x4_SRGB_BLOCK => TextureFormat::RGBA_ASTC5x4_SRGB,
        VK_FORMAT_ASTC_5x5_SRGB_BLOCK => TextureFormat::RGBA_ASTC5x5_SRGB,
        VK_FORMAT_ASTC_6x5_SRGB_BLOCK => TextureFormat::RGBA_ASTC6x5_SRGB,
        VK_FORMAT_ASTC_6x6_SRGB_BLOCK => TextureFormat::RGBA_ASTC6x6_SRGB,
        VK_FORMAT_ASTC_8x5_SRGB_BLOCK => TextureFormat::RGBA_ASTC8x5_SRGB,
        VK_FORMAT_ASTC_8x6_SRGB_BLOCK => TextureFormat::RGBA_ASTC8x6_SRGB,
        VK_FORMAT_ASTC_8x8_SRGB_BLOCK => TextureFormat::RGBA_ASTC8x8_SRGB,
        VK_FORMAT_ASTC_10x5_SRGB_BLOCK => TextureFormat::RGBA_ASTC10x5_SRGB,
        VK_FORMAT_ASTC_10x6_SRGB_BLOCK => TextureFormat::RGBA_ASTC10x6_SRGB,
        VK_FORMAT_ASTC_10x8_SRGB_BLOCK => TextureFormat::RGBA_ASTC10x8_SRGB,
        VK_FORMAT_ASTC_10x10_SRGB_BLOCK => TextureFormat::RGBA_ASTC10x10_SRGB,
        VK_FORMAT_ASTC_12x10_SRGB_BLOCK => TextureFormat::RGBA_ASTC12x10_SRGB,
        VK_FORMAT_ASTC_12x12_SRGB_BLOCK => TextureFormat::RGBA_ASTC12x12_SRGB,
        VK_FORMAT_R8G8B8A8_SRGB => TextureFormat::R8G8B8_SRGB,
        VK_FORMAT_R8G8B8_SRGB => TextureFormat::R8G8B8A8_SRGB,
        VK_FORMAT_EAC_R11_UNORM_BLOCK => TextureFormat::R_EAC11,
        VK_FORMAT_EAC_R11_SNORM_BLOCK => TextureFormat::R_EAC11_SIGNED,
        VK_FORMAT_EAC_R11G11_UNORM_BLOCK => TextureFormat::RG_EAC11,
        VK_FORMAT_EAC_R11G11_SNORM_BLOCK => TextureFormat::RG_EAC11_SIGNED,
        VK_FORMAT_ETC2_R8G8B8_UNORM_BLOCK => TextureFormat::RGB_ETC2,
        VK_FORMAT_ETC2_R8G8B8_SRGB_BLOCK => TextureFormat::RGB_ETC2_SRGB,
        VK_FORMAT_ETC2_R8G8B8A1_UNORM_BLOCK => TextureFormat::RGBA_ETC2_A1,
        VK_FORMAT_ETC2_R8G8B8A1_SRGB_BLOCK => TextureFormat::RGBA_ETC2_A1_SRGB,
        VK_FORMAT_ETC2_R8G8B8A8_UNORM_BLOCK => TextureFormat::RGBA_ETC2,
        VK_FORMAT_ETC2_R8G8B8A8_SRGB_BLOCK => TextureFormat::RGBA_ETC2_SRGB,
        VK_FORMAT_A2B10G10R10_SINT_PACK32 => TextureFormat::R10G10B10A2_I32,
        VK_FORMAT_A2B10G10R10_UINT_PACK32 => TextureFormat::R10G10B10A2_U32,
        VK_FORMAT_R16G16B16_SFLOAT => TextureFormat::R16G16B16_F16,
        VK_FORMAT_R16G16B16A16_SFLOAT => TextureFormat::R16G16B16A_F16,
        VK_FORMAT_R16G16_SFLOAT => TextureFormat::R16G16_F16,
        VK_FORMAT_R16_UINT => TextureFormat::R16_U16,
        VK_FORMAT_R32G32_UINT => TextureFormat::R32G32_U32,
        VK_FORMAT_R16G16_UINT => TextureFormat::R16G16_U16,
        VK_FORMAT_D16_UNORM => TextureFormat::Depth16F,
        VK_FORMAT_B8G8R8A8_UNORM => TextureFormat::B8G8R8A8,
        VK_FORMAT_B8G8R8A8_SRGB => TextureFormat::B8G8R8A8_SRGB,

        _ => {
            debug_assert!(false, "Unhandled format");
            TextureFormat::Total
        }
    }
}

pub(crate) fn sample_count_to_vk_sample(sample_count: u32) -> VkSampleCountFlagBits {
    match sample_count {
        1 => VkSampleCountFlagBits::VK_SAMPLE_COUNT_1_BIT,
        2 => VkSampleCountFlagBits::VK_SAMPLE_COUNT_2_BIT,
        4 => VkSampleCountFlagBits::VK_SAMPLE_COUNT_4_BIT,
        8 => VkSampleCountFlagBits::VK_SAMPLE_COUNT_8_BIT,
        16 => VkSampleCountFlagBits::VK_SAMPLE_COUNT_16_BIT,
        32 => VkSampleCountFlagBits::VK_SAMPLE_COUNT_32_BIT,
        64 => VkSampleCountFlagBits::VK_SAMPLE_COUNT_64_BIT,
        _ => VkSampleCountFlagBits::VK_SAMPLE_COUNT_FLAG_BITS_MAX_ENUM,
    }
}

fn texture_type_to_vk_type(ttype: TextureType) -> VkImageType {
    match ttype {
        TextureType::T1D => VkImageType::VK_IMAGE_TYPE_1D,
        TextureType::T2D => VkImageType::VK_IMAGE_TYPE_2D,
        TextureType::T3D => VkImageType::VK_IMAGE_TYPE_3D,
        TextureType::Total => VkImageType::VK_IMAGE_TYPE_MAX_ENUM,
    }
}

fn texture_create_info_to_vk_image_create_info(info: &TextureCreateInfo) -> VkImageCreateInfo {
    let mut image_info = vk_zero_init::<VkImageCreateInfo>();
    image_info.sType = VkStructureType::VK_STRUCTURE_TYPE_IMAGE_CREATE_INFO;
    image_info.tiling = VkImageTiling::VK_IMAGE_TILING_OPTIMAL;
    image_info.imageType = texture_type_to_vk_type(info.ttype);
    if info.transient {
        image_info.usage = VkImageUsageFlagBits::VK_IMAGE_USAGE_TRANSIENT_ATTACHMENT_BIT as u32;
    }
    image_info.usage |= VkImageUsageFlagBits::VK_IMAGE_USAGE_TRANSFER_SRC_BIT as u32;
    image_info.usage |= VkImageUsageFlagBits::VK_IMAGE_USAGE_TRANSFER_DST_BIT as u32;
    image_info.format = texture_format_to_vk_format(info.format);
    image_info.sharingMode = VkSharingMode::VK_SHARING_MODE_EXCLUSIVE;
    image_info.samples = sample_count_to_vk_sample(info.sample_count as u32);
    image_info.mipLevels = info.mip_map_levels as u32;
    image_info.extent.width = info.width;
    image_info.extent.height = info.height;
    image_info.extent.depth = info.depth;
    image_info.initialLayout = VkImageLayout::VK_IMAGE_LAYOUT_UNDEFINED;
    image_info.arrayLayers = info.array_count as u32;
    image_info
}

fn is_depth_stencil_format(format: TextureFormat) -> bool {
    matches!(
        format,
        TextureFormat::Depth24Stencil8 | TextureFormat::Depth32Stencil8
    )
}

fn is_depth_format(format: TextureFormat) -> bool {
    matches!(
        format,
        TextureFormat::Depth16F
            | TextureFormat::Depth32F
            | TextureFormat::Depth24Stencil8
            | TextureFormat::Depth32Stencil8
    )
}

struct TextureDropScope<'a> {
    texture: Texture,
    device: &'a Device,
}

impl<'a> Drop for TextureDropScope<'a> {
    fn drop(&mut self) {
        self.texture.destroy(
            self.device.state.logical_device.value(),
            self.device.state.vma.value(),
        );
    }
}

#[derive(Debug)]
pub(crate) struct Texture {
    alloc: VmaAllocationHandle,
    pub(crate) image: VkImageHandle,
    pub(crate) view: VkImageViewHandle,
    pub(crate) format: VkFormat,
    pub(crate) create_info: TextureCreateInfo,
}

impl Texture {
    pub(crate) fn create(
        device: &Device,
        create_info: &TextureCreateInfo,
        texture_data: &[TextureData],
    ) -> Result<Texture, DeviceError> {
        let mut image_info = texture_create_info_to_vk_image_create_info(create_info);

        if create_info.usage_flags & TEXTURE_USAGE_FLAG_ATTACHMENT != 0 {
            if is_depth_format(create_info.format) {
                image_info.usage |=
                    VkImageUsageFlagBits::VK_IMAGE_USAGE_DEPTH_STENCIL_ATTACHMENT_BIT as u32;
            } else {
                image_info.usage |=
                    VkImageUsageFlagBits::VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT as u32;
            }
        }

        if create_info.usage_flags & TEXTURE_USAGE_FLAG_SHADER_INPUT != 0 {
            image_info.usage |= VkImageUsageFlagBits::VK_IMAGE_USAGE_TRANSFER_DST_BIT as u32
                | VkImageUsageFlagBits::VK_IMAGE_USAGE_SAMPLED_BIT as u32;
        }

        let mut alloc_info = vk_zero_init::<VmaAllocationCreateInfo>();
        alloc_info.usage = VmaMemoryUsage::VMA_MEMORY_USAGE_GPU_ONLY;
        let mut alloc_handle = VmaAllocationHandle::null();
        let mut image_handle = VkImageHandle::null();
        unsafe {
            let alloc_result = vmaCreateImage(
                device.state.vma.value(),
                &image_info,
                &alloc_info,
                image_handle.ptr_mut(),
                alloc_handle.ptr_mut(),
                null_mut(),
            );
            if alloc_result != VkResult::VK_SUCCESS {
                return Err(DeviceError::Api(format!(
                    "Failed to allocate image: {}",
                    vk_result_to_str(alloc_result)
                )));
            }
        }

        let mut texture = Texture {
            alloc: alloc_handle,
            image: image_handle,
            view: Default::default(),
            format: image_info.format,
            create_info: *create_info,
        };

        if let Err(e) = texture.build_view(device) {
            texture.destroy(
                device.state.logical_device.value(),
                device.state.vma.value(),
            );
            return Err(DeviceError::Api(format!(
                "Failed to build image view: {}",
                vk_result_to_str(e)
            )));
        }

        // TODO: this needs to be improved so that all levels can be submitted at once
        for tex_data in texture_data {
            if let Err(e) =
                texture.set_data_impl(device, tex_data, VkImageLayout::VK_IMAGE_LAYOUT_UNDEFINED)
            {
                texture.destroy(
                    device.state.logical_device.value(),
                    device.state.vma.value(),
                );
                return Err(e);
            }
        }

        if create_info.usage_flags & TEXTURE_USAGE_FLAG_ATTACHMENT != 0 {
            // transition to attachment layout
            if let Err(e) =
                device
                    .resources
                    .uploader
                    .borrow_mut()
                    .record(device, |_, cmd_buffer| {
                        let image_barrier = VkImageMemoryBarrier {
                            sType: VkStructureType::VK_STRUCTURE_TYPE_IMAGE_MEMORY_BARRIER,
                            pNext: null_mut(),
                            srcAccessMask: 0,
                            dstAccessMask: if is_depth_format(create_info.format) {
                                VkAccessFlagBits::VK_ACCESS_DEPTH_STENCIL_ATTACHMENT_WRITE_BIT
                                    as u32
                            } else {
                                VkAccessFlagBits::VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT as u32
                            },
                            oldLayout: VkImageLayout::VK_IMAGE_LAYOUT_UNDEFINED,
                            newLayout: if is_depth_format(create_info.format) {
                                VkImageLayout::VK_IMAGE_LAYOUT_DEPTH_STENCIL_ATTACHMENT_OPTIMAL
                            } else {
                                VkImageLayout::VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL
                            },
                            srcQueueFamilyIndex: 0,
                            dstQueueFamilyIndex: 0,
                            image: texture.image.value(),
                            subresourceRange: VkImageSubresourceRange {
                                aspectMask: VkImageAspectFlagBits::VK_IMAGE_ASPECT_COLOR_BIT as u32,
                                baseMipLevel: 0,
                                levelCount: 1,
                                baseArrayLayer: 0,
                                layerCount: 1,
                            },
                        };

                        // transition image to transfer
                        unsafe {
                            vkCmdPipelineBarrier(
                            cmd_buffer,
                            VkPipelineStageFlagBits::VK_PIPELINE_STAGE_TOP_OF_PIPE_BIT as u32,
                            VkPipelineStageFlagBits::VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT
                                as u32,
                            0,
                            0,
                            null_mut(),
                            0,
                            null_mut(),
                            1,
                            &image_barrier,
                        );
                        }
                        Ok(())
                    })
            {
                texture.destroy(
                    device.state.logical_device.value(),
                    device.state.vma.value(),
                );
                return Err(e);
            }
        } else if create_info.usage_flags & TEXTURE_USAGE_FLAG_SHADER_INPUT != 0
            && texture_data.is_empty()
        {
            if let Err(e) = texture.transition_to(
                device,
                VkImageLayout::VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL,
                VkAccessFlagBits::VK_ACCESS_SHADER_READ_BIT as u32,
            ) {
                texture.destroy(
                    device.state.logical_device.value(),
                    device.state.vma.value(),
                );
                return Err(e);
            }
        }
        Ok(texture)
    }

    fn destroy(&mut self, device: VkDevice, vma: VmaAllocator) {
        unsafe {
            self.view.destroy_with(|v| {
                vkDestroyImageView(device, v, null_mut());
            });
            self.image.destroy_with(|i| {
                vkDestroyImage(device, i, null_mut());
            });
            self.alloc.destroy_with(|a| {
                vmaFreeMemory(vma, a);
            });
        }
    }

    fn build_view(&mut self, device: &Device) -> Result<(), VkResult> {
        let mut create_info = vk_zero_init::<VkImageViewCreateInfo>();
        create_info.sType = VkStructureType::VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO;
        create_info.image = self.image.value();

        //TODO: Handle cubes/arrays/etc...
        create_info.viewType = match self.create_info.ttype {
            TextureType::T1D => {
                if self.create_info.array_count == 1 {
                    VkImageViewType::VK_IMAGE_VIEW_TYPE_1D
                } else {
                    VkImageViewType::VK_IMAGE_VIEW_TYPE_1D_ARRAY
                }
            }
            TextureType::T2D => {
                if self.create_info.array_count == 1 {
                    VkImageViewType::VK_IMAGE_VIEW_TYPE_2D
                } else {
                    VkImageViewType::VK_IMAGE_VIEW_TYPE_2D_ARRAY
                }
            }
            TextureType::T3D => VkImageViewType::VK_IMAGE_VIEW_TYPE_3D,
            _ => return Err(VkResult::VK_ERROR_FORMAT_NOT_SUPPORTED),
        };
        create_info.format = self.format;

        if is_depth_stencil_format(self.create_info.format) {
            create_info.subresourceRange.aspectMask |=
                VkImageAspectFlagBits::VK_IMAGE_ASPECT_STENCIL_BIT as u32
                    | VkImageAspectFlagBits::VK_IMAGE_ASPECT_DEPTH_BIT as u32;
        } else if is_depth_format(self.create_info.format) {
            create_info.subresourceRange.aspectMask |=
                VkImageAspectFlagBits::VK_IMAGE_ASPECT_DEPTH_BIT as u32;
        } else {
            create_info.subresourceRange.aspectMask |=
                VkImageAspectFlagBits::VK_IMAGE_ASPECT_COLOR_BIT as u32;
        }
        create_info.subresourceRange.baseMipLevel = 0;
        create_info.subresourceRange.levelCount = self.create_info.mip_map_levels as u32;
        create_info.subresourceRange.baseArrayLayer = 0;
        if self.create_info.array_count > 1 {
            create_info.subresourceRange.layerCount = VK_REMAINING_ARRAY_LAYERS as u32;
        } else {
            create_info.subresourceRange.layerCount = 1;
        }

        unsafe {
            let r = vkCreateImageView(
                device.state.logical_device.value(),
                &create_info,
                null_mut(),
                self.view.ptr_mut(),
            );
            if r != VkResult::VK_SUCCESS {
                return Err(r);
            }
        }
        Ok(())
    }

    fn transition_to(
        &self,
        device: &Device,
        layout: VkImageLayout,
        access_mask: u32,
    ) -> DeviceResult<()> {
        let mut uploader = device.resources.uploader.try_borrow_mut()?;
        uploader.record(device, |_, cmd_buffer| -> DeviceResult<()> {
            // Transition to shader read layout
            let image_barrier = VkImageMemoryBarrier {
                sType: VkStructureType::VK_STRUCTURE_TYPE_IMAGE_MEMORY_BARRIER,
                pNext: null_mut(),
                srcAccessMask: 0,
                dstAccessMask: access_mask,
                oldLayout: VkImageLayout::VK_IMAGE_LAYOUT_UNDEFINED,
                newLayout: layout,
                srcQueueFamilyIndex: 0,
                dstQueueFamilyIndex: 0,
                image: self.image.value(),
                subresourceRange: VkImageSubresourceRange {
                    aspectMask: VkImageAspectFlagBits::VK_IMAGE_ASPECT_COLOR_BIT as u32,
                    baseMipLevel: 0,
                    levelCount: self.create_info.mip_map_levels as u32,
                    baseArrayLayer: 0,
                    layerCount: self.create_info.array_count as u32,
                },
            };

            unsafe {
                vkCmdPipelineBarrier(
                    cmd_buffer,
                    VkPipelineStageFlagBits::VK_PIPELINE_STAGE_TOP_OF_PIPE_BIT as u32,
                    VkPipelineStageFlagBits::VK_PIPELINE_STAGE_FRAGMENT_SHADER_BIT as u32,
                    0,
                    0,
                    null_mut(),
                    0,
                    null_mut(),
                    1,
                    &image_barrier,
                );
            }
            Ok(())
        })
    }

    pub(crate) fn set_data(&mut self, device: &Device, data: &TextureData) -> DeviceResult<()> {
        self.set_data_impl(
            device,
            data,
            VkImageLayout::VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL,
        )
    }

    fn set_data_impl(
        &mut self,
        device: &Device,
        data: &TextureData,
        old_layout: VkImageLayout,
    ) -> Result<(), DeviceError> {
        // create transfer buffer and update

        //TODO: migrate to the new system
        let mut uploader = device.resources.uploader.try_borrow_mut()?;
        uploader.upload(
            device,
            data.data,
            |_, src_buffer, src_offset, cmd_buffer| {
                let transfer_image_barrier = VkImageMemoryBarrier {
                    sType: VkStructureType::VK_STRUCTURE_TYPE_IMAGE_MEMORY_BARRIER,
                    pNext: null_mut(),
                    srcAccessMask: 0,
                    dstAccessMask: VkAccessFlagBits::VK_ACCESS_TRANSFER_WRITE_BIT as u32,
                    oldLayout: old_layout,
                    newLayout: VkImageLayout::VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL,
                    srcQueueFamilyIndex: 0,
                    dstQueueFamilyIndex: 0,
                    image: self.image.value(),
                    subresourceRange: VkImageSubresourceRange {
                        aspectMask: VkImageAspectFlagBits::VK_IMAGE_ASPECT_COLOR_BIT as u32,
                        baseMipLevel: data.mip_map_level as u32,
                        levelCount: 1,
                        baseArrayLayer: data.array_index,
                        layerCount: 1,
                    },
                };

                // transition image to transfer
                unsafe {
                    vkCmdPipelineBarrier(
                        cmd_buffer,
                        VkPipelineStageFlagBits::VK_PIPELINE_STAGE_TOP_OF_PIPE_BIT as u32,
                        VkPipelineStageFlagBits::VK_PIPELINE_STAGE_TRANSFER_BIT as u32,
                        0,
                        0,
                        null_mut(),
                        0,
                        null_mut(),
                        1,
                        &transfer_image_barrier,
                    );
                }

                // Copy buffer contents
                let copy_region = VkBufferImageCopy {
                    bufferOffset: src_offset,
                    bufferRowLength: 0,
                    bufferImageHeight: 0,
                    imageSubresource: VkImageSubresourceLayers {
                        aspectMask: VkImageAspectFlagBits::VK_IMAGE_ASPECT_COLOR_BIT as u32,
                        mipLevel: data.mip_map_level as u32,
                        baseArrayLayer: data.array_index,
                        layerCount: 1,
                    },
                    imageOffset: VkOffset3D { x: 0, y: 0, z: 0 },
                    imageExtent: VkExtent3D {
                        width: data.width,
                        height: data.height,
                        depth: data.depth,
                    },
                };

                unsafe {
                    vkCmdCopyBufferToImage(
                        cmd_buffer,
                        src_buffer,
                        self.image.value(),
                        VkImageLayout::VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL,
                        1,
                        &copy_region,
                    );
                }

                // Transition to shader read layout
                let access_image_barrier = VkImageMemoryBarrier {
                    sType: VkStructureType::VK_STRUCTURE_TYPE_IMAGE_MEMORY_BARRIER,
                    pNext: null_mut(),
                    srcAccessMask: VkAccessFlagBits::VK_ACCESS_TRANSFER_WRITE_BIT as u32,
                    dstAccessMask: VkAccessFlagBits::VK_ACCESS_SHADER_READ_BIT as u32,
                    oldLayout: VkImageLayout::VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL,
                    newLayout: VkImageLayout::VK_IMAGE_LAYOUT_SHADER_READ_ONLY_OPTIMAL,
                    srcQueueFamilyIndex: 0,
                    dstQueueFamilyIndex: 0,
                    image: self.image.value(),
                    subresourceRange: VkImageSubresourceRange {
                        aspectMask: VkImageAspectFlagBits::VK_IMAGE_ASPECT_COLOR_BIT as u32,
                        baseMipLevel: data.mip_map_level as u32,
                        levelCount: 1,
                        baseArrayLayer: data.array_index,
                        layerCount: 1,
                    },
                };

                unsafe {
                    vkCmdPipelineBarrier(
                        cmd_buffer,
                        VkPipelineStageFlagBits::VK_PIPELINE_STAGE_TRANSFER_BIT as u32,
                        VkPipelineStageFlagBits::VK_PIPELINE_STAGE_FRAGMENT_SHADER_BIT as u32,
                        0,
                        0,
                        null_mut(),
                        0,
                        null_mut(),
                        1,
                        &access_image_barrier,
                    );
                }

                Ok(())
            },
        )?;
        Ok(())
    }
}

pub(crate) type TextureMap = ResourceMap<TextureHandle, Texture>;

pub(crate) struct TextureDeleter {
    device: VkDevice,
    vma: VmaAllocator,
}

impl TextureDeleter {
    pub(crate) fn new(device: &Device) -> Self {
        Self {
            device: device.state.logical_device.value(),
            vma: device.state.vma.value(),
        }
    }
}

impl DeferredResourceDeleter<Texture> for TextureDeleter {
    fn delete(&mut self, texture: &mut Texture) {
        texture.destroy(self.device, self.vma);
    }
}

impl DeferredResourceDeleterBuilder<'_, Texture> for Texture {
    type Deleter = TextureDeleter;
    fn create_deleter(device: &Device) -> Self::Deleter {
        Self::Deleter::new(device)
    }
}
