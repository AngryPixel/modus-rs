use crate::app_interface::{NativeSurfacePtr, RenderSurfaceInterface};
use crate::device::{DeviceError, DeviceResult};
use crate::types::{SwapChainHandle, TextureFormat};
use crate::vulkan::device::Device;
use crate::vulkan::swap_chain::SwapChainState::ImageAcquired;
use crate::vulkan::texture::vk_format_to_texture_format;
use crate::vulkan::utils::{
    vk_result_to_str, vk_uninit_vec, vk_zero_init, VkFenceHandle, VkFramebufferHandle,
    VkImageViewHandle, VkQueueHandle, VkRenderPassHandle, VkSwapChainHandle, MAX_FRAMES_IN_FLIGHT,
};
use modus_core::handle_map::HandleMap;
use modus_core::sync::Mutex;
use std::ptr::null_mut;
use vulkan_sys::*;

fn choose_surface_format(formats: &[VkSurfaceFormatKHR]) -> VkSurfaceFormatKHR {
    // check for the case where there is no preferred format list
    if formats.len() == 1 && formats[0].format == VkFormat::VK_FORMAT_UNDEFINED {
        return VkSurfaceFormatKHR {
            format: VkFormat::VK_FORMAT_B8G8R8A8_UNORM,
            colorSpace: VkColorSpaceKHR::VK_COLOR_SPACE_SRGB_NONLINEAR_KHR,
        };
    } else {
        for format in formats {
            if format.colorSpace == VkColorSpaceKHR::VK_COLOR_SPACE_SRGB_NONLINEAR_KHR
                && format.format == VkFormat::VK_FORMAT_B8G8R8A8_UNORM
            {
                return *format;
            }
        }
    }
    formats[0]
}

fn choose_present_mode(modes: &[VkPresentModeKHR]) -> VkPresentModeKHR {
    // Try to find mailbox mode for triple buffering
    for mode in modes {
        if *mode == VkPresentModeKHR::VK_PRESENT_MODE_MAILBOX_KHR {
            return *mode;
        }
    }
    // Always guaranteed to be present, similar to vsync,
    VkPresentModeKHR::VK_PRESENT_MODE_FIFO_KHR
}

fn choose_swap_extent(
    capabilities: &VkSurfaceCapabilitiesKHR,
    surface_width: u32,
    surface_height: u32,
) -> VkExtent2D {
    // if current extent != u32::max(), then it means we have to use the given size, otherwise we
    // can pick on ourselves
    if capabilities.currentExtent.width != u32::MAX {
        capabilities.currentExtent
    } else {
        VkExtent2D {
            width: capabilities
                .minImageExtent
                .width
                .max(capabilities.maxImageExtent.width.min(surface_width)),
            height: capabilities
                .minImageExtent
                .height
                .max(capabilities.maxImageExtent.height.min(surface_height)),
        }
    }
}

#[derive(Debug)]
pub(crate) struct SwapChain {
    pub(crate) swap_chain: VkSwapChainHandle,
    swap_chain_format_vk: VkFormat,
    swap_chain_format: TextureFormat,
    // we don't own these images, they are part of the swap chain
    images: Vec<VkImage>,
    views: Vec<VkImageViewHandle>,
    render_pass: VkRenderPassHandle, // Temporary place holder until frame graph can be built
    framebuffers: Vec<VkFramebufferHandle>, // Temporary place holder.
    extent: VkExtent2D,
    image_in_flight_fences: Vec<usize>,
    active_frame: u32,
    active_image_index: u32,
}

#[derive(Debug)]
pub(crate) enum SwapChainState {
    ImageAcquired(u32),
    RebuildRequired,
}

impl SwapChain {
    pub(crate) fn create(
        device: &Device,
        surface_interface: &dyn RenderSurfaceInterface,
    ) -> Result<SwapChain, DeviceError> {
        let mut sw = SwapChain {
            swap_chain: VkSwapChainHandle::null(),
            swap_chain_format_vk: VkFormat::VK_FORMAT_UNDEFINED,
            swap_chain_format: TextureFormat::Total,
            images: vec![],
            views: vec![],
            framebuffers: vec![],
            render_pass: Default::default(),
            extent: VkExtent2D {
                width: 0,
                height: 0,
            },
            image_in_flight_fences: vec![],
            active_frame: 0,
            active_image_index: 0,
        };

        if let Err(e) = sw.build_swapchain(device, surface_interface) {
            sw.destroy(device);
            return Err(e);
        }

        if let Err(e) = sw.build_default_render_pass(device) {
            sw.destroy(device);
            return Err(DeviceError::Api(format!(
                "Failed to build swap chain default render pass:{}",
                vk_result_to_str(e)
            )));
        }

        if let Err(e) = sw.build_framebuffers(device) {
            sw.destroy(device);
            return Err(e);
        }

        if let Err(e) = sw.build_sync_object(device) {
            sw.destroy(device);
            return Err(e);
        }
        Ok(sw)
    }

    fn build_swapchain(
        &mut self,
        device: &Device,
        surface_interface: &dyn RenderSurfaceInterface,
    ) -> Result<(), DeviceError> {
        let vk_surface = unsafe {
            std::mem::transmute::<NativeSurfacePtr, VkSurfaceKHR>(
                surface_interface.native_surface_handle(),
            )
        };
        let (surface_width, surface_height) = surface_interface.surface_size();

        let mut capabilities = vk_zero_init::<VkSurfaceCapabilitiesKHR>();
        unsafe {
            let capabilities_result = vkGetPhysicalDeviceSurfaceCapabilitiesKHR(
                device.state.physical_device.device,
                vk_surface,
                &mut capabilities,
            );
            if capabilities_result != VkResult::VK_SUCCESS {
                return Err(DeviceError::Api(format!(
                    "Failed to obtain surface capabilities: {}",
                    vk_result_to_str(capabilities_result)
                )));
            }
        }
        let mut surface_format_count = 0_u32;
        let mut present_mode_count = 0_u32;
        unsafe {
            vkGetPhysicalDeviceSurfaceFormatsKHR(
                device.state.physical_device.device,
                vk_surface,
                &mut surface_format_count,
                null_mut(),
            );
            vkGetPhysicalDeviceSurfacePresentModesKHR(
                device.state.physical_device.device,
                vk_surface,
                &mut present_mode_count,
                null_mut(),
            );
        }

        let mut surface_formats =
            vk_uninit_vec::<VkSurfaceFormatKHR>(surface_format_count as usize);
        let mut present_modes = vk_uninit_vec::<VkPresentModeKHR>(present_mode_count as usize);
        unsafe {
            vkGetPhysicalDeviceSurfaceFormatsKHR(
                device.state.physical_device.device,
                vk_surface,
                &mut surface_format_count,
                surface_formats.as_mut_ptr(),
            );
            vkGetPhysicalDeviceSurfacePresentModesKHR(
                device.state.physical_device.device,
                vk_surface,
                &mut present_mode_count,
                present_modes.as_mut_ptr(),
            );
        }

        let surface_format = choose_surface_format(&surface_formats);
        let present_mode = choose_present_mode(&present_modes);
        let extent = choose_swap_extent(&capabilities, surface_width, surface_height);

        let mut image_count = capabilities.minImageCount + 1;
        if capabilities.maxImageCount > 0 && image_count > capabilities.maxImageCount {
            image_count = capabilities.maxImageCount;
        }

        let mut create_info = vk_zero_init::<VkSwapchainCreateInfoKHR>();
        create_info.sType = VkStructureType::VK_STRUCTURE_TYPE_SWAPCHAIN_CREATE_INFO_KHR;
        create_info.surface = vk_surface;
        create_info.minImageCount = image_count;
        create_info.imageFormat = surface_format.format;
        create_info.imageColorSpace = surface_format.colorSpace;
        create_info.imageExtent = extent;
        create_info.imageArrayLayers = 1;
        create_info.imageUsage = VkImageUsageFlagBits::VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT as u32
            | VkImageUsageFlagBits::VK_IMAGE_USAGE_TRANSFER_DST_BIT as u32
            | VkImageUsageFlagBits::VK_IMAGE_USAGE_TRANSFER_SRC_BIT as u32;
        create_info.preTransform = capabilities.currentTransform;
        create_info.presentMode = present_mode;
        // Enable clipping if we don't about reading pixels obscured by other windows
        create_info.clipped = 1;
        create_info.compositeAlpha = VkCompositeAlphaFlagBitsKHR::VK_COMPOSITE_ALPHA_OPAQUE_BIT_KHR;

        let queue_indices = [
            device.state.physical_device.graphics_queue_index,
            device.state.physical_device.presentation_queue_index,
        ];
        // Setup sharing mode based on whether the graphics and present queue are the same or not.
        // Better performance if they are the same
        if device.state.physical_device.graphics_queue_index
            != device.state.physical_device.presentation_queue_index
        {
            create_info.imageSharingMode = VkSharingMode::VK_SHARING_MODE_CONCURRENT;
            create_info.queueFamilyIndexCount = 2;
            create_info.pQueueFamilyIndices = queue_indices.as_ptr();
        } else {
            create_info.imageSharingMode = VkSharingMode::VK_SHARING_MODE_EXCLUSIVE;
        }

        unsafe {
            let create_result = vkCreateSwapchainKHR(
                device.state.logical_device.value(),
                &create_info,
                null_mut(),
                self.swap_chain.ptr_mut(),
            );
            if create_result != VkResult::VK_SUCCESS {
                return Err(DeviceError::Api(format!(
                    "Failed to create swap chain: {}",
                    vk_result_to_str(create_result)
                )));
            }
        }
        self.swap_chain_format_vk = surface_format.format;
        self.swap_chain_format = vk_format_to_texture_format(surface_format.format);
        self.extent = extent;
        self.active_image_index = 0;
        self.active_frame = 0;

        self.retrieve_images(device)?;
        Ok(())
    }

    fn destroy_swap_chain(&mut self, device: &Device) {
        unsafe {
            let device = device.state.logical_device.value();
            for view in &mut self.views {
                view.destroy_with(|v| {
                    vkDestroyImageView(device, v, null_mut());
                });
            }
            self.views.clear();
            self.swap_chain.destroy_with(|sc| {
                vkDestroySwapchainKHR(device, sc, null_mut());
            });
        }
    }

    fn retrieve_images(&mut self, device: &Device) -> DeviceResult<()> {
        let mut image_count = 0_u32;
        unsafe {
            let r1 = vkGetSwapchainImagesKHR(
                device.state.logical_device.value(),
                self.swap_chain.value(),
                &mut image_count,
                null_mut(),
            );
            if r1 != VkResult::VK_SUCCESS {
                return Err(DeviceError::Api(
                    "Failed to get swap chain image count".to_string(),
                ));
            }
            self.images = vk_uninit_vec::<VkImage>(image_count as usize);
            let r2 = vkGetSwapchainImagesKHR(
                device.state.logical_device.value(),
                self.swap_chain.value(),
                &mut image_count,
                self.images.as_mut_ptr(),
            );
            if r2 != VkResult::VK_SUCCESS {
                return Err(DeviceError::Api(
                    "Failed to get swap chain images".to_string(),
                ));
            }
        }
        // build image views
        self.views.reserve(self.images.len());
        for image in &self.images {
            let mut view_create_info = vk_zero_init::<VkImageViewCreateInfo>();
            view_create_info.sType = VkStructureType::VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO;
            view_create_info.image = *image;
            view_create_info.viewType = VkImageViewType::VK_IMAGE_VIEW_TYPE_2D;
            view_create_info.format = self.swap_chain_format_vk;
            view_create_info.subresourceRange.aspectMask =
                VkImageAspectFlagBits::VK_IMAGE_ASPECT_COLOR_BIT as u32;
            view_create_info.subresourceRange.baseMipLevel = 0;
            view_create_info.subresourceRange.levelCount = 1;
            view_create_info.subresourceRange.baseArrayLayer = 0;
            view_create_info.subresourceRange.layerCount = 1;

            let mut image_view = VkImageViewHandle::null();
            unsafe {
                let result = vkCreateImageView(
                    device.state.logical_device.value(),
                    &view_create_info,
                    null_mut(),
                    image_view.ptr_mut(),
                );
                if result != VkResult::VK_SUCCESS {
                    return Err(DeviceError::Api(
                        "Failed to crate image view for swap chain image".to_string(),
                    ));
                }
            }
            self.views.push(image_view);
        }

        device
            .resources
            .uploader
            .borrow_mut()
            .record(device, |_, cmd_buffer| {
                let mut image_barriers =
                    Vec::<VkImageMemoryBarrier>::with_capacity(self.images.len());
                for image in &self.images {
                    let image_barrier = VkImageMemoryBarrier {
                        sType: VkStructureType::VK_STRUCTURE_TYPE_IMAGE_MEMORY_BARRIER,
                        pNext: null_mut(),
                        srcAccessMask: 0,
                        dstAccessMask: VkAccessFlagBits::VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT
                            as u32,
                        oldLayout: VkImageLayout::VK_IMAGE_LAYOUT_UNDEFINED,
                        newLayout: VkImageLayout::VK_IMAGE_LAYOUT_PRESENT_SRC_KHR,
                        srcQueueFamilyIndex: 0,
                        dstQueueFamilyIndex: 0,
                        image: *image,
                        subresourceRange: VkImageSubresourceRange {
                            aspectMask: VkImageAspectFlagBits::VK_IMAGE_ASPECT_COLOR_BIT as u32,
                            baseMipLevel: 0,
                            levelCount: 1,
                            baseArrayLayer: 0,
                            layerCount: 1,
                        },
                    };
                    image_barriers.push(image_barrier);
                }

                // transition image to transfer
                unsafe {
                    vkCmdPipelineBarrier(
                        cmd_buffer,
                        VkPipelineStageFlagBits::VK_PIPELINE_STAGE_TOP_OF_PIPE_BIT as u32,
                        VkPipelineStageFlagBits::VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT
                            as u32,
                        0,
                        0,
                        null_mut(),
                        0,
                        null_mut(),
                        image_barriers.len() as u32,
                        image_barriers.as_ptr(),
                    );
                }
                Ok(())
            })?;
        Ok(())
    }

    fn build_default_render_pass(&mut self, device: &Device) -> Result<(), VkResult> {
        let mut color_attachement_desc = vk_zero_init::<VkAttachmentDescription>();
        color_attachement_desc.format = self.swap_chain_format_vk;
        color_attachement_desc.samples = VkSampleCountFlagBits::VK_SAMPLE_COUNT_1_BIT;
        color_attachement_desc.loadOp = VkAttachmentLoadOp::VK_ATTACHMENT_LOAD_OP_CLEAR;
        color_attachement_desc.storeOp = VkAttachmentStoreOp::VK_ATTACHMENT_STORE_OP_STORE;
        color_attachement_desc.stencilLoadOp = VkAttachmentLoadOp::VK_ATTACHMENT_LOAD_OP_DONT_CARE;
        color_attachement_desc.stencilStoreOp =
            VkAttachmentStoreOp::VK_ATTACHMENT_STORE_OP_DONT_CARE;
        color_attachement_desc.initialLayout = VkImageLayout::VK_IMAGE_LAYOUT_UNDEFINED;
        color_attachement_desc.finalLayout = VkImageLayout::VK_IMAGE_LAYOUT_PRESENT_SRC_KHR;

        let mut attachment_ref = vk_zero_init::<VkAttachmentReference>();
        attachment_ref.attachment = 0;
        attachment_ref.layout = VkImageLayout::VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL;

        let mut subpass_desc = vk_zero_init::<VkSubpassDescription>();
        subpass_desc.pipelineBindPoint = VkPipelineBindPoint::VK_PIPELINE_BIND_POINT_GRAPHICS;
        subpass_desc.colorAttachmentCount = 1;
        subpass_desc.pColorAttachments = &mut attachment_ref;

        let mut subpass_dep = vk_zero_init::<VkSubpassDependency>();
        subpass_dep.srcSubpass = VK_SUBPASS_EXTERNAL as u32;
        subpass_dep.dstSubpass = 0;
        subpass_dep.srcStageMask =
            VkPipelineStageFlagBits::VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT as u32;
        subpass_dep.srcAccessMask = 0;
        subpass_dep.dstStageMask =
            VkPipelineStageFlagBits::VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT as u32;
        subpass_dep.dstAccessMask = VkAccessFlagBits::VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT as u32;

        let mut render_pass_info = vk_zero_init::<VkRenderPassCreateInfo>();
        render_pass_info.sType = VkStructureType::VK_STRUCTURE_TYPE_RENDER_PASS_CREATE_INFO;
        render_pass_info.attachmentCount = 1;
        render_pass_info.pAttachments = &mut color_attachement_desc;
        render_pass_info.subpassCount = 1;
        render_pass_info.pSubpasses = &mut subpass_desc;
        render_pass_info.dependencyCount = 1;
        render_pass_info.pDependencies = &mut subpass_dep;

        let mut render_pass = VkRenderPassHandle::null();
        unsafe {
            let r = vkCreateRenderPass(
                device.state.logical_device.value(),
                &render_pass_info,
                null_mut(),
                render_pass.ptr_mut(),
            );
            if r != VkResult::VK_SUCCESS {
                return Err(r);
            }
        }
        self.render_pass = render_pass;
        Ok(())
    }

    fn build_framebuffers(&mut self, device: &Device) -> Result<(), DeviceError> {
        for image_view in &self.views {
            unsafe {
                let attachments = [image_view.value()];
                let mut framebuffer_info = vk_zero_init::<VkFramebufferCreateInfo>();
                framebuffer_info.sType = VkStructureType::VK_STRUCTURE_TYPE_FRAMEBUFFER_CREATE_INFO;
                framebuffer_info.renderPass = self.render_pass.value();
                framebuffer_info.attachmentCount = attachments.len() as u32;
                framebuffer_info.pAttachments = attachments.as_ptr();
                framebuffer_info.width = self.extent.width;
                framebuffer_info.height = self.extent.height;
                framebuffer_info.layers = 1;
                let mut fbo = VkFramebufferHandle::null();
                let r = vkCreateFramebuffer(
                    device.state.logical_device.value(),
                    &framebuffer_info,
                    null_mut(),
                    fbo.ptr_mut(),
                );
                if r != VkResult::VK_SUCCESS {
                    return Err(DeviceError::Api(format!(
                        "Failed to create swap chain framebuffer: {}",
                        vk_result_to_str(r)
                    )));
                }
                self.framebuffers.push(fbo);
            }
        }

        Ok(())
    }

    fn destroy_framebuffers(&mut self, device: &Device) {
        unsafe {
            let device = device.state.logical_device.value();
            for fbo in &mut self.framebuffers {
                fbo.destroy_with(|f| {
                    vkDestroyFramebuffer(device, f, null_mut());
                });
            }
            self.framebuffers.clear();
        }
    }

    fn build_sync_object(&mut self, _device: &Device) -> Result<(), DeviceError> {
        self.image_in_flight_fences
            .resize(self.images.len(), usize::MAX);
        Ok(())
    }

    fn destroy_sync_objects(&mut self, _device: &Device) {
        self.image_in_flight_fences.clear();
    }

    pub(crate) fn begin_frame(
        &mut self,
        device: VkDevice,
        fences: &[VkFence],
        acquire_semaphore: VkSemaphore,
    ) -> Result<SwapChainState, crate::device::DeviceError> {
        crate::profile_scope!();
        self.active_frame = (self.active_frame + 1) % MAX_FRAMES_IN_FLIGHT as u32;
        unsafe {
            let device_vk = device;
            let acquire_result = vkAcquireNextImageKHR(
                device_vk,
                self.swap_chain.value(),
                u64::MAX,
                acquire_semaphore,
                VkFenceHandle::null().value(),
                &mut self.active_image_index,
            );

            if acquire_result == VkResult::VK_ERROR_OUT_OF_DATE_KHR
                || acquire_result == VkResult::VK_SUBOPTIMAL_KHR
            {
                return Ok(SwapChainState::RebuildRequired);
            }

            if acquire_result != VkResult::VK_SUCCESS
                && acquire_result != VkResult::VK_SUBOPTIMAL_KHR
            {
                return Err(DeviceError::Api(format!(
                    "Failed to acquire next swapchain image {}",
                    vk_result_to_str(acquire_result)
                )));
            }

            // TODO: this is not safe and we are asking for trouble due to dangling references!!!
            // Check if a previous frame is using this image
            if self.image_in_flight_fences[self.active_image_index as usize] != usize::MAX {
                debug_assert!(
                    self.image_in_flight_fences[self.active_image_index as usize] < fences.len()
                );
                crate::profile_scope!("Wait for fence");
                vkWaitForFences(
                    device,
                    1,
                    &fences[self.image_in_flight_fences[self.active_image_index as usize]],
                    VK_TRUE,
                    u64::MAX,
                );
            }

            // Mark the new image as being in use by this frame.
            self.image_in_flight_fences[self.active_image_index as usize] =
                self.active_frame as usize;
        }

        Ok(ImageAcquired(self.active_image_index))
    }

    /*
    pub(crate) fn end_frame(
        &mut self,
        mut cmd_buffer: VkCommandBuffer,
        device: VkDevice,
        acquire_semaphore: VkSemaphore,
        render_semaphore: VkSemaphore,
        render_fence: VkFence,
        graphics_queue: &Mutex<VkQueueHandle>,
        present_queue: &Mutex<VkQueueHandle>,
    ) -> Result<SwapChainState, DeviceError> {
        unsafe {
            // ----------------
            // TODO: Move command execution to the device
            // ----------------
            let wait_stages =
                [VkPipelineStageFlagBits::VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT as u32];

            let submit_info = VkSubmitInfo {
                sType: VkStructureType::VK_STRUCTURE_TYPE_SUBMIT_INFO,
                pNext: null_mut(),
                waitSemaphoreCount: 1,
                pWaitSemaphores: &acquire_semaphore,
                pWaitDstStageMask: wait_stages.as_ptr(),
                commandBufferCount: 1,
                pCommandBuffers: &mut cmd_buffer,
                signalSemaphoreCount: 1,
                pSignalSemaphores: &render_semaphore,
            };
            // Fence can only be reset here or there will be issues when the swap chain
            // acquires an image we are still rendering to.
            vkResetFences(device, 1, &render_fence);

            {
                modus_core::profiling::scope!("Submit Command");
                let queue_accessor = graphics_queue.lock().unwrap();
                let submit_result =
                    vkQueueSubmit(queue_accessor.value(), 1, &submit_info, render_fence);
                if submit_result != VkResult::VK_SUCCESS {
                    return Err(DeviceError::Api(format!(
                        "Failed to submit graphics queue: {}",
                        vk_result_to_str(submit_result)
                    )));
                }
            }

            let wait_semaphores = [render_semaphore];

            let swap_chains = [self.swap_chain.value()];
            let mut present_info = vk_zero_init::<VkPresentInfoKHR>();
            present_info.sType = VkStructureType::VK_STRUCTURE_TYPE_PRESENT_INFO_KHR;
            present_info.waitSemaphoreCount = wait_semaphores.len() as u32;
            present_info.pWaitSemaphores = wait_semaphores.as_ptr();
            present_info.swapchainCount = swap_chains.len() as u32;
            present_info.pSwapchains = swap_chains.as_ptr();
            present_info.pImageIndices = &self.active_image_index;

            let present_result = {
                modus_core::profiling::scope!("Present");
                let queue_accessor = present_queue.lock().unwrap();
                vkQueuePresentKHR(queue_accessor.value(), &present_info)
            };

            if present_result == VkResult::VK_ERROR_OUT_OF_DATE_KHR
                || present_result == VkResult::VK_SUBOPTIMAL_KHR
            {
                return Ok(SwapChainState::RebuildRequired);
            } else if present_result != VkResult::VK_SUCCESS {
                return Err(DeviceError::Api(format!(
                    "Failed to present swapchain {}",
                    vk_result_to_str(present_result)
                )));
            }
        }
        Ok(SwapChainState::Presented)
    }*/

    pub(crate) fn present(
        &mut self,
        present_queue: &Mutex<VkQueueHandle>,
        render_finished_semaphore: VkSemaphore,
    ) -> DeviceResult<()> {
        let swap_chains = [self.swap_chain.value()];
        let wait_semaphores = [render_finished_semaphore];
        let mut present_info = vk_zero_init::<VkPresentInfoKHR>();
        present_info.sType = VkStructureType::VK_STRUCTURE_TYPE_PRESENT_INFO_KHR;
        present_info.waitSemaphoreCount = wait_semaphores.len() as u32;
        present_info.pWaitSemaphores = wait_semaphores.as_ptr();
        present_info.swapchainCount = swap_chains.len() as u32;
        present_info.pSwapchains = swap_chains.as_ptr();
        present_info.pImageIndices = &self.active_image_index;

        let present_result = {
            crate::profile_scope!("Present");
            let queue_accessor = present_queue.lock();
            unsafe { vkQueuePresentKHR(queue_accessor.value(), &present_info) }
        };

        if present_result == VkResult::VK_ERROR_OUT_OF_DATE_KHR
            || present_result == VkResult::VK_SUBOPTIMAL_KHR
        {
            Ok(())
        } else if present_result != VkResult::VK_SUCCESS {
            Err(DeviceError::Api(format!(
                "Failed to present swap chain {}",
                vk_result_to_str(present_result)
            )))
        } else {
            Ok(())
        }
    }

    pub(crate) fn rebuild(
        &mut self,
        device: &Device,
        surface_interface: &dyn RenderSurfaceInterface,
    ) -> Result<(), DeviceError> {
        self.destroy_sync_objects(device);
        self.destroy_framebuffers(device);
        self.destroy_swap_chain(device);

        self.build_sync_object(device)?;
        self.build_swapchain(device, surface_interface)?;
        self.build_framebuffers(device)?;
        Ok(())
    }

    #[allow(unused)]
    pub(crate) unsafe fn default_render_pass(&self) -> VkRenderPass {
        self.render_pass.value()
    }

    #[allow(unused)]
    pub(crate) fn fbo_handle(&self) -> VkFramebuffer {
        self.framebuffers[self.active_image_index as usize].value()
    }

    pub(crate) fn get_extent(&self) -> VkExtent2D {
        self.extent
    }

    pub(crate) fn get_active_image(&self) -> VkImage {
        self.images[self.active_image_index as usize]
    }

    #[allow(unused)]
    pub(crate) fn get_active_image_index(&self) -> u32 {
        self.active_image_index
    }

    #[allow(unused)]
    pub(crate) fn get_active_image_view(&self) -> VkImageView {
        self.views[self.active_image_index as usize].value()
    }

    pub(crate) fn destroy(&mut self, device: &Device) {
        let vk_device = device.state.logical_device.value();
        unsafe {
            // Ensure this isn't used anywhere when it's destroyed
            vkDeviceWaitIdle(vk_device);
            self.destroy_sync_objects(device);
            self.destroy_framebuffers(device);
            self.render_pass.destroy_with(|r| {
                vkDestroyRenderPass(vk_device, r, null_mut());
            });
            self.destroy_swap_chain(device);
        }
    }
}

impl crate::swap_chain::SwapChain for SwapChain {}

unsafe impl Send for SwapChain {}

pub(crate) type SwapChainMap = HandleMap<SwapChainHandle, SwapChain>;
