use crate::descriptors::{
    DescriptorBufferInput, DescriptorPoolCreateInfo, DescriptorSetUpdateInfo,
    DescriptorTextureInput,
};
use crate::device::{Device, DeviceError, DeviceResult};
use crate::types::{DescriptorPoolHandle, DescriptorSetHandle, DescriptorSetLayoutHandle};
use modus_core::ArrayVec;
use modus_core::HashMap;
use std::collections::hash_map::Entry;

#[derive(Debug)]
struct ActiveDescriptor {
    descriptor: DescriptorSetHandle,
    layout: DescriptorSetLayoutHandle,
    last_used_counter: i32,
}

const KEEP_ALIVE_COUNT: i32 = 8;
impl ActiveDescriptor {
    fn new(set: DescriptorSetHandle, layout: DescriptorSetLayoutHandle) -> Self {
        Self {
            descriptor: set,
            layout,
            last_used_counter: KEEP_ALIVE_COUNT,
        }
    }
}

const MAX_DESCRIPTORS_IN_KEY: usize = 4;

#[derive(Debug, Eq, PartialEq, Hash)]
struct DescriptorSetKey {
    buffers: ArrayVec<DescriptorBufferInput, 4>,
    textures: ArrayVec<DescriptorTextureInput, 4>,
    layout: DescriptorSetLayoutHandle,
}

impl DescriptorSetKey {
    fn new(info: &DescriptorSetUpdateInfo, layout: DescriptorSetLayoutHandle) -> Self {
        assert!(info.buffers.len() <= MAX_DESCRIPTORS_IN_KEY);
        assert!(info.textures.len() <= MAX_DESCRIPTORS_IN_KEY);
        let mut buffers = ArrayVec::<DescriptorBufferInput, MAX_DESCRIPTORS_IN_KEY>::new();
        let mut textures = ArrayVec::<DescriptorTextureInput, MAX_DESCRIPTORS_IN_KEY>::new();
        for b in info.buffers {
            buffers.push(*b);
        }
        for t in info.textures {
            textures.push(*t)
        }
        Self {
            buffers,
            textures,
            layout,
        }
    }
}

/// Descriptor which allocates all resources on demand and tries to re-use existing descriptor
/// sets as soon as they are no longer used. The descriptors are kept alive for [KEEP_ALIVE_COUNT]
/// frames before being re-cycled.
/// Descriptor pools are only release when the allocator is cleared.
#[derive(Debug)]
pub struct CachedDescriptorAllocator {
    active_list: HashMap<DescriptorSetKey, ActiveDescriptor>,
    free_list: HashMap<DescriptorSetLayoutHandle, Vec<DescriptorSetHandle>>,
    pools: Vec<DescriptorPoolHandle>,
    pool_create_info: DescriptorPoolCreateInfo,
}

impl Drop for CachedDescriptorAllocator {
    fn drop(&mut self) {
        assert!(self.pools.is_empty())
    }
}
impl CachedDescriptorAllocator {
    pub fn new(pool_info: DescriptorPoolCreateInfo) -> Self {
        Self {
            active_list: HashMap::with_capacity(16),
            free_list: HashMap::with_capacity(16),
            pools: Vec::with_capacity(4),
            pool_create_info: pool_info,
        }
    }

    /// Allocate a descriptor. First we check whether the matching descriptor still exists. If no
    /// matching descriptor exists we try to re-use an inactive one. Finally, if there are no
    /// inactive descriptors, we allocate a new one from a descriptor pool. The descriptor pools
    /// are allocated as needed.
    pub fn allocate(
        &mut self,
        device: &dyn Device,
        update_info: DescriptorSetUpdateInfo,
        layout: DescriptorSetLayoutHandle,
    ) -> DeviceResult<DescriptorSetHandle> {
        let key = DescriptorSetKey::new(&update_info, layout);
        let free_list = &mut self.free_list;
        let pools = &mut self.pools;
        match self.active_list.entry(key) {
            Entry::Occupied(mut o) => {
                let descriptor = o.get_mut();
                descriptor.last_used_counter += 1;
                Ok(descriptor.descriptor)
            }
            Entry::Vacant(v) => {
                // Check if the free list has any descriptor available
                if let Some(list) = free_list.get_mut(&layout) {
                    if let Some(handle) = list.pop() {
                        // if we have an existing one that can be re-used update it
                        if let Err(e) = device.update_descriptor_set(handle, update_info) {
                            list.push(handle);
                            return Err(e);
                        }
                        v.insert(ActiveDescriptor::new(handle, layout));
                        return Ok(handle);
                    }
                }
                // No existing matches, we need to allocate a new one
                let descriptor = Self::allocate_descriptor_from_pool(
                    pools,
                    device,
                    layout,
                    &self.pool_create_info,
                )?;
                if let Err(e) = device.update_descriptor_set(descriptor, update_info) {
                    // in case update fails add it to the free list
                    match free_list.entry(layout) {
                        Entry::Occupied(mut o) => o.get_mut().push(descriptor),
                        Entry::Vacant(v) => {
                            v.insert(vec![descriptor]);
                        }
                    }
                    return Err(e);
                }
                v.insert(ActiveDescriptor::new(descriptor, layout));
                Ok(descriptor)
            }
        }
    }

    fn allocate_descriptor_from_pool(
        pools: &mut Vec<DescriptorPoolHandle>,
        device: &dyn Device,
        layout: DescriptorSetLayoutHandle,
        pool_create_info: &DescriptorPoolCreateInfo,
    ) -> DeviceResult<DescriptorSetHandle> {
        if pools.is_empty() {
            let pool = device.create_descriptor_pool(pool_create_info)?;
            pools.push(pool);
        }
        // loop so that we can handle the case where a pool is full and we may need to allocate
        // another one.
        for _ in 0..3 {
            match device.create_descriptor_set(layout, *pools.last().unwrap(), None) {
                Ok(descriptor) => {
                    return Ok(descriptor);
                }
                Err(e) => {
                    if e == DeviceError::OutOfMemory {
                        // try to allocate a new pool and continue the loop
                        let pool = device.create_descriptor_pool(pool_create_info)?;
                        pools.push(pool);
                        continue;
                    } else {
                        return Err(e);
                    }
                }
            }
        }
        // We should never get here.
        Err(DeviceError::OutOfMemory)
    }

    /// Perform book keeping to re-cycle unused descriptors
    pub fn update(&mut self) {
        let free_list = &mut self.free_list;
        self.active_list.retain(|_, descriptor| {
            descriptor.last_used_counter -= 1;
            if descriptor.last_used_counter <= 0 {
                match free_list.entry(descriptor.layout) {
                    Entry::Occupied(mut o) => {
                        o.get_mut().push(descriptor.descriptor);
                    }
                    Entry::Vacant(v) => {
                        let mut vec = Vec::with_capacity(4);
                        vec.push(descriptor.descriptor);
                        v.insert(vec);
                    }
                }
                false
            } else {
                true
            }
        });
        self.free_list.retain(|_, k| !k.is_empty())
    }

    pub fn clear(&mut self, device: &dyn Device) -> DeviceResult<()> {
        self.free_list.clear();
        self.active_list.clear();
        for pool in &self.pools {
            device.destroy_descriptor_pool(*pool)?
        }
        self.pools.clear();
        Ok(())
    }
}
