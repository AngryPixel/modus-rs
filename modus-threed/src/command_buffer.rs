//! Command buffer abstraction

use crate::descriptors::DescriptorSetBindInfo;
use crate::device::{DeviceError, DeviceResult};
use crate::render_pass::RenderPassClearInfo;
use crate::types::{
    BufferHandle, FramebufferHandle, IndexType, PipelineHandle, Rect, RenderPassHandle,
    SwapChainHandle, TextureHandle, Viewport,
};

pub trait CommandBuffer: std::fmt::Debug {
    fn blit_texture_to_swap_chain(
        &mut self,
        texture: TextureHandle,
        swap_chain: SwapChainHandle,
    ) -> Result<(), DeviceError>;

    fn begin_render_pass(
        &mut self,
        pass: RenderPassHandle,
        framebuffer: FramebufferHandle,
        clear_info: Option<RenderPassClearInfo>,
    ) -> Result<(), DeviceError>;

    fn begin_render_pass_with_swap_chain(
        &mut self,
        pass: RenderPassHandle,
        swap_chain: SwapChainHandle,
        clear_info: Option<RenderPassClearInfo>,
    ) -> Result<(), DeviceError>;

    fn end_render_pass(&mut self) -> Result<(), DeviceError>;

    fn bind_pipeline(&mut self, h: PipelineHandle) -> Result<(), DeviceError>;

    fn bind_index_buffer(
        &mut self,
        h: BufferHandle,
        offset: usize,
        index_type: IndexType,
    ) -> Result<(), DeviceError>;

    fn draw(
        &mut self,
        vertex_count: u32,
        instance_count: u32,
        first_vertex: u32,
        first_instance: u32,
    ) -> Result<(), DeviceError>;

    fn draw_indexed(
        &mut self,
        index_count: u32,
        instance_count: u32,
        first_index: u32,
        vertex_offset: i32,
        first_instance: u32,
    ) -> Result<(), DeviceError>;

    fn set_viewport(&mut self, viewport: &Viewport, inverted: bool) -> Result<(), DeviceError>;

    fn set_scissor_rect(&mut self, rect: &Rect) -> Result<(), DeviceError>;

    //TODO: cache pipeline state when we bind the pipeline

    fn bind_descriptors(
        &mut self,
        pipeline: PipelineHandle,
        descriptors: &[DescriptorSetBindInfo],
    ) -> DeviceResult<()>;
}
