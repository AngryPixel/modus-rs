//! Instance Creation/Wrapper

use crate::app_interface::{NativeSurfacePtr, RenderSurfaceInterface};
use crate::device::DeviceError;
use std::fmt::Debug;
use std::sync::Arc;

#[derive(Debug, Copy, Clone)]
pub struct InstanceCreateInfo<'a> {
    pub name: &'a str,
    pub version_major: u32,
    pub version_minor: u32,
    pub version_patch: u32,
    pub debug: bool,
    pub api_validation: bool,
}

#[derive(Debug, Copy, Clone, Eq, PartialEq)]
pub enum InstanceError {
    Setup,
    AppInterface,
    MissingFeatures,
    MissingExtensions,
    MissingValidationLayers,
    Unknown,
}

#[cfg(target_pointer_width = "64")]
pub type NativeInstancePtr = *mut std::os::raw::c_void;

#[cfg(target_pointer_width = "32")]
pub type NativeInstancePtr = u64;

pub trait Instance: std::fmt::Debug {
    fn create_device(
        self: Arc<Self>,
        render_surface: &dyn RenderSurfaceInterface,
    ) -> Result<Arc<dyn crate::device::Device>, DeviceError>;

    /// Get native pointer to the whichever threed instance is active
    /// # Safety
    /// Pointer should not be store and can only be used if the Instance is alive
    unsafe fn native_ptr(&self) -> NativeInstancePtr;

    /// Destroy a render surface pointer
    /// # Safety
    /// Surface pointer must be a valid surface pointer create for this interface
    unsafe fn destroy_render_surface(&self, surface: NativeSurfacePtr);
}
