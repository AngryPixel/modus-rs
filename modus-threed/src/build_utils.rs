use std::env;
use std::io::{stderr, Write};
use std::path::Path;
use std::process::Command;

/// Compile glsl shader to SPIRV. The `path` parameter should be a relative path to where the shader
/// is located in relation to build.rs for everything to work.
/// NOTE: Only for use with build.rs
pub fn compile_shader(path: &Path) {
    let out_dir = env::var("OUT_DIR").unwrap();
    let mut target_path = Path::new(&out_dir)
        .join(path.parent().unwrap())
        .join(path.extension().unwrap());
    target_path.set_extension("spv");

    let parent_path = target_path.parent().unwrap();
    std::fs::create_dir_all(parent_path).unwrap();

    let output = Command::new("glslc")
        .arg(&path)
        .arg("-o")
        .arg(&target_path)
        .output()
        .unwrap();
    if !output.status.success() {
        eprintln!("Failed to compile shader {}", path.to_str().unwrap());
        stderr()
            .write_all(output.stderr.as_slice())
            .expect("Error write failed");
        panic!("Compilation failed");
    }

    println!("cargo:rerun-if-changed={}", path.to_str().unwrap());
}
