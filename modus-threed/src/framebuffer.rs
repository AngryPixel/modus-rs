//! Framebuffer abstraction
use crate::types::{RenderPassHandle, TextureHandle};

#[derive(Debug)]
pub struct FramebufferCreateInfo<'a> {
    pub width: u32,
    pub height: u32,
    pub depth: u32,
    pub render_pass: RenderPassHandle,
    pub textures: &'a [TextureHandle],
}
