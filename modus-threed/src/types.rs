use modus_core::define_custom_handle_type;
use modus_core::handle_map::Handle;
use std::fmt::Display;

pub const PIPELINE_STAGE_FLAG_VERTEX: u8 = 0x1;
pub const PIPELINE_STAGE_FLAG_FRAGMENT: u8 = 0x2;
pub const PIPELINE_STAGE_FLAG_ALL: u8 = PIPELINE_STAGE_FLAG_FRAGMENT | PIPELINE_STAGE_FLAG_VERTEX;

#[repr(u8)]
#[derive(Debug, Eq, PartialEq, Copy, Clone, Hash, Display)]
#[allow(non_camel_case_types)]
pub enum DataType {
    I8 = 0,
    U8,
    I16,
    U16,
    I32,
    U32,
    Vec2I8,
    Vec2U8,
    Vec2I16,
    Vec2U16,
    Vec2I32,
    Vec2U32,
    Vec2F32,
    Vec3I8,
    Vec3U8,
    Vec3I16,
    Vec3U16,
    Vec3I32,
    Vec3U32,
    Vec3F32,
    Vec4I8,
    Vec4U8,
    Vec4I16,
    Vec4U16,
    Vec4I32,
    Vec4U32,
    Vec4F32,
    Mat2F32,
    Mat3F32,
    Mat4F32,
    U32_2_3x10_REV,
    I32_2_3x10_REV,
    I8_SNORM,
    U8_UNORM,
    I16_SNORM,
    U16_UNORM,
    Vec2I8_SNORM,
    Vec2U8_UNORM,
    Vec2I16_SNORM,
    Vec2U16_UNORM,
    Vec3I8_SNORM,
    Vec3U8_UNORM,
    Vec3I16_SNORM,
    Vec3U16_UNORM,
    Vec4I8_SNORM,
    Vec4U8_UNORM,
    Vec4I16_SNORM,
    Vec4U16_UNORM,
    U32_2_3x10_REV_UNORM,
    I32_2_3x10_REV_SNORM,
    Total,
}

#[repr(u8)]
#[derive(Debug, Eq, PartialEq, Copy, Clone, Hash, Display)]
pub enum TextureType {
    T1D,
    T2D,
    T3D,
    Total,
}

#[repr(u8)]
#[derive(Debug, Eq, PartialEq, Copy, Clone, Hash, Display)]
#[allow(non_camel_case_types)]
pub enum TextureFormat {
    R8G8B8A8 = 0,
    R8G8B8,
    R5G6B5,
    R4G4B4A4,
    R5G5B5A1,
    Depth32F,
    Depth24Stencil8,
    Depth32Stencil8,
    RGBA_ASTC4x4,
    RGBA_ASTC5x4,
    RGBA_ASTC5x5,
    RGBA_ASTC6x5,
    RGBA_ASTC6x6,
    RGBA_ASTC8x5,
    RGBA_ASTC8x6,
    RGBA_ASTC8x8,
    RGBA_ASTC10x5,
    RGBA_ASTC10x6,
    RGBA_ASTC10x8,
    RGBA_ASTC10x10,
    RGBA_ASTC12x10,
    RGBA_ASTC12x12,
    R8,
    RGBA_ASTC4x4_SRGB,
    RGBA_ASTC5x4_SRGB,
    RGBA_ASTC5x5_SRGB,
    RGBA_ASTC6x5_SRGB,
    RGBA_ASTC6x6_SRGB,
    RGBA_ASTC8x5_SRGB,
    RGBA_ASTC8x6_SRGB,
    RGBA_ASTC8x8_SRGB,
    RGBA_ASTC10x5_SRGB,
    RGBA_ASTC10x6_SRGB,
    RGBA_ASTC10x8_SRGB,
    RGBA_ASTC10x10_SRGB,
    RGBA_ASTC12x10_SRGB,
    RGBA_ASTC12x12_SRGB,
    R8G8B8_SRGB,
    R8G8B8A8_SRGB,
    R_EAC11,
    R_EAC11_SIGNED,
    RG_EAC11,
    RG_EAC11_SIGNED,
    RGB_ETC2,
    RGB_ETC2_SRGB,
    RGBA_ETC2_A1,
    RGBA_ETC2_A1_SRGB,
    RGBA_ETC2,
    RGBA_ETC2_SRGB,
    R10G10B10A2_I32,
    R10G10B10A2_U32,
    R16G16B16_F16,
    R16G16B16A_F16,
    R16G16_F16,
    R16_U16,
    R32G32_U32,
    R16G16_U16,
    Depth16F,
    B8G8R8A8,
    B8G8R8A8_SRGB,
    Total,
}

#[repr(u8)]
#[derive(Debug, Eq, PartialEq, Copy, Clone, Hash, Display)]
pub enum TextureUsage {
    Static,
    Attachment,
}

#[repr(u8)]
#[derive(Debug, Eq, PartialEq, Copy, Clone, Hash, Display)]
pub enum Primitive {
    Points = 0,
    Lines,
    LineStrip,
    Triangles,
    TriangleStrip,
    TriangleFan,
}

#[repr(u8)]
#[derive(Debug, Eq, PartialEq, Copy, Clone, Hash, Display)]
pub enum IndexType {
    None = 0,
    U16,
    U32,
}

#[repr(u8)]
#[derive(Debug, Eq, PartialEq, Copy, Clone, Hash, Display)]
pub enum ShaderType {
    Vertex = 0,
    Fragment,
    Compute,
}

#[repr(u8)]
#[derive(Debug, Eq, PartialEq, Copy, Clone, Hash, Display)]
pub enum BufferType {
    Uniform = 0,
    Storage,
    Index,
    Vertex,
}

#[repr(u8)]
#[derive(Debug, Eq, PartialEq, Copy, Clone, Hash, Display)]
pub enum MemoryType {
    CpuOnly,
    GpuOnly,
    CpuToGpu,
    GpuToCpu,
    CpuCopy,
    GpuLazyAlloc,
}

#[repr(u8)]
#[derive(Debug, Eq, PartialEq, Copy, Clone, Hash, Display)]
pub enum CompareOp {
    Never = 0,
    Less,
    LessOrEqual,
    Greater,
    GreaterOrEqual,
    Equal,
    NotEqual,
    Always,
}

#[repr(u8)]
#[derive(Debug, Eq, PartialEq, Copy, Clone, Hash, Display)]
pub enum StencilOp {
    Keep = 0,
    Zero,
    Replace,
    IncrementAndClamp,
    DecrementAndClamp,
    Invert,
    IncrementAndWrap,
    DecrementAndWrap,
}

#[repr(u8)]
#[derive(Debug, Eq, PartialEq, Copy, Clone, Hash, Display)]
pub enum BlendOp {
    Add = 0,
    Subtract,
    SubtractReverse,
    Min,
    Max,
}

#[repr(u8)]
#[derive(Debug, Eq, PartialEq, Copy, Clone, Hash, Display)]
pub enum BlendFactor {
    Zero = 0,
    One,
    SourceColor,
    OneMinusSourceColor,
    SourceAlpha,
    OneMinusSourceAlpha,
    DestinationColor,
    OneMinusDestinationColor,
    DestinationAlpha,
    OneMinusDestinationAlpha,
    ConstantColor,
    OneMinusConstantColor,
    ConstantAlpha,
    OneMinusConstantAlpha,
    SourceAlphaSaturate,
}

#[repr(u8)]
#[derive(Debug, Eq, PartialEq, Copy, Clone, Hash, Display)]
pub enum LogicOp {
    Clear = 0,
    And,
    AndReverse,
    Copy,
    AndInverted,
    NoOp,
    Xor,
    Or,
    Nor,
    Equivalent,
    Invert,
    Reverse,
    CopyInverted,
    Nand,
    Set,
}

#[repr(u8)]
#[derive(Debug, Eq, PartialEq, Copy, Clone, Hash, Display)]
pub enum PolygonMode {
    Fill = 0,
    Line,
    Point,
}

#[repr(u8)]
#[derive(Debug, Eq, PartialEq, Copy, Clone, Hash, Display)]
pub enum CullMode {
    None = 0,
    Front,
    Back,
    FrontAndBack,
}

#[repr(u8)]
#[derive(Debug, Eq, PartialEq, Copy, Clone, Hash, Display)]
pub enum FrontFace {
    CounterClockwise = 0,
    ClockWise,
}

#[repr(u8)]
#[derive(Debug, Eq, PartialEq, Copy, Clone, Hash, Display)]
pub enum Filter {
    Nearest = 0,
    Linear,
}

#[repr(u8)]
#[derive(Debug, Eq, PartialEq, Copy, Clone, Hash, Display)]
pub enum SamplerMipmapMode {
    Nearest = 0,
    Linear,
}

#[repr(u8)]
#[derive(Debug, Eq, PartialEq, Copy, Clone, Hash, Display)]
pub enum SamplerAddressMode {
    Repeat = 0,
    MirroredRepeat,
    ClampToEdge,
    ClampToBorder,
}

#[derive(Debug, Copy, Clone, PartialEq)]
pub struct Viewport {
    pub x: f32,
    pub y: f32,
    pub w: f32,
    pub h: f32,
}

#[derive(Debug, Copy, Clone, PartialEq, Eq)]
pub struct Rect {
    pub x: i32,
    pub y: i32,
    pub w: u32,
    pub h: u32,
}

define_custom_handle_type!(SwapChainHandle);
define_custom_handle_type!(TextureHandle);
define_custom_handle_type!(RenderPassHandle);
define_custom_handle_type!(FramebufferHandle);
define_custom_handle_type!(ShaderHandle);
define_custom_handle_type!(PipelineHandle);
define_custom_handle_type!(BufferHandle);
define_custom_handle_type!(DescriptorSetLayoutHandle);
define_custom_handle_type!(DescriptorPoolHandle);
define_custom_handle_type!(DescriptorSetHandle);
define_custom_handle_type!(SamplerHandle);

pub type DeviceSize = u64;
