//! 3D render abstraction library
#![feature(vec_spare_capacity, maybe_uninit_extra)]
#[macro_use]
extern crate enum_display_derive;

use crate::app_interface::VkAppInterface;
use crate::instance::{Instance, InstanceCreateInfo, InstanceError};
use std::sync::Arc;

pub mod app_interface;
pub mod buffer;
pub mod build_utils;
pub mod command_buffer;
pub mod descriptor_allocators;
pub mod descriptors;
pub mod device;
pub mod framebuffer;
pub mod instance;
mod null;
pub mod pipeline;
pub mod render_graph;
pub mod render_pass;
pub mod sampler;
pub mod shader;
pub mod swap_chain;
pub mod texture;
pub mod types;
mod vulkan;

pub fn create_vk_instance(
    info: &InstanceCreateInfo,
    vk_app_interface: &dyn VkAppInterface,
) -> Result<Arc<dyn Instance>, InstanceError> {
    let instance = crate::vulkan::instance::Instance::new(info, vk_app_interface)?;
    Ok(Arc::new(instance))
}

pub fn create_null_instance() -> Arc<dyn Instance> {
    Arc::new(crate::null::Instance {})
}

pub(crate) const PROFILE_COLOR0: u32 = modus_profiler::COLOR_RED900;

#[macro_export(crate)]
macro_rules! profile_scope {
    () => {
        modus_profiler::scope_color!(crate::PROFILE_COLOR0);
    };
    ($name:literal) => {
        modus_profiler::scope_color!(crate::PROFILE_COLOR0, $name);
    };
}
