use example_lib::{
    generate_main, load_shader, ExampleError, ThreedExampleRunner, COLOR_TEXTURE_NAME,
};
use modus_app::events::{AppEvent, InputEvent};
use modus_log::{log_error, log_info};
use modus_threed::command_buffer::CommandBuffer;
use modus_threed::device::{Device, DeviceError};
use modus_threed::pipeline::{
    BlendState, ColorAttachmentBlendState, DepthStencilState, InputAssemblyState, MultisampleState,
    PipelineCreateInfo, RasterizationState, ShaderState, COLOR_MASK_ALL,
};
use modus_threed::render_graph::{
    RenderGraph, RenderPassBuildState, RenderPassColorOutputDesc, RenderPassCreateInfo, SizeType,
};
use modus_threed::render_pass::{
    AttachmentLoadOp, AttachmentStoreOp, ClearValue, PipelineStage, RenderPassClearInfo,
};
use modus_threed::types::{
    CullMode, FrontFace, PipelineHandle, PolygonMode, Primitive, Rect, ShaderHandle, ShaderType,
    SwapChainHandle, TextureFormat, Viewport,
};
use std::time::Duration;

#[derive(Debug)]
struct UserData {
    duration: Duration,
    pipeline: Option<PipelineHandle>,
    vert_shader: ShaderHandle,
    frag_shader: ShaderHandle,
}

#[derive(Debug)]
struct HelloTriangleExample {
    elapsed_time: Duration,
    swap_chain: SwapChainHandle,
    render_graph: Option<RenderGraph<UserData>>,
    user_data: UserData,
}

impl Default for HelloTriangleExample {
    fn default() -> Self {
        Self {
            swap_chain: Default::default(),
            render_graph: None,
            elapsed_time: Duration::new(0, 0),
            user_data: UserData {
                duration: Default::default(),
                pipeline: None,
                vert_shader: Default::default(),
                frag_shader: Default::default(),
            },
        }
    }
}

impl HelloTriangleExample {
    fn load_shaders(&mut self, device: &dyn Device) -> Result<(), DeviceError> {
        let vert_path = "shaders/hello_triangle/vert.spv";
        let frag_path = "shaders/hello_triangle/frag.spv";
        self.user_data.vert_shader = load_shader(vert_path, device)?;
        self.user_data.frag_shader = load_shader(frag_path, device)?;
        Ok(())
    }
}

#[derive(Debug)]
struct TrianglePass {}

impl modus_threed::render_graph::RenderPass<UserData> for TrianglePass {
    fn build(
        &self,
        user_data: &mut UserData,
        render_graph: &RenderGraph<UserData>,
        render_pass_data: &RenderPassBuildState,
        cmd_buffer: &mut dyn CommandBuffer,
    ) -> Result<(), DeviceError> {
        if user_data.pipeline.is_none() {
            let pipeline_create_info = PipelineCreateInfo {
                descriptor_set_layouts: None,
                depth_stencil_state: DepthStencilState {
                    depth: None,
                    stencil: None,
                },
                blend_state: BlendState {
                    attachments: &[ColorAttachmentBlendState {
                        state: None,
                        color_mask: COLOR_MASK_ALL,
                    }],
                    logic_op: None,
                    blend_constants: [0.0; 4],
                },
                rasterization_state: RasterizationState {
                    depth_clamp: false,
                    rasterizer_discard: false,
                    polygon_mode: PolygonMode::Fill,
                    line_width: 1.0,
                    cull_mode: CullMode::None,
                    front_face: FrontFace::CounterClockwise,
                    depth_bias: None,
                },
                input_assembly_state: InputAssemblyState {
                    primitive: Primitive::Triangles,
                    primitive_restart: false,
                },
                shader_states: &[
                    ShaderState {
                        shader: user_data.vert_shader,
                        stage: ShaderType::Vertex,
                        entry: "main",
                    },
                    ShaderState {
                        shader: user_data.frag_shader,
                        stage: ShaderType::Fragment,
                        entry: "main",
                    },
                ],
                multisample_state: MultisampleState {
                    sample_shading: false,
                    samples: 1,
                    min_sample_shading: 1.0,
                    alpha_to_coverage: false,
                    alpha_to_one: false,
                },
                render_pass: render_pass_data.render_pass,
            };

            let pipeline = render_graph.device.create_pipeline(&pipeline_create_info)?;
            user_data.pipeline = Some(pipeline);
        }

        let mut info = RenderPassClearInfo::default();
        let elapsed_seconds = user_data.duration.as_secs_f64();
        info.values[0] = ClearValue::Color([
            elapsed_seconds.sin() as f32,
            elapsed_seconds.cos() as f32,
            elapsed_seconds.sin() as f32,
            1.0,
        ]);

        cmd_buffer.begin_render_pass(
            render_pass_data.render_pass,
            render_pass_data.framebuffer,
            Some(info),
        )?;
        if let Some(p) = &user_data.pipeline {
            cmd_buffer.bind_pipeline(*p)?;
            cmd_buffer.set_scissor_rect(&Rect {
                x: 0,
                y: 0,
                w: render_pass_data.dimensions[0],
                h: render_pass_data.dimensions[1],
            })?;
            cmd_buffer.set_viewport(
                &Viewport {
                    x: 0.0,
                    y: 0.0,
                    w: render_pass_data.dimensions[0] as f32,
                    h: render_pass_data.dimensions[1] as f32,
                },
                true,
            )?;
            cmd_buffer.draw(3, 1, 0, 0)?;
        }
        cmd_buffer.end_render_pass()?;
        Ok(())
    }
}

impl example_lib::ThreedExample for HelloTriangleExample {
    fn initialize(&mut self, runner: &mut ThreedExampleRunner) -> Result<(), ExampleError> {
        let window = match runner
            .app
            .window_system_mut()
            .resole_window_mut(runner.window_id)
        {
            Some(w) => w,
            None => {
                return Err(ExampleError::App(format!(
                    "Failed to locate window {}",
                    runner.window_id
                )))
            }
        };
        let swap_chain = window.create_swap_chain(runner.device.clone())?;
        log_info!("Created swap chain {}", swap_chain);
        self.swap_chain = swap_chain;
        let mut render_graph = RenderGraph::new(runner.device.clone());
        {
            let outputs = [RenderPassColorOutputDesc {
                name: COLOR_TEXTURE_NAME,
                size: SizeType::SwapChainRelative(1.0, 1.0, 1.0),
                format: TextureFormat::B8G8R8A8,
                samples: 1,
                transient: false,
                input: None,
                load_op: AttachmentLoadOp::Clear,
                store_op: AttachmentStoreOp::Store,
            }];

            let create_info = RenderPassCreateInfo {
                name: "color_pass",
                stage: PipelineStage::Graphics,
                color_inputs: &[],
                color_outputs: &outputs,
                size: SizeType::SwapChainRelative(1.0, 1.0, 1.0),
                build_pass: true,
            };

            render_graph.add_pass(&create_info, Box::new(TrianglePass {}))?;
        }

        self.render_graph = Some(render_graph);
        self.load_shaders(runner.device.as_ref())?;
        Ok(())
    }

    fn shutdown(&mut self, runner: &mut ThreedExampleRunner) {
        if let Err(e) = runner.device.destroy_swap_chain(self.swap_chain) {
            log_error!("Failed to destroy swap chain: {}", e);
        }
    }

    fn tick(&mut self, runner: &mut ThreedExampleRunner) -> Result<bool, ExampleError> {
        let window = runner
            .app
            .window_system()
            .resole_window(runner.window_id)
            .unwrap();
        let graph = self.render_graph.as_mut().unwrap();
        self.elapsed_time += runner.frame_tick;
        self.user_data.duration = self.elapsed_time;
        graph.execute(
            &mut self.user_data,
            COLOR_TEXTURE_NAME,
            self.swap_chain,
            window.render_surface_interface(),
        )?;
        Ok(true)
    }
}

impl modus_app::app::AppEventHandler for HelloTriangleExample {
    fn on_app_event(&mut self, _event: &AppEvent) {}

    fn on_input_event(&mut self, _event: &InputEvent) {}
}

generate_main!(HelloTriangleExample);
