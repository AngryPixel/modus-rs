use example_lib::{
    generate_main, load_shader, ExampleError, ThreedExampleRunner, COLOR_TEXTURE_NAME,
};
use modus_app::events::{AppEvent, InputEvent};
use modus_log::{log_error, log_info};
use modus_math::*;
use modus_threed::buffer::BufferCreateInfo;
use modus_threed::command_buffer::CommandBuffer;
use modus_threed::descriptor_allocators::CachedDescriptorAllocator;
use modus_threed::descriptors::{
    DescriptorBufferInput, DescriptorDesc, DescriptorPoolCreateInfo, DescriptorSetBindInfo,
    DescriptorSetLayoutCreateInfo, DescriptorSetUpdateInfo, DescriptorTextureInput, DescriptorType,
};
use modus_threed::device::{Device, DeviceError, DeviceResult};
use modus_threed::pipeline::{
    BlendState, ColorAttachmentBlendState, DepthState, DepthStencilState, InputAssemblyState,
    MultisampleState, PipelineCreateInfo, RasterizationState, ShaderState, COLOR_MASK_ALL,
};
use modus_threed::render_graph::{
    RenderGraph, RenderPassBuildState, RenderPassColorOutputDesc, RenderPassCreateInfo, SizeType,
};
use modus_threed::render_pass::{
    AttachmentLoadOp, AttachmentStoreOp, ClearValue, PipelineStage, RenderPassClearInfo,
};
use modus_threed::sampler::SamplerCreateInfo;
use modus_threed::texture::{TextureCreateInfo, TextureData, TEXTURE_USAGE_FLAG_SHADER_INPUT};
use modus_threed::types::{
    BufferHandle, BufferType, CompareOp, CullMode, DescriptorSetHandle, DescriptorSetLayoutHandle,
    DeviceSize, Filter, FrontFace, IndexType, MemoryType, PipelineHandle, PolygonMode, Primitive,
    Rect, SamplerAddressMode, SamplerHandle, SamplerMipmapMode, ShaderHandle, ShaderType,
    SwapChainHandle, TextureFormat, TextureHandle, TextureType, Viewport,
    PIPELINE_STAGE_FLAG_FRAGMENT, PIPELINE_STAGE_FLAG_VERTEX,
};
use std::mem::size_of;
use std::slice::from_raw_parts;
use std::time::Duration;

#[derive(Debug)]
struct UserData {
    duration: Duration,
    pipeline: Option<PipelineHandle>,
    vert_shader: ShaderHandle,
    frag_shader: ShaderHandle,
    storage_buffer: BufferHandle,
    index_buffer: BufferHandle,
    transform_buffer: BufferHandle,
    descriptor_layout_static: DescriptorSetLayoutHandle,
    descriptor_layout_dynamic: DescriptorSetLayoutHandle,
    texture: TextureHandle,
    sampler: SamplerHandle,
    frame_counter: usize,
    desciptor_allocator: CachedDescriptorAllocator,
}

#[derive(Debug)]
struct HelloQuadExample {
    elapsed_time: Duration,
    swap_chain: SwapChainHandle,
    render_graph: Option<RenderGraph<UserData>>,
    user_data: UserData,
}

#[repr(C)]
#[repr(align(4))]
struct QuadVertexData {
    vertices: [f32; 3],
    color: [f32; 3],
    tex: [f32; 2],
}

#[repr(C)]
struct TransformMatrices {
    view_proj: Mat4,
    transform: Mat4,
}

const QUAD_RADIUS: f32 = 1.0;
const QUAD_DATA: [QuadVertexData; 4] = [
    QuadVertexData {
        vertices: [-QUAD_RADIUS, -QUAD_RADIUS, 0.0],
        color: [0.0, 0.0, 1.0],
        tex: [0.0, 1.0],
    },
    QuadVertexData {
        vertices: [QUAD_RADIUS, -QUAD_RADIUS, 0.0],
        color: [0.0, 1.0, 0.0],
        tex: [1.0, 1.0],
    },
    QuadVertexData {
        vertices: [QUAD_RADIUS, QUAD_RADIUS, 0.0],
        color: [1.0, 0.0, 0.0],
        tex: [1.0, 0.0],
    },
    QuadVertexData {
        vertices: [-QUAD_RADIUS, QUAD_RADIUS, 0.0],
        color: [1.0, 0.0, 1.0],
        tex: [0.0, 0.0],
    },
];

const QUAD_INDICIES: [u16; 6] = [0, 1, 2, 3, 0, 2];

fn create_descriptor_sets(
    user_data: &mut UserData,
    device: &dyn Device,
) -> DeviceResult<[DescriptorSetHandle; 2]> {
    modus_profiler::scope!("create descriptor sets");
    let descriptor_bind_info_static = DescriptorSetUpdateInfo {
        buffers: &[DescriptorBufferInput {
            buffer: user_data.storage_buffer,
            buffer_offset: 0,
            buffer_size: None,
            binding_point: 0,
            dtype: DescriptorType::Storage,
        }],
        textures: &[DescriptorTextureInput {
            texture: user_data.texture,
            sampler: user_data.sampler,
            binding_point: 1,
        }],
    };
    let descriptor_bind_info_dynamic = DescriptorSetUpdateInfo {
        buffers: &[DescriptorBufferInput {
            buffer: user_data.transform_buffer,
            buffer_offset: 0,
            buffer_size: Some(size_of::<TransformMatrices>() as DeviceSize),
            binding_point: 0,
            dtype: DescriptorType::ConstantDynamic,
        }],
        textures: &[],
    };
    let descriptor_set_static = user_data.desciptor_allocator.allocate(
        device,
        descriptor_bind_info_static,
        user_data.descriptor_layout_static,
    )?;
    let descriptor_set_dynamic = user_data.desciptor_allocator.allocate(
        device,
        descriptor_bind_info_dynamic,
        user_data.descriptor_layout_dynamic,
    )?;
    Ok([descriptor_set_static, descriptor_set_dynamic])
}

impl Default for HelloQuadExample {
    fn default() -> Self {
        Self {
            swap_chain: Default::default(),
            render_graph: None,
            elapsed_time: Duration::new(0, 0),
            user_data: UserData {
                duration: Default::default(),
                pipeline: None,
                vert_shader: Default::default(),
                frag_shader: Default::default(),
                storage_buffer: Default::default(),
                index_buffer: Default::default(),
                transform_buffer: Default::default(),
                descriptor_layout_static: Default::default(),
                descriptor_layout_dynamic: Default::default(),
                texture: Default::default(),
                sampler: Default::default(),
                frame_counter: 0,
                desciptor_allocator: CachedDescriptorAllocator::new(DescriptorPoolCreateInfo {
                    constant_count: 512,
                    constant_dynamic_count: 512,
                    storage_count: 512,
                    storage_dynamic_count: 512,
                    input_attachment_count: 512,
                    texture_count: 512,
                    max_set_count: 512,
                }),
            },
        }
    }
}

impl HelloQuadExample {
    fn load_shaders(&mut self, device: &dyn Device) -> Result<(), DeviceError> {
        let vert_path = "shaders/hello_quad/vert.spv";
        let frag_path = "shaders/hello_quad/frag.spv";
        self.user_data.vert_shader = load_shader(vert_path, device)?;
        self.user_data.frag_shader = load_shader(frag_path, device)?;
        Ok(())
    }

    fn create_buffers(&mut self, device: &dyn Device) -> Result<(), DeviceError> {
        let quad_size = std::mem::size_of::<QuadVertexData>() * QUAD_DATA.len();
        self.user_data.storage_buffer = device.create_buffer(&BufferCreateInfo {
            btype: BufferType::Storage,
            size: quad_size as DeviceSize,
            memory_type: MemoryType::GpuOnly,
            data: Some(unsafe { from_raw_parts(QUAD_DATA.as_ptr() as *const u8, quad_size) }),
        })?;

        let indices_size = std::mem::size_of::<u16>() * QUAD_INDICIES.len();
        self.user_data.index_buffer = device.create_buffer(&BufferCreateInfo {
            btype: BufferType::Index,
            size: indices_size as DeviceSize,
            memory_type: MemoryType::GpuOnly,
            data: Some(unsafe {
                from_raw_parts(QUAD_INDICIES.as_ptr() as *const u8, indices_size)
            }),
        })?;

        self.user_data.transform_buffer = device.create_buffer(&BufferCreateInfo {
            btype: BufferType::Uniform,
            size: size_of::<[TransformMatrices; 3]>() as DeviceSize,
            memory_type: MemoryType::CpuToGpu,
            data: None,
        })?;
        Ok(())
    }

    fn create_descriptor_layout(&mut self, device: &dyn Device) -> Result<(), DeviceError> {
        let descriptors = [
            DescriptorDesc {
                binding_point: 0,
                descriptor_type: DescriptorType::Storage,
                stages: PIPELINE_STAGE_FLAG_VERTEX,
            },
            DescriptorDesc {
                binding_point: 1,
                descriptor_type: DescriptorType::Texture,
                stages: PIPELINE_STAGE_FLAG_FRAGMENT,
            },
        ];
        let descritpors_dynamic = [DescriptorDesc {
            binding_point: 0,
            descriptor_type: DescriptorType::ConstantDynamic,
            stages: PIPELINE_STAGE_FLAG_VERTEX,
        }];

        {
            let create_info = DescriptorSetLayoutCreateInfo {
                push_constants: &[],
                descriptors: &descriptors,
            };

            self.user_data.descriptor_layout_static =
                device.create_descriptor_set_layout(&create_info)?;
        }
        {
            let create_info = DescriptorSetLayoutCreateInfo {
                push_constants: &[],
                descriptors: &descritpors_dynamic,
            };

            self.user_data.descriptor_layout_dynamic =
                device.create_descriptor_set_layout(&create_info)?;
        }
        Ok(())
    }

    fn create_texture(&mut self, device: &dyn Device) -> Result<(), DeviceError> {
        const BITMAP_WIDTH: u32 = 128;
        const BITMAP_HEIGHT: u32 = 128;
        const BITMAP_SIZE: usize = (BITMAP_HEIGHT * BITMAP_WIDTH * 4) as usize;
        let bitmap_data = std::include_bytes!("bitmap.data");

        // convert to rgba, data is rgb
        let mut bitmap_data_rgba = [255u8; BITMAP_SIZE];
        let mut offset = 0_usize;
        for i in 0..(BITMAP_WIDTH * BITMAP_HEIGHT) as usize {
            let pixel_index = i * 3;
            bitmap_data_rgba[offset] = bitmap_data[pixel_index];
            bitmap_data_rgba[offset + 1] = bitmap_data[pixel_index + 1];
            bitmap_data_rgba[offset + 2] = bitmap_data[pixel_index + 2];
            offset += 4;
        }

        let texture_info = TextureCreateInfo {
            width: BITMAP_WIDTH,
            height: BITMAP_HEIGHT,
            depth: 1,
            mip_map_levels: 1,
            sample_count: 1,
            array_count: 1,
            ttype: TextureType::T2D,
            format: TextureFormat::R8G8B8A8,
            transient: false,
            usage_flags: TEXTURE_USAGE_FLAG_SHADER_INPUT,
        };

        let texture_data = [TextureData {
            width: BITMAP_WIDTH,
            height: BITMAP_HEIGHT,
            depth: 1,
            array_index: 0,
            mip_map_level: 0,
            data: &bitmap_data_rgba,
        }];

        self.user_data.texture = device.create_texture(&texture_info, &texture_data)?;
        Ok(())
    }

    fn create_sampler(&mut self, device: &dyn Device) -> DeviceResult<()> {
        let create_info = SamplerCreateInfo {
            mag_filter: Filter::Nearest,
            min_filter: Filter::Nearest,
            mipmap_mode: SamplerMipmapMode::Nearest,
            address_mode_u: SamplerAddressMode::ClampToEdge,
            address_mode_v: SamplerAddressMode::ClampToEdge,
            address_mode_w: SamplerAddressMode::ClampToEdge,
            anisotropy: false,
            max_anisotropy: 0,
        };

        self.user_data.sampler = device.create_sampler(&create_info)?;
        Ok(())
    }
}

#[derive(Debug)]
struct CubePass {}

impl modus_threed::render_graph::RenderPass<UserData> for CubePass {
    fn build(
        &self,
        user_data: &mut UserData,
        render_graph: &RenderGraph<UserData>,
        render_pass_state: &RenderPassBuildState,
        cmd_buffer: &mut dyn CommandBuffer,
    ) -> Result<(), DeviceError> {
        modus_profiler::scope!("Build pass");
        if user_data.pipeline.is_none() {
            modus_profiler::scope!("create pipeline");
            let descriptor_set_layouts = [
                user_data.descriptor_layout_static,
                user_data.descriptor_layout_dynamic,
            ];
            let pipeline_create_info = PipelineCreateInfo {
                descriptor_set_layouts: Some(&descriptor_set_layouts),
                depth_stencil_state: DepthStencilState {
                    depth: Some(DepthState {
                        write: true,
                        compare_op: CompareOp::LessOrEqual,
                        depth_bounds: Some((0.0, 1.0)),
                    }),
                    stencil: None,
                },
                blend_state: BlendState {
                    attachments: &[ColorAttachmentBlendState {
                        state: None,
                        color_mask: COLOR_MASK_ALL,
                    }],
                    logic_op: None,
                    blend_constants: [0.0; 4],
                },
                rasterization_state: RasterizationState {
                    depth_clamp: false,
                    rasterizer_discard: false,
                    polygon_mode: PolygonMode::Fill,
                    line_width: 1.0,
                    cull_mode: CullMode::Back,
                    front_face: FrontFace::CounterClockwise,
                    depth_bias: None,
                },
                input_assembly_state: InputAssemblyState {
                    primitive: Primitive::Triangles,
                    primitive_restart: false,
                },
                shader_states: &[
                    ShaderState {
                        shader: user_data.vert_shader,
                        stage: ShaderType::Vertex,
                        entry: "main",
                    },
                    ShaderState {
                        shader: user_data.frag_shader,
                        stage: ShaderType::Fragment,
                        entry: "main",
                    },
                ],
                multisample_state: MultisampleState {
                    sample_shading: false,
                    samples: 1,
                    min_sample_shading: 1.0,
                    alpha_to_coverage: false,
                    alpha_to_one: false,
                },
                render_pass: render_pass_state.render_pass,
            };

            let pipeline = render_graph.device.create_pipeline(&pipeline_create_info)?;
            user_data.pipeline = Some(pipeline);
        }

        let mut info = RenderPassClearInfo::default();
        let elapsed_seconds = user_data.duration.as_secs_f64();
        info.values[0] = ClearValue::Color([0.1, 0.1, 0.1, 1.0]);

        {
            modus_profiler::scope!("Update Transform Buffer");
            // update transform matrices
            let transform_data = TransformMatrices {
                view_proj: perspective_fov_rh_zo(
                    60_f32.to_radians(),
                    render_pass_state.dimensions[0] as f32,
                    render_pass_state.dimensions[1] as f32,
                    0.001,
                    1000.0,
                ) * look_at(
                    &Vec3::new(elapsed_seconds.sin() as f32, 0.0, 5.0),
                    &Vec3::new(0.0, 0.0, 0.0),
                    &Vec3::new(0.0, 1.0, 0.0),
                ),
                transform: translation(&Vec3::new(0.0, 0.0, 1.0 * elapsed_seconds.sin() as f32)),
            };

            render_graph.device.update_buffer(
                user_data.transform_buffer,
                (size_of::<TransformMatrices>() * user_data.frame_counter) as DeviceSize,
                unsafe { std::mem::transmute(&transform_data) },
                std::mem::size_of::<TransformMatrices>() as DeviceSize,
            )?;
        }

        {
            modus_profiler::scope!("Record Pass");
            {
                modus_profiler::scope!("begin render pass");
                cmd_buffer.begin_render_pass(
                    render_pass_state.render_pass,
                    render_pass_state.framebuffer,
                    Some(info),
                )?;
            }
            if let Some(p) = user_data.pipeline {
                {
                    modus_profiler::scope!("Bind Pipeline");
                    cmd_buffer.bind_pipeline(p)?;
                }
                cmd_buffer.set_scissor_rect(&Rect {
                    x: 0,
                    y: 0,
                    w: render_pass_state.dimensions[0],
                    h: render_pass_state.dimensions[1],
                })?;
                cmd_buffer.set_viewport(
                    &Viewport {
                        x: 0.0,
                        y: 0.0,
                        w: render_pass_state.dimensions[0] as f32,
                        h: render_pass_state.dimensions[1] as f32,
                    },
                    true,
                )?;

                let [descriptor_set_static, descriptor_set_dynamic] =
                    create_descriptor_sets(user_data, render_graph.device.as_ref())?;

                let descriptor_bind_info_static = DescriptorSetBindInfo {
                    handle: descriptor_set_static,
                    set_index: 0,
                    dynamic_offsets: &[],
                };

                let descriptor_bind_info_dynamic = DescriptorSetBindInfo {
                    handle: descriptor_set_dynamic,
                    set_index: 1,
                    dynamic_offsets: &[
                        (user_data.frame_counter * size_of::<TransformMatrices>()) as u32
                    ],
                };

                {
                    modus_profiler::scope!("Bind Descriptor");
                    cmd_buffer.bind_descriptors(
                        p,
                        &[descriptor_bind_info_static, descriptor_bind_info_dynamic],
                    )?;
                }

                {
                    modus_profiler::scope!("Bind Index");
                    cmd_buffer.bind_index_buffer(user_data.index_buffer, 0, IndexType::U16)?;
                }
                cmd_buffer.draw_indexed(6, 1, 0, 0, 0)?;
            }
            cmd_buffer.end_render_pass()?;
        }
        user_data.frame_counter = (user_data.frame_counter + 1) % 3;
        Ok(())
    }
}

impl example_lib::ThreedExample for HelloQuadExample {
    fn initialize(&mut self, runner: &mut ThreedExampleRunner) -> Result<(), ExampleError> {
        let window = match runner
            .app
            .window_system_mut()
            .resole_window_mut(runner.window_id)
        {
            Some(w) => w,
            None => {
                return Err(ExampleError::App(format!(
                    "Failed to locate window {}",
                    runner.window_id
                )))
            }
        };
        let swap_chain = window.create_swap_chain(runner.device.clone())?;
        log_info!("Created swap chain {}", swap_chain);
        self.swap_chain = swap_chain;
        let mut render_graph = RenderGraph::new(runner.device.clone());
        {
            let outputs = [RenderPassColorOutputDesc {
                name: COLOR_TEXTURE_NAME,
                size: SizeType::SwapChainRelative(0.9, 0.9, 0.9),
                format: TextureFormat::B8G8R8A8,
                samples: 1,
                transient: false,
                input: None,
                load_op: AttachmentLoadOp::Clear,
                store_op: AttachmentStoreOp::Store,
            }];

            let create_info = RenderPassCreateInfo {
                name: "color_pass",
                stage: PipelineStage::Graphics,
                color_inputs: &[],
                color_outputs: &outputs,
                size: SizeType::SwapChainRelative(0.9, 0.9, 0.9),
                build_pass: true,
            };

            render_graph.add_pass(&create_info, Box::new(CubePass {}))?;
        }

        self.render_graph = Some(render_graph);
        self.create_buffers(runner.device.as_ref())?;
        self.create_texture(runner.device.as_ref())?;
        self.create_sampler(runner.device.as_ref())?;
        self.load_shaders(runner.device.as_ref())?;
        self.create_descriptor_layout(runner.device.as_ref())?;
        Ok(())
    }

    fn shutdown(&mut self, runner: &mut ThreedExampleRunner) {
        self.user_data
            .desciptor_allocator
            .clear(runner.device.as_ref())
            .unwrap();
        if let Err(e) = runner.device.destroy_swap_chain(self.swap_chain) {
            log_error!("Failed to destroy swap chain: {}", e);
        }
    }

    fn tick(&mut self, runner: &mut ThreedExampleRunner) -> Result<bool, ExampleError> {
        let window = runner
            .app
            .window_system()
            .resole_window(runner.window_id)
            .unwrap();
        let graph = self.render_graph.as_mut().unwrap();
        self.elapsed_time += runner.frame_tick;
        self.user_data.duration = self.elapsed_time;
        self.user_data.desciptor_allocator.update();
        graph.execute(
            &mut self.user_data,
            COLOR_TEXTURE_NAME,
            self.swap_chain,
            window.render_surface_interface(),
        )?;
        Ok(true)
    }
}

impl modus_app::app::AppEventHandler for HelloQuadExample {
    fn on_app_event(&mut self, _event: &AppEvent) {}

    fn on_input_event(&mut self, _event: &InputEvent) {}
}

generate_main!(HelloQuadExample);
