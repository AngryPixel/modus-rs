use example_lib::{generate_main, ExampleError, ThreedExampleRunner, COLOR_TEXTURE_NAME};
use modus_app::events::{AppEvent, InputEvent};
use modus_log::{log_error, log_info};
use modus_threed::command_buffer::CommandBuffer;
use modus_threed::device::DeviceError;
use modus_threed::render_graph::{
    RenderGraph, RenderPassBuildState, RenderPassColorOutputDesc, RenderPassCreateInfo, SizeType,
};
use modus_threed::render_pass::{
    AttachmentLoadOp, AttachmentStoreOp, ClearValue, PipelineStage, RenderPassClearInfo,
};
use modus_threed::types::{SwapChainHandle, TextureFormat};
use std::time::Duration;

type UserData = Duration;
#[derive(Debug)]
struct HelloSwapchainExample {
    elapsed_time: Duration,
    swap_chain: SwapChainHandle,
    render_graph: Option<RenderGraph<UserData>>,
}

impl Default for HelloSwapchainExample {
    fn default() -> Self {
        Self {
            swap_chain: Default::default(),
            render_graph: None,
            elapsed_time: Duration::new(0, 0),
        }
    }
}

#[derive(Debug)]
struct ColorPass {}

impl modus_threed::render_graph::RenderPass<UserData> for ColorPass {
    fn build(
        &self,
        user_data: &mut UserData,
        _render_graph: &RenderGraph<UserData>,
        render_pass_data: &RenderPassBuildState,
        cmd_buffer: &mut dyn CommandBuffer,
    ) -> Result<(), DeviceError> {
        let mut info = RenderPassClearInfo::default();
        let elapsed_seconds = user_data.as_secs_f64();
        info.values[0] = ClearValue::Color([
            elapsed_seconds.sin() as f32,
            elapsed_seconds.cos() as f32,
            elapsed_seconds.sin() as f32,
            1.0,
        ]);

        cmd_buffer.begin_render_pass(
            render_pass_data.render_pass,
            render_pass_data.framebuffer,
            Some(info),
        )?;
        cmd_buffer.end_render_pass()?;
        Ok(())
    }
}

impl example_lib::ThreedExample for HelloSwapchainExample {
    fn initialize(&mut self, runner: &mut ThreedExampleRunner) -> Result<(), ExampleError> {
        let window = match runner
            .app
            .window_system_mut()
            .resole_window_mut(runner.window_id)
        {
            Some(w) => w,
            None => {
                return Err(ExampleError::App(format!(
                    "Failed to locate window {}",
                    runner.window_id
                )))
            }
        };
        let swap_chain = window.create_swap_chain(runner.device.clone())?;
        log_info!("Created swap chain {}", swap_chain);
        self.swap_chain = swap_chain;
        let mut render_graph = RenderGraph::new(runner.device.clone());
        {
            let outputs = [RenderPassColorOutputDesc {
                name: COLOR_TEXTURE_NAME,
                size: SizeType::SwapChainRelative(1.0, 1.0, 1.0),
                format: TextureFormat::R8G8B8A8,
                samples: 1,
                transient: false,
                input: None,
                load_op: AttachmentLoadOp::Clear,
                store_op: AttachmentStoreOp::Store,
            }];

            let create_info = RenderPassCreateInfo {
                name: "color_pass",
                stage: PipelineStage::Graphics,
                color_inputs: &[],
                color_outputs: &outputs,
                size: SizeType::SwapChainRelative(1.0, 1.0, 1.0),
                build_pass: true,
            };

            render_graph.add_pass(&create_info, Box::new(ColorPass {}))?;
        }
        self.render_graph = Some(render_graph);
        Ok(())
    }

    fn shutdown(&mut self, runner: &mut ThreedExampleRunner) {
        if let Err(e) = runner.device.destroy_swap_chain(self.swap_chain) {
            log_error!("Failed to destroy swap chain: {}", e);
        }
    }

    fn tick(&mut self, runner: &mut ThreedExampleRunner) -> Result<bool, ExampleError> {
        let window = runner
            .app
            .window_system()
            .resole_window(runner.window_id)
            .unwrap();
        let graph = self.render_graph.as_mut().unwrap();
        self.elapsed_time += runner.frame_tick;
        graph.execute(
            &mut self.elapsed_time,
            COLOR_TEXTURE_NAME,
            self.swap_chain,
            window.render_surface_interface(),
        )?;
        Ok(true)
    }
}

impl modus_app::app::AppEventHandler for HelloSwapchainExample {
    fn on_app_event(&mut self, _event: &AppEvent) {}

    fn on_input_event(&mut self, _event: &InputEvent) {}
}

generate_main!(HelloSwapchainExample);
