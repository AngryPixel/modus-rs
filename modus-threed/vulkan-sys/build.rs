#[cfg(feature = "use-bindgen")]
fn generate_vk_bindings() {
    use bindgen::EnumVariation;
    use std::env;
    use std::path::PathBuf;

    let bindings = bindgen::Builder::default()
        .header("vulkan-headers/vulkan.h")
        .header("vma-2.3.0/src/vk_mem_alloc.h")
        .parse_callbacks(Box::new(bindgen::CargoCallbacks))
        .derive_debug(true)
        .impl_debug(true)
        .default_enum_style(EnumVariation::Rust {
            non_exhaustive: false,
        })
        .generate()
        .expect("Unable to generate vulkan bindings");

    let out_path = PathBuf::from(env::var("OUT_DIR").unwrap());
    bindings
        .write_to_file(out_path.join("vulkan_bindings.rs"))
        .expect("Failed to write bindings to file");
}

fn build_vma_source() {
    use cc::*;
    Build::new()
        .file("vma_src.cpp")
        .cpp(true)
        .warnings(false)
        .include("vulkan-headers")
        .compile("vulkan_sys_vma");
}

fn main() {
    println!("cargo:rustc-link-lib=vulkan");

    #[cfg(feature = "use-bindgen")]
    generate_vk_bindings();

    build_vma_source();
}
