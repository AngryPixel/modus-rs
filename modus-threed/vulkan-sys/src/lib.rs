#![allow(non_upper_case_globals)]
#![allow(non_camel_case_types)]
#![allow(non_snake_case)]
#![allow(deref_nullptr)]
#![allow(clippy::all)]

#[cfg(all(
    not(feature = "use-bindgen"),
    target_os = "linux",
    target_pointer_width = "64"
))]
pub mod vulkan_sys_bindings_linux_x86_64;
#[cfg(all(
    not(feature = "use-bindgen"),
    target_os = "linux",
    target_pointer_width = "64"
))]
pub use vulkan_sys_bindings_linux_x86_64::*;

//#[cfg(feature = "use-bindgen")]
include!(concat!(env!("OUT_DIR"), "/vulkan_bindings.rs"));

#[cfg(test)]
mod tests {
    use super::*;
    use std::ptr;

    #[test]
    fn test_vk_link() {
        let mut ex_count = 0;
        unsafe {
            assert_eq!(
                vkEnumerateInstanceExtensionProperties(ptr::null(), &mut ex_count, ptr::null_mut()),
                VkResult::VK_SUCCESS
            );
        }
    }
}
