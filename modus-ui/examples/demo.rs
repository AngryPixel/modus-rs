use modus_app::app::{App, AppEventHandler, AppInitInfo, TickResult};
use modus_app::create_app;
use modus_app::events::{
    AppEvent, ButtonState, InputEvent, Key, MouseButton, WindowEvent, WindowId,
};
use modus_app::window::{FullscreenMode, WindowCreateInfo};
use modus_core::fps_counter::FpsCounter;
use modus_core::timer::Timer;
use modus_log::init_log_system;
use modus_log::prelude::*;
use modus_threed::device::{Device, DeviceError, DeviceResult};
use modus_threed::framebuffer::FramebufferCreateInfo;
use modus_threed::render_graph::RenderGraphError;
use modus_threed::texture::{TextureCreateInfo, TEXTURE_USAGE_FLAG_ATTACHMENT};
use modus_threed::types::{
    FramebufferHandle, RenderPassHandle, SwapChainHandle, TextureFormat, TextureHandle, TextureType,
};
use modus_ui::context::Context;
use modus_ui::input::MouseButton as UIMouseButton;
use modus_ui::layout::LayoutStrategy;
use modus_ui::painter::Painter;
use modus_ui::shape_builder::VertexShape;
use modus_ui::text::layout::TextLayoutCalculator;
use modus_ui::view::View;
use modus_ui::views::{ScrollView, ScrollViewScope};
use modus_ui::widgets::{Button, Slider};
use modus_ui::widgets::{Label, TextBox};
use modus_ui::{Alignment, Color, Rect, Vec2};
use std::fmt::Formatter;
use std::sync::Arc;
use std::time::Duration;

#[derive(Debug)]
pub enum DemoError {
    App(String),
    Device(DeviceError),
    RenderGraph(RenderGraphError),
    Misc(String),
}

impl From<DeviceError> for DemoError {
    fn from(de: DeviceError) -> Self {
        DemoError::Device(de)
    }
}

impl From<RenderGraphError> for DemoError {
    fn from(re: RenderGraphError) -> Self {
        DemoError::RenderGraph(re)
    }
}

impl std::fmt::Display for DemoError {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        match self {
            DemoError::App(s) => write!(f, "App init error: {}", s),
            DemoError::Device(de) => write!(f, "DeviceError: {}", de),
            DemoError::Misc(s) => write!(f, "Misc: {}", s),
            DemoError::RenderGraph(g) => write!(f, "RenderGraph: {}", g),
        }
    }
}
pub struct UiDemo {
    pub device: Arc<dyn Device>,
    pub app: Box<dyn App>,
    pub window_id: WindowId,
    pub timer: Timer,
    pub frame_tick: Duration,
    pub fps_counter: FpsCounter,
    pub swap_chain: SwapChainHandle,
}

impl UiDemo {
    pub fn new() -> Result<Self, DemoError> {
        modus_profiler::register_thread!("Main");
        modus_profiler::scope!();
        if let Err(e) = init_log_system() {
            return Err(DemoError::Misc(format!("Failed to init log system: {}", e)));
        }

        let perf_name = format!("modus.ui.demo");
        let app_init_info = AppInitInfo {
            name: "UI Demo",
            name_preferences: &perf_name,
        };

        let mut args = std::env::args();

        let use_debug = !args.any(|x| x == "--disable-debug");
        let use_validation_layers = !args.any(|x| x == "--disable-validation");

        let instance_create_info = modus_threed::instance::InstanceCreateInfo {
            name: "UI Demo",
            version_major: 0,
            version_minor: 0,
            version_patch: 0,
            debug: use_debug,
            api_validation: use_validation_layers,
        };

        let mut app = match create_app(&app_init_info, &instance_create_info) {
            Err(e) => {
                return Err(DemoError::App(format!("Failed to init app: {}", e)));
            }
            Ok(a) => a,
        };

        let window_info = WindowCreateInfo {
            title: "",
            width: None,
            height: None,
            x: None,
            y: None,
            fullscreen: FullscreenMode::None,
            hidden: false,
        };

        let window_id = match app.window_system_mut().create_window(&window_info) {
            Ok(id) => id,
            Err(e) => return Err(DemoError::App(e)),
        };

        let surface = app
            .window_system()
            .resole_window(window_id)
            .unwrap()
            .render_surface_interface();
        let device = app.threed_instance().clone().create_device(surface)?;

        let window = match app.window_system_mut().resole_window_mut(window_id) {
            Some(w) => w,
            None => {
                return Err(DemoError::App(format!(
                    "Failed to locate window {}",
                    window_id
                )))
            }
        };
        let swap_chain = window.create_swap_chain(device.clone())?;

        Ok(Self {
            app,
            device,
            window_id,
            timer: Timer::new(),
            frame_tick: Duration::new(0, 0),
            fps_counter: FpsCounter::new(),
            swap_chain,
        })
    }

    #[allow(clippy::result_unit_err)]
    pub fn run(&mut self) -> Result<(), ()> {
        log_debug!("Initializing example");
        let mut demo = match DemoState::new(self) {
            Err(e) => {
                log_error!("Failed to init example: {}", e);
                return Err(());
            }
            Ok(d) => d,
        };

        log_debug!("Starting main loop");
        let mut result = Ok(());
        // set the previous tick to nw to avoid high delta on first run
        self.timer.reset_and_elapsed();
        loop {
            modus_profiler::scope!("Tick");
            {
                modus_profiler::scope!("Example FPS + TITLE");
                self.frame_tick = self.timer.reset_and_elapsed();
                self.fps_counter.update(self.frame_tick);

                if let Some(window) = self
                    .app
                    .window_system_mut()
                    .resole_window_mut(self.window_id)
                {
                    window.set_title(&format!(
                        "{} - FPS:{:0.2} Frame:{:0.2} ms",
                        "UI Demo",
                        self.fps_counter.fps(),
                        self.fps_counter.frame_time_ms(),
                    ));
                }
            }

            {
                // UI context needs to begin frame before we consume input so that all
                // the counters are correct.
                demo.context.begin();
            }
            {
                modus_profiler::scope!("Example App Tick");
                let status = self.app.tick(&mut demo);
                if status == TickResult::Quit {
                    break;
                }
            }
            {
                modus_profiler::scope!("Example Tick");
                match demo.tick(self) {
                    Ok(continue_tick) => {
                        if !continue_tick {
                            break;
                        }
                    }
                    Err(e) => {
                        log_debug!("Tick error: {}", e);
                        result = Err(());
                        break;
                    }
                }
            }
            modus_profiler::finish_frame!();
        }
        log_debug!("Terminating example");
        demo.shutdown(self);
        self.device.destroy_swap_chain(self.swap_chain).unwrap();
        log_debug!("Finished");
        result
    }
}

fn main() -> Result<(), ()> {
    let mut runner = match UiDemo::new() {
        Ok(r) => r,
        Err(e) => {
            eprintln!(
                "Failed to init test runner for {}: {}",
                stringify!($example_name),
                e
            );
            return Err(());
        }
    };
    runner.run()
}

struct DemoState {
    context: Context,
    texture: TextureHandle,
    framebuffer: FramebufferHandle,
    elapsed_time: Duration,
    slider_value: f32,
    text_input: String,
}

impl DemoState {
    fn new(demo: &UiDemo) -> Result<Self, DemoError> {
        let painter = Painter::new(demo.device.clone(), true)?;
        let mut r = Self {
            context: Context::new(painter)?,
            texture: TextureHandle::new(),
            framebuffer: FramebufferHandle::new(),
            elapsed_time: Duration::new(0, 0),
            slider_value: 0.0,
            text_input: String::from("Hello!"),
        };
        r.context.set_canvas_size(Vec2 {
            x: 1280.0,
            y: 720.0,
        });
        r.create_framebuffer(
            demo.device.as_ref(),
            1280,
            720,
            r.context.painter().render_pass(),
        )?;

        let font_data = include_bytes!("../fonts/hack.regular.ttf").to_vec();
        r.context
            .font_map_mut()
            .add_font("hack", font_data, &[12, 16, 24, 36])
            .unwrap();
        Ok(r)
    }

    fn create_framebuffer(
        &mut self,
        device: &dyn Device,
        width: u32,
        height: u32,
        render_pass: RenderPassHandle,
    ) -> DeviceResult<()> {
        device.destroy_framebuffer(self.framebuffer).unwrap();
        device.destroy_texture(self.texture).unwrap();

        // create texture
        {
            let create_info = TextureCreateInfo {
                width,
                height,
                depth: 1,
                mip_map_levels: 1,
                sample_count: 1,
                array_count: 1,
                ttype: TextureType::T2D,
                format: TextureFormat::B8G8R8A8,
                transient: false,
                usage_flags: TEXTURE_USAGE_FLAG_ATTACHMENT,
            };
            self.texture = device.create_texture(&create_info, &[])?;
        }

        // create framebuffer
        {
            let textures = [self.texture];
            let create_info = FramebufferCreateInfo {
                width,
                height,
                depth: 1,
                render_pass,
                textures: &textures,
            };
            self.framebuffer = device.create_framebuffer(&create_info)?;
        }
        Ok(())
    }

    fn do_ui(&mut self) {
        let font = self.context.font_map().resolve_font("hack", 16).unwrap();
        self.context.set_default_font(font.clone());

        {
            /*
            let mut view = View::new(
                Rect {
                    x: 0.0,
                    y: 0.0,
                    width: 500.0,
                    height: 500.0,
                },
                &mut self.context,
            );*/

            {
                modus_profiler::scope!("UI Scroll");
                let rect = Rect::new_at(20.0, 20.0, 500.0, 500.0);
                let mut view = ScrollViewScope::new(
                    ScrollView::vertical()
                        .offset(Vec2::new(0.0, -600.0))
                        .scroll_offset(200.0),
                    rect,
                    &mut self.context,
                );
                view.v_rows(20, 50.0, |index, view| {
                    let text = format!("Button {:04}", index);
                    if view.add(Button::new(&text)) {
                        log_info!("{}  clicked", text);
                    }
                });
            }
        }

        {
            let mut view = View::new(
                "ABC",
                Rect {
                    x: 800.0,
                    y: 200.0,
                    width: 300.0,
                    height: 300.0,
                },
                &mut self.context,
            );
            //TODO: Try to see if we can collect these and execute them later without allocating
            // memory
            view.set_layout_strategy(LayoutStrategy::horizontal_right_to_left());
            view.add(Label::new(&"Label Check").color(Color::green()));
            view.fill(|v| {
                v.add(
                    Label::new(&"<<--")
                        .alignment(Alignment::Center)
                        .color(Color::red()),
                );
            });
        }

        {
            let mut view = View::new(
                "DEF",
                Rect {
                    x: 1000.0,
                    y: self.context.canvas_size().y - 300.0,
                    width: 200.0,
                    height: 300.0,
                },
                &mut self.context,
            );
            let vrect = view.rect();
            view.context_mut()
                .shape_builder_mut()
                .stroke_rect(&vrect, 2.0, Color::white());
            view.set_layout_strategy(LayoutStrategy::vertical_bottom_to_top());
            view.add(Label::new(&"bottom layer").color(Color::green()));
            view.fill_alt(|v| {
                v.add(
                    Label::new(&"top layer")
                        .alignment(Alignment::Right)
                        .color(Color::red()),
                );
                if v.add(Slider::new(0.0..=1024.0, &mut self.slider_value).step(0.5)) {
                    log_info!("Slider Value Changed: {}", self.slider_value);
                }
                v.add(TextBox::new(&mut self.text_input));
            });
        }
        // test draw triangle
        {
            let center_x = self.context.canvas_size().x / 2.0;
            let center_y = self.context.canvas_size().y / 2.0;
            let vertices = [
                VertexShape {
                    v: [center_x, center_y],
                    color: Color::red(),
                },
                VertexShape {
                    v: [center_x - 100.0, center_y + 100.0],
                    color: Color::green(),
                },
                VertexShape {
                    v: [center_x + 100.0, center_y + 100.0],
                    color: Color::blue(),
                },
            ];
            let indices = [0u32, 1u32, 2u32];
            self.context
                .shape_builder_mut()
                .add_vertices(&vertices, &indices);
        }
        // test draw text
        {
            let mut layout_processor = TextLayoutCalculator::new();
            layout_processor.multi_line = true;
            self.context.shape_builder_mut().add_text(
                &font,
                Vec2 { x: 400.0, y: -10.0 },
                "Hello World!\n\tFrom me\n!!",
                &layout_processor,
                Color::white(),
            );
        }
        self.context.end();
    }

    fn tick(&mut self, demo: &UiDemo) -> Result<bool, DemoError> {
        let window = demo
            .app
            .window_system()
            .resole_window(demo.window_id)
            .unwrap();
        self.elapsed_time += demo.frame_tick;
        self.do_ui();
        demo.device.begin_frame()?;
        demo.device
            .acquire_swap_chain_image(demo.swap_chain, window.render_surface_interface())?;
        self.context.update_painter_buffers()?;
        let canvas_size = self.context.canvas_size();
        let bg_color = self.context.style().background_color;
        self.context
            .painter_mut()
            .draw(canvas_size, self.framebuffer, bg_color)?;
        demo.device.end_frame(self.texture, demo.swap_chain)?;
        demo.device.present_swap_chain(demo.swap_chain)?;
        Ok(true)
    }

    fn shutdown(&mut self, demo: &UiDemo) {
        demo.device.destroy_framebuffer(self.framebuffer).unwrap();
        demo.device.destroy_texture(self.texture).unwrap();
    }
}

impl AppEventHandler for DemoState {
    fn on_app_event(&mut self, event: &AppEvent) {
        match event {
            AppEvent::Window(wr) => match wr {
                WindowEvent::Resized(wr) => {
                    self.context.set_canvas_size(Vec2 {
                        x: wr.width as f32,
                        y: wr.height as f32,
                    });
                    let device = self.context.painter().threed_device();
                    self.create_framebuffer(
                        device.as_ref(),
                        wr.width,
                        wr.height,
                        self.context.painter().render_pass(),
                    )
                    .expect("Failed to rebuild framebuffer");
                }
                WindowEvent::FocusLost(_) => {
                    self.context.on_focus_lost();
                }
                _ => {}
            },
        }
    }

    fn on_input_event(&mut self, event: &InputEvent) {
        match event {
            InputEvent::MouseButton(mb) => {
                let button = match mb.button {
                    MouseButton::Left => UIMouseButton::Left,
                    MouseButton::Middle => UIMouseButton::Middle,
                    MouseButton::Right => UIMouseButton::Right,
                    MouseButton::Extra0 => UIMouseButton::Extra1,
                    MouseButton::Extra1 => UIMouseButton::Extra2,
                    _ => {
                        panic!("Should not be reached");
                    }
                };
                self.context
                    .input_state_mut()
                    .set_mouse_button_down(button, mb.state == ButtonState::Down);
            }
            InputEvent::MouseMove(mm) => {
                self.context.input_state_mut().current.mouse_position = Vec2 {
                    x: mm.x as f32,
                    y: mm.y as f32,
                };
            }
            InputEvent::MouseWheel(mw) => {
                self.context.input_state_mut().current.mouse_scroll = Vec2 {
                    x: mw.x as f32,
                    y: mw.y as f32,
                }
            }
            InputEvent::TextInput(ti) => {
                self.context.input_state_mut().add_char(ti.ch);
            }
            InputEvent::Keyboard(kb) => {
                let key = match kb.key {
                    Key::KeyA => modus_ui::input::Key::A,
                    Key::KeyB => modus_ui::input::Key::B,
                    Key::KeyC => modus_ui::input::Key::C,
                    Key::KeyD => modus_ui::input::Key::D,
                    Key::KeyE => modus_ui::input::Key::E,
                    Key::KeyF => modus_ui::input::Key::F,
                    Key::KeyG => modus_ui::input::Key::G,
                    Key::KeyH => modus_ui::input::Key::H,
                    Key::KeyI => modus_ui::input::Key::I,
                    Key::KeyJ => modus_ui::input::Key::J,
                    Key::KeyK => modus_ui::input::Key::K,
                    Key::KeyL => modus_ui::input::Key::L,
                    Key::KeyM => modus_ui::input::Key::M,
                    Key::KeyN => modus_ui::input::Key::N,
                    Key::KeyO => modus_ui::input::Key::O,
                    Key::KeyP => modus_ui::input::Key::P,
                    Key::KeyQ => modus_ui::input::Key::Q,
                    Key::KeyR => modus_ui::input::Key::R,
                    Key::KeyS => modus_ui::input::Key::S,
                    Key::KeyT => modus_ui::input::Key::T,
                    Key::KeyU => modus_ui::input::Key::U,
                    Key::KeyV => modus_ui::input::Key::V,
                    Key::KeyW => modus_ui::input::Key::W,
                    Key::KeyX => modus_ui::input::Key::X,
                    Key::KeyY => modus_ui::input::Key::Y,
                    Key::KeyZ => modus_ui::input::Key::Z,
                    Key::Key0 => modus_ui::input::Key::Num0,
                    Key::Key1 => modus_ui::input::Key::Num1,
                    Key::Key2 => modus_ui::input::Key::Num2,
                    Key::Key3 => modus_ui::input::Key::Num3,
                    Key::Key4 => modus_ui::input::Key::Num4,
                    Key::Key5 => modus_ui::input::Key::Num5,
                    Key::Key6 => modus_ui::input::Key::Num6,
                    Key::Key7 => modus_ui::input::Key::Num7,
                    Key::Key8 => modus_ui::input::Key::Num8,
                    Key::Key9 => modus_ui::input::Key::Num9,
                    Key::KeyUp => modus_ui::input::Key::ArrowUp,
                    Key::KeyDown => modus_ui::input::Key::ArrowDown,
                    Key::KeyRight => modus_ui::input::Key::ArrowRight,
                    Key::KeyLeft => modus_ui::input::Key::ArrowLeft,
                    Key::KeySpace => modus_ui::input::Key::Space,
                    Key::KeyLeftShift => modus_ui::input::Key::Shift,
                    Key::KeyRightShift => modus_ui::input::Key::Shift,
                    Key::KeyLeftCtrl => modus_ui::input::Key::Ctrl,
                    Key::KeyRightCtrl => modus_ui::input::Key::Ctrl,
                    Key::KeyLeftAlt => modus_ui::input::Key::Alt,
                    Key::KeyRightAlt => modus_ui::input::Key::Alt,
                    Key::KeyTab => modus_ui::input::Key::Tab,
                    Key::KeyBackSpace => modus_ui::input::Key::Backspace,
                    Key::KeyEnter => modus_ui::input::Key::Enter,
                    Key::KeyEscape => modus_ui::input::Key::Escape,
                    Key::KeyHome => modus_ui::input::Key::Home,
                    Key::KeyDelete => modus_ui::input::Key::Delete,
                    Key::KeyInsert => modus_ui::input::Key::Insert,
                    Key::KeyEnd => modus_ui::input::Key::Enter,
                    Key::KeyPgUp => modus_ui::input::Key::PageUp,
                    Key::KeyPgDown => modus_ui::input::Key::PageDown,
                    Key::KeyKPEnter => modus_ui::input::Key::Enter,
                    Key::KeyKP0 => modus_ui::input::Key::Num0,
                    Key::KeyKP1 => modus_ui::input::Key::Num0,
                    Key::KeyKP2 => modus_ui::input::Key::Num0,
                    Key::KeyKP3 => modus_ui::input::Key::Num0,
                    Key::KeyKP4 => modus_ui::input::Key::Num0,
                    Key::KeyKP5 => modus_ui::input::Key::Num0,
                    Key::KeyKP6 => modus_ui::input::Key::Num0,
                    Key::KeyKP7 => modus_ui::input::Key::Num0,
                    Key::KeyKP8 => modus_ui::input::Key::Num0,
                    Key::KeyKP9 => modus_ui::input::Key::Num0,
                    _ => {
                        return;
                    }
                };
                self.context
                    .input_state_mut()
                    .set_key_down(key, kb.state == ButtonState::Down);
            }
            _ => {}
        }
    }
}
