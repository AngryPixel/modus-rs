use std::path::Path;

fn compile_shaders() {
    let shaders = [
        "shaders/non-bindless/shader.vert",
        "shaders/non-bindless/shader.frag",
    ];
    for entry in &shaders {
        let p = Path::new(*entry);
        modus_threed::build_utils::compile_shader(p);
    }
}

fn main() {
    compile_shaders();
    println!("cargo:rerun-if-changed=build.rs");
}
