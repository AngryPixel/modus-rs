use crate::context::Context;
use crate::id::{Id, IdStateMap};
use crate::layout::{LayoutAllocator, LayoutStrategy};
use crate::widgets::Widget;
use crate::{Rect, Vec2};

#[derive(Debug)]
pub struct View<'a> {
    id: Id,
    context: &'a mut Context,
    rect: Rect,
    layout_allocator: LayoutAllocator,
    layout_strategy: LayoutStrategy,
    id_counter: u64,
}

impl<'a> View<'a> {
    pub fn new(name: &str, area: Rect, context: &'a mut Context) -> Self {
        let id = Id::with_parent(context.parent_id(), name);
        context.push_id(id);
        context.shape_builder_mut().push_clip_rect(area);
        Self {
            id,
            layout_allocator: LayoutAllocator::new(area, context.style().element_padding),
            context,
            rect: area,
            layout_strategy: Default::default(),
            id_counter: 0,
        }
    }

    #[inline(always)]
    pub fn layout_allocator(&self) -> &LayoutAllocator {
        &self.layout_allocator
    }

    #[inline(always)]
    pub fn layout_allocator_mut(&mut self) -> &mut LayoutAllocator {
        &mut self.layout_allocator
    }

    #[inline(always)]
    pub fn add<R>(&mut self, widget: impl Widget<R>) -> R {
        widget.build(self)
    }

    #[inline(always)]
    pub fn context(&self) -> &Context {
        self.context
    }

    #[inline(always)]
    pub fn context_mut(&mut self) -> &mut Context {
        self.context
    }

    #[inline(always)]
    pub fn set_layout_strategy(&mut self, layout: LayoutStrategy) {
        self.layout_strategy = layout;
    }

    #[inline(always)]
    pub fn layout_strategy(&self) -> LayoutStrategy {
        self.layout_strategy
    }

    #[inline(always)]
    pub fn rect(&self) -> Rect {
        self.rect
    }

    #[inline(always)]
    pub fn id_state(&self) -> &IdStateMap {
        self.context.id_state()
    }

    #[inline(always)]
    pub fn id_state_mut(&mut self) -> &mut IdStateMap {
        self.context.id_state_mut()
    }

    pub fn allocate_rect(&mut self, dimensions: Vec2) -> Rect {
        self.layout_allocator
            .allocate(&self.layout_strategy, dimensions)
    }

    pub fn with_min_size(&mut self, size: Vec2, f: impl FnOnce(&mut View)) {
        let current_min_size = self.layout_allocator.min_size();
        self.layout_allocator.set_min_size(size);
        f(self);
        self.layout_allocator.set_min_size(current_min_size);
    }

    pub fn with_layout_strategy(&mut self, layout: LayoutStrategy, f: impl FnOnce(&mut View)) {
        let current_strategy = self.layout_strategy;
        self.layout_strategy = layout;
        f(self);
        self.layout_strategy = current_strategy;
    }

    pub fn fill(&mut self, f: impl FnOnce(&mut View)) {
        let curr_fill = self.layout_strategy.fill_main;
        self.layout_strategy.fill_main = true;
        f(self);
        self.layout_strategy.fill_main = curr_fill;
    }

    pub fn fill_alt(&mut self, f: impl FnOnce(&mut View)) {
        let curr_fill = self.layout_strategy.fill_alt;
        self.layout_strategy.fill_alt = true;
        f(self);
        self.layout_strategy.fill_alt = curr_fill;
    }

    pub fn fill_all(&mut self, f: impl FnOnce(&mut View)) {
        let curr_fill_main = self.layout_strategy.fill_main;
        let curr_fill_alt = self.layout_strategy.fill_alt;
        self.layout_strategy.fill_alt = true;
        self.layout_strategy.fill_main = true;
        f(self);
        self.layout_strategy.fill_alt = curr_fill_alt;
        self.layout_strategy.fill_main = curr_fill_main;
    }

    pub fn allocate_id(&mut self) -> Id {
        self.id_counter = self.id_counter.wrapping_add(1);
        Id::with_parent(self.context.parent_id(), &self.id_counter)
    }

    pub fn advance_id_allocation(&mut self, count: u64) {
        self.id_counter = self.id_counter.wrapping_add(count);
    }
}

impl<'a> Drop for View<'a> {
    fn drop(&mut self) {
        self.context.pop_id(self.id);
        self.context.shape_builder_mut().pop_clip_rect()
    }
}
