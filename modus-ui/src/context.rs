use crate::id::{Id, IdScratchBuffer, IdStateMap};
use crate::input::{InputState, MouseButton};
use crate::painter::Painter;
use crate::shape_builder::ShapeBuilder;
use crate::style::Style;
use crate::text::font::{FontMap, ScaledFontInstance};
use crate::texture_atlas::TextureAtlas;
use crate::{Rect, Vec2};
use modus_threed::device::DeviceResult;
use std::rc::Rc;

#[derive(Debug, Copy, Clone, Hash, Eq, PartialEq)]
pub enum MouseState {
    Hover,
    Down,
    Up,
    Noop,
}

#[derive(Debug)]
pub struct Context {
    pub(crate) active_item: Id,
    pub(crate) hot_item: Id,
    pub(crate) prev_hot_item: Id,
    pub(crate) focused_item: Id,
    prev_focused_item: Id,
    input: InputState,
    shape_builder: ShapeBuilder,
    canvas_size: Vec2,
    painter: Painter,
    fonts: FontMap,
    default_atlas: Rc<TextureAtlas>,
    default_font: Option<ScaledFontInstance>,
    style: Rc<Style>,
    id_stack: Vec<Id>,
    id_state: IdStateMap,
    id_scratch_buffer: IdScratchBuffer,
}

impl Context {
    pub fn new(painter: Painter) -> DeviceResult<Self> {
        let atlas = TextureAtlas::new_gray_scale(&painter, 1024, 1024, 8)?;
        atlas.allocate(2, 2);
        atlas.write_at_gray_scale(0, 0, 0, 255);
        atlas.write_at_gray_scale(1, 0, 0, 255);
        atlas.write_at_gray_scale(0, 1, 0, 255);
        atlas.write_at_gray_scale(1, 1, 0, 255);
        let rc_atlas = Rc::new(atlas);
        Ok(Self {
            active_item: Id::invalid(),
            hot_item: Id::invalid(),
            prev_hot_item: Id::invalid(),
            focused_item: Id::invalid(),
            input: InputState::new(),
            shape_builder: ShapeBuilder::new(rc_atlas.painter_texture()),
            canvas_size: Vec2 { x: 0.0, y: 0.0 },
            painter,
            fonts: FontMap::new(rc_atlas.clone()),
            default_atlas: rc_atlas,
            default_font: None,
            style: Rc::new(Style::new()),
            id_stack: Vec::with_capacity(8),
            id_state: IdStateMap::new(),
            id_scratch_buffer: IdScratchBuffer::new(),
            prev_focused_item: Id::invalid(),
        })
    }

    pub fn begin(&mut self) {
        self.input.begin_frame();
        self.shape_builder.begin_frame(self.canvas_size);
    }

    pub fn end(&mut self) {
        self.prev_hot_item = self.hot_item;
        self.prev_focused_item = self.focused_item;
        self.input.end_frame();
    }

    pub fn update_painter_buffers(&mut self) -> DeviceResult<()> {
        self.default_atlas.upload_to_painter()?;
        self.painter
            .update_buffers(&self.shape_builder, self.canvas_size)
    }

    #[inline(always)]
    pub fn input_state(&self) -> &InputState {
        &self.input
    }

    #[inline(always)]
    pub fn input_state_mut(&mut self) -> &mut InputState {
        &mut self.input
    }

    #[inline(always)]
    pub fn shape_builder(&self) -> &ShapeBuilder {
        &self.shape_builder
    }

    #[inline(always)]
    pub fn shape_builder_mut(&mut self) -> &mut ShapeBuilder {
        &mut self.shape_builder
    }

    #[inline(always)]
    pub fn canvas_size(&self) -> Vec2 {
        self.canvas_size
    }

    #[inline(always)]
    pub fn set_canvas_size(&mut self, size: Vec2) {
        self.canvas_size = size;
    }

    #[inline(always)]
    pub fn font_map(&self) -> &FontMap {
        &self.fonts
    }

    #[inline(always)]
    pub fn font_map_mut(&mut self) -> &mut FontMap {
        &mut self.fonts
    }

    #[inline(always)]
    pub fn default_atlas(&self) -> Rc<TextureAtlas> {
        self.default_atlas.clone()
    }

    #[inline(always)]
    pub fn painter(&self) -> &Painter {
        &self.painter
    }

    #[inline(always)]
    pub fn painter_mut(&mut self) -> &mut Painter {
        &mut self.painter
    }

    pub fn set_default_font(&mut self, font: ScaledFontInstance) {
        self.default_font = Some(font);
    }

    #[inline(always)]
    pub fn style(&self) -> Rc<Style> {
        self.style.clone()
    }

    #[inline(always)]
    pub fn id_scratch_buffer(&self) -> &IdScratchBuffer {
        &self.id_scratch_buffer
    }

    #[inline(always)]
    pub fn id_scratch_buffer_mut(&mut self) -> &mut IdScratchBuffer {
        &mut self.id_scratch_buffer
    }

    pub fn default_font(&self) -> ScaledFontInstance {
        self.default_font
            .as_ref()
            .expect("Don't forget to set the default font ")
            .clone()
    }

    pub fn push_id(&mut self, id: Id) {
        self.id_stack.push(id);
    }

    pub fn pop_id(&mut self, id: Id) {
        let r = self.id_stack.pop();
        debug_assert!(r.is_some());
        if let Some(stack_id) = r {
            assert_eq!(
                stack_id, id,
                "Id push/pop mismatch got {}, expected {}",
                stack_id, id
            );
        }
    }

    pub fn parent_id(&self) -> Id {
        *self.id_stack.last().unwrap_or(&Id::invalid())
    }

    pub fn handle_mouse(
        &mut self,
        widget_id: Id,
        rect: Rect,
        mouse_button: MouseButton,
    ) -> MouseState {
        //TODO: This will fail when one widget is over the other
        let is_inside = rect.is_point_in_rect(self.input.current.mouse_position)
            && self
                .shape_builder
                .clip_rect()
                .is_point_in_rect(self.input.current.mouse_position);
        if is_inside {
            self.hot_item = widget_id;
        } else if self.input.is_mouse_button_pressed(mouse_button) {
            self.hot_item = Id::invalid();
            self.focused_item = Id::invalid();
        }

        if self.active_item == widget_id && !self.input.is_mouse_button_down(mouse_button) {
            if is_inside {
                self.hot_item = widget_id;
                self.active_item = Id::invalid();
                return MouseState::Up;
            } else {
                self.hot_item = Id::invalid();
                self.active_item = Id::invalid();
            }
        } else if self.hot_item == widget_id && self.input.is_mouse_button_down(mouse_button) {
            self.focused_item = widget_id;
            self.active_item = widget_id;
            return MouseState::Down;
        } else if is_inside {
            return MouseState::Hover;
        }
        MouseState::Noop
    }

    pub fn handle_mouse_scroll(&mut self, widget_id: Id, rect: Rect) -> Vec2 {
        let mouse_pos = self.input.mouse_position();
        let is_inside = rect.is_point_in_rect(mouse_pos)
            && self.shape_builder.clip_rect().is_point_in_rect(mouse_pos);
        if is_inside {
            self.hot_item = widget_id;
        }

        if self.hot_item == widget_id {
            self.input.mouse_scroll()
        } else {
            Vec2::zero()
        }
    }

    pub fn on_focus_lost(&mut self) {
        self.input.reset_input_buttons();
        self.focused_item = Id::invalid();
        self.active_item = Id::invalid();
        self.hot_item = Id::invalid();
    }

    #[inline(always)]
    pub fn id_state(&self) -> &IdStateMap {
        &self.id_state
    }

    #[inline(always)]
    pub fn id_state_mut(&mut self) -> &mut IdStateMap {
        &mut self.id_state
    }

    #[inline(always)]
    pub fn was_previously_focused(&self, id: Id) -> bool {
        self.prev_focused_item == id
    }
}
