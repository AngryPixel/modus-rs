//! UI style control

use crate::{Color, Vec2};

#[derive(Debug)]
pub struct Style {
    pub min_element_size: Vec2,
    pub interactive_min_size: f32,
    pub element_padding: f32,
    pub border_size: f32,
    pub border_color: Color,
    pub background_color: Color,
    pub button_background_color: Color,
    pub button_hover_color: Color,
    pub button_down_color: Color,
    pub label_color: Color,
    pub input_background_color: Color,
    pub focused_element_color: Color,
}

impl Style {
    pub fn new() -> Self {
        Self {
            min_element_size: Vec2::new(50.0, 20.0),
            element_padding: 4.0,
            border_size: 1.0,
            interactive_min_size: 20.0,
            border_color: Color::from_u32_argb(0xff1b1b1b),
            background_color: Color::from_u32_argb(0xff2d2d2d),
            button_background_color: Color::from_u32_argb(0xff333333),
            button_hover_color: Color::from_u32_argb(0xff424242),
            button_down_color: Color::from_u32_argb(0xff1c1c1c),
            label_color: Color::from_u32_argb(0xffeeeeec),
            input_background_color: Color::from_u32_argb(0xff222222),
            focused_element_color: Color::from_u32_argb(0xff0f4199),
        }
    }
}

impl Default for Style {
    fn default() -> Self {
        Self::new()
    }
}
