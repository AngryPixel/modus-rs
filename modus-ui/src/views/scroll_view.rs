//! Scrollable View

use crate::context::Context;
use crate::id::Id;
use crate::layout::{Direction, LayoutStrategyBuilder};
use crate::view::View;
use crate::widgets::{VScrollBar, Widget};
use crate::{Rect, Vec2};

#[derive(Debug, Copy, Clone)]
struct State {
    offset: Vec2,
    max_size: Vec2,
}

impl State {
    fn with_offset(offset: Option<Vec2>) -> Self {
        Self {
            offset: offset.unwrap_or_else(Vec2::zero),
            max_size: Vec2::zero(),
        }
    }
}

impl Default for State {
    fn default() -> Self {
        Self {
            offset: Vec2::zero(),
            max_size: Vec2::max(),
        }
    }
}

#[derive(Debug)]
pub struct ScrollView {
    vertical: bool,
    horizontal: bool,
    max_size: Vec2,
    offset: Option<Vec2>,
    enabled: bool,
    scroll_offset: f32,
}

impl ScrollView {
    pub fn new(horizontal: bool, vertical: bool) -> Self {
        Self {
            vertical,
            horizontal,
            max_size: Vec2::max(),
            offset: None,
            enabled: true,
            scroll_offset: 30.0,
        }
    }

    pub fn horizontal() -> Self {
        Self::new(true, false)
    }

    pub fn vertical() -> Self {
        Self::new(false, true)
    }

    pub fn both() -> Self {
        Self::new(true, true)
    }

    pub fn max_size(mut self, size: Vec2) -> Self {
        debug_assert!(size.x > 0.0 && size.y > 0.0);
        self.max_size = size;
        self
    }

    pub fn offset(mut self, offset: Vec2) -> Self {
        self.offset = Some(offset);
        self
    }

    pub fn enabled(mut self, v: bool) -> Self {
        self.enabled = v;
        self
    }

    pub fn scroll_offset(mut self, offset: f32) -> Self {
        self.scroll_offset = offset;
        self
    }
}

#[derive(Debug)]
pub struct ScrollViewScope<'a> {
    id: Id,
    widget_area: Rect,
    scroll_widget: ScrollView,
    scroll_view: View<'a>,
    state: State,
}

impl<'a> ScrollViewScope<'a> {
    fn build_view_rect(widget: &ScrollView, widget_area: Rect) -> Rect {
        Rect {
            x: 0.0,
            y: 0.0,
            width: if widget.horizontal {
                f32::MAX
            } else {
                widget_area.width
            },
            height: if widget.vertical {
                f32::MAX
            } else {
                widget_area.height
            },
        }
    }
    pub fn new(widget: ScrollView, rect: Rect, context: &'a mut Context) -> Self {
        let view_rect = Self::build_view_rect(&widget, rect);
        let mut scroll_view = View::new("ScrollViewChild", view_rect, context);
        let id = scroll_view.allocate_id();
        let state = if let Some(s) = scroll_view.id_state_mut().get_mut::<State>(id) {
            *s
        } else {
            State::with_offset(widget.offset)
        };
        let mut r = Self {
            id,
            widget_area: rect,
            scroll_widget: widget,
            scroll_view,
            state,
        };
        r.begin();
        r
    }

    pub fn in_view(widget: ScrollView, view: &'a mut View<'a>) -> Self {
        let id = view.allocate_id();
        let widget_area = view.allocate_rect(Vec2::zero());
        let view_rect = Self::build_view_rect(&widget, widget_area);
        let mut scroll_view = View::new("ScrollViewChild", view_rect, view.context_mut());
        let state = if let Some(s) = scroll_view.id_state_mut().get_mut::<State>(id) {
            *s
        } else {
            State::with_offset(widget.offset)
        };
        let mut r = Self {
            id,
            widget_area,
            scroll_widget: widget,
            scroll_view,
            state,
        };
        r.begin();
        r
    }

    fn begin(&mut self) {
        // setup view to fill width or height, if is both, do neither
        let layout_strategy = if self.scroll_widget.vertical && self.scroll_widget.horizontal {
            LayoutStrategyBuilder::new(Direction::LeftToRight)
                .fill_alt(false)
                .fill(false)
        } else if self.scroll_widget.horizontal {
            LayoutStrategyBuilder::new(Direction::LeftToRight)
                .fill_alt(true)
                .fill(false)
        } else {
            LayoutStrategyBuilder::new(Direction::TopToBottom)
                .fill_alt(true)
                .fill(false)
        }
        .build();
        self.scroll_view.set_layout_strategy(layout_strategy);
        self.scroll_view
            .layout_allocator_mut()
            .set_offset(Vec2::new(self.widget_area.x, self.widget_area.y));

        // push id
        self.scroll_view.context_mut().push_id(self.id);
        // set clip rect
        self.scroll_view
            .context_mut()
            .shape_builder_mut()
            .push_clip_rect(self.widget_area);

        let scrollable_area = Vec2 {
            x: self.state.max_size.x - self.widget_area.width,
            y: self.state.max_size.y - self.widget_area.height,
        };

        // check mouse events for scroll
        let mouse_scroll = self
            .scroll_view
            .context_mut()
            .handle_mouse_scroll(self.id, self.widget_area);

        // clamp values
        if self.scroll_widget.horizontal && self.state.max_size.x > self.widget_area.width {
            self.state.offset.x += mouse_scroll.x * self.scroll_widget.scroll_offset;
            self.state.offset.x = self.state.offset.x.clamp(-scrollable_area.x, 0.0);
            todo!("Missing HScroll implementation");
        } else {
            self.state.offset.x = 0.0;
            self.scroll_view
                .layout_allocator_mut()
                .set_max_size(Vec2::new(f32::MAX, f32::MAX));
        }

        if self.scroll_widget.vertical && self.state.max_size.y > self.widget_area.height {
            self.state.offset.y += mouse_scroll.y * self.scroll_widget.scroll_offset;
            self.state.offset.y = self.state.offset.y.clamp(-scrollable_area.y, 0.0);

            // draw scroll bar
            let mut scroll_bar_offset_y = (-self.state.offset.y) / scrollable_area.y;
            let result = self.scroll_view.add(VScrollBar::new(
                self.widget_area.height / self.state.max_size.y,
                &mut scroll_bar_offset_y,
                self.widget_area,
            ));
            self.state.offset.y = scroll_bar_offset_y * (-scrollable_area.y);
            self.scroll_view
                .layout_allocator_mut()
                .set_max_size(Vec2::new(
                    self.widget_area.width - result.rect.width,
                    f32::MAX,
                ));
        } else {
            self.state.offset.y = 0.0;
            self.scroll_view
                .layout_allocator_mut()
                .set_max_size(Vec2::new(f32::MAX, f32::MAX));
        }

        // apply offset to view
        self.scroll_view
            .layout_allocator_mut()
            .set_xy(self.state.offset);
    }

    fn end(&mut self) {
        let cursor = self.scroll_view.layout_allocator().cursor();
        let spacing = self.scroll_view.layout_allocator().spacing();

        // Update max size & store state
        self.state.max_size.x = cursor.x + spacing;
        self.state.max_size.y = cursor.y + spacing;
        let context = self.scroll_view.context_mut();
        context.pop_id(self.id);
        context.shape_builder_mut().pop_clip_rect();
        context.id_state_mut().insert(self.id, self.state);
    }

    pub fn add<R>(&mut self, widget: impl Widget<R>) -> R {
        self.scroll_view.add(widget)
    }

    pub fn v_rows<R>(
        &mut self,
        element_count: usize,
        widget_height: f32,
        mut add_widgets: impl FnMut(usize, &mut View) -> R,
    ) {
        let total_size = element_count as f32 * widget_height;
        let rows_begin = if total_size > self.widget_area.height {
            (self.scroll_view.layout_allocator().area().y.abs() / widget_height).floor() as usize
        } else {
            0
        };
        let rows_count =
            ((self.widget_area.height / widget_height).ceil() as usize).min(element_count);
        self.scroll_view.advance_id_allocation(rows_begin as u64);
        let prev_min_size = self.scroll_view.layout_allocator().min_size();
        self.scroll_view.layout_allocator_mut().set_min_size(Vec2 {
            x: prev_min_size.x,
            y: widget_height,
        });

        // update cursor to reflect current scroll offset offset
        self.scroll_view
            .layout_allocator_mut()
            .advance_cursor_y(widget_height * (rows_begin as f32));

        for i in rows_begin..(rows_begin + rows_count) {
            add_widgets(i, &mut self.scroll_view);
        }
        self.scroll_view
            .layout_allocator_mut()
            .set_min_size(prev_min_size);

        // update cursor again so that max calculations are correct
        self.scroll_view
            .layout_allocator_mut()
            .advance_cursor_y(widget_height * (element_count - (rows_begin + rows_count)) as f32);
    }

    #[inline(always)]
    pub fn rect(&self) -> Rect {
        self.widget_area
    }

    #[inline(always)]
    pub fn context_mut(&mut self) -> &mut Context {
        self.scroll_view.context_mut()
    }

    #[inline(always)]
    pub fn context(&self) -> &Context {
        self.scroll_view.context()
    }
}

impl<'a> Drop for ScrollViewScope<'a> {
    fn drop(&mut self) {
        self.end();
    }
}
