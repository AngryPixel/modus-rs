use crate::Vec2;

#[derive(Debug, Copy, Clone, Eq, PartialEq, Ord, PartialOrd)]
pub enum MouseButton {
    Left,
    Right,
    Middle,
    Extra1,
    Extra2,
    Total,
}

#[derive(Debug, Copy, Clone, Eq, PartialEq, Ord, PartialOrd)]
pub enum Key {
    Backspace = 0,
    Enter,
    Escape,
    Tab,
    Space,
    Insert,
    Delete,
    Home,
    End,
    PageUp,
    PageDown,
    ArrowLeft,
    ArrowRight,
    ArrowUp,
    ArrowDown,
    Ctrl,
    Shift,
    Alt,
    A,
    B,
    C,
    D,
    E,
    F,
    G,
    H,
    I,
    J,
    K,
    L,
    M,
    N,
    O,
    P,
    Q,
    R,
    S,
    T,
    U,
    V,
    W,
    X,
    Y,
    Z,
    Num0,
    Num1,
    Num2,
    Num3,
    Num4,
    Num5,
    Num6,
    Num7,
    Num8,
    Num9,
    Total,
}

#[derive(Debug, Copy, Clone)]
struct ClickState {
    is_down: bool,
    click_count: u32,
}

impl ClickState {
    fn new() -> Self {
        Self {
            is_down: false,
            click_count: 0,
        }
    }

    #[inline(always)]
    fn update(&mut self, is_down: bool) {
        self.is_down = is_down;
        self.click_count += 1;
    }

    #[inline(always)]
    fn is_down(&self) -> bool {
        self.is_down
    }

    #[inline(always)]
    fn is_pressed(&self) -> bool {
        (self.is_down && self.click_count > 0) || (!self.is_down && self.click_count > 2)
    }

    #[inline(always)]
    fn is_released(&self) -> bool {
        (!self.is_down && self.click_count > 0) || (self.is_down && self.click_count > 2)
    }
}

#[derive(Debug, Clone)]
pub struct FrameValues {
    pub mouse_position: Vec2,
    pub mouse_scroll: Vec2,
    text: String,
}

impl FrameValues {
    fn new() -> Self {
        Self {
            mouse_position: Vec2::zero(),
            mouse_scroll: Vec2::zero(),
            text: String::new(),
        }
    }

    fn save_to_previous(&mut self, previous: &mut FrameValues) {
        previous.mouse_position = self.mouse_position;
        previous.mouse_scroll = self.mouse_scroll;
        std::mem::swap(&mut previous.text, &mut self.text);
        self.mouse_scroll = Vec2::zero();
        self.text.clear();
    }
}

#[derive(Debug)]
pub struct InputState {
    pub current: FrameValues,
    pub previous: FrameValues,
    keys: [ClickState; Key::Total as usize],
    mouse_buttons: [ClickState; MouseButton::Total as usize],
}

impl InputState {
    pub fn new() -> Self {
        Self {
            current: FrameValues::new(),
            previous: FrameValues::new(),
            mouse_buttons: [ClickState::new(); MouseButton::Total as usize],
            keys: [ClickState::new(); Key::Total as usize],
        }
    }

    pub fn set_mouse_button_down(&mut self, button: MouseButton, is_down: bool) {
        debug_assert!(button < MouseButton::Total);
        self.mouse_buttons[button as usize].update(is_down);
    }

    #[inline(always)]
    pub fn is_mouse_button_down(&self, button: MouseButton) -> bool {
        debug_assert!(button < MouseButton::Total);
        self.mouse_buttons[button as usize].is_down()
    }

    #[inline(always)]
    pub fn is_mouse_button_pressed(&self, button: MouseButton) -> bool {
        debug_assert!(button < MouseButton::Total);
        self.mouse_buttons[button as usize].is_pressed()
    }

    #[inline(always)]
    pub fn is_mouse_button_released(&self, button: MouseButton) -> bool {
        debug_assert!(button < MouseButton::Total);
        self.mouse_buttons[button as usize].is_released()
    }

    #[inline(always)]
    pub fn is_any_mouse_button_down(&self) -> bool {
        let mut is_down = false;
        for button in &self.mouse_buttons {
            is_down |= button.is_down()
        }
        is_down
    }

    pub fn set_key_down(&mut self, key: Key, is_down: bool) {
        debug_assert!(key < Key::Total);
        let key_state = &mut self.keys[key as usize];
        key_state.update(is_down);
    }

    #[inline(always)]
    pub fn is_key_pressed(&self, key: Key) -> bool {
        debug_assert!(key < Key::Total);
        let state = self.keys[key as usize];
        state.is_pressed()
    }

    pub fn is_key_released(&self, key: Key) -> bool {
        debug_assert!(key < Key::Total);
        let state = self.keys[key as usize];
        state.is_released()
    }

    pub fn is_key_down(&self, key: Key) -> bool {
        debug_assert!(key < Key::Total);
        self.keys[key as usize].is_down
    }

    pub fn reset_input_buttons(&mut self) {
        for key in &mut self.keys {
            key.is_down = false;
            key.click_count = 0;
        }
        for button in &mut self.mouse_buttons {
            button.is_down = false;
            button.click_count = 0;
        }
    }

    pub(crate) fn begin_frame(&mut self) {
        // reset scroll to zero on every frame
        self.current.save_to_previous(&mut self.previous);
        for key in &mut self.keys {
            key.click_count = 0;
        }
        for button in &mut self.mouse_buttons {
            button.click_count = 0;
        }
    }

    pub(crate) fn end_frame(&mut self) {}

    #[inline(always)]
    pub fn mouse_position(&self) -> Vec2 {
        self.current.mouse_position
    }

    #[inline(always)]
    pub fn mouse_scroll(&self) -> Vec2 {
        self.previous.mouse_scroll
    }

    #[inline(always)]
    pub fn add_text(&mut self, text: &str) {
        self.current.text.push_str(text);
    }

    #[inline(always)]
    pub fn add_char(&mut self, ch: char) {
        self.current.text.push(ch);
    }

    #[inline(always)]
    pub fn text(&self) -> &str {
        &self.previous.text
    }
}

impl Default for InputState {
    fn default() -> Self {
        Self::new()
    }
}
