use crate::painter::PainterTexture;
use crate::text::font::ScaledFontInstance;
use crate::text::layout::TextLayoutCalculator;
use crate::{Color, Rect, Vec2};
use modus_math::{pack_snorm16, pack_unorm16};
use std::rc::Rc;

#[repr(u8)]
enum ShapeType {
    Rect = 0,
    Vertex,
    RectUV,
}

const MAX_INDEX_OFFSET: u32 = 1 << 24;
const INDEX_MASK: u32 = MAX_INDEX_OFFSET - 1;
const SHAPE_TYPE_MASK: u8 = (1 << 6) - 1;
const CORNER_MASK: u8 = 3;
const SHAPE_TYPE_SHIFT: u8 = 2;

pub(crate) const DATA_BUFFER_SIZE: usize = 4 * 1024 * 1024;
pub(crate) const INDEX_BUFFER_SIZE: usize = 1024 * 1024;
pub(crate) const MAX_CLIP_RECTS: usize = 256;
pub(crate) const CLIP_RECT_BUFFER_SIZE: usize = std::mem::size_of::<ClipRectGPU>() * MAX_CLIP_RECTS;

fn encode_index(shape_type: ShapeType, corner: u8, offset: u32) -> u32 {
    debug_assert!(offset < MAX_INDEX_OFFSET);
    debug_assert!(corner < 4);
    let shape_id =
        (((shape_type as u8) & SHAPE_TYPE_MASK) << SHAPE_TYPE_SHIFT) | (corner & CORNER_MASK);
    let shape_id_u32 = (shape_id as u32) << 24;
    shape_id_u32 | (offset & INDEX_MASK)
}

fn update_index(index: u32, offset: u32) -> u32 {
    let mut current_index = index & INDEX_MASK;
    current_index += offset;
    debug_assert!(current_index < MAX_INDEX_OFFSET);
    (index & (!INDEX_MASK)) | (current_index & INDEX_MASK)
}

const CORNER_TOP_LEFT: u8 = 0;
const CORNER_BOTTOM_LEFT: u8 = 1;
const CORNER_TOP_RIGHT: u8 = 2;
const CORNER_BOTTOM_RIGHT: u8 = 3;

#[derive(Debug)]
#[repr(C, align(4))]
struct RectShapeUVGPU {
    pub x: i16,
    pub y: i16,
    pub w: u16,
    pub h: u16,
    pub color: [u8; 4],
    pub uv_left: u16,
    pub uv_top: u16,
    pub uv_right: u16,
    pub uv_bottom: u16,
    pub uv_z: u16,
    pub clip_rect_index: u16,
}

impl RectShapeUVGPU {
    fn from(
        s: &Rect,
        canvas_size: Vec2,
        color: Color,
        uv_top_left: Vec2,
        uv_bottom_right: Vec2,
        uv_z: f32,
        clip_rect_index: u16,
    ) -> Self {
        Self {
            x: pack_snorm16(s.x / canvas_size.x),
            y: pack_snorm16(s.y / canvas_size.y),
            w: pack_unorm16(s.width / canvas_size.x),
            h: pack_unorm16(s.height / canvas_size.y),
            color: color.into(),
            uv_top: pack_unorm16(uv_top_left.y),
            uv_left: pack_unorm16(uv_top_left.x),
            uv_right: pack_unorm16(uv_bottom_right.x),
            uv_bottom: pack_unorm16(uv_bottom_right.y),
            uv_z: pack_unorm16(uv_z),
            clip_rect_index,
        }
    }
}

pub struct VertexShape {
    pub v: [f32; 2],
    pub color: Color,
}

#[derive(Debug)]
#[repr(C, align(4))]
pub struct ClipRectGPU {
    pub x: f32,
    pub y: f32,
    pub xmax: f32,
    pub ymax: f32,
}

impl From<Rect> for ClipRectGPU {
    fn from(r: Rect) -> Self {
        Self {
            x: r.x,
            y: r.y,
            xmax: r.x + r.width,
            ymax: r.y + r.height,
        }
    }
}

#[derive(Debug)]
#[repr(C, align(4))]
struct VertexShapeGPU {
    pub v: [i16; 2],
    pub color: [u8; 4],
    pub uv: [u16; 2],
    pub uv_z: u16,
    pub clip_rect_index: u16,
}

impl VertexShapeGPU {
    fn from(s: &VertexShape, canvas_size: Vec2, clip_rect_index: u16) -> Self {
        Self {
            v: [
                pack_snorm16(s.v[0] / canvas_size.x),
                pack_snorm16(s.v[1] / canvas_size.y),
            ],
            color: s.color.into(),
            uv: [0, 0],
            uv_z: 0,
            clip_rect_index,
        }
    }
}

#[derive(Debug)]
pub struct CmdData {
    pub offset: u32,
    pub count: u32,
    pub texture: Rc<PainterTexture>,
}

#[derive(Debug)]
pub struct PrimitiveBuffer {
    pub data_buffer: Vec<u8>,
    pub data_buffer_offset: isize,
    pub index_buffer: Vec<u32>,
    pub commands: Vec<CmdData>,
}

impl PrimitiveBuffer {
    fn new() -> Self {
        debug_assert!(DATA_BUFFER_SIZE < MAX_INDEX_OFFSET as usize);
        let mut r = Self {
            data_buffer: Vec::with_capacity(DATA_BUFFER_SIZE),
            data_buffer_offset: 0,
            index_buffer: Vec::with_capacity(INDEX_BUFFER_SIZE),
            commands: Vec::with_capacity(64),
        };
        unsafe { r.data_buffer.set_len(DATA_BUFFER_SIZE) };
        r
    }

    fn can_fit_data(&self, data_size: usize, index_count: usize) -> bool {
        !(self.data_buffer_offset + (data_size as isize) >= MAX_INDEX_OFFSET as isize
            || self.index_buffer.len() + index_count >= INDEX_BUFFER_SIZE)
    }

    #[inline(always)]
    #[cfg(not(feature = "debug-draw-commands"))]
    fn update_cmd_buffer(&mut self, offset: u32, len: u32, texture: Rc<PainterTexture>) {
        if !self.commands.is_empty() {
            let last = self.commands.last_mut().unwrap();
            if last.texture == texture {
                last.count += len;
            } else {
                self.commands.push(CmdData {
                    offset,
                    count: len,
                    texture,
                });
            }
        } else {
            self.commands.push(CmdData {
                offset,
                count: len,
                texture,
            });
        }
    }

    #[inline(always)]
    #[cfg(feature = "debug-draw-commands")]
    fn update_cmd_buffer(&mut self, offset: u32, len: u32, texture: Rc<PainterTexture>) {
        self.commands.push(CmdData {
            offset,
            count: len,
            texture,
        });
    }

    fn write_primitives<T: Sized>(
        &mut self,
        primitive: &[T],
        indices: &[u32],
        texture: Rc<PainterTexture>,
    ) -> (usize, usize) {
        let type_size = std::mem::size_of::<T>() as isize;
        debug_assert!(
            (self.data_buffer_offset + (type_size as isize) < MAX_INDEX_OFFSET as isize
                || self.index_buffer.len() + indices.len() < INDEX_BUFFER_SIZE)
        );
        let mut current_data_offset = self.data_buffer_offset;
        // make sure data offset is on the same type boundary
        let mod_val = current_data_offset % type_size;
        if mod_val != 0 {
            current_data_offset += type_size - mod_val;
            self.data_buffer_offset = current_data_offset;
        }
        let current_index_offset = self.index_buffer.len();
        unsafe {
            std::ptr::copy_nonoverlapping(
                std::mem::transmute(primitive.as_ptr()),
                self.data_buffer.as_mut_ptr().offset(current_data_offset),
                type_size as usize * primitive.len(),
            );
        }
        debug_assert_eq!(current_data_offset % type_size as isize, 0);
        self.index_buffer.extend(
            indices
                .iter()
                .map(|i| update_index(*i, (current_data_offset / type_size) as u32)),
        );
        self.data_buffer_offset += type_size * (primitive.len() as isize);

        // try to combine draw calls together
        self.update_cmd_buffer(current_index_offset as u32, indices.len() as u32, texture);
        (current_data_offset as usize, current_index_offset)
    }

    fn reset(&mut self) {
        self.index_buffer.clear();
        self.data_buffer_offset = 0;
        self.commands.clear();
    }
}

/// Shape builder which prepares the GPU buffers for drawing. Unlike the standard approach of
/// using the input assembler to extract the buffer layout, this type uses the approach used
/// by [OurMachinery](https://ourmachinery.com/post/ui-rendering-using-primitive-buffers/) where
/// we construct a bunch of primitives in the buffers and do the unpacking on the GPU itself.
#[derive(Debug)]
pub struct ShapeBuilder {
    buffers: Vec<PrimitiveBuffer>,
    free_buffers: Vec<PrimitiveBuffer>,
    canvas_size: Vec2,
    temp_index_buffer: Vec<u32>,
    temp_vertex_buffer: Vec<VertexShapeGPU>,
    default_texture: Rc<PainterTexture>,
    clip_rect_stack: Vec<(Rect, usize)>, // rect, index into data buffer
    clip_rect_data_buffer: Vec<ClipRectGPU>,
}

impl ShapeBuilder {
    pub fn new(default_texture: Rc<PainterTexture>) -> Self {
        Self {
            buffers: Vec::with_capacity(4),
            free_buffers: Vec::with_capacity(4),
            canvas_size: Vec2 { x: 0.0, y: 0.0 },
            temp_index_buffer: Vec::with_capacity(32),
            temp_vertex_buffer: Vec::with_capacity(32),
            default_texture,
            clip_rect_stack: Vec::with_capacity(4),
            clip_rect_data_buffer: Vec::with_capacity(4),
        }
    }

    pub(crate) fn begin_frame(&mut self, canvas_size: Vec2) {
        self.canvas_size = canvas_size;
        while let Some(mut buffer) = self.buffers.pop() {
            buffer.reset();
            self.free_buffers.push(buffer);
        }
        self.clip_rect_stack.clear();
        self.clip_rect_data_buffer.clear();
        self.clip_rect_data_buffer.push(ClipRectGPU {
            x: 0.0,
            y: 0.0,
            xmax: canvas_size.x,
            ymax: canvas_size.y,
        })
    }

    fn get_buffer(&mut self, data_size: usize, index_count: usize) -> usize {
        debug_assert!(data_size < DATA_BUFFER_SIZE);
        if !self.buffers.is_empty()
            && self
                .buffers
                .last()
                .unwrap()
                .can_fit_data(data_size, index_count)
        {
            // do nothing
        } else if !self.free_buffers.is_empty() {
            self.buffers.push(self.free_buffers.pop().unwrap());
        } else {
            self.buffers.push(PrimitiveBuffer::new());
        }
        self.buffers.len() - 1
    }

    fn write_primitive<T>(
        &mut self,
        primitive: &T,
        indices: &[u32],
        texture: Option<Rc<PainterTexture>>,
    ) -> (usize, usize) {
        let buffer_index = self.get_buffer(std::mem::size_of::<T>(), indices.len());
        self.buffers[buffer_index].write_primitives::<T>(
            std::slice::from_ref(primitive),
            indices,
            texture.unwrap_or_else(|| self.default_texture.clone()),
        )
    }

    #[inline(always)]
    fn get_clip_rect_index(&self) -> u16 {
        if let Some((_, index)) = self.clip_rect_stack.last() {
            debug_assert!(*index < u16::MAX as usize);
            *index as u16
        } else {
            0
        }
    }

    pub fn add_rect(&mut self, rect: &Rect, color: Color) -> (usize, usize) {
        crate::profile_scope!();
        let indices = [
            encode_index(ShapeType::Rect, CORNER_TOP_LEFT, 0),
            encode_index(ShapeType::Rect, CORNER_BOTTOM_LEFT, 0),
            encode_index(ShapeType::Rect, CORNER_TOP_RIGHT, 0),
            encode_index(ShapeType::Rect, CORNER_TOP_RIGHT, 0),
            encode_index(ShapeType::Rect, CORNER_BOTTOM_LEFT, 0),
            encode_index(ShapeType::Rect, CORNER_BOTTOM_RIGHT, 0),
        ];
        let optimized_shape = RectShapeUVGPU::from(
            rect,
            self.canvas_size,
            color,
            Vec2::zero(),
            Vec2::zero(),
            0.0,
            self.get_clip_rect_index(),
        );
        self.write_primitive(&optimized_shape, &indices, None)
    }

    pub fn add_rect_with_border(
        &mut self,
        rect: &Rect,
        background_color: Color,
        border_size: f32,
        border_color: Color,
    ) {
        self.add_rect(rect, background_color);
        self.stroke_rect(rect, border_size, border_color);
    }

    pub fn stroke_rect(&mut self, rect: &Rect, thickness: f32, color: Color) {
        let rects = [
            // left
            Rect {
                x: rect.x,
                y: rect.y,
                width: thickness,
                height: rect.height,
            },
            // top
            Rect {
                x: rect.x,
                y: rect.y,
                width: rect.width,
                height: thickness,
            },
            // right
            Rect {
                x: rect.x + rect.width - thickness,
                y: rect.y,
                width: thickness,
                height: rect.height,
            },
            // bottom
            Rect {
                x: rect.x,
                y: rect.y + rect.height - thickness,
                width: rect.width,
                height: thickness,
            },
        ];
        for rect in &rects {
            self.add_rect(rect, color);
        }
    }

    pub fn add_rect_with_uv(
        &mut self,
        rect: &Rect,
        color: Color,
        uv_top_left: Vec2,
        uv_bottom_right: Vec2,
        uv_z: f32,
        texture: Rc<PainterTexture>,
    ) -> (usize, usize) {
        crate::profile_scope!();
        let indices = [
            encode_index(ShapeType::RectUV, CORNER_TOP_LEFT, 0),
            encode_index(ShapeType::RectUV, CORNER_BOTTOM_LEFT, 0),
            encode_index(ShapeType::RectUV, CORNER_TOP_RIGHT, 0),
            encode_index(ShapeType::RectUV, CORNER_TOP_RIGHT, 0),
            encode_index(ShapeType::RectUV, CORNER_BOTTOM_LEFT, 0),
            encode_index(ShapeType::RectUV, CORNER_BOTTOM_RIGHT, 0),
        ];
        let optimized_shape = RectShapeUVGPU::from(
            rect,
            self.canvas_size,
            color,
            uv_top_left,
            uv_bottom_right,
            uv_z,
            self.get_clip_rect_index(),
        );
        self.write_primitive(&optimized_shape, &indices, Some(texture))
    }

    pub fn add_vertices(&mut self, vertices: &[VertexShape], indices: &[u32]) -> (usize, usize) {
        crate::profile_scope!();
        self.temp_vertex_buffer.clear();
        self.temp_index_buffer.clear();
        self.temp_index_buffer.extend(
            indices
                .iter()
                .map(|i| encode_index(ShapeType::Vertex, 0, *i)),
        );
        let clip_rect_index = self.get_clip_rect_index();
        self.temp_vertex_buffer.extend(
            vertices
                .iter()
                .map(|v| VertexShapeGPU::from(v, self.canvas_size, clip_rect_index)),
        );

        let buffer_index = self.get_buffer(
            std::mem::size_of::<VertexShapeGPU>() * vertices.len(),
            indices.len(),
        );
        self.buffers[buffer_index].write_primitives(
            &self.temp_vertex_buffer,
            &self.temp_index_buffer,
            self.default_texture.clone(),
        )
    }

    pub fn add_text(
        &mut self,
        font: &ScaledFontInstance,
        point: Vec2,
        text: &str,
        layout_processor: &TextLayoutCalculator,
        color: Color,
    ) {
        layout_processor.layout(font, text, point, |glyph, location| {
            let rect = Rect {
                x: location.x,
                y: location.y,
                width: glyph.width(),
                height: glyph.height(),
            };
            self.add_rect_with_uv(
                &rect,
                color,
                glyph.uv_top_left(),
                glyph.uv_bottom_right(),
                glyph.uv_z(),
                font.texture(),
            );
        });
    }

    #[inline(always)]
    pub fn primitive_buffers(&self) -> &[PrimitiveBuffer] {
        &self.buffers
    }

    #[inline(always)]
    pub fn clip_rect_data_buffer(&self) -> &[ClipRectGPU] {
        &self.clip_rect_data_buffer
    }

    pub fn push_clip_rect(&mut self, rect: Rect) {
        self.clip_rect_data_buffer.push(ClipRectGPU::from(rect));
        self.clip_rect_stack
            .push((rect, self.clip_rect_data_buffer.len() - 1));
    }

    pub fn pop_clip_rect(&mut self) {
        debug_assert!(!self.clip_rect_stack.is_empty());
        self.clip_rect_stack.pop();
    }

    pub fn clip_rect(&self) -> Rect {
        self.clip_rect_stack
            .last()
            .unwrap_or(&(Rect::new(self.canvas_size.x, self.canvas_size.y), 0))
            .0
    }
}
