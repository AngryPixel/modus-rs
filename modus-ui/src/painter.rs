use crate::shape_builder::{
    ClipRectGPU, ShapeBuilder, CLIP_RECT_BUFFER_SIZE, DATA_BUFFER_SIZE, INDEX_BUFFER_SIZE,
};
use crate::Vec2;
use modus_core::ArrayVec;
use modus_math::color::Color;
use modus_threed::buffer::BufferCreateInfo;
use modus_threed::command_buffer::CommandBuffer;
use modus_threed::descriptor_allocators::CachedDescriptorAllocator;
use modus_threed::descriptors::{
    DescriptorBufferInput, DescriptorDesc, DescriptorPoolCreateInfo, DescriptorSetBindInfo,
    DescriptorSetLayoutCreateInfo, DescriptorSetUpdateInfo, DescriptorTextureInput, DescriptorType,
};
use modus_threed::device::{update_buffer_typed, Device, DeviceError, DeviceResult};
use modus_threed::pipeline::{
    BlendState, ColorAttachmentBlendState, ColorBlendState, DepthStencilState, InputAssemblyState,
    MultisampleState, PipelineCreateInfo, RasterizationState, ShaderState, COLOR_MASK_ALL,
};
use modus_threed::render_pass::{
    AttachmentAccess, AttachmentLoadOp, AttachmentStoreOp, ClearValue, PipelineStage,
    RenderPassAttachmentDesc, RenderPassClearInfo, RenderPassCreateInfo,
};
use modus_threed::sampler::SamplerCreateInfo;
use modus_threed::shader::ShaderCreateInfo;
use modus_threed::texture::{TextureCreateInfo, TextureData, TEXTURE_USAGE_FLAG_SHADER_INPUT};
use modus_threed::types::{
    BlendFactor, BlendOp, BufferHandle, BufferType, CullMode, DescriptorSetLayoutHandle,
    DeviceSize, Filter, FramebufferHandle, FrontFace, IndexType, MemoryType, PipelineHandle,
    PolygonMode, Primitive, RenderPassHandle, SamplerAddressMode, SamplerHandle, SamplerMipmapMode,
    ShaderHandle, ShaderType, TextureFormat, TextureHandle, TextureType, Viewport,
    PIPELINE_STAGE_FLAG_FRAGMENT, PIPELINE_STAGE_FLAG_VERTEX,
};
use std::sync::Arc;

#[repr(C, align(4))]
struct GpuConstantBuffer {
    canvas_width: f32,
    canvas_height: f32,
}

#[derive(Debug)]
struct PaintCommand {
    buffer_index: u32,
    index_offset: u32,
    index_count: u32,
    texture: TextureHandle,
}

#[derive(Debug)]
pub struct Painter {
    device: Arc<dyn Device>,
    render_pass: RenderPassHandle,
    pipeline: PipelineHandle,
    shader_handles: [ShaderHandle; 2],
    storage_buffer: BufferHandle,
    index_buffer: BufferHandle,
    constants_buffer: BufferHandle,
    clip_rect_buffer: BufferHandle,
    constants_descriptor_set_layout: DescriptorSetLayoutHandle,
    data_descriptor_set_layout: DescriptorSetLayoutHandle,
    texture_descriptor_set_layout: DescriptorSetLayoutHandle,
    descriptor_allocator: CachedDescriptorAllocator,
    clip_rect_descriptor_set_layout: DescriptorSetLayoutHandle,
    pass_clear: bool,
    paint_commands: Vec<PaintCommand>,
    sampler: SamplerHandle,
}

impl Painter {
    pub fn new(device: Arc<dyn Device>, clear: bool) -> DeviceResult<Self> {
        let mut r = Self {
            device,
            pipeline: Default::default(),
            shader_handles: [Default::default(), Default::default()],
            storage_buffer: BufferHandle::default(),
            index_buffer: BufferHandle::default(),
            constants_buffer: BufferHandle::default(),
            clip_rect_buffer: BufferHandle::default(),
            constants_descriptor_set_layout: Default::default(),
            data_descriptor_set_layout: Default::default(),
            clip_rect_descriptor_set_layout: Default::default(),
            texture_descriptor_set_layout: Default::default(),
            descriptor_allocator: CachedDescriptorAllocator::new(DescriptorPoolCreateInfo {
                constant_count: 1,
                constant_dynamic_count: 0,
                storage_count: 128,
                storage_dynamic_count: 0,
                input_attachment_count: 0,
                texture_count: 128,
                max_set_count: 128,
            }),
            render_pass: Default::default(),
            pass_clear: clear,
            paint_commands: Vec::with_capacity(16),
            sampler: Default::default(),
        };
        r.init_device_data()?;
        Ok(r)
    }

    pub fn render_pass(&self) -> RenderPassHandle {
        self.render_pass
    }

    fn init_device_data(&mut self) -> DeviceResult<()> {
        // create descriptor layout
        {
            let descriptors = [DescriptorDesc {
                binding_point: 0,
                descriptor_type: DescriptorType::Constant,
                stages: PIPELINE_STAGE_FLAG_FRAGMENT & PIPELINE_STAGE_FLAG_VERTEX,
            }];
            let create_info = DescriptorSetLayoutCreateInfo {
                push_constants: &[],
                descriptors: &descriptors,
            };
            self.constants_descriptor_set_layout =
                self.device.create_descriptor_set_layout(&create_info)?;
        }
        {
            let descriptors = [DescriptorDesc {
                binding_point: 0,
                descriptor_type: DescriptorType::Storage,
                stages: PIPELINE_STAGE_FLAG_VERTEX,
            }];
            let create_info = DescriptorSetLayoutCreateInfo {
                push_constants: &[],
                descriptors: &descriptors,
            };
            self.data_descriptor_set_layout =
                self.device.create_descriptor_set_layout(&create_info)?;
        }
        {
            let descriptors = [DescriptorDesc {
                binding_point: 0,
                descriptor_type: DescriptorType::Texture,
                stages: PIPELINE_STAGE_FLAG_FRAGMENT,
            }];
            let create_info = DescriptorSetLayoutCreateInfo {
                push_constants: &[],
                descriptors: &descriptors,
            };
            self.texture_descriptor_set_layout =
                self.device.create_descriptor_set_layout(&create_info)?;
        }
        {
            let descriptors = [DescriptorDesc {
                binding_point: 0,
                descriptor_type: DescriptorType::Storage,
                stages: PIPELINE_STAGE_FLAG_FRAGMENT,
            }];
            let create_info = DescriptorSetLayoutCreateInfo {
                push_constants: &[],
                descriptors: &descriptors,
            };
            self.clip_rect_descriptor_set_layout =
                self.device.create_descriptor_set_layout(&create_info)?;
        }
        // create vertex shader
        {
            let shader_bytes =
                include_bytes!(concat!(env!("OUT_DIR"), "/shaders/non-bindless/vert.spv"));
            let create_info = ShaderCreateInfo { data: shader_bytes };
            self.shader_handles[0] = self.device.create_shader(&create_info)?;
        }
        // create fragment shader
        {
            let shader_bytes =
                include_bytes!(concat!(env!("OUT_DIR"), "/shaders/non-bindless/frag.spv"));
            let create_info = ShaderCreateInfo { data: shader_bytes };
            self.shader_handles[1] = self.device.create_shader(&create_info)?;
        }
        // create render pass
        {
            let attachment_descs = [RenderPassAttachmentDesc {
                load_op: if self.pass_clear {
                    AttachmentLoadOp::Clear
                } else {
                    AttachmentLoadOp::Load
                },
                store_op: AttachmentStoreOp::Store,
                access: AttachmentAccess::Write,
                format: TextureFormat::B8G8R8A8,
                samples: 1,
            }];
            let color_attachments = [0_usize];
            let create_info = RenderPassCreateInfo {
                stage: PipelineStage::Graphics,
                attachments: &attachment_descs,
                input_attachments: &[],
                color_attachments: &color_attachments,
                depth_stencil_attachment: None,
            };

            self.render_pass = self.device.create_render_pass(&create_info)?;
        }
        // create pipeline
        {
            let shader_states = [
                ShaderState {
                    shader: self.shader_handles[0],
                    stage: ShaderType::Vertex,
                    entry: "main",
                },
                ShaderState {
                    shader: self.shader_handles[1],
                    stage: ShaderType::Fragment,
                    entry: "main",
                },
            ];
            let descriptor_layouts = [
                self.constants_descriptor_set_layout,
                self.data_descriptor_set_layout,
                self.texture_descriptor_set_layout,
                self.clip_rect_descriptor_set_layout,
            ];
            let color_blend_state = [ColorAttachmentBlendState {
                state: Some(ColorBlendState {
                    src_color_blend_factor: BlendFactor::SourceAlpha,
                    dst_color_blend_factor: BlendFactor::OneMinusSourceAlpha,
                    color_blend_op: BlendOp::Add,
                    src_alpha_blend_factor: BlendFactor::One,
                    dst_alpha_blend_factor: BlendFactor::Zero,
                    alpha_blend_op: BlendOp::Add,
                }),
                color_mask: COLOR_MASK_ALL,
            }];
            let create_info = PipelineCreateInfo {
                descriptor_set_layouts: Some(&descriptor_layouts),
                depth_stencil_state: DepthStencilState {
                    depth: None,
                    stencil: None,
                },
                blend_state: BlendState {
                    attachments: &color_blend_state,
                    logic_op: None,
                    blend_constants: [0.0; 4],
                },
                rasterization_state: RasterizationState {
                    depth_clamp: false,
                    rasterizer_discard: false,
                    polygon_mode: PolygonMode::Fill,
                    line_width: 1.0,
                    cull_mode: CullMode::Back,
                    front_face: FrontFace::CounterClockwise,
                    depth_bias: None,
                },
                input_assembly_state: InputAssemblyState {
                    primitive: Primitive::Triangles,
                    primitive_restart: false,
                },
                shader_states: &shader_states,
                multisample_state: MultisampleState {
                    sample_shading: false,
                    samples: 1,
                    min_sample_shading: 1.0,
                    alpha_to_coverage: false,
                    alpha_to_one: false,
                },
                render_pass: self.render_pass,
            };

            self.pipeline = self.device.create_pipeline(&create_info)?;
        }

        // create data_buffer
        {
            self.storage_buffer = self.device.create_buffer(&BufferCreateInfo {
                btype: BufferType::Storage,
                size: DATA_BUFFER_SIZE as DeviceSize,
                memory_type: MemoryType::GpuOnly,
                data: None,
            })?;
        }
        // create index buffer
        {
            self.index_buffer = self.device.create_buffer(&BufferCreateInfo {
                btype: BufferType::Index,
                size: (INDEX_BUFFER_SIZE * 4) as DeviceSize,
                memory_type: MemoryType::GpuOnly,
                data: None,
            })?;
        }
        // create clip rect buffer
        {
            self.clip_rect_buffer = self.device.create_buffer(&BufferCreateInfo {
                btype: BufferType::Storage,
                size: CLIP_RECT_BUFFER_SIZE as DeviceSize,
                memory_type: MemoryType::GpuOnly,
                data: None,
            })?;
        }
        // create constant buffer
        {
            self.constants_buffer = self.device.create_buffer(&BufferCreateInfo {
                btype: BufferType::Uniform,
                size: std::mem::size_of::<GpuConstantBuffer>() as DeviceSize,
                memory_type: MemoryType::CpuToGpu,
                data: None,
            })?;
        }
        // create sampler
        {
            let create_info = SamplerCreateInfo {
                mag_filter: Filter::Linear,
                min_filter: Filter::Linear,
                mipmap_mode: SamplerMipmapMode::Nearest,
                address_mode_u: SamplerAddressMode::ClampToEdge,
                address_mode_v: SamplerAddressMode::ClampToEdge,
                address_mode_w: SamplerAddressMode::ClampToEdge,
                anisotropy: false,
                max_anisotropy: 0,
            };
            self.sampler = self.device.create_sampler(&create_info)?;
        }
        Ok(())
    }

    pub fn update_buffers(
        &mut self,
        shape_builder: &ShapeBuilder,
        canvas_size: Vec2,
    ) -> DeviceResult<()> {
        crate::profile_scope!();
        self.paint_commands.clear();
        let uniform_buffer = GpuConstantBuffer {
            canvas_width: canvas_size.x,
            canvas_height: canvas_size.y,
        };
        let prim_buffers = shape_builder.primitive_buffers();

        if prim_buffers.is_empty() {
            return Ok(());
        }
        update_buffer_typed(
            self.device.as_ref(),
            self.constants_buffer,
            0,
            &uniform_buffer,
        )?;
        debug_assert!(prim_buffers.len() <= 1);

        self.device.update_buffer(
            self.storage_buffer,
            0,
            prim_buffers[0].data_buffer.as_ptr(),
            prim_buffers[0].data_buffer_offset as DeviceSize,
        )?;
        self.device.update_buffer(
            self.index_buffer,
            0,
            prim_buffers[0].index_buffer.as_ptr() as *const u8,
            (prim_buffers[0].index_buffer.len() * 4) as DeviceSize,
        )?;
        self.device.update_buffer(
            self.clip_rect_buffer,
            0,
            shape_builder.clip_rect_data_buffer().as_ptr() as *const u8,
            (shape_builder.clip_rect_data_buffer().len() * std::mem::size_of::<ClipRectGPU>())
                as DeviceSize,
        )?;
        for cmd in &prim_buffers[0].commands {
            self.paint_commands.push(PaintCommand {
                buffer_index: 0,
                index_offset: cmd.offset,
                index_count: cmd.count,
                texture: cmd.texture.handle,
            });
        }

        Ok(())
    }

    pub fn draw(
        &mut self,
        canvas_size: Vec2,
        framebuffer: FramebufferHandle,
        clear_color: Color,
    ) -> DeviceResult<()> {
        crate::profile_scope!();
        let norm = clear_color.normalize();
        let clear_info = RenderPassClearInfo {
            values: [
                ClearValue::Color([norm.x, norm.y, norm.z, norm.w]),
                ClearValue::Color([norm.x, norm.y, norm.z, norm.w]),
                ClearValue::Color([norm.x, norm.y, norm.z, norm.w]),
                ClearValue::Color([norm.x, norm.y, norm.z, norm.w]),
                ClearValue::Color([norm.x, norm.y, norm.z, norm.w]),
                ClearValue::Color([norm.x, norm.y, norm.z, norm.w]),
                ClearValue::Color([norm.x, norm.y, norm.z, norm.w]),
                ClearValue::Color([norm.x, norm.y, norm.z, norm.w]),
            ],
        };

        let mut previous_dat_buffer = BufferHandle::default();
        let mut previous_index_buffer = BufferHandle::default();
        let mut previous_texture = TextureHandle::default();

        self.device.execute_commands(
            &mut |cmd_buffer: &mut dyn CommandBuffer| -> Result<(), DeviceError> {
                cmd_buffer.begin_render_pass(self.render_pass, framebuffer, Some(clear_info))?;
                cmd_buffer.bind_pipeline(self.pipeline)?;

                cmd_buffer.set_viewport(
                    &Viewport {
                        x: 0.0,
                        y: 0.0,
                        w: canvas_size.x,
                        h: canvas_size.y,
                    },
                    false,
                )?;
                cmd_buffer.set_scissor_rect(&modus_threed::types::Rect {
                    x: 0,
                    y: 0,
                    w: canvas_size.x as u32,
                    h: canvas_size.y as u32,
                })?;

                // bind constants
                {
                    let uniform_desc_set_info = DescriptorSetUpdateInfo {
                        buffers: &[DescriptorBufferInput {
                            buffer: self.constants_buffer,
                            buffer_offset: 0,
                            buffer_size: None,
                            binding_point: 0,
                            dtype: DescriptorType::Constant,
                        }],
                        textures: &[],
                    };
                    let uniform_desc_set = self.descriptor_allocator.allocate(
                        self.device.as_ref(),
                        uniform_desc_set_info,
                        self.constants_descriptor_set_layout,
                    )?;

                    let descriptor_bind_info = [DescriptorSetBindInfo {
                        handle: uniform_desc_set,
                        set_index: 0,
                        dynamic_offsets: &[],
                    }];
                    cmd_buffer.bind_descriptors(self.pipeline, &descriptor_bind_info)?;
                }

                // bind clip rects
                {
                    let clip_rect_desc_info = DescriptorSetUpdateInfo {
                        buffers: &[DescriptorBufferInput {
                            buffer: self.clip_rect_buffer,
                            buffer_offset: 0,
                            buffer_size: None,
                            binding_point: 0,
                            dtype: DescriptorType::Storage,
                        }],
                        textures: &[],
                    };
                    let clip_rect_desc_set = self.descriptor_allocator.allocate(
                        self.device.as_ref(),
                        clip_rect_desc_info,
                        self.clip_rect_descriptor_set_layout,
                    )?;

                    let descriptor_bind_info = [DescriptorSetBindInfo {
                        handle: clip_rect_desc_set,
                        set_index: 3,
                        dynamic_offsets: &[],
                    }];
                    cmd_buffer.bind_descriptors(self.pipeline, &descriptor_bind_info)?;
                }

                let mut descriptor_bind_info = ArrayVec::<DescriptorSetBindInfo, 8>::new();
                for cmd in &self.paint_commands {
                    descriptor_bind_info.clear();
                    if previous_dat_buffer != self.storage_buffer {
                        previous_dat_buffer = self.storage_buffer;
                        assert_eq!(cmd.buffer_index, 0);
                        let storage_desc_set_info = DescriptorSetUpdateInfo {
                            buffers: &[DescriptorBufferInput {
                                buffer: self.storage_buffer,
                                buffer_offset: 0,
                                buffer_size: None,
                                binding_point: 0,
                                dtype: DescriptorType::Storage,
                            }],
                            textures: &[],
                        };
                        let storage_desc_set = self.descriptor_allocator.allocate(
                            self.device.as_ref(),
                            storage_desc_set_info,
                            self.data_descriptor_set_layout,
                        )?;
                        descriptor_bind_info.push(DescriptorSetBindInfo {
                            handle: storage_desc_set,
                            set_index: 1,
                            dynamic_offsets: &[],
                        });
                    }

                    if previous_texture != cmd.texture {
                        previous_texture = cmd.texture;
                        let texture_desc_set_info = DescriptorSetUpdateInfo {
                            buffers: &[],
                            textures: &[DescriptorTextureInput {
                                texture: cmd.texture,
                                sampler: self.sampler,
                                binding_point: 0,
                            }],
                        };

                        let texture_desc_set = self.descriptor_allocator.allocate(
                            self.device.as_ref(),
                            texture_desc_set_info,
                            self.texture_descriptor_set_layout,
                        )?;

                        descriptor_bind_info.push(DescriptorSetBindInfo {
                            handle: texture_desc_set,
                            set_index: 2,
                            dynamic_offsets: &[],
                        });
                    }
                    cmd_buffer.bind_descriptors(self.pipeline, &descriptor_bind_info)?;
                    if previous_index_buffer != self.index_buffer {
                        previous_index_buffer = self.index_buffer;
                        cmd_buffer.bind_index_buffer(self.index_buffer, 0, IndexType::U32)?;
                    }
                    cmd_buffer.draw_indexed(cmd.index_count, 1, cmd.index_offset, 0, 0)?;
                }
                cmd_buffer.end_render_pass()?;
                Ok(())
            },
        )
    }

    pub fn threed_device(&self) -> Arc<dyn Device> {
        self.device.clone()
    }
}

impl Drop for Painter {
    fn drop(&mut self) {
        self.descriptor_allocator
            .clear(self.device.as_ref())
            .unwrap();
        self.device
            .destroy_descriptor_set_layout(self.data_descriptor_set_layout)
            .unwrap();
        self.device
            .destroy_descriptor_set_layout(self.constants_descriptor_set_layout)
            .unwrap();
        self.device
            .destroy_descriptor_set_layout(self.clip_rect_descriptor_set_layout)
            .unwrap();
        self.device.destroy_pipeline(self.pipeline).unwrap();
        self.device.destroy_render_pass(self.render_pass).unwrap();
        self.device.destroy_buffer(self.constants_buffer).unwrap();
        self.device.destroy_buffer(self.index_buffer).unwrap();
        self.device.destroy_buffer(self.storage_buffer).unwrap();
        self.device.destroy_shader(self.shader_handles[0]).unwrap();
        self.device.destroy_shader(self.shader_handles[1]).unwrap();
        self.device.destroy_sampler(self.sampler).unwrap();
    }
}

#[derive(Debug)]
pub struct PainterTexture {
    device: Arc<dyn Device>,
    handle: TextureHandle,
    width: u32,
    height: u32,
    _layers: u8,
}

impl PartialEq for PainterTexture {
    fn eq(&self, other: &Self) -> bool {
        self.handle == other.handle
    }
}

impl Eq for PainterTexture {}

impl Drop for PainterTexture {
    fn drop(&mut self) {
        self.device.destroy_texture(self.handle).unwrap();
    }
}

impl PainterTexture {
    pub fn create_2d(
        painter: &Painter,
        width: u32,
        height: u32,
        gray_scale: bool,
    ) -> DeviceResult<Self> {
        Self::create_2d_array(painter, width, height, 1, gray_scale)
    }
    pub fn create_2d_array(
        painter: &Painter,
        width: u32,
        height: u32,
        layers: u8,
        gray_scale: bool,
    ) -> DeviceResult<Self> {
        let create_info = TextureCreateInfo {
            width,
            height,
            depth: 1,
            mip_map_levels: 1,
            sample_count: 1,
            array_count: layers,
            ttype: TextureType::T2D,
            format: if gray_scale {
                TextureFormat::R8
            } else {
                TextureFormat::R8G8B8A8
            },
            transient: false,
            usage_flags: TEXTURE_USAGE_FLAG_SHADER_INPUT,
        };
        let device = painter.device.clone();
        let texture = device.create_texture(&create_info, &[])?;

        Ok(PainterTexture {
            device,
            handle: texture,
            width,
            height,
            _layers: layers,
        })
    }

    pub fn update_bytes(&self, layer: u8, bytes: &[u8]) -> DeviceResult<()> {
        let image_data = TextureData {
            width: self.width,
            height: self.height,
            depth: 1,
            array_index: layer as u32,
            mip_map_level: 0,
            data: bytes,
        };
        self.device
            .update_texture(self.handle, std::slice::from_ref(&image_data))
    }
}
