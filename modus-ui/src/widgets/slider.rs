use crate::context::MouseState;
use crate::input::MouseButton;
use crate::view::View;
use crate::widgets::Widget;
use crate::Rect;
use std::ops::RangeInclusive;

#[derive(Debug)]
pub struct Slider<'a> {
    range: RangeInclusive<f32>,
    value: &'a mut f32,
    step: f32,
}

impl<'a> Slider<'a> {
    pub fn new(range: RangeInclusive<f32>, value: &'a mut f32) -> Self {
        Self {
            range,
            value,
            step: 1.0,
        }
    }

    pub fn step(mut self, value: f32) -> Self {
        debug_assert!(value < *self.range.end());
        debug_assert!(value >= 0.0);
        self.step = value.min(*self.range.end());
        self
    }
}

impl<'a> Widget<bool> for Slider<'a> {
    fn build(self, view: &mut View) -> bool {
        let mut value_changed = false;
        let id = view.allocate_id();

        let style = view.context().style();
        let min_size = style.min_element_size;

        let area_rect = view.allocate_rect(min_size);
        let clip_rect = view.context().shape_builder().clip_rect();
        if !clip_rect.intersects(area_rect) {
            return false;
        }

        // calculate rect
        let slider_relative_offset = *self.value / *self.range.end();
        let slider_bg_rect = area_rect.inset_y(style.element_padding);
        let slider_x = (area_rect.width - area_rect.height) * (slider_relative_offset as f32);
        let mut slider_button_rect = Rect {
            x: area_rect.x + slider_x,
            y: area_rect.y,
            width: area_rect.height,
            height: area_rect.height,
        };

        // Handle logic
        let context = view.context_mut();
        let mouse_state = context.handle_mouse(id, area_rect, MouseButton::Left);

        let slider_button_travel_distance_max = slider_bg_rect.width - slider_button_rect.width;
        let slider_button_half_width = slider_button_rect.width * 0.5;
        let slider_step_count = self.step / self.range.end();

        if mouse_state == MouseState::Down {
            let delta = context.input_state().mouse_position().x
                - (slider_button_rect.x + slider_button_half_width);
            let step_px = slider_step_count * slider_button_travel_distance_max;
            if delta.abs() >= step_px {
                let num_steps = (delta / step_px).round();
                let mut new_value = (*self.value) + self.step * num_steps;
                new_value = new_value.clamp(*self.range.start(), *self.range.end());
                if *self.value != new_value {
                    *self.value = new_value.clamp(*self.range.start(), *self.range.end());
                    value_changed = true;
                }
                slider_button_rect.x = (slider_button_rect.x + delta).clamp(
                    slider_bg_rect.x,
                    slider_bg_rect.x + slider_button_travel_distance_max,
                );
            }
        }

        // draw slider
        let bg_color = style.input_background_color;
        let border_color = style.border_color;
        let border_size = style.border_size;

        let slider_button_color = match mouse_state {
            MouseState::Hover => style.button_hover_color,
            MouseState::Down => style.button_down_color,
            MouseState::Up | MouseState::Noop => style.button_background_color,
        };

        let shape_builder = context.shape_builder_mut();
        shape_builder.add_rect(&slider_bg_rect, bg_color);
        shape_builder.stroke_rect(&slider_bg_rect, border_size, border_color);

        shape_builder.add_rect(&slider_button_rect, slider_button_color);
        shape_builder.stroke_rect(&slider_button_rect, border_size, border_color);

        value_changed
    }
}
