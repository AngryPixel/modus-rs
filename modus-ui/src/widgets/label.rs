use crate::text::layout::TextLayoutCalculator;
use crate::view::View;
use crate::widgets::Widget;
use crate::{Alignment, Rect, Vec2};
use modus_math::color::Color;

#[derive(Debug)]
pub struct Label<'a> {
    text: &'a str,
    color: Option<Color>,
    alignment: Alignment,
}

impl<'a> Label<'a> {
    pub fn new<S: AsRef<str>>(text: &'a S) -> Self {
        Self {
            text: text.as_ref(),
            color: None,
            alignment: Alignment::Left,
        }
    }

    pub fn color(mut self, color: Color) -> Self {
        self.color = Some(color);
        self
    }

    pub fn alignment(mut self, alignment: Alignment) -> Self {
        self.alignment = alignment;
        self
    }
}

impl<'a> Widget<()> for Label<'a> {
    fn build(self, view: &mut View) {
        let font = view.context().default_font();
        let layout_processor = TextLayoutCalculator {
            multi_line: false,
            tab_size: 4,
            max_width: None,
            max_height: None,
        };

        let text_dimensions = layout_processor.text_dimensions(&font, self.text);
        let rect = view.allocate_rect(text_dimensions);

        // check whether this component will be visible
        let clip_rect = view.context().shape_builder().clip_rect();
        if !clip_rect.intersects(rect) {
            return;
        }

        let aligned_rect = match self.alignment {
            Alignment::Left => rect,
            Alignment::Center => rect.center_inside(text_dimensions),
            Alignment::Right => Rect {
                x: if text_dimensions.x <= rect.width {
                    rect.x + (rect.width - text_dimensions.x)
                } else {
                    rect.x
                },
                y: rect.y,
                width: text_dimensions.x,
                height: text_dimensions.y,
            },
        };
        let context = view.context_mut();
        let color = self.color.unwrap_or(context.style().label_color);
        let shape_builder = context.shape_builder_mut();

        shape_builder.add_text(
            &font,
            Vec2 {
                x: aligned_rect.x,
                y: aligned_rect.y,
            },
            self.text,
            &layout_processor,
            color,
        );
    }
}
