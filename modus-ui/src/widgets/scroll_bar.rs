use crate::context::MouseState;
use crate::input::MouseButton;
use crate::view::View;
use crate::widgets::Widget;
use crate::{Rect, Vec2};

#[derive(Debug)]
pub struct ScrollBar<'a, const HORIZONTAL: bool> {
    scrollbar_ratio: f32,
    scroll_ratio: &'a mut f32,
    parent_rect: Rect,
}

impl<'a, const HORIZONTAL: bool> ScrollBar<'a, HORIZONTAL> {
    pub fn new(scrollbar_ratio: f32, scroll_offset: &'a mut f32, parent_rect: Rect) -> Self {
        Self {
            scrollbar_ratio,
            scroll_ratio: scroll_offset,
            parent_rect,
        }
    }
}

#[derive(Debug, Copy, Clone)]
pub struct ScrollBarResult {
    pub rect: Rect,
    pub updated: bool,
}

impl<'a> Widget<ScrollBarResult> for ScrollBar<'a, false> {
    fn build(self, view: &mut View) -> ScrollBarResult {
        let style = view.context().style();
        let id = view.allocate_id();

        let block_size = Vec2::new(
            style.interactive_min_size * 0.5,
            (self.scrollbar_ratio * self.parent_rect.height - style.element_padding * 2.0)
                .max(style.interactive_min_size),
        );

        let bg_rect = Rect::new_at(
            self.parent_rect.x + self.parent_rect.width - block_size.x,
            self.parent_rect.y + style.element_padding,
            block_size.x,
            self.parent_rect.height - (style.element_padding * 2.0),
        );

        let max_block_travel_distance = bg_rect.height - block_size.y;
        let block_offset = *self.scroll_ratio * max_block_travel_distance;
        let half_block_height = block_size.y * 0.5;

        let mut block_rect = Rect::new_at(
            bg_rect.x,
            bg_rect.y + block_offset,
            block_size.x,
            block_size.y,
        );

        let mouse_state = view
            .context_mut()
            .handle_mouse(id, bg_rect, MouseButton::Left);

        let updated = if mouse_state == MouseState::Down {
            let delta = view.context_mut().input_state().mouse_position().y
                - (block_rect.y + half_block_height);
            block_rect.y =
                (block_rect.y + delta).clamp(bg_rect.y, bg_rect.y + max_block_travel_distance);
            *self.scroll_ratio = (block_rect.y - bg_rect.y) / max_block_travel_distance;
            true
        } else {
            false
        };

        let block_color = match mouse_state {
            MouseState::Hover | MouseState::Down => style.button_hover_color,
            MouseState::Up | MouseState::Noop => style.button_background_color,
        };
        let border_size = style.border_size;
        let border_color = style.border_color;

        let painter = view.context_mut().shape_builder_mut();
        painter.add_rect(&bg_rect, style.input_background_color);
        painter.add_rect_with_border(&block_rect, block_color, border_size, border_color);
        ScrollBarResult {
            rect: bg_rect,
            updated,
        }
    }
}

pub type HScrollBar<'a> = ScrollBar<'a, true>;
pub type VScrollBar<'a> = ScrollBar<'a, false>;
