use crate::context::MouseState;
use crate::id::Id;
use crate::input::MouseButton;
use crate::text::layout::TextLayoutCalculator;
use crate::view::View;
use crate::widgets::Widget;
use crate::{Color, Vec2};

#[derive(Debug)]
pub struct Button<'a> {
    text: &'a str,
    background_color: Option<Color>,
    hover_color: Option<Color>,
    down_color: Option<Color>,
    label_color: Option<Color>,
    border_color: Option<Color>,
}

impl<'a> Button<'a> {
    pub fn new<S: AsRef<str>>(text: &'a S) -> Self {
        Self {
            text: text.as_ref(),
            background_color: None,
            hover_color: None,
            down_color: None,
            label_color: None,
            border_color: None,
        }
    }

    pub fn background_color(mut self, color: Color) -> Self {
        self.background_color = Some(color);
        self
    }

    pub fn hover_color(mut self, color: Color) -> Self {
        self.hover_color = Some(color);
        self
    }

    pub fn down_color(mut self, color: Color) -> Self {
        self.down_color = Some(color);
        self
    }

    pub fn label_color(mut self, color: Color) -> Self {
        self.label_color = Some(color);
        self
    }

    pub fn border_color(mut self, color: Color) -> Self {
        self.border_color = Some(color);
        self
    }
}

impl<'a> Widget<bool> for Button<'a> {
    fn build(self, view: &mut View) -> bool {
        let id = Id::with_parent(view.context().parent_id(), self.text);
        let mut result = false;

        let layout_processor = TextLayoutCalculator {
            multi_line: false,
            tab_size: 4,
            max_width: None,
            max_height: None,
        };

        let text_dimensions = {
            let context = view.context_mut();
            let font = context.default_font();
            layout_processor.text_dimensions(&font, self.text)
        };

        let style = view.context().style();
        let padding = style.element_padding;

        let dimensions = Vec2 {
            x: text_dimensions.x + padding * 2.0,
            y: text_dimensions.y + padding * 2.0,
        };

        let rect = view.allocate_rect(dimensions);

        // check whether this component will be visible
        let clip_rect = view.context().shape_builder().clip_rect();
        if !clip_rect.intersects(rect) {
            return false;
        }

        let context = view.context_mut();
        let font = context.default_font();

        let color = match context.handle_mouse(id, rect, MouseButton::Left) {
            MouseState::Hover => self.hover_color.unwrap_or(style.button_hover_color),
            MouseState::Down => self.down_color.unwrap_or(style.button_down_color),
            MouseState::Up => {
                result = true;
                self.background_color
                    .unwrap_or(style.button_background_color)
            }
            MouseState::Noop => self
                .background_color
                .unwrap_or(style.button_background_color),
        };

        // center text in rect
        let centered = rect.center_inside(text_dimensions);
        let label_color = self.label_color.unwrap_or(style.label_color);
        let border_color = self.border_color.unwrap_or(style.border_color);
        let border_size = style.border_size;

        {
            let shape_builder = context.shape_builder_mut();
            shape_builder.add_rect(&rect, color);
            shape_builder.stroke_rect(&rect, border_size, border_color);
            shape_builder.add_text(
                &font,
                Vec2 {
                    x: centered.x,
                    y: centered.y,
                },
                self.text,
                &layout_processor,
                label_color,
            );
        }
        result
    }
}
