use crate::input::{Key, MouseButton};
use crate::text::layout::TextLayoutCalculator;
use crate::view::View;
use crate::widgets::Widget;
use crate::{Rect, Vec2};

/// Single line text box
pub struct TextBox<'a> {
    str: &'a mut String,
}

impl<'a> TextBox<'a> {
    pub fn new(str: &'a mut String) -> Self {
        Self { str }
    }
}

#[derive(Debug, Copy, Clone)]
struct State {
    cursor_start: usize,
    cursor_end: usize,
}

impl State {
    fn new() -> Self {
        Self {
            cursor_start: 0,
            cursor_end: 0,
        }
    }
}

fn byte_index_for_char_index(s: &str, index: usize) -> usize {
    for (char_index, (byte_index, _)) in s.char_indices().enumerate() {
        if char_index == index {
            return byte_index;
        }
    }
    s.len()
}

impl<'a> Widget<()> for TextBox<'a> {
    fn build(self, view: &mut View) {
        let id = view.allocate_id();
        let style = view.context().style();
        let font = view.context().default_font();

        let request_size = Vec2::new(
            style.min_element_size.x,
            style
                .min_element_size
                .y
                .max(font.line_height() + (style.element_padding * 2.0)),
        );
        let rect = view.allocate_rect(request_size);
        let text_rect = rect.inset(style.element_padding);

        view.context_mut().handle_mouse(id, rect, MouseButton::Left);

        let is_focused = view.context().focused_item == id;

        if is_focused {
            let mut state = *view
                .context_mut()
                .id_scratch_buffer_mut()
                .get_or_set(id, State::new);
            if !view.context().was_previously_focused(id) {
                state = State::new();
            }
            let input_text = view.context().input_state().text();
            if !input_text.is_empty() {
                self.str.insert_str(state.cursor_start, input_text);
                state.cursor_start += input_text.len();
                state.cursor_end = state.cursor_start;
            }

            if view.context().input_state().is_key_down(Key::Ctrl)
                && view.context().input_state().is_key_pressed(Key::A)
            {
                state.cursor_start = 0;
                state.cursor_end = self.str.len();
            }

            if view.context().input_state().is_key_pressed(Key::Backspace) {
                let (mut min, max) = if state.cursor_start > state.cursor_end {
                    (state.cursor_end, state.cursor_start)
                } else {
                    (state.cursor_start, state.cursor_end)
                };

                if min == max && min > 0 {
                    min -= 1;
                }
                self.str.drain(
                    byte_index_for_char_index(self.str, min)
                        ..byte_index_for_char_index(self.str, max),
                );
                state.cursor_start = min;
                state.cursor_end = state.cursor_start;
            }

            if view.context().input_state().is_key_pressed(Key::ArrowLeft) {
                if state.cursor_start > 0 {
                    state.cursor_start -= 1;
                }
                if !view.context().input_state().is_key_down(Key::Shift) {
                    state.cursor_start = state.cursor_start.min(state.cursor_end);
                    state.cursor_end = state.cursor_start;
                }
            }

            if view.context().input_state().is_key_pressed(Key::ArrowRight) {
                if state.cursor_start < self.str.len() {
                    state.cursor_start += 1;
                }
                if !view.context().input_state().is_key_down(Key::Shift) {
                    state.cursor_start = state.cursor_start.max(state.cursor_end);
                    state.cursor_end = state.cursor_start;
                }
            }

            view.context_mut().id_scratch_buffer_mut().set(id, state);
        }

        let text_layout_calculator = TextLayoutCalculator {
            multi_line: false,
            tab_size: 4,
            max_width: Some(text_rect.width),
            max_height: Some(text_rect.height),
        };

        let text_layout_calculator_unbound = TextLayoutCalculator {
            multi_line: false,
            tab_size: 4,
            max_width: None,
            max_height: None,
        };

        let bg_color = style.input_background_color;
        let text_color = style.label_color;
        let border_color = if is_focused {
            style.focused_element_color
        } else {
            style.border_color
        };
        let border_size = style.border_size;

        {
            view.context_mut().shape_builder_mut().add_rect_with_border(
                &rect,
                bg_color,
                border_size,
                border_color,
            );
            // Draw selection
            if is_focused {
                // state is already set, should always be present at this point
                let state = *view.context().id_scratch_buffer().get::<State>(id).unwrap();
                if state.cursor_start != state.cursor_end {
                    let (min_index, max_index) = if state.cursor_start > state.cursor_end {
                        (state.cursor_end, state.cursor_start)
                    } else {
                        (state.cursor_start, state.cursor_end)
                    };
                    let dim_min = text_layout_calculator_unbound
                        .text_dimensions(&font, &self.str[0..min_index]);
                    let dim_max = text_layout_calculator_unbound
                        .text_dimensions(&font, &self.str[min_index..max_index]);

                    view.context_mut().shape_builder_mut().add_rect(
                        &Rect {
                            x: text_rect.x + dim_min.x,
                            y: text_rect.y,
                            width: dim_max.x,
                            height: font.line_height(),
                        },
                        style.focused_element_color,
                    );
                }
            }
            view.context_mut().shape_builder_mut().add_text(
                &font,
                text_rect.xy(),
                self.str,
                &text_layout_calculator,
                text_color,
            );
        }

        // draw caret
        if is_focused {
            // State is already set up by the checks above
            let state = *view.context().id_scratch_buffer().get::<State>(id).unwrap();
            // only draw caret if text is not fully selected
            if !(state.cursor_start == 0 && state.cursor_end == self.str.len()) {
                let text_slice = &self.str[0..self.str.len().min(state.cursor_start)];
                let slice_dim = text_layout_calculator_unbound.text_dimensions(&font, text_slice);
                let caret_rect = Rect::new_at(
                    text_rect.x + slice_dim.x,
                    text_rect.y,
                    2.0,
                    font.line_height(),
                );
                if view
                    .context()
                    .shape_builder()
                    .clip_rect()
                    .intersects(caret_rect)
                {
                    view.context_mut()
                        .shape_builder_mut()
                        .add_rect(&caret_rect, text_color);
                }
            }
        }
    }
}
