use crate::view::View;

mod button;
mod label;
mod scroll_bar;
mod slider;
mod text;

pub use button::Button;
pub use label::Label;
pub use scroll_bar::HScrollBar;
pub use scroll_bar::VScrollBar;
pub use slider::Slider;
pub use text::TextBox;

pub trait Widget<R> {
    fn build(self, view: &mut View) -> R;
}
