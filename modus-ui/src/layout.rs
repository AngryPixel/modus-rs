//! Collection of layouts
use crate::{Rect, Vec2};

#[derive(Debug, Copy, Clone, Eq, PartialEq, Hash)]
pub enum Mode {
    Horizontal,
    Vertical,
    Center,
}

#[derive(Debug, Copy, Clone, Eq, PartialEq, Hash)]
pub enum Direction {
    LeftToRight,
    RightToLeft,
    TopToBottom,
    BottomToTop,
}

impl Direction {
    pub fn is_horizontal(self) -> bool {
        matches!(self, Direction::LeftToRight | Direction::RightToLeft)
    }

    pub fn is_vertical(self) -> bool {
        matches!(self, Direction::TopToBottom | Direction::BottomToTop)
    }
}

#[derive(Debug, Copy, Clone, Eq, PartialEq, Hash)]
/// Define how an item should be allocated and in which direction.
/// If `wrap` is true, then once the directional area limits has been reached, we wrap
/// into a new line.
/// If `fill_main` is true we will consume the remaining size of the given direction for the next
/// allocation.
/// If `fill_alt` is true we will consume the remaining size of the alternate direction for the next
/// allocation. E.g.: if, main direction is on the x axis, alternate will be the y axis.
pub struct LayoutStrategy {
    pub(crate) direction: Direction,
    pub(crate) wrap: bool,
    pub(crate) fill_main: bool,
    pub(crate) fill_alt: bool,
}

impl Default for LayoutStrategy {
    fn default() -> Self {
        Self::horizontal()
    }
}

pub struct LayoutStrategyBuilder {
    l: LayoutStrategy,
}

impl LayoutStrategyBuilder {
    pub fn new(direction: Direction) -> Self {
        Self {
            l: LayoutStrategy {
                direction,
                wrap: false,
                fill_main: false,
                fill_alt: false,
            },
        }
    }

    pub fn from(l: LayoutStrategy) -> Self {
        Self { l }
    }

    pub fn wrap(mut self, v: bool) -> Self {
        self.l.wrap = v;
        self
    }

    pub fn fill(mut self, v: bool) -> Self {
        self.l.fill_main = v;
        self
    }

    pub fn fill_alt(mut self, v: bool) -> Self {
        self.l.fill_alt = v;
        self
    }

    pub fn direction(mut self, direction: Direction) -> Self {
        self.l.direction = direction;
        self
    }

    pub fn build(self) -> LayoutStrategy {
        self.l
    }
}

impl LayoutStrategy {
    pub fn horizontal() -> Self {
        Self {
            direction: Direction::LeftToRight,
            wrap: false,
            fill_main: false,
            fill_alt: false,
        }
    }

    pub fn horizontal_right_to_left() -> Self {
        Self {
            direction: Direction::RightToLeft,
            wrap: false,
            fill_main: false,
            fill_alt: false,
        }
    }

    pub fn vertical_bottom_to_top() -> Self {
        Self {
            direction: Direction::BottomToTop,
            wrap: false,
            fill_main: false,
            fill_alt: false,
        }
    }

    pub fn vertical() -> Self {
        Self {
            direction: Direction::TopToBottom,
            wrap: false,
            fill_main: false,
            fill_alt: false,
        }
    }
}

#[derive(Debug)]
pub struct LayoutAllocator {
    // Offset to be applied to allocation results
    offset: Vec2,
    // allocation area
    area: Rect,
    // allocation cursor
    cursor: Rect,
    spacing: f32,
    // Minimum element size
    min_size: Vec2,
    // Maximum element size,
    max_size: Vec2,
}

impl LayoutAllocator {
    pub fn new(area: Rect, spacing: f32) -> Self {
        Self {
            offset: Vec2::zero(),
            area,
            cursor: Rect::zero(),
            spacing,
            min_size: Vec2::zero(),
            max_size: Vec2 {
                x: f32::MAX - spacing * 2.0,
                y: f32::MAX - spacing * 2.0,
            },
        }
    }

    #[inline(always)]
    pub fn set_xy(&mut self, xy: Vec2) {
        self.area.x = xy.x;
        self.area.y = xy.y;
    }

    #[inline(always)]
    pub fn cursor(&self) -> Rect {
        self.cursor
    }

    #[inline(always)]
    pub fn set_cursor(&mut self, rect: Rect) {
        self.cursor = rect;
    }

    #[inline(always)]
    pub fn set_min_size(&mut self, min_size: Vec2) {
        self.min_size = min_size;
    }

    #[inline(always)]
    pub fn min_size(&self) -> Vec2 {
        self.min_size
    }

    #[inline(always)]
    pub fn set_max_size(&mut self, max_size: Vec2) {
        self.max_size = max_size;
        self.max_size.x -= self.spacing * 2.0;
        self.max_size.y -= self.spacing * 2.0;
    }

    #[inline(always)]
    pub fn max_size(&self) -> Vec2 {
        self.max_size
    }

    #[inline(always)]
    pub fn spacing(&self) -> f32 {
        self.spacing
    }

    #[inline(always)]
    pub fn set_offset(&mut self, offset: Vec2) {
        self.offset = offset;
    }

    #[inline(always)]
    pub fn area(&self) -> Rect {
        self.area
    }

    #[inline(always)]
    pub fn advance_cursor(&mut self, offset: Vec2) {
        self.cursor.x += offset.x;
        self.cursor.y += offset.y;
    }

    #[inline(always)]
    pub fn advance_cursor_x(&mut self, offset: f32) {
        self.cursor.x += offset;
    }

    #[inline(always)]
    pub fn advance_cursor_y(&mut self, offset: f32) {
        self.cursor.y += offset;
    }
}

impl LayoutAllocator {
    fn wrap_x(&mut self, layout: &LayoutStrategy) {
        if layout.wrap && self.cursor.x + self.spacing >= self.area.width {
            self.cursor.x = 0.0;
            self.cursor.y = self.cursor.height + self.spacing;
            self.cursor.width = 0.0;
            self.cursor.height = 0.0;
        }
    }

    fn wrap_y(&mut self, layout: &LayoutStrategy) {
        if layout.wrap && self.cursor.y + self.spacing >= self.area.height {
            self.cursor.x = self.cursor.width + self.spacing;
            self.cursor.y = 0.0;
            self.cursor.width = 0.0;
            self.cursor.height = 0.0;
        }
    }

    fn fill_size_x(&self, size: f32, layout: &LayoutStrategy) -> f32 {
        if (layout.fill_main && layout.direction.is_horizontal())
            || (layout.direction.is_vertical() && layout.fill_alt)
        {
            self.area.width - (self.cursor.x + self.spacing * 2.0)
        } else {
            size
        }
    }

    fn fill_size_y(&self, size: f32, layout: &LayoutStrategy) -> f32 {
        if (layout.fill_main && layout.direction.is_vertical())
            || (layout.direction.is_horizontal() && layout.fill_alt)
        {
            self.area.height - (self.cursor.y + self.spacing * 2.0)
        } else {
            size
        }
    }

    pub fn allocate(&mut self, layout: &LayoutStrategy, area_size: Vec2) -> Rect {
        let size = Vec2 {
            x: self
                .fill_size_x(
                    area_size.x.max(self.min_size.x).min(self.max_size.x),
                    layout,
                )
                .min(self.max_size.x),
            y: self.fill_size_y(
                area_size.y.max(self.min_size.y).min(self.max_size.y),
                layout,
            ),
        };

        self.cursor.width = self.cursor.width.max(size.x);
        self.cursor.height = self.cursor.height.max(size.y);
        let mut rect = match layout.direction {
            Direction::LeftToRight => {
                let result = Rect {
                    x: self.cursor.x + self.area.x + self.spacing,
                    y: self.cursor.y + self.area.y + self.spacing,
                    width: size.x,
                    height: size.y,
                };
                self.cursor.x += size.x + self.spacing;
                self.wrap_x(layout);
                result
            }
            Direction::RightToLeft => {
                let result = Rect {
                    x: self.area.x + self.area.width - self.cursor.x - size.x - self.spacing,
                    y: self.cursor.y + self.area.y + self.spacing,
                    width: size.x,
                    height: size.y,
                };
                self.cursor.x += size.x + self.spacing;
                self.wrap_x(layout);
                result
            }
            Direction::TopToBottom => {
                let result = Rect {
                    x: self.cursor.x + self.area.x + self.spacing,
                    y: self.cursor.y + self.area.y + self.spacing,
                    width: size.x,
                    height: size.y,
                };
                self.cursor.y += size.y + self.spacing;
                self.wrap_y(layout);
                result
            }
            Direction::BottomToTop => {
                let result = Rect {
                    x: self.cursor.x + self.area.x + self.spacing,
                    y: self.area.height + self.area.y - self.cursor.y - size.y - self.spacing,
                    width: size.x,
                    height: size.y,
                };
                self.cursor.y += size.y + self.spacing;
                self.wrap_y(layout);
                result
            }
        };

        rect.x += self.offset.x;
        rect.y += self.offset.y;
        rect
    }
}
