//! Immediate Mode GUI implementation

pub mod context;
pub mod id;
pub mod input;
pub mod layout;
pub mod painter;
pub mod shape_builder;
pub mod style;
pub mod text;
pub mod texture_atlas;
pub mod view;
pub mod views;
pub mod widgets;

pub type Color = modus_math::color::Color;

#[derive(Debug, Copy, Clone)]
pub struct Rect {
    pub x: f32,
    pub y: f32,
    pub width: f32,
    pub height: f32,
}

impl Rect {
    #[inline(always)]
    pub fn zero() -> Self {
        Self {
            x: 0.0,
            y: 0.0,
            width: 0.0,
            height: 0.0,
        }
    }

    #[inline(always)]
    pub fn new(width: f32, height: f32) -> Self {
        Self {
            x: 0.0,
            y: 0.0,
            width,
            height,
        }
    }

    #[inline(always)]
    pub fn new_at(x: f32, y: f32, width: f32, height: f32) -> Self {
        Self {
            x,
            y,
            width,
            height,
        }
    }

    #[inline(always)]
    pub fn inset(&self, amount: f32) -> Self {
        Self {
            x: self.x + amount,
            y: self.y + amount,
            width: self.width - amount * 2.0,
            height: self.height - amount * 2.0,
        }
    }

    #[inline(always)]
    pub fn inset_x(&self, amount: f32) -> Self {
        Self {
            x: self.x + amount,
            y: self.y,
            width: self.width - amount * 2.0,
            height: self.height,
        }
    }

    #[inline(always)]
    pub fn inset_y(&self, amount: f32) -> Self {
        Self {
            x: self.x,
            y: self.y + amount,
            width: self.width,
            height: self.height - amount * 2.0,
        }
    }

    #[inline(always)]
    pub fn xy(&self) -> Vec2 {
        Vec2::new(self.x, self.y)
    }

    #[inline(always)]
    pub fn size(&self) -> Vec2 {
        Vec2::new(self.width, self.height)
    }
}

#[derive(Debug, Copy, Clone)]
pub struct Vec2 {
    pub x: f32,
    pub y: f32,
}

impl Vec2 {
    pub fn zero() -> Self {
        Vec2 { x: 0.0, y: 0.0 }
    }

    pub fn max() -> Self {
        Vec2 {
            x: f32::MAX,
            y: f32::MAX,
        }
    }

    pub fn new(x: f32, y: f32) -> Self {
        Self { x, y }
    }
}

impl Rect {
    pub fn is_point_in_rect(&self, point: Vec2) -> bool {
        point.x >= self.x
            && point.y >= self.y
            && point.x <= self.x + self.width
            && point.y <= self.y + self.height
    }

    pub fn intersects(&self, rect: Rect) -> bool {
        //   l +----+
        //     |    |
        //     +----+ r
        let l1 = Vec2 {
            x: self.x,
            y: self.y,
        };
        let r1 = Vec2 {
            x: self.x + self.width,
            y: self.y + self.height,
        };
        let l2 = Vec2 {
            x: rect.x,
            y: rect.y,
        };
        let r2 = Vec2 {
            x: rect.x + rect.width,
            y: rect.y + rect.height,
        };

        if l1.x >= r2.x || l2.x >= r1.x {
            return false;
        }

        if l1.y >= r2.y || l2.y >= r1.y {
            return false;
        }
        true
    }

    /// Create a rect center inside the current rect based on `dimensions`. If the dimensions exceed
    /// the rect area, the new rect begins at the current rect's x and y.
    pub fn center_inside(&self, dimensions: Vec2) -> Rect {
        let x = if dimensions.x < self.width {
            (self.width - dimensions.x) / 2.0
        } else {
            0.0
        };

        let y = if dimensions.y < self.height {
            (self.height - dimensions.y) / 2.0
        } else {
            0.0
        };

        Rect {
            x: x + self.x,
            y: y + self.y,
            width: dimensions.x,
            height: dimensions.y,
        }
    }
}

#[derive(Debug, Copy, Clone, Eq, PartialEq, Hash)]
pub enum Alignment {
    Left,
    Center,
    Right,
}

pub(crate) const PROFILE_COLOR0: u32 = modus_profiler::COLOR_INDIGO700;
#[macro_export(crate)]
macro_rules! profile_scope {
    () => {
        modus_profiler::scope_color!(crate::PROFILE_COLOR0);
    };
    ($name:literal) => {
        modus_profiler::scope_color!(crate::PROFILE_COLOR0, $name);
    };
}
