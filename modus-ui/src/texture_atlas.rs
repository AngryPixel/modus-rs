use crate::painter::{Painter, PainterTexture};
use modus_core::texture_atlas::{AllocResult, TextureAtlas as MTextureAtlas};
use modus_threed::device::DeviceResult;
use std::cell::RefCell;
use std::rc::Rc;

const DEFAULT_ATLAS_PADDING_PX: usize = 2;

/// Texture atlas representation used by the UI
#[derive(Debug)]
pub struct TextureAtlas {
    atlas: RefCell<MTextureAtlas>,
    texture: Rc<PainterTexture>,
}

impl TextureAtlas {
    pub fn new_gray_scale(
        painter: &Painter,
        width: u32,
        height: u32,
        layers: u8,
    ) -> DeviceResult<Self> {
        let texture = PainterTexture::create_2d_array(painter, width, height, layers as u8, true)?;
        let atlas = MTextureAtlas::new(
            width,
            height,
            layers as u32,
            true,
            DEFAULT_ATLAS_PADDING_PX as u32,
        );
        Ok(Self {
            atlas: RefCell::new(atlas),
            texture: Rc::new(texture),
        })
    }

    #[inline(always)]
    pub fn width(&self) -> u32 {
        self.atlas.borrow().width() as u32
    }

    #[inline(always)]
    pub fn height(&self) -> u32 {
        self.atlas.borrow().height() as u32
    }

    #[inline(always)]
    pub fn layers(&self) -> u32 {
        self.atlas.borrow().layers() as u32
    }

    #[inline(always)]
    pub fn color_channels(&self) -> u32 {
        self.atlas.borrow().color_channels()
    }

    #[inline(always)]
    pub fn allocate(&self, width: u32, height: u32) -> Option<AllocResult> {
        self.atlas.borrow_mut().allocate_slot(width, height)
    }

    #[inline(always)]
    pub fn write_at_gray_scale(&self, x: u32, y: u32, l: u8, pixel: u8) {
        self.atlas
            .borrow_mut()
            .write_at_gray_scale(x, y, l as u32, pixel);
    }

    #[inline(always)]
    pub fn write_at_color(&self, x: u32, y: u32, l: u8, pixel: impl Into<[u8; 4]>) {
        self.atlas
            .borrow_mut()
            .write_at_color(x, y, l as u32, pixel.into())
    }

    pub fn upload_to_painter(&self) -> DeviceResult<()> {
        let mut atlas = self.atlas.borrow_mut();
        if atlas.is_dirty() {
            for (index, layer) in atlas.atlas_layers().iter().enumerate() {
                if layer.dirty {
                    self.texture.update_bytes(index as u8, &layer.data)?;
                }
            }
            atlas.reset_dirty_state();
        }
        Ok(())
    }

    #[inline(always)]
    pub fn painter_texture(&self) -> Rc<PainterTexture> {
        self.texture.clone()
    }

    pub fn with_layer_data(&self, layer: u8, f: impl FnOnce(&[u8])) {
        f(self.atlas.borrow().layer_data(layer as u32))
    }
}
