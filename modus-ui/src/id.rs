use modus_core::HashMap;
use std::any::{Any, TypeId};
use std::collections::hash_map::Entry;
use std::fmt::{Display, Formatter};
use std::hash::Hasher;

/// UI Identifier
#[derive(Debug, Copy, Clone, Eq, PartialEq, Hash)]
pub struct Id(u64);

impl Id {
    pub fn invalid() -> Self {
        Self(0)
    }

    pub fn new(source: impl std::hash::Hash) -> Self {
        let mut hasher = modus_core::DefaultHasher::new_with_keys(123, 57);
        source.hash(&mut hasher);
        Self(hasher.finish())
    }

    pub fn with_parent(parent: Id, source: impl std::hash::Hash) -> Self {
        let mut hasher = modus_core::DefaultHasher::new_with_keys(123, 57);
        hasher.write_u64(parent.0);
        source.hash(&mut hasher);
        Self(hasher.finish())
    }

    #[inline(always)]
    pub fn is_valid(&self) -> bool {
        self.0 != 0
    }

    #[inline(always)]
    pub fn value(&self) -> u64 {
        self.0
    }
}

impl Display for Id {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        write!(f, "ID:{}", self.0)
    }
}

// Idea taken from the `nohash_hasher` crate.
#[derive(Default)]
pub struct IdHasher(u64);

impl std::hash::Hasher for IdHasher {
    fn write(&mut self, _: &[u8]) {
        unreachable!("Invalid use of IdHasher");
    }

    fn write_u8(&mut self, _n: u8) {
        unreachable!("Invalid use of IdHasher");
    }
    fn write_u16(&mut self, _n: u16) {
        unreachable!("Invalid use of IdHasher");
    }
    fn write_u32(&mut self, _n: u32) {
        unreachable!("Invalid use of IdHasher");
    }

    #[inline(always)]
    fn write_u64(&mut self, n: u64) {
        self.0 = n;
    }

    fn write_usize(&mut self, _n: usize) {
        unreachable!("Invalid use of IdHasher");
    }

    fn write_i8(&mut self, _n: i8) {
        unreachable!("Invalid use of IdHasher");
    }
    fn write_i16(&mut self, _n: i16) {
        unreachable!("Invalid use of IdHasher");
    }
    fn write_i32(&mut self, _n: i32) {
        unreachable!("Invalid use of IdHasher");
    }
    fn write_i64(&mut self, _n: i64) {
        unreachable!("Invalid use of IdHasher");
    }
    fn write_isize(&mut self, _n: isize) {
        unreachable!("Invalid use of IdHasher");
    }

    #[inline(always)]
    fn finish(&self) -> u64 {
        self.0
    }
}

#[derive(Copy, Clone, Debug, Default)]
pub struct BuildIdHasher {}

impl std::hash::BuildHasher for BuildIdHasher {
    type Hasher = IdHasher;

    #[inline(always)]
    fn build_hasher(&self) -> IdHasher {
        IdHasher::default()
    }
}

pub type IdHashMap<V> = HashMap<Id, V, BuildIdHasher>;
pub trait IdStateTrait: Any + Clone {}
impl<T: Any + Clone> IdStateTrait for T {}

#[derive(Debug)]
struct IdState {
    data: Box<dyn Any>,
    access_counter: i32,
}

impl IdState {
    fn new<T: IdStateTrait>(value: T) -> Self {
        Self {
            data: Box::new(value),
            access_counter: 1,
        }
    }

    #[inline]
    fn get_type_id(&self) -> TypeId {
        self.data.type_id()
    }

    #[inline]
    fn get_mut<T: IdStateTrait>(&mut self) -> Option<&mut T> {
        let r = self.data.downcast_mut();
        if r.is_some() {
            self.access_counter += 1;
        }
        r
    }

    #[inline]
    fn get<T: IdStateTrait>(&self) -> Option<&T> {
        self.data.downcast_ref()
    }

    fn get_mut_or_set_with<T: IdStateTrait>(&mut self, set_with: impl FnOnce() -> T) -> &mut T {
        if !self.data.is::<T>() {
            debug_assert!(false, "Id collision, we are overwriting old widget data");
            *self = Self::new(set_with());
        }
        self.access_counter += 1;
        self.data.downcast_mut().unwrap() // This will never panic because the types are the same
    }
}

#[derive(Debug)]
pub struct IdStateMap {
    map: IdHashMap<IdState>,
}

impl Default for IdStateMap {
    fn default() -> Self {
        Self::new()
    }
}

impl IdStateMap {
    pub fn new() -> Self {
        let mut r = Self {
            map: IdHashMap::with_hasher(BuildIdHasher {}),
        };
        r.map.reserve(64);
        r
    }

    pub fn erase_stale_elements(&mut self) {
        // decrement last access counter and if <= 0 erase item from the map
        self.map.retain(|_, v| {
            v.access_counter -= 1;
            v.access_counter > 0
        });
    }

    pub fn get_mut_or_insert_with<T: IdStateTrait>(
        &mut self,
        id: Id,
        or_insert_fn: impl FnOnce() -> T,
    ) -> &mut T {
        match self.map.entry(id) {
            Entry::Occupied(o) => o.into_mut().get_mut_or_set_with(or_insert_fn),
            Entry::Vacant(v) => {
                v.insert(IdState::new(or_insert_fn())).get_mut().unwrap() // will never panic
            }
        }
    }

    pub fn insert<T: IdStateTrait>(&mut self, id: Id, value: T) {
        self.map.insert(id, IdState::new(value));
    }

    pub fn get<T: IdStateTrait>(&self, id: Id) -> Option<&T> {
        self.map.get(&id)?.get()
    }

    pub fn get_mut<T: IdStateTrait>(&mut self, id: Id) -> Option<&mut T> {
        self.map.get_mut(&id)?.get_mut()
    }

    #[inline]
    pub fn is_empty(&self) -> bool {
        self.map.is_empty()
    }

    pub fn clear(&mut self) {
        self.map.clear();
    }

    pub fn remove(&mut self, id: Id) {
        self.map.remove(&id);
    }

    pub fn remove_by_type<T: IdStateTrait>(&mut self) {
        let type_id = TypeId::of::<T>();
        self.map.retain(|_, v| v.get_type_id() != type_id);
    }
}

const ID_SCRATCH_BUFFER_SIZE: usize = 1024 * 1024;
/// A buffer that can be used by any widget to write data to while the item in question is
/// focused. E.g.: Text Box cursor data.
/// As soon as the focused widget changes, the old widget data is erased and replaced with the new
/// widget data. This is useful for widgets that do not require to persist state when not in
/// focus.
#[derive(Debug)]
pub struct IdScratchBuffer {
    id: Id,
    #[cfg(any(test, debug_assertions))]
    stored_type_id: Option<TypeId>,
    data: Box<[u8; ID_SCRATCH_BUFFER_SIZE]>,
}

impl Default for IdScratchBuffer {
    fn default() -> Self {
        Self::new()
    }
}

impl IdScratchBuffer {
    pub fn new() -> Self {
        Self {
            id: Id::invalid(),
            data: Box::new([0; ID_SCRATCH_BUFFER_SIZE]),
            #[cfg(any(test, debug_assertions))]
            stored_type_id: None,
        }
    }
}

impl IdScratchBuffer {
    #[inline(always)]
    fn data_as_ref<T: 'static>(&self) -> &'_ T {
        #[cfg(any(test, debug_assertions))]
        {
            debug_assert_eq!(*self.stored_type_id.as_ref().unwrap(), TypeId::of::<T>());
        }
        let ptr = self.data.as_ptr();
        let alignment = std::mem::align_of::<T>();
        let alignment_offset = ptr.align_offset(alignment);
        assert!(
            alignment_offset + std::mem::size_of::<T>() < ID_SCRATCH_BUFFER_SIZE,
            "Type exceeds buffer capacity"
        );
        unsafe { &*(ptr.add(alignment_offset) as *const T) }
    }

    #[inline(always)]
    fn data_as_mut<T: 'static>(&mut self) -> &'_ mut T {
        #[cfg(any(test, debug_assertions))]
        {
            debug_assert_eq!(*self.stored_type_id.as_ref().unwrap(), TypeId::of::<T>());
        }
        let ptr = self.data.as_mut_ptr();
        let alignment = std::mem::align_of::<T>();
        let alignment_offset = ptr.align_offset(alignment);
        assert!(
            alignment_offset + std::mem::size_of::<T>() < ID_SCRATCH_BUFFER_SIZE,
            "Type exceeds buffer capacity"
        );
        unsafe { &mut *(ptr.add(alignment_offset) as *mut T) }
    }

    fn store<T: 'static>(&mut self, value: T) -> &'_ mut T {
        let ptr = self.data.as_mut_ptr();
        let alignment = std::mem::align_of::<T>();
        let alignment_offset = ptr.align_offset(alignment);
        assert!(
            alignment_offset + std::mem::size_of::<T>() < ID_SCRATCH_BUFFER_SIZE,
            "Type exceeds buffer capacity"
        );
        #[cfg(any(test, debug_assertions))]
        {
            self.stored_type_id = Some(TypeId::of::<T>());
        }
        unsafe {
            let adjusted_ptr = ptr.add(alignment_offset) as *mut T;
            std::ptr::write(adjusted_ptr, value);
            &mut *adjusted_ptr
        }
    }

    #[inline(always)]
    pub fn active_id(&self) -> Id {
        self.id
    }

    #[cfg(any(test, debug_assertions))]
    pub fn is_same_storage_type<T: 'static>(&self) -> bool {
        if let Some(id) = self.stored_type_id {
            id == TypeId::of::<T>()
        } else {
            false
        }
    }

    pub fn get<T: 'static>(&self, id: Id) -> Option<&'_ T> {
        if self.id == id {
            Some(self.data_as_ref())
        } else {
            None
        }
    }

    pub fn get_mut<T: 'static>(&mut self, id: Id) -> Option<&'_ mut T> {
        if self.id == id {
            Some(self.data_as_mut())
        } else {
            None
        }
    }

    pub fn get_or_set<T: 'static>(&mut self, id: Id, new_value: impl FnOnce() -> T) -> &'_ mut T {
        assert!(
            !std::mem::needs_drop::<T>(),
            "You can't use this with droppable types"
        );

        if self.id == id {
            self.data_as_mut()
        } else {
            self.id = id;
            self.store(new_value())
        }
    }

    pub fn set<T: 'static>(&mut self, id: Id, value: T) {
        self.id = id;
        self.store(value);
    }
}

#[cfg(test)]
mod test {
    use super::*;
    use crate::id::{Id, IdStateMap};

    #[test]
    fn different_state_access() {
        let v1 = 20u32;

        let mut map = IdStateMap::new();
        let id1 = Id::new(&v1);

        map.insert(id1, v1);
        assert!(map.get_mut::<u32>(id1).is_some());
        assert_eq!(*map.get_mut::<u32>(id1).unwrap(), v1);
        assert!(map.get_mut::<f32>(id1).is_none());

        map.erase_stale_elements();
        assert_eq!(map.is_empty(), false);
        map.erase_stale_elements();
        assert_eq!(map.is_empty(), false);
        map.erase_stale_elements();
        assert_eq!(map.is_empty(), true);
    }

    #[test]
    fn test_id_scratch_buffer() {
        let mut sb = IdScratchBuffer::new();
        let id1 = Id::new("Foo");
        let id2 = Id::new("Bar");

        sb.set::<u32>(id1, 20);
        assert_eq!(sb.get::<f64>(id2), None);
        assert!(sb.is_same_storage_type::<u32>());
        sb.get_or_set(id2, || 45.0);
        assert!(sb.get::<f64>(id2).is_some());
        assert_eq!(sb.get::<u32>(id1), None);
        assert!(sb.is_same_storage_type::<f64>());
    }
}
