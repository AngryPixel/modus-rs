//! generates text draw commands for the layout

use crate::text::font::{ScaledFontInstance, ScaledGlyphInfo};
use crate::{Rect, Vec2};

#[derive(Debug)]
pub struct TextLayoutCalculator {
    pub max_width: Option<f32>,
    pub max_height: Option<f32>,
    pub tab_size: u32,
    pub multi_line: bool,
}

impl TextLayoutCalculator {
    pub fn new() -> Self {
        Self {
            max_width: None,
            max_height: None,
            tab_size: 4,
            multi_line: false,
        }
    }

    pub fn layout(
        &self,
        font: &ScaledFontInstance,
        text: &str,
        point: Vec2,
        mut emitter: impl FnMut(&ScaledGlyphInfo, Vec2),
    ) -> Rect {
        let mut current_loc = Vec2 { x: 0.0, y: 0.0 };

        let max_x = self.max_width.unwrap_or(f32::MAX);
        let max_y = self.max_height.unwrap_or(f32::MAX);
        let mut result = Rect {
            x: point.x,
            y: point.y,
            width: 0.0,
            height: font.line_height(),
        };

        let space_advance = font.glyph_info(' ').unwrap().advance_x();
        for ch in text.chars() {
            match ch {
                '\n' => {
                    if self.multi_line {
                        result.width = result.width.max(current_loc.x - result.x);
                        current_loc.x = 0.0;
                        current_loc.y += font.line_height();
                        result.height += font.line_height();
                        continue;
                    } else {
                        return result;
                    }
                }
                '\t' => {
                    current_loc.x += self.tab_size as f32 * space_advance;
                    continue;
                }
                ' ' => {
                    current_loc.x += space_advance;
                    continue;
                }
                _ => {}
            };

            if current_loc.x >= max_x {
                if !self.multi_line {
                    return result;
                }
                result.width = result.width.max(current_loc.x - result.x);
                current_loc.x = 0.0;
                current_loc.y += font.line_height();
                result.height += font.line_height();
            }
            if current_loc.y >= max_y {
                return result;
            }

            let glyph_info = if let Some(g) = font.glyph_info(ch) {
                g
            } else {
                font.unknown_sym_glyph_info()
            };
            let bearing = glyph_info.bearing();
            let glyph_location = Vec2 {
                x: point.x + current_loc.x + bearing.x,
                y: point.y + current_loc.y + bearing.y,
            };
            emitter(&glyph_info, glyph_location);
            current_loc.x += glyph_info.advance_x();
        }

        result.width = result.width.max(current_loc.x - result.x);
        result
    }

    pub fn text_dimensions(&self, font: &ScaledFontInstance, text: &str) -> Vec2 {
        let rect = self.layout(font, text, Vec2::zero(), |_, _| {});
        Vec2 {
            x: rect.width,
            y: rect.height,
        }
    }
}

impl Default for TextLayoutCalculator {
    fn default() -> Self {
        Self::new()
    }
}
