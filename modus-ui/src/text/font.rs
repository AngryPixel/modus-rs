use crate::painter::PainterTexture;
use crate::texture_atlas::TextureAtlas;
use crate::Vec2;
use ab_glyph::ScaleFont;
use modus_core::HashMap;
use std::collections::hash_map::Entry;
use std::rc::Rc;

#[derive(Debug, Copy, Clone)]
pub struct GlyphInfo {
    pub width: u32,
    pub height: u32,
    pub bearing: Vec2,
    pub advance_x: f32,
    pub uv_top_left: Vec2,
    pub uv_bottom_right: Vec2,
    pub uv_z: f32,
}

impl Default for GlyphInfo {
    fn default() -> Self {
        Self {
            width: 0,
            height: 0,
            bearing: Vec2 { x: 0.0, y: 0.0 },
            advance_x: 0.0,
            uv_top_left: Vec2 { x: 0.0, y: 0.0 },
            uv_bottom_right: Vec2 { x: 0.0, y: 0.0 },
            uv_z: 0.0,
        }
    }
}

pub struct ScaledGlyphInfo<'a> {
    glyph_info: &'a GlyphInfo,
    scale: f32,
}

impl<'a> ScaledGlyphInfo<'a> {
    #[inline(always)]
    pub fn width(&self) -> f32 {
        self.glyph_info.width as f32 * self.scale
    }

    #[inline(always)]
    pub fn height(&self) -> f32 {
        self.glyph_info.height as f32 * self.scale
    }

    #[inline(always)]
    pub fn advance_x(&self) -> f32 {
        self.glyph_info.advance_x * self.scale
    }

    #[inline(always)]
    pub fn bearing(&self) -> Vec2 {
        Vec2 {
            x: self.glyph_info.bearing.x * self.scale,
            y: self.glyph_info.bearing.y * self.scale,
        }
    }

    #[inline(always)]
    pub fn uv_top_left(&self) -> Vec2 {
        self.glyph_info.uv_top_left
    }

    #[inline(always)]
    pub fn uv_bottom_right(&self) -> Vec2 {
        self.glyph_info.uv_bottom_right
    }

    #[inline(always)]
    pub fn uv_z(&self) -> f32 {
        self.glyph_info.uv_z
    }
}

#[derive(Debug, Copy, Clone, Eq, PartialEq)]
pub enum FontError {
    InvalidFont,
    Duplicate,
    AtlasFailure,
}

#[derive(Debug)]
pub struct Font {
    ab_glyph_font: ab_glyph::FontVec,
    characters: HashMap<char, ab_glyph::GlyphId>,
}

impl Font {
    pub fn new(data: Vec<u8>) -> Result<Font, FontError> {
        use ab_glyph::Font as _;
        let ab_glyph_font = match ab_glyph::FontVec::try_from_vec(data) {
            Ok(g) => g,
            Err(_) => return Err(FontError::InvalidFont),
        };
        let mut supported_chars = HashMap::with_capacity(ab_glyph_font.glyph_count());
        for (glyph_id, char) in ab_glyph_font.codepoint_ids() {
            supported_chars.insert(char, glyph_id);
        }

        Ok(Font {
            ab_glyph_font,
            characters: supported_chars,
        })
    }
}

#[derive(Debug)]
pub(crate) struct FontInstance {
    font: Rc<Font>,
    glyph_table: HashMap<char, GlyphInfo>,
    size_px: u32,
    atlas: Rc<TextureAtlas>,
    unknown_char: char,
    line_height: f32,
}

const DEFAULT_UNKNOWN_CHAR_SYM: char = '\u{2337}';
const DEFAULT_UNKNOWN_CHAR_SYM_FALLBACK: char = '?';

impl FontInstance {
    pub fn new(font: Rc<Font>, atlas: Rc<TextureAtlas>, size_px: u32) -> Result<Self, FontError> {
        use ab_glyph::Font as _;
        let unknown_char = if font.characters.contains_key(&DEFAULT_UNKNOWN_CHAR_SYM) {
            DEFAULT_UNKNOWN_CHAR_SYM
        } else {
            DEFAULT_UNKNOWN_CHAR_SYM_FALLBACK
        };
        let scaled_info = font.ab_glyph_font.as_scaled(size_px as f32);
        let mut instance = Self {
            line_height: scaled_info.line_gap() + scaled_info.height(),
            font,
            glyph_table: HashMap::with_capacity(255),
            size_px,
            atlas,
            unknown_char,
        };
        instance.build_ascii_range();
        if !instance.build_atlas() {
            return Err(FontError::AtlasFailure);
        }
        Ok(instance)
    }

    fn load_char(&mut self, ch: char) {
        use ab_glyph::Font as _;
        let scaled_info = self.font.ab_glyph_font.as_scaled(self.size_px as f32);
        if let Some(glyph_id) = self.font.characters.get(&ch) {
            let glyph = glyph_id
                .with_scale_and_position(self.size_px as f32, ab_glyph::Point { x: 0.0, y: 0.0 });
            let glyph_info = self.font.ab_glyph_font.outline_glyph(glyph).map(|glyph| {
                let bounds = glyph.px_bounds();
                let width = bounds.width() as u32;
                let height = bounds.height() as u32;
                if width == 0 || height == 0 {
                    return GlyphInfo::default();
                }

                GlyphInfo {
                    width,
                    height,
                    bearing: Vec2 {
                        x: bounds.min.x,
                        // correct Y to start as close to the origin point as possible
                        y: bounds.min.y + scaled_info.height() - scaled_info.line_gap(),
                    },
                    advance_x: scaled_info.h_advance(*glyph_id),
                    uv_top_left: Vec2 { x: 0.0, y: 0.0 },
                    uv_bottom_right: Vec2 { x: 0.0, y: 0.0 },
                    uv_z: 0.0,
                }
            });
            let glyph_info = glyph_info.unwrap_or_default();
            self.glyph_table.insert(ch, glyph_info);
        }
    }

    fn load_space_char(&mut self) {
        use ab_glyph::Font as _;
        let scaled_info = self.font.ab_glyph_font.as_scaled(self.size_px as f32);
        let ch: char = 32_u8 as char;
        let glyph_id = self
            .font
            .characters
            .get(&ch)
            .expect("Space should always be available");
        self.glyph_table.insert(
            ch,
            GlyphInfo {
                width: 0,
                height: 0,
                bearing: Vec2::zero(),
                advance_x: scaled_info.h_advance(*glyph_id),
                uv_top_left: Vec2::zero(),
                uv_bottom_right: Vec2::zero(),
                uv_z: 0.0,
            },
        );
    }

    fn build_ascii_range(&mut self) {
        self.load_space_char();
        for i in 33..127 {
            let ch = i as u8 as char;
            self.load_char(ch);
        }
        self.load_char(self.unknown_char);
    }

    fn build_atlas(&mut self) -> bool {
        use ab_glyph::Font as _;
        for (ch, glyph_info) in &mut self.glyph_table {
            if let Some(glyph_id) = self.font.characters.get(ch) {
                let glyph = glyph_id.with_scale_and_position(
                    self.size_px as f32,
                    ab_glyph::Point { x: 0.0, y: 0.0 },
                );

                if let Some(glyph) = self.font.ab_glyph_font.outline_glyph(glyph) {
                    let width = glyph_info.width as usize;
                    let height = glyph_info.height as usize;
                    if width == 0 || height == 0 {
                        continue;
                    }
                    let alloc_result = match self.atlas.allocate(width as u32, height as u32) {
                        Some(r) => r,
                        None => return false,
                    };

                    glyph.draw(|x, y, v| {
                        let value = (v * 255.0).clamp(0.0, 255.0) as u8;
                        self.atlas.write_at_gray_scale(
                            alloc_result.x + x,
                            alloc_result.y + y,
                            alloc_result.l as u8,
                            value,
                        );
                    });

                    glyph_info.uv_top_left.x = alloc_result.uv_top_left[0];
                    glyph_info.uv_top_left.y = alloc_result.uv_top_left[1];
                    glyph_info.uv_bottom_right.x = alloc_result.ub_bottom_right[0];
                    glyph_info.uv_bottom_right.y = alloc_result.ub_bottom_right[1];
                    glyph_info.uv_z = alloc_result.l as f32;
                };
            }
        }
        true
    }

    #[inline(always)]
    pub fn glyph_info(&self, ch: char) -> Option<&GlyphInfo> {
        self.glyph_table.get(&ch)
    }
}

#[derive(Debug, Clone)]
pub struct ScaledFontInstance {
    font: Rc<FontInstance>,
    scale: f32,
}

impl ScaledFontInstance {
    #[inline(always)]
    pub fn glyph_info(&self, ch: char) -> Option<ScaledGlyphInfo<'_>> {
        self.font.glyph_info(ch).map(|g| ScaledGlyphInfo {
            glyph_info: g,
            scale: self.scale,
        })
    }

    #[inline(always)]
    pub fn unknown_sym_glyph_info(&self) -> ScaledGlyphInfo<'_> {
        self.glyph_info(self.font.unknown_char).unwrap()
    }

    #[inline(always)]
    pub fn line_height(&self) -> f32 {
        self.font.line_height * self.scale
    }

    #[inline(always)]
    pub fn texture(&self) -> Rc<PainterTexture> {
        self.font.atlas.painter_texture()
    }
}

#[derive(Debug)]
struct FontFamily {
    font: Rc<Font>,
    instances: Vec<Rc<FontInstance>>,
}

#[derive(Debug)]
pub struct FontMap {
    families: HashMap<String, FontFamily>,
    texture_atlas: Rc<TextureAtlas>,
}

impl FontMap {
    pub fn new(atlas: Rc<TextureAtlas>) -> Self {
        Self {
            families: Default::default(),
            texture_atlas: atlas,
        }
    }

    pub fn add_font(
        &mut self,
        name: &str,
        font_data: Vec<u8>,
        sizes: &[u32],
    ) -> Result<(), FontError> {
        match self.families.entry(name.to_string()) {
            Entry::Occupied(_) => {
                return Err(FontError::Duplicate);
            }
            Entry::Vacant(v) => {
                let font = Font::new(font_data)?;
                let mut font_family = FontFamily {
                    font: Rc::new(font),
                    instances: Vec::with_capacity(sizes.len()),
                };

                for size in sizes {
                    let font_instance = FontInstance::new(
                        font_family.font.clone(),
                        self.texture_atlas.clone(),
                        *size,
                    )?;
                    font_family.instances.push(Rc::new(font_instance));
                }
                v.insert(font_family);
            }
        }
        Ok(())
    }

    pub fn resolve_font(&self, name: &str, size: u32) -> Option<ScaledFontInstance> {
        let key_str = name.to_string();
        if let Some(family) = self.families.get(&key_str) {
            for instance in &family.instances {
                if instance.size_px == size {
                    return Some(ScaledFontInstance {
                        font: instance.clone(),
                        scale: 1.0,
                    });
                }
                if instance.size_px > size {
                    return Some(ScaledFontInstance {
                        font: instance.clone(),
                        scale: size as f32 / instance.size_px as f32,
                    });
                }
            }
        }
        None
    }
}
