#version 450

layout(location = 0) out vec4 outColor;
layout(location = 0) in vec4 inColor;
layout(location = 1) in vec3 inUV;
layout(location = 2) flat in uint in_clip_rect;

layout(set=2, binding=0) uniform sampler2DArray atlas;

struct ClipRect {
    float x;
    float y;
    float xmax;
    float ymax;
};

layout(std430, set =3, binding = 0) readonly buffer ClipRectBuffer {
    ClipRect rects[];
} clip_rects;

void main() {
    ClipRect clip_rect = clip_rects.rects[in_clip_rect];
    vec2 frag_coord = gl_FragCoord.xy;

    if (frag_coord.x >= clip_rect.x
            && frag_coord.y >= clip_rect.y
            && frag_coord.x <= clip_rect.xmax
            && frag_coord.y <= clip_rect.ymax) {
        vec4 tex_color = texture(atlas, inUV);
        outColor = vec4(inColor.xyz, inColor.w * (tex_color.r * 1.0));
    } else {
        discard;
    }
}
