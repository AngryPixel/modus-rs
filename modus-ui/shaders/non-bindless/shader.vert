#version 450

struct Rect {
    uint xy;
    uint wh;
    uint color;
    uint uv_top_left;
    uint uv_bottom_right;
    uint uvz_and_clip_index;
};

struct Vertex {
    uint xy;
    uint color;
    uint uv;
    uint uvz_and_clip_index;
};

layout(set = 0, binding = 0) uniform ConstantState {
    float canvas_width;
    float canvas_height;
};

layout(std430, set =1, binding = 0) readonly buffer ObjectBuffer {
    Rect vertices[];
} objBuffer;

layout(std430, set =1, binding = 0) readonly buffer ObjectBufferVertex {
    Vertex vertices[];
} vertexBuffer;

layout(location = 0) out vec4 outColor;
layout(location = 1) out vec3 outUV;
layout(location = 2) flat out uint out_clip_rect;

uint get_index(uint index_data) {
    return index_data & ((1 << 24) -1);
}

// returns {shape_type, corner}
uvec2 get_shape_type(uint index_data) {
    uint shifted = (index_data >> 24) & 0xFF;
    return uvec2(shifted>> 2 , shifted & 0x3);
}

#define CORNER_TOP_LEFT 0
#define CORNER_BOTTOM_LEFT 1
#define CORNER_TOP_RIGHT 2
#define CORNER_BOTTOM_RIGHT 3

#define SHAPE_TYPE_RECT 0
#define SHAPE_TYPE_VERTEX 1
#define SHAPE_TYPE_RECT_UV 2

vec2 screen_normalize(vec2 v) {
    return vec2(2.0 * (v.x / canvas_width) - 1.0, 2.0 * (v.y / canvas_height) - 1.0);
}

vec2 screen_normalize_xy(float x, float y) {
    return vec2(2.0 * (x / canvas_width) - 1.0, 2.0 * (y / canvas_height) - 1.0);
}

vec2 screen_normalize_unorm(vec2 v) {
    return (2.0 * v) - 1.0;
}

vec2 screen_normalize_unorm_xy(float x, float y) {
    return (2.0 * vec2(x, y)) - 1.0;
}


vec4 build_vertex_rect(uvec2 shape_type, uint index, inout vec4 color) {
    Rect r = objBuffer.vertices[index];
    color = unpackUnorm4x8(r.color);
    vec2 xy = unpackSnorm2x16(r.xy);
    vec2 wh = unpackUnorm2x16(r.wh);
    out_clip_rect = bitfieldExtract(r.uvz_and_clip_index, 16, 16);
    switch (shape_type.y) {
        case CORNER_TOP_LEFT:
            return vec4(screen_normalize_unorm(xy), 0.0, 1.0);
        case CORNER_BOTTOM_LEFT:
            return vec4(screen_normalize_unorm_xy(xy.x,xy.y + wh.y), 0.0, 1.0);
        case CORNER_TOP_RIGHT:
            return vec4(screen_normalize_unorm_xy(xy.x + wh.x, xy.y), 0.0, 1.0);
        case CORNER_BOTTOM_RIGHT:
            return vec4(screen_normalize_unorm(xy + wh), 0.0, 1.0);
    }
}

vec4 build_vertex_rect_uv(uvec2 shape_type, uint index, inout vec4 color, inout vec3 uv) {
    Rect r = objBuffer.vertices[index];
    color = unpackUnorm4x8(r.color);
    vec2 xy = unpackSnorm2x16(r.xy);
    vec2 wh = unpackUnorm2x16(r.wh);
    vec2 uv_tl = unpackUnorm2x16(r.uv_top_left);
    vec2 uv_br = unpackUnorm2x16(r.uv_bottom_right);
    vec2 uv_z = unpackUnorm2x16(r.uvz_and_clip_index);
    out_clip_rect = bitfieldExtract(r.uvz_and_clip_index, 16, 16);
    switch (shape_type.y) {
        case CORNER_TOP_LEFT:
            uv = vec3(uv_tl, 0.0);
            return vec4(screen_normalize_unorm(xy), 0.0, 1.0);
        case CORNER_BOTTOM_LEFT:
            uv = vec3(uv_tl.x, uv_br.y, 0.0);
            return vec4(screen_normalize_unorm_xy(xy.x,xy.y + wh.y), 0.0, 1.0);
        case CORNER_TOP_RIGHT:
            uv = vec3(uv_br.x, uv_tl.y, 0.0);
            return vec4(screen_normalize_unorm_xy(xy.x + wh.x, xy.y), 0.0, 1.0);
        case CORNER_BOTTOM_RIGHT:
            uv = vec3(uv_br, 0.0);
            return vec4(screen_normalize_unorm(xy + wh), 0.0, 1.0);
    }
}

vec4 build_vertex_vertex(uvec2 shape_type, uint index, inout vec4 color, inout vec3 uv) {
    Vertex r = vertexBuffer.vertices[index];
    vec2 xy = unpackSnorm2x16(r.xy);
    color = unpackUnorm4x8(r.color);
    uv = vec3(unpackUnorm2x16(r.uv), 0.0);
    color = unpackUnorm4x8(r.color);
    out_clip_rect = 0;
    return vec4(screen_normalize_unorm(xy), 0.0, 1.0);
}

void main() {
    uint index = get_index(gl_VertexIndex);
    uvec2 shape_type = get_shape_type(gl_VertexIndex);
    vec4 vert = vec4(0.0);
    switch (shape_type.x) {
        case SHAPE_TYPE_RECT:
            outUV= vec3(0,0,0);
            vert = build_vertex_rect(shape_type, index, outColor);
            break;
        case SHAPE_TYPE_RECT_UV:
            vert = build_vertex_rect_uv(shape_type, index, outColor, outUV);
            break;
        case SHAPE_TYPE_VERTEX:
            vert = build_vertex_vertex(shape_type, index, outColor, outUV);
            break;
    }
    gl_Position = vert;
}
