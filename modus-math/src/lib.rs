//! Module which adds mathematical features as well as ensures that certain third party crates
//! are only tracked as dependencies here.

pub use nalgebra_glm::*;
pub mod color;

#[inline(always)]
pub fn pack_unorm16(v: f32) -> u16 {
    (v.clamp(0.0, 1.0) * (u16::MAX) as f32).ceil() as u16
}

#[inline(always)]
pub fn unpack_unorm16(v: u16) -> f32 {
    (v as f32) / (u16::MAX as f32)
}

#[inline(always)]
pub fn pack_snorm16(v: f32) -> i16 {
    (v.clamp(-1.0, 1.0) * (i16::MAX) as f32).ceil() as i16
}

#[inline(always)]
pub fn unpack_unorm32(v: i16) -> f32 {
    ((v as f32) / (i16::MAX as f32)).clamp(-1.0, 1.0)
}
