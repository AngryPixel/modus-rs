use crate::Vec4;

pub fn norm_srgb_to_linear(srgb: f32) -> f32 {
    if srgb < 0.04045 {
        srgb / 12.92
    } else {
        ((srgb + 0.055) / 1.055).powf(2.4)
    }
}

pub fn norm_linear_to_srgb(linear: f32) -> f32 {
    if linear <= 0.0031308 {
        linear * 12.92
    } else {
        linear.powf(1.0 / 2.4) * 1.055 - 0.055
    }
}

/// 8bit RGBA color representation
#[derive(Debug, Copy, Clone, PartialEq, Eq, Hash)]
pub struct Color {
    pub r: u8,
    pub g: u8,
    pub b: u8,
    pub a: u8,
}

impl Color {
    pub fn black() -> Self {
        Self {
            r: 0,
            b: 0,
            g: 0,
            a: 255,
        }
    }

    pub fn white() -> Self {
        Self {
            r: 255,
            b: 255,
            g: 255,
            a: 255,
        }
    }

    pub fn red() -> Self {
        Self {
            r: 255,
            b: 0,
            g: 0,
            a: 255,
        }
    }

    pub fn green() -> Self {
        Self {
            r: 0,
            b: 0,
            g: 255,
            a: 255,
        }
    }

    pub fn blue() -> Self {
        Self {
            r: 0,
            b: 255,
            g: 0,
            a: 255,
        }
    }

    pub fn gray(intensity: u8) -> Self {
        Self {
            r: intensity,
            b: intensity,
            g: intensity,
            a: 255,
        }
    }

    pub fn from_u32_argb(v: u32) -> Self {
        Self {
            a: (v >> 24) as u8,
            r: ((v >> 16) & 0xFF) as u8,
            g: ((v >> 8) & 0xFF) as u8,
            b: (v & 0xFF) as u8,
        }
    }

    pub fn from_u32_rgba(v: u32) -> Self {
        Self {
            r: (v >> 24) as u8,
            g: ((v >> 16) & 0xFF) as u8,
            b: ((v >> 8) & 0xFF) as u8,
            a: (v & 0xFF) as u8,
        }
    }

    pub fn as_array(&self) -> [u8; 4] {
        [self.r, self.g, self.b, self.a]
    }

    pub fn normalize(&self) -> Vec4 {
        let norm = [
            (self.r as f32) / 255.0,
            (self.g as f32) / 255.0,
            (self.b as f32) / 255.0,
            (self.a as f32) / 255.0,
        ];
        Vec4::from(norm)
    }

    pub fn linear_to_srgb(&self) -> Self {
        let mut tmp = self.normalize();
        tmp[0] = norm_linear_to_srgb(tmp[0]);
        tmp[1] = norm_linear_to_srgb(tmp[1]);
        tmp[2] = norm_linear_to_srgb(tmp[2]);
        tmp[3] = norm_linear_to_srgb(tmp[3]);
        Self::from(tmp)
    }

    pub fn srgb_to_linear(&self) -> Self {
        let mut tmp = self.normalize();
        tmp[0] = norm_srgb_to_linear(tmp[0]);
        tmp[1] = norm_srgb_to_linear(tmp[1]);
        tmp[2] = norm_srgb_to_linear(tmp[2]);
        tmp[3] = norm_srgb_to_linear(tmp[3]);
        Self::from(tmp)
    }

    pub fn is_transparent(&self) -> bool {
        self.a != 255
    }
}

impl From<[u8; 4]> for Color {
    fn from(c: [u8; 4]) -> Self {
        Self {
            r: c[0],
            g: c[1],
            b: c[2],
            a: c[3],
        }
    }
}

impl From<Color> for [u8; 4] {
    fn from(v: Color) -> Self {
        v.as_array()
    }
}

impl From<Vec4> for Color {
    fn from(v: Vec4) -> Self {
        Self {
            r: (v[0] * 255.0).clamp(0.0, 255.0) as u8,
            g: (v[1] * 255.0).clamp(0.0, 255.0) as u8,
            b: (v[2] * 255.0).clamp(0.0, 255.0) as u8,
            a: (v[3] * 255.0).clamp(0.0, 255.0) as u8,
        }
    }
}

impl From<Color> for Vec4 {
    fn from(v: Color) -> Self {
        v.normalize()
    }
}
