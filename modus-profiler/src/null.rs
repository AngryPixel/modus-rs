//! Null implementation, does nothing
#[macro_export]
macro_rules! scope {
    // Note: callstack_depth is 0 since this has significant overhead
    () => {};
    ($name:literal) => {};
    ($name:literal, $data:expr) => {};
}

#[macro_export]
macro_rules! scope_color {
    // Note: callstack_depth is 0 since this has significant overhead
    ($color:expr) => {};
    ($color:expr, $name:literal) => {};
    ($color:expr, $name:literal, $data:expr) => {};
}

#[macro_export]
macro_rules! register_thread {
    () => {};
    ($name:expr) => {};
}

#[macro_export]
macro_rules! finish_frame {
    () => {};
}
