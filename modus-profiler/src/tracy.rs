#[macro_export]
macro_rules! function {
    () => {{
        fn f() {}
        fn type_name_of<T>(_: T) -> &'static str {
            std::any::type_name::<T>()
        }
        let name = type_name_of(f);
        &name[..name.len() - 3]
    }};
}

#[macro_export]
macro_rules! scope {
    // Note: callstack_depth is 0 since this has significant overhead
    () => {
        let _tracy_span =
            $crate::tracy_client::Span::new($crate::function!(), "", file!(), line!(), 0);
        _tracy_span.emit_color($crate::COLOR_DEFAULT);
    };
    ($name:literal) => {
        let _tracy_span = $crate::tracy_client::Span::new($name, "", file!(), line!(), 0);
        _tracy_span.emit_color($crate::COLOR_DEFAULT);
    };
    ($name:literal, $data:expr) => {
        let _tracy_span = $crate::tracy_client::Span::new($name, "", file!(), line!(), 0);
        _tracy_span.emit_text($data);
        _tracy_span.emit_color($crate::COLOR_DEFAULT);
    };
}

#[macro_export]
macro_rules! scope_color {
    // Note: callstack_depth is 0 since this has significant overhead
    ($color:expr) => {
        let _tracy_span =
            $crate::tracy_client::Span::new($crate::function!(), "", file!(), line!(), 0);
        _tracy_span.emit_color($color);
    };
    ($color:expr, $name:literal) => {
        let _tracy_span = $crate::tracy_client::Span::new($name, "", file!(), line!(), 0);
        _tracy_span.emit_color($color);
    };
    ($color:expr, $name:literal, $data:expr) => {
        let _tracy_span = $crate::tracy_client::Span::new($name, "", file!(), line!(), 0);
        _tracy_span.emit_text($data);
        _tracy_span.emit_color($color);
    };
}

#[macro_export]
macro_rules! register_thread {
    () => {
        let thread_name = std::thread::current()
            .name()
            .map(|x| x.to_string())
            .unwrap_or_else(|| format!("Thread {:?}", std::thread::current().id()));

        $crate::register_thread!(&thread_name);
    };
    ($name:expr) => {
        $crate::tracy_client::set_thread_name($name);
    };
}

#[macro_export]
macro_rules! finish_frame {
    () => {
        $crate::tracy_client::finish_continuous_frame!();
    };
}
