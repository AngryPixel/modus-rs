//! Profiler macros abstractions for use with the modus project
mod colors;
pub use colors::*;

#[cfg(feature = "use-tracy")]
pub use tracy_client;
#[cfg(feature = "use-tracy")]
mod tracy;
#[cfg(feature = "use-tracy")]
pub use tracy::*;

#[cfg(not(any(feature = "use-tracy")))]
mod null;
#[cfg(not(any(feature = "use-tracy")))]
pub use null::*;
