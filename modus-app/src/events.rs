//! Event definitions

use modus_core::define_custom_handle_type;
use modus_core::handle_map::Handle;
use std::fmt::{Debug, Display, Formatter};

define_custom_handle_type!(DeviceId);
define_custom_handle_type!(WindowId);

#[derive(Debug, Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash)]
#[repr(u8)]
pub enum MouseButton {
    Left = 0,
    Middle,
    Right,
    Extra0,
    Extra1,
    Total,
}

impl MouseButton {
    pub fn to_str(self) -> &'static str {
        match self {
            MouseButton::Left => "Left",
            MouseButton::Middle => "Middle",
            MouseButton::Right => "Right",
            MouseButton::Extra0 => "Extra0",
            MouseButton::Extra1 => "Extra1",
            _ => "Unknown",
        }
    }
}

#[derive(Debug, Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash)]
#[repr(u8)]
pub enum Key {
    KeyA = 0,
    KeyB,
    KeyC,
    KeyD,
    KeyE,
    KeyF,
    KeyG,
    KeyH,
    KeyI,
    KeyJ,
    KeyK,
    KeyL,
    KeyM,
    KeyN,
    KeyO,
    KeyP,
    KeyQ,
    KeyR,
    KeyS,
    KeyT,
    KeyU,
    KeyV,
    KeyW,
    KeyX,
    KeyY,
    KeyZ,
    Key0,
    Key1,
    Key2,
    Key3,
    Key4,
    Key5,
    Key6,
    Key7,
    Key8,
    Key9,
    KeyUp,
    KeyDown,
    KeyRight,
    KeyLeft,
    KeySpace,
    KeyLeftShift,
    KeyRightShift,
    KeyLeftCtrl,
    KeyRightCtrl,
    KeyLeftAlt,
    KeyRightAlt,
    KeyTab,
    KeyTilde,
    KeyPlus,
    KeyMinus,
    KeyBackSlash,
    KeyPeriod,
    KeyComma,
    KeySlash,
    KeyColon,
    KeyApostrophe,
    KeyBackSpace,
    KeyEnter,
    KeyCapsLock,
    KeyEscape,
    KeyEquals,
    KeyLeftBracket,
    KeyRightBracket,
    KeySemiColon,
    KeyF1,
    KeyF2,
    KeyF3,
    KeyF4,
    KeyF5,
    KeyF6,
    KeyF7,
    KeyF8,
    KeyF9,
    KeyF10,
    KeyF11,
    KeyF12,
    KeyPrintScreen,
    KeyScrollLock,
    KeyPause,
    KeyHome,
    KeyDelete,
    KeyInsert,
    KeyEnd,
    KeyPgUp,
    KeyPgDown,
    KeyKPDivide,
    KeyKPMulitply,
    KeyKPPlus,
    KeyKPMinus,
    KeyKPEnter,
    KeyKPPeriod,
    KeyKP0,
    KeyKP1,
    KeyKP2,
    KeyKP3,
    KeyKP4,
    KeyKP5,
    KeyKP6,
    KeyKP7,
    KeyKP8,
    KeyKP9,
    KeyLeftOS,
    KeyRightOS,
    Total,
}

impl Key {
    pub fn to_asccii(self) -> char {
        match self {
            Key::KeyA => 'a',
            Key::KeyB => 'b',
            Key::KeyC => 'c',
            Key::KeyD => 'd',
            Key::KeyE => 'e',
            Key::KeyF => 'f',
            Key::KeyG => 'g',
            Key::KeyH => 'h',
            Key::KeyI => 'i',
            Key::KeyJ => 'j',
            Key::KeyK => 'k',
            Key::KeyL => 'l',
            Key::KeyM => 'm',
            Key::KeyN => 'n',
            Key::KeyO => 'o',
            Key::KeyP => 'p',
            Key::KeyQ => 'q',
            Key::KeyR => 'r',
            Key::KeyS => 's',
            Key::KeyT => 't',
            Key::KeyU => 'u',
            Key::KeyV => 'v',
            Key::KeyW => 'w',
            Key::KeyX => 'x',
            Key::KeyY => 'y',
            Key::KeyZ => 'z',
            Key::Key0 => '0',
            Key::Key1 => '1',
            Key::Key2 => '2',
            Key::Key3 => '3',
            Key::Key4 => '4',
            Key::Key5 => '5',
            Key::Key6 => '6',
            Key::Key7 => '7',
            Key::Key8 => '8',
            Key::Key9 => '9',
            Key::KeyKP0 => '0',
            Key::KeyKP1 => '1',
            Key::KeyKP2 => '2',
            Key::KeyKP3 => '3',
            Key::KeyKP4 => '4',
            Key::KeyKP5 => '5',
            Key::KeyKP6 => '6',
            Key::KeyKP7 => '7',
            Key::KeyKP8 => '8',
            Key::KeyKP9 => '9',
            Key::KeySpace => ' ',
            Key::KeyTilde => '~',
            Key::KeyPlus => '+',
            Key::KeyMinus => '-',
            Key::KeyBackSlash => '\\',
            Key::KeyPeriod => '.',
            Key::KeyComma => ',',
            Key::KeySlash => '/',
            Key::KeyColon => ':',
            Key::KeyApostrophe => '\'',
            Key::KeyEquals => '=',
            Key::KeyLeftBracket => '[',
            Key::KeyRightBracket => ']',
            Key::KeySemiColon => ';',
            Key::KeyKPDivide => '/',
            Key::KeyKPMulitply => '*',
            Key::KeyKPPlus => '+',
            Key::KeyKPMinus => '-',
            Key::KeyKPPeriod => '.',
            _ => '?',
        }
    }

    pub fn to_str(self) -> &'static str {
        match self {
            Key::KeyA => "A",
            Key::KeyB => "B",
            Key::KeyC => "C",
            Key::KeyD => "D",
            Key::KeyE => "E",
            Key::KeyF => "F",
            Key::KeyG => "G",
            Key::KeyH => "H",
            Key::KeyI => "I",
            Key::KeyJ => "J",
            Key::KeyK => "K",
            Key::KeyL => "L",
            Key::KeyM => "M",
            Key::KeyN => "N",
            Key::KeyO => "O",
            Key::KeyP => "P",
            Key::KeyQ => "Q",
            Key::KeyR => "R",
            Key::KeyS => "S",
            Key::KeyT => "T",
            Key::KeyU => "U",
            Key::KeyV => "V",
            Key::KeyW => "W",
            Key::KeyX => "X",
            Key::KeyY => "Y",
            Key::KeyZ => "Z",
            Key::Key0 => "0",
            Key::Key1 => "1",
            Key::Key2 => "2",
            Key::Key3 => "3",
            Key::Key4 => "4",
            Key::Key5 => "5",
            Key::Key6 => "6",
            Key::Key7 => "7",
            Key::Key8 => "8",
            Key::Key9 => "9",
            Key::KeyUp => "Up",
            Key::KeyDown => "Down",
            Key::KeyRight => "Right",
            Key::KeyLeft => "Left",
            Key::KeySpace => "Space",
            Key::KeyLeftShift => "LeftShift",
            Key::KeyRightShift => "RightShift",
            Key::KeyLeftCtrl => "LeftCtrl",
            Key::KeyRightCtrl => "RightCtrl",
            Key::KeyLeftAlt => "LeftAlt",
            Key::KeyRightAlt => "RightAlt",
            Key::KeyTab => "Tab",
            Key::KeyTilde => "Tilde",
            Key::KeyPlus => "Plus",
            Key::KeyMinus => "Minus",
            Key::KeyBackSlash => "BackSlash",
            Key::KeyPeriod => "Period",
            Key::KeyComma => "Comma",
            Key::KeySlash => "Slash",
            Key::KeyColon => "Colon",
            Key::KeyApostrophe => "Apostrophe",
            Key::KeyBackSpace => "BackSpace",
            Key::KeyEnter => "Enter",
            Key::KeyCapsLock => "CapsLock",
            Key::KeyEscape => "Escape",
            Key::KeyEquals => "Equals",
            Key::KeyLeftBracket => "LeftBracket",
            Key::KeyRightBracket => "RightBracket",
            Key::KeySemiColon => "SemiColon",
            Key::KeyF1 => "F1",
            Key::KeyF2 => "F2",
            Key::KeyF3 => "F3",
            Key::KeyF4 => "F4",
            Key::KeyF5 => "F5",
            Key::KeyF6 => "F6",
            Key::KeyF7 => "F7",
            Key::KeyF8 => "F8",
            Key::KeyF9 => "F9",
            Key::KeyF10 => "F10",
            Key::KeyF11 => "F11",
            Key::KeyF12 => "F12",
            Key::KeyPrintScreen => "PrintScreen",
            Key::KeyScrollLock => "ScrollLock",
            Key::KeyPause => "Pause",
            Key::KeyHome => "Home",
            Key::KeyDelete => "Delete",
            Key::KeyInsert => "Insert",
            Key::KeyEnd => "End",
            Key::KeyPgUp => "PgUp",
            Key::KeyPgDown => "PgDown",
            Key::KeyKPDivide => "KPDivide",
            Key::KeyKPMulitply => "KPMulitply",
            Key::KeyKPPlus => "KPPlus",
            Key::KeyKPMinus => "KPMinus",
            Key::KeyKPEnter => "KPEnter",
            Key::KeyKPPeriod => "KPPeriod",
            Key::KeyKP0 => "KP0",
            Key::KeyKP1 => "KP1",
            Key::KeyKP2 => "KP2",
            Key::KeyKP3 => "KP3",
            Key::KeyKP4 => "KP4",
            Key::KeyKP5 => "KP5",
            Key::KeyKP6 => "KP6",
            Key::KeyKP7 => "KP7",
            Key::KeyKP8 => "KP8",
            Key::KeyKP9 => "KP9",
            Key::KeyLeftOS => "LeftOS",
            Key::KeyRightOS => "RightOS",
            _ => "Unknown",
        }
    }
}

#[derive(Debug, Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash)]
pub enum GameControllerAxis {
    LeftX = 0,
    LeftY,
    RightX,
    RightY,
    LeftTrigger,
    RightTrigger,
    Total,
}

impl GameControllerAxis {
    fn to_str(self) -> &'static str {
        match self {
            GameControllerAxis::LeftX => "LeftX",
            GameControllerAxis::LeftY => "LeftY",
            GameControllerAxis::RightX => "RightX",
            GameControllerAxis::RightY => "RightY",
            GameControllerAxis::LeftTrigger => "LeftTrigger",
            GameControllerAxis::RightTrigger => "RightTrigger",
            _ => "Unknown",
        }
    }
}

#[derive(Debug, Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash)]
#[repr(u8)]
pub enum GameControllerButton {
    A = 0,
    B,
    X,
    Y,
    Back,
    Guide,
    Start,
    LeftStick,
    RightStick,
    RightShoulder,
    LeftShoulder,
    DPadUp,
    DPadDown,
    DPadLeft,
    DPadRight,
    Total,
}

impl GameControllerButton {
    pub fn to_str(self) -> &'static str {
        match self {
            GameControllerButton::A => "A",
            GameControllerButton::B => "B",
            GameControllerButton::X => "X",
            GameControllerButton::Y => "Y",
            GameControllerButton::Back => "Back",
            GameControllerButton::Guide => "Guide",
            GameControllerButton::Start => "Start",
            GameControllerButton::LeftStick => "LeftStick",
            GameControllerButton::RightStick => "RightStick",
            GameControllerButton::RightShoulder => "RightShoulder",
            GameControllerButton::LeftShoulder => "LeftShoulder",
            GameControllerButton::DPadUp => "DPadUp",
            GameControllerButton::DPadDown => "DPadDown",
            GameControllerButton::DPadLeft => "DPadLeft",
            GameControllerButton::DPadRight => "DPadRight",
            _ => "Unknown",
        }
    }
}

#[derive(Debug, Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash)]
#[repr(u8)]
pub enum ButtonState {
    Down = 1,
    Up = 2,
}

impl ButtonState {
    pub fn to_str(&self) -> &'static str {
        match self {
            ButtonState::Down => "Down",
            ButtonState::Up => "Up",
        }
    }
}

#[derive(Debug, Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash)]
pub struct KeyboardEvent {
    pub state: ButtonState,
    pub key: Key,
}

impl Display for KeyboardEvent {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        write!(
            f,
            "KeyboardEvent {{ key:{} asccii:{} state:{} }}",
            self.key.to_str(),
            self.key.to_asccii(),
            self.state.to_str()
        )
    }
}

#[derive(Debug, Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash)]
pub struct MouseButtonEvent {
    pub state: ButtonState,
    pub button: MouseButton,
    pub x: i32,
    pub y: i32,
}

impl Display for MouseButtonEvent {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        write!(
            f,
            "KeyboardButtonEvent {{ button:{} state:{}, x:{} y:{} }}",
            self.button.to_str(),
            self.state.to_str(),
            self.x,
            self.y,
        )
    }
}

#[derive(Debug, Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash)]
pub struct MouseWheelEvent {
    pub x: i32,
    pub y: i32,
}

impl Display for MouseWheelEvent {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        write!(f, "MouseScrollEvent {{ x:{} y:{} }}", self.x, self.y,)
    }
}

#[derive(Debug, Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash)]
pub struct MouseMoveEvent {
    pub x: i32,
    pub y: i32,
    pub deltax: i32,
    pub deltay: i32,
}

impl Display for MouseMoveEvent {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        write!(
            f,
            "MouseMoveEvent {{ x:{} y:{} dx:{} dy{} }}",
            self.x, self.y, self.deltax, self.deltay,
        )
    }
}

pub const GAME_CONTROLLER_AXIS_VALUE_MAX: i32 = 32767;
pub const GAME_CONTROLLER_AXIS_VALUE_MIN: i32 = -32768;
#[derive(Debug, Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash)]
pub struct GameControllerAxisEvent {
    pub device_id: DeviceId,
    pub axis: GameControllerAxis,
    pub value: i32,
}

impl Display for GameControllerAxisEvent {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        write!(
            f,
            "GameControllerAxisEvent {{ device_id:{} axis:{} value:{} }}",
            self.device_id,
            self.axis.to_str(),
            self.value,
        )
    }
}

impl GameControllerAxisEvent {
    #[inline(always)]
    pub fn normalized(&self) -> f32 {
        self.value as f32 / GAME_CONTROLLER_AXIS_VALUE_MAX as f32
    }

    #[inline(always)]
    pub fn normalized_positive_only(&self) -> f32 {
        if self.value < 0 {
            0_f32
        } else {
            self.normalized()
        }
    }
}

#[derive(Debug, Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash)]
pub struct GameControllerButtonEvent {
    pub device_id: DeviceId,
    pub button: GameControllerButton,
    pub state: ButtonState,
}

impl Display for GameControllerButtonEvent {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        write!(
            f,
            "GameControllerButtonEvent {{ device_id:{} button:{} state:{} }}",
            self.device_id,
            self.button.to_str(),
            self.state.to_str(),
        )
    }
}

#[derive(Debug, Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash)]
pub struct TextInputEvent {
    pub ch: char,
}

impl Display for TextInputEvent {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        write!(f, "TextInputEvent {{ char: {} }}", self.ch)
    }
}

#[derive(Debug, Copy, Clone, Eq, PartialEq, Ord, PartialOrd, Hash)]
pub enum InputEvent {
    Keyboard(KeyboardEvent),
    MouseButton(MouseButtonEvent),
    MouseWheel(MouseWheelEvent),
    MouseMove(MouseMoveEvent),
    GameControllerButton(GameControllerButtonEvent),
    GameControllerAxis(GameControllerAxisEvent),
    GameControllerConnected(DeviceId),
    GameControllerDisconnected(DeviceId),
    TextInput(TextInputEvent),
}

impl Display for InputEvent {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        write!(f, "InputEvent:")?;
        match self {
            InputEvent::Keyboard(evt) => std::fmt::Display::fmt(evt, f),
            InputEvent::MouseButton(evt) => std::fmt::Display::fmt(evt, f),
            InputEvent::MouseWheel(evt) => std::fmt::Display::fmt(evt, f),
            InputEvent::MouseMove(evt) => std::fmt::Display::fmt(evt, f),
            InputEvent::GameControllerAxis(evt) => std::fmt::Display::fmt(evt, f),
            InputEvent::GameControllerButton(evt) => std::fmt::Display::fmt(evt, f),
            InputEvent::GameControllerConnected(device_id) => {
                write!(f, "GameControllerConnected {{{}}}", device_id)
            }
            InputEvent::GameControllerDisconnected(device_id) => {
                write!(f, "GameControllerDisconnected {{{}}}", device_id)
            }
            InputEvent::TextInput(evt) => std::fmt::Display::fmt(evt, f),
        }
    }
}

#[derive(Debug, Copy, Clone, Eq, PartialEq, Ord, PartialOrd)]
pub struct WindowResizedEvent {
    pub window_id: WindowId,
    pub width: u32,
    pub height: u32,
}

impl std::fmt::Display for WindowResizedEvent {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(
            f,
            "WindowResizedEvent{{ window:{} width:{} height:{} }}",
            self.window_id, self.width, self.height
        )
    }
}

#[derive(Debug, Copy, Clone, Eq, PartialEq, Ord, PartialOrd)]
pub enum WindowEvent {
    Resized(WindowResizedEvent),
    FocusLost(WindowId),
    FocusGained(WindowId),
    MouseEnter(WindowId),
    MouseLeave(WindowId),
}

impl std::fmt::Display for WindowEvent {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            WindowEvent::Resized(e) => std::fmt::Display::fmt(e, f),
            WindowEvent::FocusGained(window_id) => {
                write!(f, "WindowFocusGained {{ window:{} }}", window_id)
            }
            WindowEvent::FocusLost(window_id) => {
                write!(f, "WindowFocusLost {{ window:{} }}", window_id)
            }
            WindowEvent::MouseEnter(window_id) => {
                write!(f, "WindowMouseEnter {{ window:{} }}", window_id)
            }
            WindowEvent::MouseLeave(window_id) => {
                write!(f, "WindowMouseLeave {{ window:{} }}", window_id)
            }
        }
    }
}

#[derive(Debug, Copy, Clone, Eq, PartialEq, Ord, PartialOrd)]
pub enum AppEvent {
    Window(WindowEvent),
}

impl std::fmt::Display for AppEvent {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "AppEvent:")?;
        match self {
            AppEvent::Window(wevt) => std::fmt::Display::fmt(wevt, f),
        }
    }
}
