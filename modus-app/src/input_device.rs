//! Input device repr
use crate::events::DeviceId;

pub trait InputDevice {
    fn name(&self) -> &str;

    fn device_id(&self) -> DeviceId;
}

pub trait InputSystem {
    fn resolve_input_device(&self, id: DeviceId) -> Option<&'_ dyn InputDevice>;

    fn resolve_input_device_mut(&mut self, id: DeviceId) -> Option<&'_ mut dyn InputDevice>;
}
