//! App interface

use crate::events::{AppEvent, InputEvent};
use crate::input_device::InputSystem;
use crate::window::WindowSystem;
use std::sync::Arc;

#[derive(Debug)]
pub struct AppInitInfo<'a> {
    pub name: &'a str,
    pub name_preferences: &'a str,
}

#[derive(Debug, Default)]
pub struct AppPaths {
    pub tmp: std::path::PathBuf,
    pub config: std::path::PathBuf,
    pub data: std::path::PathBuf,
    pub cache: std::path::PathBuf,
    pub resource: std::path::PathBuf,
}

impl AppPaths {
    pub fn new() -> Self {
        Self {
            tmp: std::path::PathBuf::new(),
            config: std::path::PathBuf::new(),
            data: std::path::PathBuf::new(),
            cache: std::path::PathBuf::new(),
            resource: std::path::PathBuf::new(),
        }
    }
}

pub type AppResult<T> = Result<T, String>;

pub trait AppEventHandler {
    fn on_app_event(&mut self, event: &AppEvent);
    fn on_input_event(&mut self, event: &InputEvent);
}

#[derive(Debug, Copy, Clone, Eq, PartialEq)]
pub enum TickResult {
    Continue,
    Paused,
    Quit,
}

pub trait App {
    fn paths(&self) -> &AppPaths;

    fn tick(&mut self, handler: &mut dyn AppEventHandler) -> TickResult;

    fn window_system(&self) -> &dyn WindowSystem;

    fn window_system_mut(&mut self) -> &mut dyn WindowSystem;

    fn input_system(&self) -> &dyn InputSystem;

    fn input_system_mut(&mut self) -> &mut dyn InputSystem;

    fn threed_instance(&self) -> Arc<dyn modus_threed::instance::Instance>;
}
