//!
//! Application runtime framework
//!

use crate::app::AppInitInfo;
use modus_threed::instance::InstanceCreateInfo;

pub mod app;
pub mod events;
pub mod input_device;
pub mod window;

mod sdl2;

pub fn create_app<'a>(
    app_info: &'a AppInitInfo,
    threed_info: &'a InstanceCreateInfo,
) -> Result<Box<dyn app::App>, String> {
    sdl2::app::Sdl2App::create(app_info, threed_info)
}

pub(crate) const PROFILE_COLOR0: u32 = modus_profiler::COLOR_CYAN_A400;
#[macro_export(crate)]
macro_rules! profile_scope {
    () => {
        modus_profiler::scope_color!(crate::PROFILE_COLOR0);
    };
    ($name:literal) => {
        modus_profiler::scope_color!(crate::PROFILE_COLOR0, $name);
    };
}
