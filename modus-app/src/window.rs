//! Window Abstraction

use crate::events::WindowId;
use std::sync::Arc;

#[derive(Debug, Copy, Clone, Eq, PartialEq)]
pub enum FullscreenMode {
    None,
    Window,
    Borderless,
    Exclusive,
}

#[derive(Debug)]
pub struct WindowCreateInfo<'a> {
    pub title: &'a str,
    pub width: Option<u32>,
    pub height: Option<u32>,
    pub x: Option<i32>,
    pub y: Option<i32>,
    pub fullscreen: FullscreenMode,
    pub hidden: bool,
}

#[derive(Debug, Copy, Clone, Eq, PartialEq)]
pub struct WindowState {
    pub x: i32,
    pub y: i32,
    pub width: i32,
    pub height: i32,
    pub fullscreen: FullscreenMode,
}

pub trait Window: std::fmt::Debug {
    fn set_title(&mut self, title: &str);

    fn window_state(&self) -> WindowState;

    fn set_window_state(&mut self, state: WindowState);

    fn window_id(&self) -> WindowId;

    fn show(&mut self);

    fn hide(&mut self);

    fn render_surface_interface(&self) -> &dyn modus_threed::app_interface::RenderSurfaceInterface;

    fn create_swap_chain(
        &mut self,
        device: Arc<dyn modus_threed::device::Device>,
    ) -> Result<modus_threed::types::SwapChainHandle, modus_threed::device::DeviceError>;
}

pub type WindowResult<T> = Result<T, String>;

pub trait WindowSystem {
    fn create_window(&mut self, info: &WindowCreateInfo) -> WindowResult<WindowId>;

    fn destroy_window(&mut self, id: WindowId);

    fn resole_window_mut(&mut self, id: WindowId) -> Option<&'_ mut dyn Window>;

    fn resole_window(&self, id: WindowId) -> Option<&'_ dyn Window>;
}
