//! SDL2 Input Device
use crate::events::DeviceId;
use crate::input_device::InputDevice;
use modus_core::handle_map::ArrayHandleMap;
use sdl2_sys as sys;
use std::ffi::CStr;
use std::ptr::NonNull;

#[derive(Debug)]
pub(crate) struct GameController {
    pub device_id: DeviceId,
    sdl_device_id: i32,
    device_name: String,
    controller: NonNull<sys::SDL_GameController>,
}

impl Drop for GameController {
    fn drop(&mut self) {
        unsafe { sys::SDL_GameControllerClose(self.controller.as_ptr()) };
    }
}

impl crate::input_device::InputDevice for GameController {
    fn name(&self) -> &str {
        &self.device_name
    }

    fn device_id(&self) -> DeviceId {
        self.device_id
    }
}

const MAX_CONTROLLER_COUNT: usize = 8;
type ControllerMapType = ArrayHandleMap<DeviceId, GameController, MAX_CONTROLLER_COUNT>;

#[derive(Debug, Default)]
pub(crate) struct InputSystem {
    controllers: ControllerMapType,
}

impl InputSystem {
    pub(crate) fn new() -> Self {
        Self {
            controllers: ControllerMapType::default(),
        }
    }

    pub(crate) fn shutdown(&mut self) {
        self.controllers.clear();
    }

    pub(crate) fn find_controller_by_sdl_id(
        &self,
        sdl_controller_id: i32,
    ) -> Option<&'_ GameController> {
        for controller in &self.controllers {
            if controller.sdl_device_id == sdl_controller_id {
                return Some(controller);
            }
        }
        None
    }

    pub(crate) fn add_controller(&mut self, index: i32) -> Option<DeviceId> {
        if unsafe { sys::SDL_IsGameController(index) } == sys::SDL_bool::SDL_TRUE {
            let controller: *mut sys::SDL_GameController =
                unsafe { sys::SDL_GameControllerOpen(index) };
            if !controller.is_null() {
                let joystick = unsafe { sys::SDL_GameControllerGetJoystick(controller) };
                debug_assert!(!joystick.is_null());
                let sdl_device_id = unsafe { sys::SDL_JoystickInstanceID(joystick) };
                if sdl_device_id >= 0 {
                    let cstr_name = unsafe { sys::SDL_GameControllerName(controller) };
                    let device_name: String = if cstr_name.is_null() {
                        String::from("Unknown")
                    } else {
                        let cstr = unsafe { CStr::from_ptr(cstr_name) };
                        if let Ok(str_slice) = cstr.to_str() {
                            str_slice.to_owned()
                        } else {
                            String::from("Unknown")
                        }
                    };
                    let controller = GameController {
                        device_id: Default::default(),
                        sdl_device_id,
                        device_name,
                        controller: NonNull::new(controller).unwrap(),
                    };
                    return if let Ok(h) = self.controllers.insert_with(controller, |id, dev| {
                        dev.device_id = id;
                    }) {
                        Some(h)
                    } else {
                        None
                    };
                }
            }
        }
        None
    }

    pub(crate) fn remove_controller(&mut self, index: i32) -> Option<DeviceId> {
        let mut result = None;
        if index >= 0 {
            for controller in &self.controllers {
                if controller.sdl_device_id == index {
                    result = Some(controller.device_id);
                    break;
                }
            }
            result = result.map(|id| {
                self.controllers.remove(id);
                id
            });
        }
        result
    }
}

impl crate::input_device::InputSystem for InputSystem {
    fn resolve_input_device(&self, id: DeviceId) -> Option<&dyn InputDevice> {
        self.controllers
            .get(id.into())
            .map(|c| c as &dyn InputDevice)
    }

    fn resolve_input_device_mut(&mut self, id: DeviceId) -> Option<&mut dyn InputDevice> {
        self.controllers
            .get_mut(id.into())
            .map(|c| c as &mut dyn InputDevice)
    }
}
