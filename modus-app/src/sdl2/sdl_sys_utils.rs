use sdl2_sys as sys;
use std::ffi::CStr;

pub(crate) fn sdl2_error() -> &'static str {
    let c_str = unsafe { CStr::from_ptr(sys::SDL_GetError()) };
    if let Ok(str) = c_str.to_str() {
        str
    } else {
        "Unknown Error: Could not convert text to ut8"
    }
}
