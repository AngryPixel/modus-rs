//!Window impl for SDL2
use crate::events::WindowId;
use crate::sdl2::sdl_sys_utils::sdl2_error;
use crate::window::{WindowCreateInfo, WindowResult, WindowState};
use modus_core::handle_map::ArrayHandleMap;
use modus_threed::app_interface::RenderSurfaceInterface;
use modus_threed::device::{Device, DeviceError};
use modus_threed::types::SwapChainHandle;
use sdl2_sys as sys;
use std::ffi::CString;
use std::os::raw::c_int;
use std::sync::Arc;

const MAX_OPEN_WINDOWS: usize = 4;

#[derive(Debug)]
struct WindowSwapChain {
    device: Arc<dyn Device>,
    handle: SwapChainHandle,
}

#[derive(Debug)]
pub(crate) struct Window {
    window: std::ptr::NonNull<sys::SDL_Window>,
    pub(crate) window_id: WindowId,
    sdl_window_id: u32,
    vk_surface: Sdl2VkRenderSurface,
    swap_chain: Option<WindowSwapChain>,
}

impl Drop for Window {
    fn drop(&mut self) {
        if let Some(wsc) = &self.swap_chain {
            if let Err(DeviceError::InvalidThreadAccess) = wsc.device.destroy_swap_chain(wsc.handle)
            {
                panic!("Could not destroy swap chain due to invalid thread access");
            }
        };
        self.vk_surface.destroy();
        unsafe { sys::SDL_DestroyWindow(self.window.as_ptr()) };
    }
}

const SDL_FULLSCREEN_BORDERLESS_FLAG: u32 = sys::SDL_WindowFlags::SDL_WINDOW_FULLSCREEN_DESKTOP
    as u32
    | sys::SDL_WindowFlags::SDL_WINDOW_BORDERLESS as u32;

fn to_sdl_fullscreen_falgs(fullscreen_mode: crate::window::FullscreenMode) -> u32 {
    use sys::SDL_WindowFlags::*;
    match fullscreen_mode {
        crate::window::FullscreenMode::Window => SDL_WINDOW_FULLSCREEN_DESKTOP as u32,
        crate::window::FullscreenMode::Borderless => SDL_FULLSCREEN_BORDERLESS_FLAG,
        crate::window::FullscreenMode::Exclusive => SDL_WINDOW_FULLSCREEN as u32,
        _ => 0,
    }
}

const SDL_WINDOWPOS_CENTERED: i32 = sys::SDL_WINDOWPOS_CENTERED_MASK as i32;

impl Window {
    fn new(
        threed_instance: Arc<dyn modus_threed::instance::Instance>,
        info: &crate::window::WindowCreateInfo,
    ) -> crate::window::WindowResult<Window> {
        let cstr: CString = CString::new(info.title).unwrap_or_default();
        let width: i32 = info.width.map_or(1280, |x| x as i32);
        let height: i32 = info.height.map_or(720, |x| x as i32);
        let x: i32 = info.x.unwrap_or(SDL_WINDOWPOS_CENTERED);
        let y: i32 = info.y.unwrap_or(SDL_WINDOWPOS_CENTERED);

        use sys::SDL_WindowFlags::*;
        let mut flags = SDL_WINDOW_RESIZABLE as u32
            | SDL_WINDOW_ALLOW_HIGHDPI as u32
            | SDL_WINDOW_MOUSE_FOCUS as u32
            | SDL_WINDOW_INPUT_FOCUS as u32
            | SDL_WINDOW_VULKAN as u32;

        flags |= to_sdl_fullscreen_falgs(info.fullscreen);

        let sdl_window: *mut sys::SDL_Window =
            unsafe { sys::SDL_CreateWindow(cstr.as_ptr(), x, y, width, height, flags) };
        if sdl_window.is_null() {
            return Err(String::from(sdl2_error()));
        }

        let vk_surface = match Sdl2VkRenderSurface::create(threed_instance.clone(), sdl_window) {
            Err(e) => {
                unsafe { sys::SDL_DestroyWindow(sdl_window) };
                return Err(e);
            }
            Ok(s) => s,
        };

        let sdl_window_id = unsafe { sys::SDL_GetWindowID(sdl_window) };
        let mut window = Self {
            window: std::ptr::NonNull::new(sdl_window).expect("Ptr was supposed to be valid"),
            window_id: WindowId::default(),
            sdl_window_id,
            vk_surface,
            swap_chain: None,
        };

        if info.hidden {
            window.hide();
        } else {
            window.show();
        }

        Ok(window)
    }

    fn hide(&mut self) {
        unsafe { sys::SDL_HideWindow(self.window.as_ptr()) };
    }

    fn show(&mut self) {
        unsafe { sys::SDL_ShowWindow(self.window.as_ptr()) };
    }

    fn window_state(&self) -> crate::window::WindowState {
        use sys::SDL_WindowFlags::*;
        let mut x: c_int = 0;
        let mut y: c_int = 0;
        let mut w: c_int = 0;
        let mut h: c_int = 0;

        unsafe { sys::SDL_GetWindowPosition(self.window.as_ptr(), &mut x, &mut y) };
        unsafe { sys::SDL_GetWindowSize(self.window.as_ptr(), &mut w, &mut h) };
        let flags: u32 = unsafe { sys::SDL_GetWindowFlags(self.window.as_ptr()) };

        let fullscreen_mode =
            if flags & (SDL_WINDOW_FULLSCREEN as u32) == SDL_WINDOW_FULLSCREEN as u32 {
                crate::window::FullscreenMode::Exclusive
            } else if flags & SDL_FULLSCREEN_BORDERLESS_FLAG == SDL_FULLSCREEN_BORDERLESS_FLAG {
                crate::window::FullscreenMode::Borderless
            } else if flags & SDL_WINDOW_FULLSCREEN_DESKTOP as u32
                == SDL_WINDOW_FULLSCREEN_DESKTOP as u32
            {
                crate::window::FullscreenMode::Window
            } else {
                crate::window::FullscreenMode::None
            };

        crate::window::WindowState {
            x,
            y,
            width: w,
            height: w,
            fullscreen: fullscreen_mode,
        }
    }

    fn set_window_state(&mut self, state: crate::window::WindowState) {
        use crate::window::FullscreenMode::*;
        use sys::SDL_WindowFlags::*;
        unsafe { sys::SDL_SetWindowSize(self.window.as_ptr(), state.width, state.height) };
        unsafe { sys::SDL_SetWindowPosition(self.window.as_ptr(), state.x, state.y) };

        match state.fullscreen {
            None => {
                unsafe {
                    sys::SDL_SetWindowBordered(self.window.as_ptr(), sys::SDL_bool::SDL_TRUE)
                };
                unsafe { sys::SDL_SetWindowFullscreen(self.window.as_ptr(), 0) };
            }
            Exclusive => {
                unsafe {
                    sys::SDL_SetWindowFullscreen(self.window.as_ptr(), SDL_WINDOW_FULLSCREEN as u32)
                };
            }
            Borderless => {
                unsafe {
                    sys::SDL_SetWindowBordered(self.window.as_ptr(), sys::SDL_bool::SDL_FALSE)
                };
                unsafe {
                    sys::SDL_SetWindowFullscreen(
                        self.window.as_ptr(),
                        SDL_WINDOW_FULLSCREEN_DESKTOP as u32,
                    )
                };
            }
            Window => {
                unsafe {
                    sys::SDL_SetWindowBordered(self.window.as_ptr(), sys::SDL_bool::SDL_TRUE)
                };
                unsafe {
                    sys::SDL_SetWindowFullscreen(
                        self.window.as_ptr(),
                        SDL_WINDOW_FULLSCREEN_DESKTOP as u32,
                    )
                };
            }
        };
    }
}

impl crate::window::Window for Window {
    fn set_title(&mut self, title: &str) {
        let str = CString::new(title).unwrap_or_default();
        unsafe { sys::SDL_SetWindowTitle(self.window.as_ptr(), str.as_ptr()) };
    }

    fn window_state(&self) -> WindowState {
        self.window_state()
    }

    fn set_window_state(&mut self, state: WindowState) {
        self.set_window_state(state);
    }

    fn window_id(&self) -> WindowId {
        self.window_id
    }

    fn show(&mut self) {
        self.show();
    }

    fn hide(&mut self) {
        self.hide();
    }

    fn render_surface_interface(&self) -> &dyn RenderSurfaceInterface {
        self as &dyn RenderSurfaceInterface
    }

    fn create_swap_chain(
        &mut self,
        device: Arc<dyn Device>,
    ) -> Result<SwapChainHandle, DeviceError> {
        let h = device.create_swap_chain(self)?;
        self.swap_chain = Some(WindowSwapChain { device, handle: h });
        Ok(h)
    }
}

#[derive(Debug)]
pub(crate) struct WindowSystem {
    pub(crate) threed_instance: Arc<dyn modus_threed::instance::Instance>,
    windows: ArrayHandleMap<WindowId, Window, MAX_OPEN_WINDOWS>,
}

impl WindowSystem {
    pub(crate) fn new(thread_instance: Arc<dyn modus_threed::instance::Instance>) -> Self {
        Self {
            windows: Default::default(),
            threed_instance: thread_instance.clone(),
        }
    }

    pub(crate) fn shutdown(&mut self) {
        self.windows.clear();
    }

    pub(crate) fn find_window_by_sdl_id(&self, sdl_window_id: u32) -> Option<&'_ Window> {
        for window in &self.windows {
            if window.sdl_window_id == sdl_window_id {
                return Some(window);
            }
        }
        None
    }
}

impl crate::window::WindowSystem for WindowSystem {
    fn create_window<'a>(&mut self, info: &WindowCreateInfo<'a>) -> WindowResult<WindowId> {
        let window = Window::new(self.threed_instance.clone(), info)?;
        if let Ok(h) = self.windows.insert_with(window, |h, w| {
            w.window_id = h;
        }) {
            Ok(h)
        } else {
            Err(String::from("Failed to add window to window list"))
        }
    }

    fn destroy_window(&mut self, id: WindowId) {
        self.windows.remove(id);
    }

    fn resole_window_mut(&mut self, id: WindowId) -> Option<&'_ mut dyn crate::window::Window> {
        self.windows
            .get_mut(id.into())
            .map(|r| r as &mut dyn crate::window::Window)
    }

    fn resole_window(&self, id: WindowId) -> Option<&'_ dyn crate::window::Window> {
        self.windows
            .get(id.into())
            .map(|r| r as &dyn crate::window::Window)
    }
}

#[derive(Debug)]
struct Sdl2VkRenderSurface {
    vk_surface: sdl2_sys::SDL_vulkanSurface,
    vk_instance: Arc<dyn modus_threed::instance::Instance>,
}

impl Sdl2VkRenderSurface {
    fn create(
        threed_instance: Arc<dyn modus_threed::instance::Instance>,
        window: *mut sdl2_sys::SDL_Window,
    ) -> Result<Sdl2VkRenderSurface, String> {
        let native_instance = unsafe { threed_instance.native_ptr() };
        let mut vk_surface: sdl2_sys::VkSurfaceKHR = 0;
        if sdl2_sys::SDL_bool::SDL_FALSE
            == unsafe {
                sdl2_sys::SDL_Vulkan_CreateSurface(
                    window,
                    native_instance as usize,
                    &mut vk_surface,
                )
            }
        {
            return Err(format!("Failed to create window surface: {}", sdl2_error()));
        }
        Ok(Sdl2VkRenderSurface {
            vk_surface,
            vk_instance: threed_instance,
        })
    }

    fn destroy(&mut self) {
        if self.vk_surface != 0 {
            unsafe {
                self.vk_instance
                    .destroy_render_surface(std::mem::transmute::<
                        sdl2_sys::VkSurfaceKHR,
                        modus_threed::app_interface::NativeSurfacePtr,
                    >(self.vk_surface));
            };
            self.vk_surface = 0;
        }
    }

    fn get_native_ptr(&self) -> modus_threed::app_interface::NativeSurfacePtr {
        unsafe {
            std::mem::transmute::<
                sdl2_sys::VkSurfaceKHR,
                modus_threed::app_interface::NativeSurfacePtr,
            >(self.vk_surface)
        }
    }

    fn get_size(&self, window: *mut sdl2_sys::SDL_Window) -> (u32, u32) {
        let mut w: c_int = 0;
        let mut h: c_int = 0;
        unsafe {
            sys::SDL_Vulkan_GetDrawableSize(window, &mut w, &mut h);
        }
        (w as u32, h as u32)
    }
}

impl modus_threed::app_interface::RenderSurfaceInterface for Window {
    unsafe fn native_surface_handle(&self) -> modus_threed::app_interface::NativeSurfacePtr {
        self.vk_surface.get_native_ptr()
    }

    fn surface_size(&self) -> (u32, u32) {
        self.vk_surface.get_size(self.window.as_ptr())
    }
}
