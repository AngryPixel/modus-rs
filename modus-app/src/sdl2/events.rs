//! Mapping of SDL2 events
use crate::events::{DeviceId, TextInputEvent};
use sdl2_sys as sys;
use std::ffi::CStr;
use std::os::raw::c_char;
use std::str::Utf8Error;

#[allow(non_snake_case)]
impl From<sys::SDL_Scancode> for crate::events::Key {
    fn from(k: sys::SDL_Scancode) -> crate::events::Key {
        use crate::events::Key;
        use sys::SDL_Scancode::*;

        match k {
            SDL_SCANCODE_0 => Key::Key0,
            SDL_SCANCODE_1 => Key::Key1,
            SDL_SCANCODE_2 => Key::Key2,
            SDL_SCANCODE_3 => Key::Key3,
            SDL_SCANCODE_4 => Key::Key4,
            SDL_SCANCODE_5 => Key::Key5,
            SDL_SCANCODE_6 => Key::Key6,
            SDL_SCANCODE_7 => Key::Key7,
            SDL_SCANCODE_8 => Key::Key8,
            SDL_SCANCODE_9 => Key::Key9,
            SDL_SCANCODE_A => Key::KeyA,
            SDL_SCANCODE_B => Key::KeyB,
            SDL_SCANCODE_C => Key::KeyC,
            SDL_SCANCODE_D => Key::KeyD,
            SDL_SCANCODE_E => Key::KeyE,
            SDL_SCANCODE_F => Key::KeyF,
            SDL_SCANCODE_G => Key::KeyG,
            SDL_SCANCODE_H => Key::KeyH,
            SDL_SCANCODE_K => Key::KeyK,
            SDL_SCANCODE_L => Key::KeyL,
            SDL_SCANCODE_M => Key::KeyM,
            SDL_SCANCODE_N => Key::KeyN,
            SDL_SCANCODE_O => Key::KeyO,
            SDL_SCANCODE_P => Key::KeyP,
            SDL_SCANCODE_Q => Key::KeyQ,
            SDL_SCANCODE_R => Key::KeyR,
            SDL_SCANCODE_S => Key::KeyS,
            SDL_SCANCODE_T => Key::KeyT,
            SDL_SCANCODE_U => Key::KeyU,
            SDL_SCANCODE_V => Key::KeyV,
            SDL_SCANCODE_W => Key::KeyW,
            SDL_SCANCODE_X => Key::KeyX,
            SDL_SCANCODE_Y => Key::KeyY,
            SDL_SCANCODE_Z => Key::KeyZ,
            SDL_SCANCODE_RETURN => Key::KeyEnter,
            SDL_SCANCODE_ESCAPE => Key::KeyEscape,
            SDL_SCANCODE_BACKSPACE => Key::KeyBackSpace,
            SDL_SCANCODE_TAB => Key::KeyTab,
            SDL_SCANCODE_SPACE => Key::KeySpace,
            SDL_SCANCODE_MINUS => Key::KeyMinus,
            SDL_SCANCODE_EQUALS => Key::KeyEquals,
            SDL_SCANCODE_LEFTBRACKET => Key::KeyLeftBracket,
            SDL_SCANCODE_RIGHTBRACKET => Key::KeyRightBracket,
            SDL_SCANCODE_BACKSLASH => Key::KeyBackSlash,
            SDL_SCANCODE_SEMICOLON => Key::KeySemiColon,
            SDL_SCANCODE_APOSTROPHE => Key::KeyApostrophe,
            SDL_SCANCODE_GRAVE => Key::KeyTilde,
            SDL_SCANCODE_COMMA => Key::KeyComma,
            SDL_SCANCODE_PERIOD => Key::KeyPeriod,
            SDL_SCANCODE_SLASH => Key::KeySlash,
            SDL_SCANCODE_CAPSLOCK => Key::KeyCapsLock,
            SDL_SCANCODE_F1 => Key::KeyF1,
            SDL_SCANCODE_F2 => Key::KeyF2,
            SDL_SCANCODE_F3 => Key::KeyF3,
            SDL_SCANCODE_F4 => Key::KeyF4,
            SDL_SCANCODE_F5 => Key::KeyF5,
            SDL_SCANCODE_F6 => Key::KeyF6,
            SDL_SCANCODE_F7 => Key::KeyF7,
            SDL_SCANCODE_F8 => Key::KeyF8,
            SDL_SCANCODE_F9 => Key::KeyF9,
            SDL_SCANCODE_F10 => Key::KeyF10,
            SDL_SCANCODE_F11 => Key::KeyF11,
            SDL_SCANCODE_F12 => Key::KeyF12,
            SDL_SCANCODE_PRINTSCREEN => Key::KeyPrintScreen,
            SDL_SCANCODE_SCROLLLOCK => Key::KeyScrollLock,
            SDL_SCANCODE_PAUSE => Key::KeyPause,
            SDL_SCANCODE_INSERT => Key::KeyInsert,
            SDL_SCANCODE_HOME => Key::KeyHome,
            SDL_SCANCODE_PAGEUP => Key::KeyPgUp,
            SDL_SCANCODE_DELETE => Key::KeyDelete,
            SDL_SCANCODE_END => Key::KeyEnd,
            SDL_SCANCODE_PAGEDOWN => Key::KeyPgDown,
            SDL_SCANCODE_RIGHT => Key::KeyRight,
            SDL_SCANCODE_LEFT => Key::KeyLeft,
            SDL_SCANCODE_DOWN => Key::KeyDown,
            SDL_SCANCODE_UP => Key::KeyUp,
            SDL_SCANCODE_KP_DIVIDE => Key::KeyKPDivide,
            SDL_SCANCODE_KP_MULTIPLY => Key::KeyKPMulitply,
            SDL_SCANCODE_KP_MINUS => Key::KeyKPMinus,
            SDL_SCANCODE_KP_PLUS => Key::KeyKPPlus,
            SDL_SCANCODE_KP_ENTER => Key::KeyKPEnter,
            SDL_SCANCODE_KP_1 => Key::KeyKP1,
            SDL_SCANCODE_KP_2 => Key::KeyKP2,
            SDL_SCANCODE_KP_3 => Key::KeyKP3,
            SDL_SCANCODE_KP_4 => Key::KeyKP4,
            SDL_SCANCODE_KP_5 => Key::KeyKP5,
            SDL_SCANCODE_KP_6 => Key::KeyKP6,
            SDL_SCANCODE_KP_7 => Key::KeyKP7,
            SDL_SCANCODE_KP_8 => Key::KeyKP8,
            SDL_SCANCODE_KP_9 => Key::KeyKP9,
            SDL_SCANCODE_KP_0 => Key::KeyKP0,
            SDL_SCANCODE_KP_PERIOD => Key::KeyKPPeriod,
            SDL_SCANCODE_LSHIFT => Key::KeyLeftShift,
            SDL_SCANCODE_RSHIFT => Key::KeyRightShift,
            SDL_SCANCODE_LCTRL => Key::KeyLeftCtrl,
            SDL_SCANCODE_RCTRL => Key::KeyRightCtrl,
            SDL_SCANCODE_LALT => Key::KeyLeftAlt,
            SDL_SCANCODE_RALT => Key::KeyRightAlt,
            SDL_SCANCODE_LGUI => Key::KeyLeftOS,
            SDL_SCANCODE_RGUI => Key::KeyRightOS,
            SDL_SCANCODE_NONUSBACKSLASH => Key::KeyBackSlash,
            _ => Key::Total,
        }
    }
}

#[allow(non_snake_case)]
impl From<sys::SDL_GameControllerButton> for crate::events::GameControllerButton {
    fn from(b: sys::SDL_GameControllerButton) -> crate::events::GameControllerButton {
        use crate::events::GameControllerButton as GCB;
        use sys::SDL_GameControllerButton::*;
        match b {
            SDL_CONTROLLER_BUTTON_A => GCB::A,
            SDL_CONTROLLER_BUTTON_B => GCB::B,
            SDL_CONTROLLER_BUTTON_X => GCB::X,
            SDL_CONTROLLER_BUTTON_Y => GCB::Y,
            SDL_CONTROLLER_BUTTON_BACK => GCB::Back,
            SDL_CONTROLLER_BUTTON_GUIDE => GCB::Guide,
            SDL_CONTROLLER_BUTTON_START => GCB::Start,
            SDL_CONTROLLER_BUTTON_LEFTSTICK => GCB::LeftStick,
            SDL_CONTROLLER_BUTTON_RIGHTSTICK => GCB::RightStick,
            SDL_CONTROLLER_BUTTON_LEFTSHOULDER => GCB::LeftShoulder,
            SDL_CONTROLLER_BUTTON_RIGHTSHOULDER => GCB::RightShoulder,
            SDL_CONTROLLER_BUTTON_DPAD_UP => GCB::DPadUp,
            SDL_CONTROLLER_BUTTON_DPAD_DOWN => GCB::DPadDown,
            SDL_CONTROLLER_BUTTON_DPAD_LEFT => GCB::DPadLeft,
            SDL_CONTROLLER_BUTTON_DPAD_RIGHT => GCB::DPadRight,
            _ => GCB::Total,
        }
    }
}

#[allow(non_snake_case)]
impl From<sys::SDL_GameControllerAxis> for crate::events::GameControllerAxis {
    fn from(b: sys::SDL_GameControllerAxis) -> crate::events::GameControllerAxis {
        use crate::events::GameControllerAxis as GCA;
        use sys::SDL_GameControllerAxis::*;
        match b {
            SDL_CONTROLLER_AXIS_LEFTX => GCA::LeftX,
            SDL_CONTROLLER_AXIS_LEFTY => GCA::LeftY,
            SDL_CONTROLLER_AXIS_RIGHTX => GCA::RightX,
            SDL_CONTROLLER_AXIS_RIGHTY => GCA::RightY,
            SDL_CONTROLLER_AXIS_TRIGGERLEFT => GCA::LeftTrigger,
            SDL_CONTROLLER_AXIS_TRIGGERRIGHT => GCA::RightTrigger,
            _ => GCA::Total,
        }
    }
}

pub(crate) fn convert_keyboard_event(event: &sys::SDL_Event) -> crate::events::KeyboardEvent {
    let event_type = unsafe { event.type_ };
    debug_assert!(
        event_type == sys::SDL_EventType::SDL_KEYDOWN as u32
            || event_type == sys::SDL_EventType::SDL_KEYUP as u32
    );
    let key = crate::events::Key::from(unsafe { event.key.keysym.scancode });
    let state = if event_type == sys::SDL_EventType::SDL_KEYDOWN as u32 {
        crate::events::ButtonState::Down
    } else {
        crate::events::ButtonState::Up
    };
    crate::events::KeyboardEvent { state, key }
}

pub(crate) fn convert_mouse_move_event(event: &sys::SDL_Event) -> crate::events::MouseMoveEvent {
    debug_assert!(unsafe { event.type_ } == sys::SDL_EventType::SDL_MOUSEMOTION as u32);
    unsafe {
        crate::events::MouseMoveEvent {
            x: event.motion.x,
            y: event.motion.y,
            deltax: event.motion.xrel,
            deltay: event.motion.yrel,
        }
    }
}

pub(crate) fn convert_mouse_button_event(
    event: &sys::SDL_Event,
) -> crate::events::MouseButtonEvent {
    let event_type = unsafe { event.type_ };
    debug_assert!(
        event_type == sys::SDL_EventType::SDL_MOUSEBUTTONDOWN as u32
            || event_type == sys::SDL_EventType::SDL_MOUSEBUTTONUP as u32
    );
    let state = if event_type == sys::SDL_EventType::SDL_MOUSEBUTTONDOWN as u32 {
        crate::events::ButtonState::Down
    } else {
        crate::events::ButtonState::Up
    };
    unsafe {
        crate::events::MouseButtonEvent {
            state,
            button: match event.button.button as u32 {
                sys::SDL_BUTTON_LEFT => crate::events::MouseButton::Left,
                sys::SDL_BUTTON_RIGHT => crate::events::MouseButton::Right,
                sys::SDL_BUTTON_MIDDLE => crate::events::MouseButton::Middle,
                sys::SDL_BUTTON_X1 => crate::events::MouseButton::Extra0,
                sys::SDL_BUTTON_X2 => crate::events::MouseButton::Extra1,
                _ => crate::events::MouseButton::Total,
            },
            x: event.button.x,
            y: event.button.y,
        }
    }
}

pub(crate) fn convert_mouse_wheel_event(event: &sys::SDL_Event) -> crate::events::MouseWheelEvent {
    debug_assert!(unsafe { event.type_ } == sys::SDL_EventType::SDL_MOUSEWHEEL as u32);
    unsafe {
        crate::events::MouseWheelEvent {
            x: event.wheel.x,
            y: event.wheel.y,
        }
    }
}

pub(crate) fn convert_controller_button_event(
    event: &sys::SDL_Event,
    device_id: DeviceId,
) -> crate::events::GameControllerButtonEvent {
    let event_type = unsafe { event.type_ };
    debug_assert!(
        event_type == sys::SDL_EventType::SDL_CONTROLLERBUTTONUP as u32
            || event_type == sys::SDL_EventType::SDL_CONTROLLERBUTTONDOWN as u32
    );
    let button = crate::events::GameControllerButton::from(unsafe {
        std::mem::transmute::<i32, sys::SDL_GameControllerButton>(event.cbutton.button as i32)
    });
    let state = if event_type == sys::SDL_EventType::SDL_CONTROLLERBUTTONDOWN as u32 {
        crate::events::ButtonState::Down
    } else {
        crate::events::ButtonState::Up
    };
    crate::events::GameControllerButtonEvent {
        device_id,
        state,
        button,
    }
}

pub(crate) fn convert_controller_axis_event(
    event: &sys::SDL_Event,
    device_id: DeviceId,
) -> crate::events::GameControllerAxisEvent {
    debug_assert!(unsafe { event.type_ } == sys::SDL_EventType::SDL_CONTROLLERAXISMOTION as u32);
    let axis = crate::events::GameControllerAxis::from(unsafe {
        std::mem::transmute::<i32, sys::SDL_GameControllerAxis>(event.caxis.axis as i32)
    });
    crate::events::GameControllerAxisEvent {
        device_id,
        axis,
        value: unsafe { event.caxis.value as i32 },
    }
}

pub(crate) fn convert_text_event(
    event: &sys::SDL_Event,
) -> Result<crate::events::TextInputEvent, Utf8Error> {
    let event_type = unsafe { event.type_ };
    debug_assert!(event_type == sys::SDL_EventType::SDL_TEXTINPUT as u32);
    let str = unsafe { CStr::from_ptr(event.text.text.as_ptr() as *const c_char).to_str()? };
    // Only reports one character at a time, if this breaks then we need to adjust the code
    debug_assert_eq!(str.len(), 1);
    Ok(TextInputEvent {
        ch: str.chars().next().unwrap(),
    })
}
