//! SDL2 App implementation

use crate::app::{App, AppEventHandler, AppInitInfo, AppPaths, TickResult};
use crate::events::{AppEvent, InputEvent, WindowEvent, WindowResizedEvent};
use crate::input_device::InputSystem;
use crate::sdl2::events::{
    convert_controller_axis_event, convert_controller_button_event, convert_keyboard_event,
    convert_mouse_button_event, convert_mouse_move_event, convert_mouse_wheel_event,
    convert_text_event,
};
use crate::sdl2::input_device::InputSystem as SdlInputSystem;
use crate::sdl2::sdl_sys_utils::sdl2_error;
use crate::sdl2::window::WindowSystem as SdlWindowSystem;
use crate::window::WindowSystem;
use modus_log::log_error;
use modus_threed::instance::{Instance, InstanceCreateInfo};
use sdl2_sys as sys;
use std::ffi::CString;
use std::mem::transmute;
use std::os::raw::c_char;
use std::sync::Arc;

pub(crate) struct Sdl2App {
    paths: AppPaths,
    window_system: SdlWindowSystem,
    input_system: SdlInputSystem,
    is_paused: bool,
}

impl<'i> Sdl2App {
    pub(crate) fn create<'a>(
        _info: &AppInitInfo<'a>,
        threed_info: &'a InstanceCreateInfo,
    ) -> Result<Box<dyn crate::app::App>, String> {
        crate::profile_scope!();
        if unsafe {
            sys::SDL_Init(
                sys::SDL_INIT_VIDEO | sdl2_sys::SDL_INIT_EVENTS | sdl2_sys::SDL_INIT_GAMECONTROLLER,
            )
        } != 0
        {
            return Err(String::from(sdl2_error()));
        }

        unsafe {
            // Allow mouse click events to pass through if the window is not focused. If this is not
            // set, we have to click on the window to focus once and then again to get the event to
            // trigger.
            sys::SDL_SetHint(
                sys::SDL_HINT_MOUSE_FOCUS_CLICKTHROUGH.as_ptr() as *const c_char,
                [1, 0].as_ptr(),
            );
        }

        let vk_app_interface = Sdl2VkAppInterface {};
        let instance = match modus_threed::create_vk_instance(threed_info, &vk_app_interface) {
            Ok(instance) => instance,
            Err(e) => {
                unsafe { sys::SDL_Quit() };
                return Err(format!("Failed to create threed instance {:?}", e));
            }
        };

        let app = Self {
            paths: AppPaths::new(),
            window_system: SdlWindowSystem::new(instance),
            input_system: SdlInputSystem::new(),
            is_paused: false,
        };

        Ok(Box::new(app))
    }

    fn shutdown(&mut self) {
        crate::profile_scope!();
        self.input_system.shutdown();
        self.window_system.shutdown();
        unsafe { sys::SDL_Quit() };
    }
}

#[derive(Debug)]
struct Sdl2VkAppInterface {}

impl<'app> modus_threed::app_interface::VkAppInterface for Sdl2VkAppInterface {
    fn required_extension_list(&self) -> Result<Vec<*const c_char>, String> {
        use sys::SDL_WindowFlags::*;
        // Sadly we need an active Window to query this extension list. This is scheduled to
        // be deprecated in a future SDL2 version. For now we create a dummy window to extract
        // the info;
        let window_title = CString::new("VkInfo").unwrap_or_default();
        let window = unsafe {
            sys::SDL_CreateWindow(
                window_title.as_ptr(),
                0,
                0,
                10,
                10,
                SDL_WINDOW_VULKAN as u32 | SDL_WINDOW_HIDDEN as u32,
            )
        };
        if window.is_null() {
            return Err(format!(
                "Failed to get instance extension due to SDL2 error: {}",
                sdl2_error()
            ));
        }
        unsafe {
            sys::SDL_HideWindow(window);
        }
        let mut extensions = Vec::<*const c_char>::new();
        let mut ext_count = 0u32;
        if unsafe {
            sys::SDL_Vulkan_GetInstanceExtensions(window, &mut ext_count, std::ptr::null_mut())
        } == sys::SDL_bool::SDL_FALSE
        {
            unsafe { sys::SDL_DestroyWindow(window) };
            return Err(format!(
                "Failed to get instance extension count due to SDL2 error: {}",
                sdl2_error()
            ));
        }

        if ext_count > 0 {
            extensions.resize(ext_count as usize, std::ptr::null());
            if unsafe {
                sys::SDL_Vulkan_GetInstanceExtensions(
                    window,
                    &mut ext_count,
                    extensions.as_mut_ptr(),
                )
            } == sys::SDL_bool::SDL_FALSE
            {
                unsafe { sys::SDL_DestroyWindow(window) };
                return Err(format!(
                    "Failed to get instance extensions due to SDL2 error: {}",
                    sdl2_error()
                ));
            }
        }
        unsafe { sys::SDL_DestroyWindow(window) };
        Ok(extensions)
    }
}

impl App for Sdl2App {
    fn paths(&self) -> &AppPaths {
        &self.paths
    }

    #[allow(non_snake_case)]
    fn tick(&mut self, handler: &mut dyn AppEventHandler) -> TickResult {
        crate::profile_scope!();
        let mut sdl_event_uninit = std::mem::MaybeUninit::<sys::SDL_Event>::uninit();
        while unsafe { sys::SDL_PollEvent(sdl_event_uninit.as_mut_ptr()) != 0 } {
            use sys::SDL_EventType::*;
            let sdl_event = unsafe { sdl_event_uninit.assume_init() };

            match unsafe { transmute::<u32, sys::SDL_EventType>(sdl_event.type_) } {
                SDL_QUIT => {
                    return TickResult::Quit;
                }
                SDL_WINDOWEVENT => {
                    let sdl_window_id: u32 = unsafe { sdl_event.window.windowID };
                    let window = if let Some(win) =
                        self.window_system.find_window_by_sdl_id(sdl_window_id)
                    {
                        win
                    } else {
                        continue;
                    };
                    let window_id = window.window_id;
                    use sys::SDL_WindowEventID::*;
                    match unsafe {
                        transmute::<u32, sys::SDL_WindowEventID>(sdl_event.window.event as u32)
                    } {
                        SDL_WINDOWEVENT_HIDDEN => self.is_paused = true,
                        SDL_WINDOWEVENT_FOCUS_LOST => {
                            let event = AppEvent::Window(WindowEvent::FocusLost(window_id));
                            handler.on_app_event(&event);
                        }
                        SDL_WINDOWEVENT_FOCUS_GAINED => {
                            let event = AppEvent::Window(WindowEvent::FocusGained(window_id));
                            handler.on_app_event(&event);
                        }
                        SDL_WINDOWEVENT_LEAVE => {
                            let event = AppEvent::Window(WindowEvent::MouseLeave(window_id));
                            handler.on_app_event(&event);
                        }
                        SDL_WINDOWEVENT_ENTER => {
                            let event = AppEvent::Window(WindowEvent::MouseEnter(window_id));
                            handler.on_app_event(&event);
                        }
                        SDL_WINDOWEVENT_SHOWN | SDL_WINDOWEVENT_EXPOSED => self.is_paused = true,
                        SDL_WINDOWEVENT_RESIZED => {
                            let w = unsafe { sdl_event.window.data1 };
                            let h = unsafe { sdl_event.window.data2 };
                            debug_assert!(w >= 0 && h >= 0);
                            crate::profile_scope!("Window Resized Event");
                            let event =
                                AppEvent::Window(WindowEvent::Resized(WindowResizedEvent {
                                    window_id,
                                    width: w as u32,
                                    height: h as u32,
                                }));
                            handler.on_app_event(&event);
                        }
                        _ => {}
                    }
                }
                SDL_KEYDOWN | SDL_KEYUP => {
                    let event = InputEvent::Keyboard(convert_keyboard_event(&sdl_event));
                    handler.on_input_event(&event);
                }
                SDL_MOUSEMOTION => {
                    let event = InputEvent::MouseMove(convert_mouse_move_event(&sdl_event));
                    handler.on_input_event(&event);
                }
                SDL_MOUSEBUTTONDOWN | SDL_MOUSEBUTTONUP => {
                    let event = InputEvent::MouseButton(convert_mouse_button_event(&sdl_event));
                    handler.on_input_event(&event);
                }
                SDL_MOUSEWHEEL => {
                    let event = InputEvent::MouseWheel(convert_mouse_wheel_event(&sdl_event));
                    handler.on_input_event(&event);
                }
                SDL_TEXTINPUT => {
                    if let Ok(evt) = convert_text_event(&sdl_event) {
                        let event = InputEvent::TextInput(evt);
                        handler.on_input_event(&event);
                    } else {
                        log_error!("Text input utf8 conversion error");
                    }
                }
                SDL_CONTROLLERBUTTONUP | SDL_CONTROLLERBUTTONDOWN => {
                    if let Some(controller) = self
                        .input_system
                        .find_controller_by_sdl_id(unsafe { sdl_event.cbutton.which })
                    {
                        let event = InputEvent::GameControllerButton(
                            convert_controller_button_event(&sdl_event, controller.device_id),
                        );
                        handler.on_input_event(&event);
                    }
                }
                SDL_CONTROLLERAXISMOTION => {
                    if let Some(controller) = self
                        .input_system
                        .find_controller_by_sdl_id(unsafe { sdl_event.cbutton.which })
                    {
                        let event = InputEvent::GameControllerAxis(convert_controller_axis_event(
                            &sdl_event,
                            controller.device_id,
                        ));
                        handler.on_input_event(&event);
                    }
                }
                SDL_CONTROLLERDEVICEADDED => {
                    if let Some(id) = self
                        .input_system
                        .add_controller(unsafe { sdl_event.cdevice.which })
                    {
                        let event = InputEvent::GameControllerConnected(id);
                        handler.on_input_event(&event);
                    }
                }
                SDL_CONTROLLERDEVICEREMOVED => {
                    if let Some(id) = self
                        .input_system
                        .remove_controller(unsafe { sdl_event.cdevice.which })
                    {
                        let event = InputEvent::GameControllerDisconnected(id);
                        handler.on_input_event(&event);
                    }
                }
                _ => {}
            }
        }
        if !self.is_paused {
            TickResult::Continue
        } else {
            TickResult::Paused
        }
    }

    fn window_system(&self) -> &dyn WindowSystem {
        &self.window_system
    }

    fn window_system_mut(&mut self) -> &mut dyn WindowSystem {
        &mut self.window_system
    }

    fn input_system(&self) -> &dyn InputSystem {
        &self.input_system
    }

    fn input_system_mut(&mut self) -> &mut dyn InputSystem {
        &mut self.input_system
    }

    fn threed_instance(&self) -> Arc<dyn Instance> {
        self.window_system.threed_instance.clone()
    }
}

impl Drop for Sdl2App {
    fn drop(&mut self) {
        self.shutdown();
    }
}
