use modus_app::app::AppInitInfo;
use modus_app::create_app;
use modus_app::window::{FullscreenMode, WindowCreateInfo};
use modus_threed::instance::InstanceCreateInfo;
use serial_test::serial;
use std::process::exit;

#[test]
#[serial]
fn startup_shutdown() {
    let app_init_info = AppInitInfo {
        name: "test",
        name_preferences: "test",
    };

    let threed_init_info = InstanceCreateInfo {
        name: "Test",
        version_major: 0,
        version_minor: 0,
        version_patch: 0,
        debug: false,
        api_validation: false,
    };

    let app = create_app(&app_init_info, &threed_init_info);
    assert!(app.is_ok());
    if let Err(e) = app {
        eprintln!("Failed to init app: {}", e);
    }
}

#[test]
#[serial]
fn create_destroy_window() {
    let app_init_info = AppInitInfo {
        name: "test",
        name_preferences: "test",
    };

    let threed_init_info = InstanceCreateInfo {
        name: "Test",
        version_major: 0,
        version_minor: 0,
        version_patch: 0,
        debug: false,
        api_validation: false,
    };

    let mut app = match create_app(&app_init_info, &threed_init_info) {
        Err(e) => {
            eprintln!("Failed to init app: {}", e);
            assert!(false);
            exit(-1);
        }
        Ok(a) => a,
    };

    let window_info = WindowCreateInfo {
        title: "Test Window",
        width: None,
        height: None,
        x: None,
        y: None,
        fullscreen: FullscreenMode::None,
        hidden: false,
    };

    let window_sys = app.window_system_mut();

    let window = window_sys.create_window(&window_info);
    assert!(window.is_ok());
    window_sys.resole_window_mut(window.unwrap()).map(|w| {
        w.show();
    });
}
