use modus_app::app::{AppEventHandler, AppInitInfo, TickResult};
use modus_app::create_app;
use modus_app::events::{AppEvent, InputEvent};
use modus_app::window::{FullscreenMode, WindowCreateInfo};
use modus_log::*;
use modus_threed::instance::InstanceCreateInfo;

struct Printer {}

impl AppEventHandler for Printer {
    fn on_app_event(&mut self, event: &AppEvent) {
        log_info!(target:"app","{}", event);
    }

    fn on_input_event(&mut self, event: &InputEvent) {
        log_info!(target:"input", "{}", event);
    }
}

fn main() -> Result<(), ()> {
    if let Err(e) = init_log_system() {
        eprintln!("Failed to init log system: {}", e);
        return Err(());
    }
    let app_init_info = AppInitInfo {
        name: "Input Print Example",
        name_preferences: "modus.input_print_example",
    };

    let threed_init_info = InstanceCreateInfo {
        name: "Input Print Example",
        version_major: 0,
        version_minor: 0,
        version_patch: 0,
        debug: false,
        api_validation: false,
    };

    let mut app = match create_app(&app_init_info, &threed_init_info) {
        Err(e) => {
            eprintln!("Failed to init app: {}", e);
            return Err(());
        }
        Ok(a) => a,
    };

    let window_info = WindowCreateInfo {
        title: "Input Print Test",
        width: None,
        height: None,
        x: None,
        y: None,
        fullscreen: FullscreenMode::None,
        hidden: false,
    };

    let window_sys = app.window_system_mut();

    let window = window_sys.create_window(&window_info);
    assert!(window.is_ok());
    window_sys.resole_window_mut(window.unwrap()).map(|w| {
        w.show();
    });

    let mut printer = Printer {};
    loop {
        let status = app.tick(&mut printer);
        if status == TickResult::Quit {
            break;
        }
    }
    Ok(())
}
