use crate::queues::Queue;
use crate::storage::JobStorage;
use modus_core::sync::Mutex;
use std::cell::RefCell;
use std::fmt::{Debug, Formatter};
use std::marker::PhantomData;
use std::sync::atomic::AtomicI32;
use std::sync::atomic::Ordering;

pub struct Job {
    ref_count: AtomicI32,
    storage: RefCell<JobStorage>,
    pub(crate) unfinished_count: AtomicI32,
    pub(crate) parents: Mutex<Vec<JobPtr>>,
    pub(crate) chained_jobs: Mutex<Vec<JobPtr>>,
}

pub trait JobTrait: Sized + Send + FnOnce() {}
impl<T: Sized + Send + FnOnce()> JobTrait for T {}

impl Job {
    fn build_parent_vec(parents: &[JobPtr]) -> Vec<JobPtr> {
        let vec = Vec::from(parents);
        for parent in &vec {
            parent
                .unfinished_count
                .fetch_add(1, std::sync::atomic::Ordering::AcqRel);
        }
        vec
    }

    pub(crate) fn new<T: JobTrait>(value: T, parents: &[JobPtr]) -> Self {
        Self {
            ref_count: AtomicI32::new(1),
            storage: RefCell::new(JobStorage::new(value)),
            unfinished_count: AtomicI32::new(1),
            chained_jobs: Mutex::new(Vec::new()),
            parents: Mutex::new(Self::build_parent_vec(parents)),
        }
    }

    pub(crate) fn with_chained<T: JobTrait>(
        value: T,
        parents: &[JobPtr],
        chained: &[JobPtr],
    ) -> Self {
        Self {
            ref_count: AtomicI32::new(1),
            storage: RefCell::new(JobStorage::new(value)),
            unfinished_count: AtomicI32::new(1),
            chained_jobs: Mutex::new(Vec::from(chained)),
            parents: Mutex::new(Self::build_parent_vec(parents)),
        }
    }

    pub(crate) fn is_finished(&self) -> bool {
        self.unfinished_count.load(Ordering::Acquire) <= 0
    }

    pub(crate) fn run(&self, queue: &Queue) {
        self.storage.borrow_mut().run();
        self.finish(queue);
    }

    fn launch_chained_jobs(&self, queue: &Queue) {
        let lock_guard = self.chained_jobs.lock();
        for job in lock_guard.iter() {
            queue.push(job.clone());
        }
    }

    fn on_child_finished(&self, queue: &Queue) {
        let prev = self.unfinished_count.fetch_sub(1, Ordering::AcqRel);
        if prev == 1 {
            self.launch_chained_jobs(queue);
        }
        let lock_guard = self.parents.lock();
        for parent in lock_guard.iter() {
            parent.on_child_finished(queue);
        }
    }

    fn finish(&self, queue: &Queue) {
        self.on_child_finished(queue);
    }

    pub(crate) fn add_chained(&self, jobs: &[JobPtr]) {
        let mut lock_guard = self.chained_jobs.lock();
        lock_guard.extend_from_slice(jobs);
    }

    pub(crate) fn add_parents(&self, jobs: &[JobPtr]) {
        let mut lock_guard = self.parents.lock();
        for job in jobs {
            job.unfinished_count.fetch_add(1, Ordering::AcqRel);
            lock_guard.push(job.clone());
        }
    }
}

impl Debug for Job {
    fn fmt(&self, f: &mut Formatter<'_>) -> std::fmt::Result {
        write!(
            f,
            "Job {{unfinished:{}}}",
            self.unfinished_count
                .load(std::sync::atomic::Ordering::Acquire)
        )
    }
}

unsafe impl Send for Job {}

impl modus_core::sync::intrusiverc::RefCounted for Job {
    fn increment_ref(&self) {
        self.ref_count
            .fetch_add(1, std::sync::atomic::Ordering::AcqRel);
    }

    fn decrement_ref(&self) -> i32 {
        self.ref_count
            .fetch_sub(1, std::sync::atomic::Ordering::AcqRel)
    }
}

modus_core::declare_tls_pool_allocator_intrusiverc!(pub, JobAllocator, JOB_ALLOCATOR, Job);

pub type JobPtr = modus_core::sync::intrusiverc::IntrusiveArc<Job, JobAllocator>;

#[derive(Debug)]
pub struct ScopedJobPtr<'scope, T: JobTrait> {
    pub(crate) job: JobPtr,
    pub(crate) scope: PhantomData<&'scope mut ()>,
    pub(crate) p: PhantomData<T>,
}
