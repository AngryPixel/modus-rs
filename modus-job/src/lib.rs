//! Job System implementation
use crate::job::{Job, JobPtr, ScopedJobPtr};
use crate::storage::can_store_type;
use crate::thread::{JobThread, ThreadData};
use std::sync::Arc;

mod job;
pub mod job_graph;
mod queues;
mod storage;
mod thread;

pub(crate) const PROFILE_COLOR0: u32 = modus_profiler::COLOR_DARK_TEAL;
#[macro_export(crate)]
macro_rules! profile_scope {
    () => {
        modus_profiler::scope_color!(crate::PROFILE_COLOR0);
    };
    ($name:literal) => {
        modus_profiler::scope_color!(crate::PROFILE_COLOR0, $name);
    };
}

#[derive(Debug)]
pub enum Error {
    /// Failed to create/start a job Thread
    ThreadCreate,
    /// The thread queue is full and can't be submitted to
    QueueFull,
    /// We have detected multiple instances of a job system created on the same thread. This is not
    /// supported.
    MultipleInstances,
    /// Parent and child handles are the same
    ParentEqualsChild,
    /// Thread Local Job Pool has not been initialized
    InvalidThread,
}

pub struct JobSystem {
    thread_data: Arc<Vec<ThreadData>>,
    threads: Vec<JobThread>,
}

impl JobSystem {
    /// # Safety
    /// Only safe if you can guarantee that during the execution, there will be no writes to shared
    /// data.
    pub unsafe fn allocate_job<T: Sized + FnOnce() + Send>(
        value: T,
        parents: &[JobPtr],
    ) -> crate::job::JobPtr {
        assert!(can_store_type::<T>());
        crate::profile_scope!();
        job::JobAllocator::allocate(Job::new(value, parents))
    }

    /// # Safety
    /// Only safe if you can guarantee that during the execution, there will be no writes to shared
    /// data.
    pub unsafe fn allocate_job_with_chained<T: Sized + FnOnce() + Send>(
        value: T,
        parents: &[JobPtr],
        chained: &[JobPtr],
    ) -> crate::job::JobPtr {
        assert!(can_store_type::<T>());
        crate::profile_scope!();
        job::JobAllocator::allocate(Job::with_chained(value, parents, chained))
    }
    /// Create a new instance of a JobSystem. To create jobs, please create a new instance of a
    /// [JobScope].
    ///
    /// * `thread_count` - Number of worker threads.
    /// 2 it will be rounded up to the next value which is a power of 2.
    pub fn new(thread_count: usize) -> Result<Self, Error> {
        crate::profile_scope!();
        if !thread::tls_set_main_thread_index() {
            return Err(Error::MultipleInstances);
        }
        let actual_thread_count = thread_count.max(1) + 1;
        let mut data_vec: Vec<ThreadData> = Vec::with_capacity(actual_thread_count);
        data_vec.resize_with(actual_thread_count, ThreadData::new);
        let thread_data = Arc::new(data_vec);
        let mut threads = Vec::with_capacity(thread_count);
        for index in 1..actual_thread_count {
            let mut thread = JobThread::new(thread_data.clone(), index);
            if thread.start().is_err() {
                return Err(Error::ThreadCreate);
            }
            threads.push(thread);
        }
        thread::tls_set_thread_data(thread_data.clone());
        Ok(Self {
            thread_data,
            threads,
        })
    }

    fn shutdown(&mut self) {
        crate::profile_scope!();
        for thread in &mut self.threads {
            thread.finish().unwrap()
        }
        self.threads.clear();
        self.thread_data = Arc::new(vec![]);
        thread::tls_reset_main_thread_index();
        thread::tls_reset_thread_data();
    }

    #[inline(always)]
    pub fn is_finished(job: &Job) -> bool {
        job.is_finished()
    }

    /// Execute a job
    /// # Safety
    /// This call is only safe if you can guarantee that the job does not perform multiple writes
    /// to any data.
    pub unsafe fn run(job: JobPtr) -> Result<(), Error> {
        if let Some(thread_data) = thread::tls_get_thread_data() {
            thread::start_job(&thread_data, job);
            Ok(())
        } else {
            Err(Error::InvalidThread)
        }
    }

    /// Block and wait until the job has finished.
    /// While we wait, the system will try to execute other jobs.
    pub fn wait(job: &Job) -> Result<(), Error> {
        crate::profile_scope!();
        if let Some(thread_data) = thread::tls_get_thread_data() {
            while !job.is_finished() {
                if let Some(other_job) = thread::get_job(&thread_data) {
                    thread::run_job(&thread_data, other_job);
                }
            }
            Ok(())
        } else {
            Err(Error::InvalidThread)
        }
    }
}

impl Drop for JobSystem {
    fn drop(&mut self) {
        self.shutdown();
    }
}

#[derive(Debug)]
pub struct ChainList {
    list: Vec<JobPtr>,
}

impl ChainList {
    pub fn new() -> Self {
        Self {
            list: Vec::with_capacity(4),
        }
    }

    pub fn push_scoped<T: FnOnce() + Send>(&mut self, scoped_job: &ScopedJobPtr<T>) {
        self.list.push(scoped_job.job.clone())
    }

    pub fn push(&mut self, job_ptr: JobPtr) {
        self.list.push(job_ptr)
    }

    pub fn clear(&mut self) {
        self.list.clear();
    }
}

impl Default for ChainList {
    fn default() -> Self {
        Self::new()
    }
}

/// Provides a safe implementation for job creation and execution
#[derive(Debug)]
pub struct JobScope {
    thread_data: std::sync::Arc<Vec<ThreadData>>,
}

impl JobScope {
    pub fn new() -> Result<Self, Error> {
        if let Some(thread_data) = thread::tls_get_thread_data() {
            return Ok(Self { thread_data });
        }
        Err(Error::InvalidThread)
    }

    pub fn from_system(job_system: &JobSystem) -> Self {
        Self {
            thread_data: job_system.thread_data.clone(),
        }
    }

    pub fn create_noop(&self) -> ScopedJobPtr<fn()> {
        ScopedJobPtr {
            job: unsafe { JobSystem::allocate_job(|| {}, &[]) },
            scope: Default::default(),
            p: Default::default(),
        }
    }

    pub fn create<T: Sized + FnOnce() + Send>(&self, job: T) -> ScopedJobPtr<T> {
        ScopedJobPtr {
            // This is safe since the scope guarantees the requirements
            job: unsafe { JobSystem::allocate_job(job, &[]) },
            scope: Default::default(),
            p: Default::default(),
        }
    }

    pub fn create_with_chained<T: Sized + FnOnce() + Send, Y: Sized + FnOnce() + Send>(
        &self,
        job: T,
        parents: &[JobPtr],
        chained: &ScopedJobPtr<Y>,
    ) -> ScopedJobPtr<T> {
        let chained = std::slice::from_ref(&chained.job);
        ScopedJobPtr {
            // This is safe since the scope guarantees the requirements
            job: unsafe { JobSystem::allocate_job_with_chained(job, parents, chained) },
            scope: Default::default(),
            p: Default::default(),
        }
    }

    pub fn create_with_chained_list<T: Sized + FnOnce() + Send>(
        &self,
        job: T,
        parents: &[JobPtr],
        list: &ChainList,
    ) -> ScopedJobPtr<T> {
        ScopedJobPtr {
            // This is safe since the scope guarantees the requirements
            job: unsafe { JobSystem::allocate_job_with_chained(job, parents, &list.list) },
            scope: Default::default(),
            p: Default::default(),
        }
    }

    pub fn create_with_parent<T: Sized + FnOnce() + Send, Y: Sized + FnOnce() + Send>(
        &self,
        job: T,
        parent: &ScopedJobPtr<Y>,
    ) -> ScopedJobPtr<T> {
        ScopedJobPtr {
            // This is safe since the scope guarantees the requirements
            job: unsafe { JobSystem::allocate_job(job, &[parent.job.clone()]) },
            scope: Default::default(),
            p: Default::default(),
        }
    }

    pub fn run<T: Sized + FnOnce() + Send>(&self, scoped_job: &ScopedJobPtr<T>) {
        thread::start_job(&self.thread_data, scoped_job.job.clone());
    }

    pub(crate) fn run_job_ptr(&self, job: JobPtr) {
        thread::start_job(&self.thread_data, job);
    }

    pub fn wait<T: Sized + FnOnce() + Send>(&self, scoped_job: &ScopedJobPtr<T>) {
        crate::profile_scope!();
        while !scoped_job.job.is_finished() {
            if let Some(other_job) = thread::get_job(&self.thread_data) {
                thread::run_job(&self.thread_data, other_job);
            }
        }
    }

    pub(crate) fn wait_job_ptr(&self, job: &JobPtr) {
        crate::profile_scope!();
        while !job.is_finished() {
            if let Some(other_job) = thread::get_job(&self.thread_data) {
                thread::run_job(&self.thread_data, other_job);
            }
        }
    }

    #[inline(always)]
    pub fn is_finished<T: Sized + FnOnce() + Send>(&self, scoped_job: &ScopedJobPtr<T>) -> bool {
        scoped_job.job.is_finished()
    }
}

#[cfg(test)]
mod tests {
    use crate::{ChainList, JobScope, JobSystem};

    const THREAD_COUNT: usize = 4;

    #[test]
    fn start_stop() {
        let r = JobSystem::new(THREAD_COUNT);
        assert!(r.is_ok());
    }

    #[test]
    fn launch_jobs_check_overflow() {
        let job_sys = JobSystem::new(THREAD_COUNT).expect("Failed to init job system");
        let scope = JobScope::from_system(&job_sys);
        let mut _counter = 0_usize;
        for _ in 0..256_u32 {
            const JOB_COUNT: usize = 128;
            let mut jobs = Vec::<_>::with_capacity(JOB_COUNT);

            for _ in 0..JOB_COUNT {
                let handle = scope.create_noop();
                scope.run(&handle);
                jobs.push(handle);
            }
            for job in &jobs {
                scope.wait(job);
            }
            _counter += JOB_COUNT;
        }
    }
    #[test]
    fn launch_jobs_from_job_threads() {
        let job_sys = JobSystem::new(THREAD_COUNT).expect("Failed to init job system");
        let scope = JobScope::from_system(&job_sys);
        const JOB_COUNT: u32 = 32;
        let mut jobs = Vec::<_>::with_capacity(JOB_COUNT as usize);
        for _ in 0..JOB_COUNT {
            let job = scope.create(|| {
                let thread_job_scope = JobScope::new().unwrap();
                let thread_job = thread_job_scope.create_noop();
                thread_job_scope.run(&thread_job);
            });
            scope.run(&job);
            jobs.push(job);
        }
        for job in &jobs {
            scope.wait(job);
        }
    }

    #[test]
    fn launch_jobs_with_ref() {
        let job_sys = JobSystem::new(THREAD_COUNT).expect("Failed to init job system");
        let scope = JobScope::from_system(&job_sys);
        const JOB_COUNT: usize = 100;
        let mut jobs = Vec::<_>::with_capacity(JOB_COUNT);

        let val = std::sync::atomic::AtomicU32::new(0);
        for _ in 0..JOB_COUNT {
            let val_ref = &val;
            let job = scope.create(move || {
                val_ref.fetch_add(10, std::sync::atomic::Ordering::Release);
            });
            scope.run(&job);
            jobs.push(job);
        }
        for job in jobs {
            scope.wait(&job);
        }
        assert_eq!(
            val.load(std::sync::atomic::Ordering::Acquire),
            10 * JOB_COUNT as u32
        );
    }

    /*
     this test is always supposed to fail since we have multiple writable access to `val`
    #[test]
    fn launch_jobs_with_ref2() {
        //NOTE: This should not be possible, undefined behavior
        let job_sys =
            JobSystem::new(THREAD_COUNT, JOB_CAPACITY).expect("Failed to init job system");
        let scope = JobScope::from_system(&job_sys);
        const JOB_COUNT: usize = 100;
        let mut jobs = Vec::<_>::with_capacity(JOB_COUNT);

        let mut val = 0u32;
        for _ in 0..JOB_COUNT {
            let job = scope.create(|| {
                val += 10;
            });
            let r = scope.run(&job);
            assert!(r.is_ok());
            jobs.push(job);
        }
        for job in &jobs {
            scope.wait(&job);
        }
        assert_eq!(val, 10 * JOB_COUNT as u32);
    }
     */

    #[test]
    fn launch_jobs_chained() {
        // will print out the execution order in reverse
        let job_sys = JobSystem::new(THREAD_COUNT).expect("Failed to init job system");
        const JOB_COUNT: usize = 20;
        let job_scope = JobScope::from_system(&job_sys);
        let mut jobs = Vec::<_>::with_capacity(JOB_COUNT);
        for i in 0..JOB_COUNT {
            let closure = move || {
                println!("Chained {:?}: Job {:02}", std::thread::current().id(), i);
            };
            let handle = if i > 0 {
                let prev_handle = &jobs[i - 1];
                job_scope.create_with_chained(closure, &[], prev_handle)
            } else {
                job_scope.create(closure)
            };
            jobs.push(handle);
        }
        job_scope.run(&jobs.last().unwrap());
        job_scope.wait(&jobs.first().unwrap());
    }

    #[test]
    fn launch_with_parent() {
        let job_sys = JobSystem::new(THREAD_COUNT).expect("Failed to init job system");
        let job_scope = JobScope::from_system(&job_sys);
        const JOB_COUNT: usize = 20;
        let parent = job_scope.create_noop();
        let mut jobs = Vec::<_>::with_capacity(JOB_COUNT);
        for i in 1..JOB_COUNT {
            let handle = job_scope.create_with_parent(
                move || {
                    println!(
                        "LaunchWithParent {:?}: Job {:02}",
                        std::thread::current().id(),
                        i
                    );
                },
                &parent,
            );
            jobs.push(handle);
        }
        job_scope.run(&parent);
        for job in &jobs {
            job_scope.run(job);
        }
        println!("Waiting on Parent {:?}", std::thread::current().id());
        while !job_scope.is_finished(&parent) {}
    }

    #[test]
    fn launch_job_groups() {
        let job_sys = JobSystem::new(THREAD_COUNT).expect("Failed to init job system");
        let job_scope = JobScope::from_system(&job_sys);
        // Dependency chain:
        //  A <- B <-----\
        //        \- D <- + <- F
        //       C <------/
        //       E <-----/

        let job_f = job_scope.create(|| println!("{:?}: Job F", std::thread::current().id()));

        let glue_job_f = job_scope.create_with_chained(|| {}, &[], &job_f);

        let job_d = job_scope.create_with_parent(
            || println!("{:?}: Job D", std::thread::current().id()),
            &glue_job_f,
        );

        let job_c = job_scope.create_with_parent(
            || println!("{:?}: Job C", std::thread::current().id()),
            &glue_job_f,
        );

        let job_e = job_scope.create_with_parent(
            || println!("{:?}: Job E", std::thread::current().id()),
            &glue_job_f,
        );

        let job_b = job_scope.create_with_chained(
            || println!("{:?}: Job B", std::thread::current().id()),
            &[glue_job_f.job.clone()],
            &job_d,
        );

        let mut chain_list = ChainList::new();
        chain_list.push_scoped(&job_b);
        chain_list.push_scoped(&job_c);
        chain_list.push_scoped(&job_e);

        let job_a = job_scope.create_with_chained_list(
            || println!("{:?}: Job A", std::thread::current().id()),
            &[],
            &chain_list,
        );

        job_scope.run(&glue_job_f);
        job_scope.run(&job_a);
        job_scope.wait(&job_f);
    }
}
