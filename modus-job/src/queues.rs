#![allow(dead_code)]

use crate::JobPtr;
use crossbeam_deque::Worker;
pub type Queue = Worker<JobPtr>;
