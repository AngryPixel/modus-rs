const JOB_STORAGE_SIZE: usize = 128;

pub(crate) const fn can_store_type<T: Sized>() -> bool {
    let self_alignment = std::mem::align_of::<JobStorage>();
    let type_alignment = std::mem::align_of::<T>();
    let type_size = std::mem::size_of::<T>();
    if type_alignment <= self_alignment {
        // If the alignment of the type <= our alignment then will fit as long as the size
        // of the type is smaller than our capacity
        type_size <= JOB_STORAGE_SIZE
    } else {
        // If the type has greater capacity, we also need to account for fact that the
        // returned pointer needs to be properly aligned so we will lose at the maximum
        // the alignment of the type - our own alignment bytes.
        type_size + (type_alignment - self_alignment) <= JOB_STORAGE_SIZE
    }
}

pub(crate) struct JobStorage {
    data: [u8; JOB_STORAGE_SIZE],
    run_fn: Option<fn(&mut JobStorage)>,
    drop_fn: Option<fn(&mut JobStorage)>,
}

impl JobStorage {
    unsafe fn type_ptr<T>(&mut self) -> *mut T {
        let type_alignment = std::mem::align_of::<T>();
        let alignment_offset = self.data.as_mut_ptr().align_offset(type_alignment);
        self.data.as_mut_ptr().add(alignment_offset) as *mut T
    }

    fn take<T>(&mut self) -> T {
        self.run_fn = None;
        self.drop_fn = None;
        unsafe { std::ptr::read(self.type_ptr()) }
    }

    fn store<T>(&mut self, value: T) {
        unsafe {
            std::ptr::write(self.type_ptr(), value);
        }
    }

    pub(crate) fn new<T: FnOnce() + Sized + Send>(value: T) -> Self {
        assert!(can_store_type::<T>(), "Can't store type in job storage");
        let run_fn = |storage: &mut JobStorage| {
            let value = storage.take::<T>();
            value();
        };
        let drop_fn = |storage: &mut JobStorage| {
            let value = storage.take::<T>();
            std::mem::drop(value);
        };
        let mut storage = Self {
            run_fn: Some(run_fn),
            drop_fn: Some(drop_fn),
            data: [0; JOB_STORAGE_SIZE],
        };
        storage.store(value);
        storage
    }

    pub(crate) fn run(&mut self) {
        if let Some(run_fn) = self.run_fn {
            (run_fn)(self);
        }
    }
}

impl Drop for JobStorage {
    fn drop(&mut self) {
        if let Some(drop_fn) = self.drop_fn {
            (drop_fn)(self)
        }
    }
}
