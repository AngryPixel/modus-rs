use crate::job::JobTrait;
use crate::{Error, JobPtr, JobScope};
use modus_core::sorting::{TopologicalSortError, TopologicalSorter};
use modus_core::HashMap;
use std::collections::hash_map::Entry;
use std::fmt::Debug;
use std::hash::Hash;

pub trait JobGraphKeyTrait: Debug + Copy + Clone + Eq + PartialEq + Hash {}
impl<T: Debug + Copy + Clone + Eq + PartialEq + Hash> JobGraphKeyTrait for T {}

#[derive(Debug, Copy, Clone, Eq, PartialEq, Hash)]
pub enum JobGraphError<T: JobGraphKeyTrait> {
    DuplicateId(T),
    InvalidJobThread,
    EmptyGraph,
    DependencyCycle(T, T),
    DependencyNotFound(T),
}

impl<T: JobGraphKeyTrait> From<Error> for JobGraphError<T> {
    fn from(e: Error) -> Self {
        match e {
            Error::InvalidThread => JobGraphError::InvalidJobThread,
            _ => {
                unreachable!()
            }
        }
    }
}

impl<T: JobGraphKeyTrait> From<TopologicalSortError<T>> for JobGraphError<T> {
    fn from(e: TopologicalSortError<T>) -> Self {
        match e {
            TopologicalSortError::DuplicateNode(k) => JobGraphError::DuplicateId(k),
            TopologicalSortError::DependencyCycle(k1, k2) => JobGraphError::DependencyCycle(k1, k2),
            TopologicalSortError::DependencyNotFound(k) => JobGraphError::DependencyNotFound(k),
            TopologicalSortError::EmtpyGraph => JobGraphError::EmptyGraph,
        }
    }
}

pub type JobGraphResult<T, K> = Result<T, JobGraphError<K>>;

#[derive(Debug)]
struct JobElement<K: JobGraphKeyTrait> {
    job: JobPtr,
    dependencies: Vec<K>,
}

#[derive(Debug)]
struct JobGraphCache<K: JobGraphKeyTrait> {
    jobs: HashMap<K, JobElement<K>>,
    sorter: TopologicalSorter<K>,
}

#[derive(Debug)]
pub struct JobGraph<K: JobGraphKeyTrait> {
    cache: JobGraphCache<K>,
    job_scope: JobScope,
}

impl<K: JobGraphKeyTrait> JobGraph<K> {
    pub fn new() -> JobGraphResult<Self, K> {
        let job_scope = JobScope::new()?;
        Ok(Self {
            cache: JobGraphCache {
                jobs: HashMap::new(),
                sorter: TopologicalSorter::new(),
            },
            job_scope,
        })
    }

    pub fn add<T: JobTrait>(&mut self, id: K, dependencies: &[K], job: T) -> JobGraphResult<(), K> {
        let scoped_job = self.job_scope.create(job);
        match self.cache.jobs.entry(id) {
            Entry::Occupied(_) => return Err(JobGraphError::DuplicateId(id)),
            Entry::Vacant(v) => {
                v.insert(JobElement {
                    job: scoped_job.job,
                    dependencies: Vec::from(dependencies),
                });
            }
        }
        self.cache.sorter.add(id, dependencies)?;
        Ok(())
    }

    pub fn build_and_execute(&mut self) -> JobGraphResult<(), K> {
        crate::profile_scope!();
        let order = self.cache.sorter.build()?;

        let mut group_jobs = Vec::new();
        let mut root_jobs = Vec::new();
        for id in order.iter().rev() {
            let job_element = if let Some(e) = self.cache.jobs.get(id) {
                e
            } else {
                return Err(JobGraphError::DependencyNotFound(*id));
            };

            if job_element.dependencies.is_empty() {
                root_jobs.push(job_element.job.clone());
            } else if job_element.dependencies.len() == 1 {
                let dependency = if let Some(e) = self.cache.jobs.get(&job_element.dependencies[0])
                {
                    e
                } else {
                    return Err(JobGraphError::DependencyNotFound(*id));
                };
                dependency.job.add_chained(&[job_element.job.clone()]);
            } else {
                let group_job = self.job_scope.create(|| {});
                group_job.job.add_chained(&[job_element.job.clone()]);
                for dep in &job_element.dependencies {
                    let dependency = if let Some(e) = self.cache.jobs.get(dep) {
                        e
                    } else {
                        return Err(JobGraphError::DependencyNotFound(*id));
                    };
                    dependency.job.add_parents(&[group_job.job.clone()]);
                }
                group_jobs.push(group_job.job.clone());
            }
            // if job has no dependencies it's a root job and it can be executed,
            // if a job has dependencies:
            //  1-> add as a chained job
            //  N-> add group job that has dependencies as child jobs, and which chain
            //      launches the current job when completed - may require, adding
            //      more parent jobs
        }

        // execute all top level jobs
        for job in &group_jobs {
            self.job_scope.run_job_ptr(job.clone());
        }
        root_jobs.reverse();
        for job in &root_jobs {
            self.job_scope.run_job_ptr(job.clone());
        }
        Ok(())
    }

    pub fn wait_on_job(&self, id: K) -> JobGraphResult<(), K> {
        crate::profile_scope!();
        if let Some(job) = self.cache.jobs.get(&id) {
            self.job_scope.wait_job_ptr(&job.job);
            Ok(())
        } else {
            Err(JobGraphError::DependencyNotFound(id))
        }
    }
}

#[cfg(test)]
mod tests {
    use crate::job_graph::JobGraph;
    use crate::JobSystem;

    #[test]
    fn dependency_test() {
        // Dependency chain:
        //  A <- B <-----\
        //        \- D <- + <- F
        //       C <------/
        //       E <-----/
        let _job_sys = JobSystem::new(3).expect("Failed to init job system");
        let mut job_graph = JobGraph::new().unwrap();

        job_graph
            .add("A", &[], || {
                println!("Job A");
            })
            .unwrap();
        job_graph
            .add("B", &["A"], || {
                println!("Job B");
            })
            .unwrap();
        job_graph
            .add("C", &["A"], || {
                println!("Job C");
            })
            .unwrap();
        job_graph
            .add("D", &["B"], || {
                println!("Job D");
            })
            .unwrap();
        job_graph
            .add("E", &["A"], || {
                println!("Job E");
            })
            .unwrap();
        job_graph
            .add("F", &["B", "D", "C", "E"], || {
                println!("Job F");
            })
            .unwrap();

        let r = job_graph.build_and_execute();
        assert!(r.is_ok());

        assert!(job_graph.wait_on_job("F").is_ok());
    }
}
